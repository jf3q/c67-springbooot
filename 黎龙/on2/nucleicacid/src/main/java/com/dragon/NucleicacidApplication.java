package com.dragon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NucleicacidApplication {

    public static void main(String[] args) {
        SpringApplication.run(NucleicacidApplication.class, args);
    }

}
