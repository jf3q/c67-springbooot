package com.dragon.control;

import com.dragon.entity.Hospital;
import com.dragon.service.HospitalServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/hospital")
public class HospitalControl {

    @Autowired
    HospitalServer hospitalServer;

    @GetMapping
    @ResponseBody
    public List<Hospital> SelHospital(){
        return hospitalServer.SelHospital();
    }
}
