package com.dragon.service;

import com.dragon.dao.HospitalDao;
import com.dragon.entity.Hospital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HospitalServer {

    @Autowired
    HospitalDao hospitalDao;
    public List<Hospital> SelHospital() {
        return hospitalDao.queryAll();
    }
}
