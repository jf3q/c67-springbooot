package com.dragon.control;

import com.dragon.entity.MedicalAssay;
import com.dragon.service.MedicalAssayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/medical")
public class MedicalAssayControl {
    @Autowired
    MedicalAssayService medicalAssayService;

    @RequestMapping("/med")
    public String medicalAssay(Model model, Integer hospitalId){
     List<MedicalAssay> list= medicalAssayService.selAll(hospitalId);
     model.addAttribute("list",list);
     model.addAttribute("hospitalId",hospitalId);
     return "list";
    }

    /*跳转页面*/
    @GetMapping("/toAdd")
    public String toAdd(){
        return "add";
    }

    /*新增*/
    @PostMapping("/save")
    public String save(Model model,MedicalAssay medicalAssay){
      MedicalAssay medicalAssay1= medicalAssayService.save(medicalAssay);
        System.out.println(medicalAssay1);
      model.addAttribute(medicalAssay1);
      if (medicalAssay1.getId()!=null){
          return "redirect:/medical/med";
      }
      return "add";
    }

    /*修改*/
    @PutMapping("/update/{id}")
    @ResponseBody
    public int update(@PathVariable Integer id){
        try {
            medicalAssayService.update(id);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
        return 1;
    }
}
