package com.dragon.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * (MedicalAssay)实体类
 *
 * @author makejava
 * @since 2023-12-28 11:44:38
 */
public class MedicalAssay implements Serializable {
    private static final long serialVersionUID = 679313057692139825L;

    private Integer id;

    private String assayUser;

    private Integer hospitalId;

    private Integer assayResult;

    private String phone;

    private String cardNum;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date assayTime;

    //只用于前端保存数据 不走数据库
    private String hospitalName;  //机构名称

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAssayUser() {
        return assayUser;
    }

    public void setAssayUser(String assayUser) {
        this.assayUser = assayUser;
    }

    public Integer getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Integer hospitalId) {
        this.hospitalId = hospitalId;
    }

    public Integer getAssayResult() {
        return assayResult;
    }

    public void setAssayResult(Integer assayResult) {
        this.assayResult = assayResult;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public Date getAssayTime() {
        return assayTime;
    }

    public void setAssayTime(Date assayTime) {
        this.assayTime = assayTime;
    }

}

