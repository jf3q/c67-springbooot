package com.dragon.service;

import com.dragon.dao.MedicalAssayDao;
import com.dragon.entity.Hospital;
import com.dragon.entity.MedicalAssay;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicalAssayService {
    @Autowired
    MedicalAssayDao medicalAssayDao;


    public List<MedicalAssay> selAll(Integer hospitalId) {
        MedicalAssay medicalAssay=new MedicalAssay();
        medicalAssay.setHospitalId(hospitalId);
        List<MedicalAssay> list = medicalAssayDao.queryAll(medicalAssay);
        return list;
    }


    public MedicalAssay save(MedicalAssay medicalAssay) {
        medicalAssay.setAssayResult(0);
       medicalAssayDao.insert(medicalAssay);
        return medicalAssay;
    }

    public void update(Integer id) {
        MedicalAssay medicalAssay=medicalAssayDao.queryById(id);
        if (medicalAssay.getAssayResult()==0){
            medicalAssay.setAssayResult(1);
        }
        medicalAssayDao.update(medicalAssay);
    }
}
