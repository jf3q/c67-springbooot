package com.yxc;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "yxc.redis")
public class MyRedisProperties {
    private int database = 0;
    private String host="localhost";
    private int timeout=2000;//单位是秒，默认值为0,表示无限制
    private String password;
    private int port=6379;
}