package com.yxc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
@EnableConfigurationProperties(MyRedisProperties.class)
@ConditionalOnClass(JedisPool.class)
public class MyRedisAutoConfiguration {
    @Autowired
    MyRedisProperties redisProperties;

    @Bean
    public JedisPoolConfig setPoolConfig(){
        return new JedisPoolConfig();
    }

    @Bean
    public JedisPool setPool(){
        JedisPool jedisPool = new JedisPool(setPoolConfig(),
                redisProperties.getHost(),
                redisProperties.getPort(),
                redisProperties.getTimeout(),
                redisProperties.getPassword(),
                redisProperties.getDatabase());

        return  jedisPool;

    }

    //RedisAPI这是我专门定义的一个工具类
    @Bean
    public RedisAPI setProperties(){

        RedisAPI redisAPI = new RedisAPI();
        redisAPI.setJedisPool(setPool());

        return redisAPI;

    }
}