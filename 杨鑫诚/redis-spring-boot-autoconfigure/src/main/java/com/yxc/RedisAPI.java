package com.yxc;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class RedisAPI {

    JedisPool jedisPool;
    Jedis jedis;

    public void setJedisPool(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    public  void set(String key, String value, Integer expireTime){
        try {
            jedis = jedisPool.getResource();
            jedis.setex(key,expireTime,value);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            jedisPool.close();
        }
    }

    public  void set(String key, String value){
        try {
            jedis = jedisPool.getResource();
            jedis.set(key,value);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
             jedisPool.close();
        }
    }




    public String get(String tokenString) {
        try {
            jedis = jedisPool.getResource();
            return jedis.get(tokenString);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            jedisPool.close();
        }


        return null;
    }

}