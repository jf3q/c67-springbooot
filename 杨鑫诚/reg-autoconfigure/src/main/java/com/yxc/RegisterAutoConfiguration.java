package com.yxc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({RegisterProperties.class})
public class RegisterAutoConfiguration {

    @Autowired
    RegisterProperties registerProperties;
    @Bean
    @ConditionalOnMissingBean(RegService.class)//不存在这个bean的时候创建
    RegService regService(){
        return new RegService(registerProperties.getUsername(),registerProperties.getPassword());
    }
}