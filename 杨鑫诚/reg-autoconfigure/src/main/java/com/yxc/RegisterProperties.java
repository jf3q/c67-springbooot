package com.yxc;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "yxc.register")
public class RegisterProperties {
    private String username="admin";
    private String password="123456";

}