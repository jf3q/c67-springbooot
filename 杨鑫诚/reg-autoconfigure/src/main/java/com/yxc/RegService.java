package com.yxc;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RegService {
    private String username;
    private String password;

    public void login(){
        System.out.println("用户名："+username);
        System.out.println("密码："+password);
    }
}