package com.yxc.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultVo<T> {
    private String code;
    private String message;
    private T data;
    public static<T> ResultVo<T> success(String message,T data){
        return new ResultVo<>("2000",message,data);
    }

    public static<T> ResultVo<T> success(String message){
        return new ResultVo<>("2000",message,null);
    }

    public static<T> ResultVo<T> success(T data){
        return new ResultVo<>("2000",null,data);
    }

    public static<T> ResultVo<T> error(String message){
        return new ResultVo<>("5000",message,null);
    }
}
