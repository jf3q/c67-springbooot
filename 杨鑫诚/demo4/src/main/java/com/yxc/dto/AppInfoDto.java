package com.yxc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "app分类信息类",description = "描述app分类信息")
public class AppInfoDto {
    @ApiModelProperty(value = "软件名称")
    private String softwarename;
    @ApiModelProperty(value = "APK名称（唯一）")
    private String apkname;
    @ApiModelProperty(value = "状态（来源于：data_dictionary，1 待审核 2 审核通过 3 审核不通过 4 已上架 5 已下架）")
    private Long status;
    @ApiModelProperty(value = "所属平台（来源于：data_dictionary，1 手机 2 平板 3 通用）")
    private Long flatformid;
    @ApiModelProperty(value = "所属一级分类（来源于：data_dictionary）")
    private Long categorylevel1;
    @ApiModelProperty(value = "所属二级分类（来源于：data_dictionary）")
    private Long categorylevel2;
    @ApiModelProperty(value = "所属三级分类（来源于：data_dictionary）")
    private Long categorylevel3;
    @ApiModelProperty(value = "开发者id（来源于：dev_user表的开发者id）")
    private Long devid;
}
