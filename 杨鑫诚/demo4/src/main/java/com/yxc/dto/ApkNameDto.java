package com.yxc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Apk名称类",description = "描述Apk名称信息")
public class ApkNameDto {
    @ApiModelProperty(value = "主键id")
    private Long id;
    @ApiModelProperty(value = "apk名称")
    private String apkName;
}
