package com.yxc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "审核类",description = "描述审核信息")
public class AppInfoReviewDto {
    @ApiModelProperty(value = "主键id")
    private Long id;
    @ApiModelProperty(value = "状态（来源于：data_dictionary，1 待审核 2 审核通过 3 审核不通过 4 已上架 5 已下架）")
    private Long status;
}
