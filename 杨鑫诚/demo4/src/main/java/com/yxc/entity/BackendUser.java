package com.yxc.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * (BackendUser)实体类
 *
 * @author makejava
 * @since 2023-12-11 09:07:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "管理员类",description = "描述管理员信息")
public class BackendUser  {
    /**
     * 主键id
     */
    @ApiModelProperty(value = "主键ID")
    private Long id;
    /**
     * 用户编码
     */
    @ApiModelProperty(value = "用户编码")
    private String usercode;
    /**
     * 用户名称
     */
    @ApiModelProperty(value = "用户名称")
    private String username;
    /**
     * 用户角色类型（来源于数据字典表，分为：超管、财务、市场、运营、销售）
     */
    @ApiModelProperty(value = "用户角色类型（来源于数据字典表，分为：超管、财务、市场、运营、销售）")
    private Long usertype;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    @ApiModelProperty(value = "创建者（来源于backend_user用户表的用户id）")
    private Long createdby;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date creationdate;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    @ApiModelProperty(value = "更新者（来源于backend_user用户表的用户id）")
    private Long modifyby;
    /**
     * 最新更新时间
     */
    @ApiModelProperty(value = "最新更新时间")
    private Date modifydate;
    /**
     * 用户密码
     */
    @ApiModelProperty(value = "用户密码")
    private String userpassword;
}

