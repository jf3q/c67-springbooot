package com.yxc.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * (AppInfo)实体类
 *
 * @author makejava
 * @since 2023-12-11 09:07:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "app信息类",description = "描述app信息信息")
public class AppInfo {
    /**
     * 主键id
     */
    @ApiModelProperty(value = "主键id")
    private Long id;
    /**
     * 软件名称
     */
    @ApiModelProperty(value = "软件名称")
    private String softwarename;
    /**
     * APK名称（唯一）
     */
    @ApiModelProperty(value = "APK名称（唯一）")
    private String apkname;
    /**
     * 支持ROM
     */
    @ApiModelProperty(value = "支持ROM")
    private String supportrom;
    /**
     * 界面语言
     */
    @ApiModelProperty(value = "界面语言")
    private String interfacelanguage;
    /**
     * 软件大小（单位：M）
     */
    @ApiModelProperty(value = "软件大小（单位：M）")
    private Double softwaresize;
    /**
     * 更新日期
     */
    @ApiModelProperty(value = "更新日期")
    private Date updatedate;
    /**
     * 开发者id（来源于：dev_user表的开发者id）
     */
    @ApiModelProperty(value = "开发者id（来源于：dev_user表的开发者id）")
    private Long devid;
    /**
     * 应用简介
     */
    @ApiModelProperty(value = "应用简介")
    private String appinfo;
    /**
     * 状态（来源于：data_dictionary，1 待审核 2 审核通过 3 审核不通过 4 已上架 5 已下架）
     */
    @ApiModelProperty(value = "状态（来源于：data_dictionary，1 待审核 2 审核通过 3 审核不通过 4 已上架 5 已下架）")
    private Long status;
    /**
     * 上架时间
     */
    @ApiModelProperty(value = "上架时间")
    private Date onsaledate;
    /**
     * 下架时间
     */
    @ApiModelProperty(value = "下架时间")
    private Date offsaledate;
    /**
     * 所属平台（来源于：data_dictionary，1 手机 2 平板 3 通用）
     */
    @ApiModelProperty(value = "所属平台（来源于：data_dictionary，1 手机 2 平板 3 通用）")
    private Long flatformid;
    /**
     * 所属三级分类（来源于：data_dictionary）
     */
    @ApiModelProperty(value = "所属三级分类（来源于：data_dictionary）")
    private Long categorylevel3;
    /**
     * 下载量（单位：次）
     */
    @ApiModelProperty(value = "下载量（单位：次）")
    private Long downloads;
    /**
     * 创建者（来源于dev_user开发者信息表的用户id）
     */
    @ApiModelProperty(value = "创建者（来源于dev_user开发者信息表的用户id）")
    private Long createdby;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date creationdate;
    /**
     * 更新者（来源于dev_user开发者信息表的用户id）
     */
    @ApiModelProperty(value = "更新者（来源于dev_user开发者信息表的用户id）")
    private Long modifyby;
    /**
     * 最新更新时间
     */
    @ApiModelProperty(value = "最新更新时间")
    private Date modifydate;
    /**
     * 所属一级分类（来源于：data_dictionary）
     */
    @ApiModelProperty(value = "所属一级分类（来源于：data_dictionary）")
    private Long categorylevel1;
    /**
     * 所属二级分类（来源于：data_dictionary）
     */
    @ApiModelProperty(value = "所属二级分类（来源于：data_dictionary）")
    private Long categorylevel2;
    /**
     * LOGO图片url路径
     */
    @ApiModelProperty(value = "LOGO图片url路径")
    private String logopicpath;
    /**
     * LOGO图片的服务器存储路径
     */
    @ApiModelProperty(value = "LOGO图片的服务器存储路径")
    private String logolocpath;
    /**
     * 最新的版本id
     */
    @ApiModelProperty(value = "最新的版本id")
    private Long versionid;
    @ApiModelProperty(value = "所属一级分类名称")
    private String categorylevel1Name;
    @ApiModelProperty(value = "所属二级分类名称")
    private String categorylevel2Name;
    @ApiModelProperty(value = "所属三级分类名称")
    private String categorylevel3Name;
    @ApiModelProperty(value = "开发者名称")
    private String devName;
}

