package com.yxc.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * (DataDictionary)实体类
 *
 * @author makejava
 * @since 2023-12-11 09:07:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataDictionary{
    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID")
    private Long id;
    /**
     * 类型编码
     */
    @ApiModelProperty(value = "类型编码")
    private String typecode;
    /**
     * 类型名称
     */
    @ApiModelProperty(value = "类型名称")
    private String typename;
    /**
     * 类型值ID
     */
    @ApiModelProperty(value = "类型值ID")
    private Long valueid;
    /**
     * 类型值Name
     */
    @ApiModelProperty(value = "类型值Name")
    private String valuename;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    @ApiModelProperty(value = "创建者（来源于backend_user用户表的用户id）")
    private Long createdby;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date creationdate;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    @ApiModelProperty(value = "更新者（来源于backend_user用户表的用户id）")
    private Long modifyby;
    /**
     * 最新更新时间
     */
    @ApiModelProperty(value = "最新更新时间")
    private Date modifydate;
}

