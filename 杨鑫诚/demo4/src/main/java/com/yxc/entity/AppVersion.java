package com.yxc.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * (AppVersion)实体类
 *
 * @author makejava
 * @since 2023-12-11 09:07:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "app信息类",description = "描述app信息信息")
public class AppVersion {
    /**
     * 主键id
     */
    @ApiModelProperty(value = "主键id")
    private Long id;
    /**
     * appId（来源于：app_info表的主键id）
     */
    @ApiModelProperty(value = "appId（来源于：app_info表的主键id）")
    private Long appid;
    /**
     * 版本号
     */
    @ApiModelProperty(value = "版本号")
    private String versionno;
    /**
     * 版本介绍
     */
    @ApiModelProperty(value = "版本介绍")
    private String versioninfo;
    /**
     * 发布状态（来源于：data_dictionary，1 不发布 2 已发布 3 预发布）
     */
    @ApiModelProperty(value = "发布状态（来源于：data_dictionary，1 不发布 2 已发布 3 预发布）")
    private Long publishstatus;
    /**
     * 下载链接
     */
    @ApiModelProperty(value = "下载链接")
    private String downloadlink;
    /**
     * 版本大小（单位：M）
     */
    @ApiModelProperty(value = "版本大小（单位：M）")
    private Double versionsize;
    /**
     * 创建者（来源于dev_user开发者信息表的用户id）
     */
    @ApiModelProperty(value = "创建者（来源于dev_user开发者信息表的用户id）")
    private Long createdby;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy.MM.dd")
    private Date creationdate;
    /**
     * 更新者（来源于dev_user开发者信息表的用户id）
     */
    @ApiModelProperty(value = "更新者（来源于dev_user开发者信息表的用户id）")
    private Long modifyby;
    /**
     * 最新更新时间
     */
    @ApiModelProperty(value = "最新更新时间")
    private Date modifydate;
    /**
     * apk文件的服务器存储路径
     */
    @ApiModelProperty(value = "apk文件的服务器存储路径")
    private String apklocpath;
    /**
     * 上传的apk文件名称
     */
    @ApiModelProperty(value = "上传的apk文件名称")
    private String apkfilename;
}

