package com.yxc.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 手游类别(AppCategory)实体类
 *
 * @author makejava
 * @since 2023-12-11 09:07:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "手游类别类",description = "描述手游类别信息")
public class AppCategory{
    /**
     * 主键ID
     */
    @ApiModelProperty(value = "主键ID")
    private Long id;
    /**
     * 分类编码
     */
    @ApiModelProperty(value = "分类编码")
    private String categorycode;
    /**
     * 分类名称
     */
    @ApiModelProperty(value = "分类名称")
    private String categoryname;
    /**
     * 父级节点id
     */
    @ApiModelProperty(value = "父级节点id")
    private Long parentid;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    @ApiModelProperty(value = "创建者（来源于backend_user用户表的用户id）")
    private Long createdby;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date creationtime;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    @ApiModelProperty(value = "更新者（来源于backend_user用户表的用户id）")
    private Long modifyby;
    /**
     * 最新更新时间
     */
    @ApiModelProperty(value = "最新更新时间")
    private Date modifydate;
}

