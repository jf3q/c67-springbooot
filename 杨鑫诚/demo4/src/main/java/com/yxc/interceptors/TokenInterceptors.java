package com.yxc.interceptors;

import com.yxc.utils.SessionUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class TokenInterceptors implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        String requestURI = request.getRequestURI();
        if (token == null) {
            response.setStatus(401);
        } else if (SessionUtils.get(token) == null) {
            response.setStatus(403);
        } else {
            String[] split = token.split("-");
            String time = split[4];
            if (System.currentTimeMillis() - Long.valueOf(time) > 2 * 3600 * 1000) {
                response.setStatus(403);
                SessionUtils.remove(token);
            }
            if (requestURI.equals("/appInfo/review")) {
                if (split[3].equals("dev")) {
                    response.setStatus(403);
                    return false;
                }
            }
            return true;
        }
        return false;
    }

}
