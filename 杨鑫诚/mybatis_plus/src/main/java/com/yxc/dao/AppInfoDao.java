package com.yxc.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.yxc.entity.AppInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (AppInfo)表数据库访问层
 *
 * @author makejava
 * @since 2023-12-11 09:07:13
 */
public interface AppInfoDao extends BaseMapper<AppInfo> {
    /**
     * 查询指定行数据
     *
     * @param appInfo  查询条件
     * @return 对象列表
     */
    List<AppInfo> queryAllBy(AppInfo appInfo);

    IPage<AppInfo> page(@Param("page") IPage<AppInfo> page, @Param(Constants.WRAPPER) Wrapper<AppInfo> queryWrapper);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AppInfo queryById(Long id);

    /**
     * 新增数据
     *
     * @param appInfo 实例对象
     * @return 影响行数
     */
    int insert(AppInfo appInfo);

    /**
     * 修改数据
     *
     * @param appInfo 实例对象
     * @return 影响行数
     */
    int update(AppInfo appInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);
}

