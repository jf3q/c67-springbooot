package com.yxc.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryTreeVo {
    private Long id;
    private String categoryname;
    private Long parentid;
    private List<CategoryTreeVo> children;
}
