package com.yxc.controller;

import com.yxc.dto.LoginUserDto;
import com.yxc.service.LoginUserService;
import com.yxc.utils.SessionUtils;
import com.yxc.vo.LoginUserVo;
import com.yxc.vo.ResultVo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/devUser")
public class DevUserController {

    @Autowired
    LoginUserService loginUserService;

    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginUserDto loginUserDto) {
        LoginUserVo vo = null;
        try {
            vo = loginUserService.login(loginUserDto);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
        return ResultVo.success("登录成功", vo);
    }

    @GetMapping("/loginOut")
    public ResultVo loginOut(HttpServletRequest request) {
        String token = request.getHeader("token");
        SessionUtils.remove(token);
        return ResultVo.success("退出成功");
    }
}
