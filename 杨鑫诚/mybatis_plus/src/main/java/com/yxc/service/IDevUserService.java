package com.yxc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxc.entity.DevUser;

public interface IDevUserService extends IService<DevUser> {
}
