package com.yxc.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yxc.dao.AppInfoDao;
import com.yxc.dao.AppVersionDao;
import com.yxc.dto.ApkNameDto;
import com.yxc.dto.AppInfoDto;
import com.yxc.entity.AppInfo;
import com.yxc.entity.AppVersion;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Date;
import java.util.List;

@Service
public class AppInfoService extends ServiceImpl<AppInfoDao, AppInfo> implements IAppInfoService{

    @Autowired
    AppInfoDao appInfoDao;

    @Autowired
    AppVersionDao appVersionDao;


    public IPage<AppInfo> getPage(AppInfoDto appInfoDto, Integer pageNo) {
        AppInfo appInfo = new AppInfo();
        BeanUtils.copyProperties(appInfoDto, appInfo);
        IPage<AppInfo> appInfoIPage = new Page<>(pageNo, 10);
        LambdaQueryWrapper<AppInfo> queryWrapper = new LambdaQueryWrapper<>();
        if (appInfo.getSoftwarename() != null && appInfo.getSoftwarename() != "") {
            queryWrapper.like(AppInfo::getSoftwarename,appInfo.getSoftwarename());
        }
        if (appInfo.getApkname() != null && appInfo.getApkname() != "") {
            queryWrapper.like(AppInfo::getApkname,appInfo.getApkname());
        }
        if (appInfo.getStatus() != null) {
            queryWrapper.eq(AppInfo::getStatus,appInfo.getStatus());
        }
        if (appInfo.getFlatformid() != null) {
            queryWrapper.eq(AppInfo::getFlatformid,appInfo.getFlatformid());
        }
        if (appInfo.getCategorylevel1() != null) {
            queryWrapper.eq(AppInfo::getCategorylevel1,appInfo.getCategorylevel1());
        }
        if (appInfo.getCategorylevel2() != null) {
            queryWrapper.eq(AppInfo::getCategorylevel2,appInfo.getCategorylevel2());
        }
        if (appInfo.getCategorylevel3() != null) {
            queryWrapper.eq(AppInfo::getCategorylevel3,appInfo.getCategorylevel3());
        }
        if (appInfo.getDevid() != null) {
            queryWrapper.eq(AppInfo::getDevid,appInfo.getDevid());
        }
        return appInfoDao.page(appInfoIPage, queryWrapper);
    }

    public AppInfo getById(Long id) {
        return appInfoDao.selectById(id);
    }

    public boolean saveOrUpdate(AppInfo appInfo) {
        if (appInfo.getId() == null) {
            appInfoDao.insert(appInfo);
        } else {
            appInfoDao.update(appInfo,null);
        }
        return false;
    }

    public void del(Long id, HttpServletRequest request) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(id);
        List<AppVersion> appVersions = appVersionDao.queryAllBy(appVersion);
        for (AppVersion version : appVersions) {
            if (version.getDownloadlink() != null) {
                String realPath = request.getServletContext().getRealPath(version.getDownloadlink());
                File file = new File(realPath);
                if (file.exists()) {
                    file.delete();
                }
            }
            appVersionDao.deleteById(version.getId());
        }

        AppInfo delApp = appInfoDao.selectById(id);
        if (delApp.getLogopicpath() != null) {
            String realPath = request.getServletContext().getRealPath(delApp.getLogopicpath());
            File delFile = new File(realPath);
            if (delFile.exists()) {
                delFile.delete();
            }
        }
        appInfoDao.deleteById(id);
    }

    public Boolean varApkName(ApkNameDto apkNameDto) {
        if (apkNameDto.getId() != null) {
            AppInfo appInfo = appInfoDao.selectById(apkNameDto.getId());
            if (apkNameDto.getApkName().equals(appInfo.getApkname())) {
                return true;
            }
        }

        AppInfo appInfo = new AppInfo();
        appInfo.setApkname(apkNameDto.getApkName());
        List<AppInfo> appInfos = appInfoDao.queryAllBy(appInfo);
        if (appInfos.size() > 0) {
            return false;
        }
        return true;
    }


    public void sale(Long id) {
        AppInfo appInfo = appInfoDao.selectById(id);
        if (appInfo.getStatus() == 5L) {
            appInfo.setStatus(4L);
            appInfo.setOnsaledate(new Date());
        } else if (appInfo.getStatus() == 4L) {
            appInfo.setStatus(5L);
            appInfo.setOffsaledate(new Date());
        } else if (appInfo.getStatus() == 2L) {
            appInfo.setStatus(4L);
            appInfo.setOffsaledate(new Date());
        }
        appInfoDao.update(appInfo,null);
    }
}
