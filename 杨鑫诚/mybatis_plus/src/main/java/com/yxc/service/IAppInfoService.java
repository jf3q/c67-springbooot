package com.yxc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxc.entity.AppInfo;

public interface IAppInfoService extends IService<AppInfo> {
}
