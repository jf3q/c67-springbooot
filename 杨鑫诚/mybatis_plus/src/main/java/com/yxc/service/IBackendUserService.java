package com.yxc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxc.entity.BackendUser;
import com.yxc.entity.DevUser;

public interface IBackendUserService extends IService<BackendUser> {
}
