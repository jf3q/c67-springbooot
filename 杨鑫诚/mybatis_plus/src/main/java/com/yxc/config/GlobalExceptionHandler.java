package com.yxc.config;

import com.yxc.vo.ResultVo;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NullPointerException.class)
    public ResultVo exception(NullPointerException exception) {
        return ResultVo.error("空指针异常");
    }

    @ExceptionHandler(IndexOutOfBoundsException.class)
    public ResultVo exception(IndexOutOfBoundsException exception) {
        return ResultVo.error("数组越界异常");
    }

    @ExceptionHandler(Exception.class)
    public ResultVo exception(Exception exception) {
        return ResultVo.error(exception.getMessage());
    }

}