package com.yxc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.Date;

/**
 * (AppInfo)实体类
 *
 * @author makejava
 * @since 2023-12-11 09:07:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("app_info")
public class AppInfo implements Serializable {
    /**
     * 主键id
     */
    @TableId(value="id",type= IdType.AUTO)
    private Long id;
    /**
     * 软件名称
     */
    private String softwarename;
    /**
     * APK名称（唯一）
     */
    private String apkname;
    /**
     * 支持ROM
     */
    private String supportrom;
    /**
     * 界面语言
     */
    private String interfacelanguage;
    /**
     * 软件大小（单位：M）
     */
    private Double softwaresize;
    /**
     * 更新日期
     */
    private Date updatedate;
    /**
     * 开发者id（来源于：dev_user表的开发者id）
     */
    private Long devid;
    /**
     * 应用简介
     */
    private String appinfo;
    /**
     * 状态（来源于：data_dictionary，1 待审核 2 审核通过 3 审核不通过 4 已上架 5 已下架）
     */
    private Long status;
    /**
     * 上架时间
     */
    private Date onsaledate;
    /**
     * 下架时间
     */
    private Date offsaledate;
    /**
     * 所属平台（来源于：data_dictionary，1 手机 2 平板 3 通用）
     */
    private Long flatformid;
    /**
     * 所属三级分类（来源于：data_dictionary）
     */
    private Long categorylevel3;
    /**
     * 下载量（单位：次）
     */
    private Long downloads;
    /**
     * 创建者（来源于dev_user开发者信息表的用户id）
     */
    private Long createdby;
    /**
     * 创建时间
     */
    private Date creationdate;
    /**
     * 更新者（来源于dev_user开发者信息表的用户id）
     */
    private Long modifyby;
    /**
     * 最新更新时间
     */
    private Date modifydate;
    /**
     * 所属一级分类（来源于：data_dictionary）
     */
    private Long categorylevel1;
    /**
     * 所属二级分类（来源于：data_dictionary）
     */
    private Long categorylevel2;
    /**
     * LOGO图片url路径
     */
    private String logopicpath;
    /**
     * LOGO图片的服务器存储路径
     */
    private String logolocpath;
    /**
     * 最新的版本id
     */
    private Long versionid;

    @TableField(exist = false)
    private String categorylevel1Name;
    @TableField(exist = false)
    private String categorylevel2Name;
    @TableField(exist = false)
    private String categorylevel3Name;
    @TableField(exist = false)
    private String devName;
}

