package com.yxc;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class No8TestStaterApplicationTests {

    @Autowired
    RegService regService;

    @Autowired
    RedisAPI redisAPI;

    @Test
    void testUser(){
        regService.login();
    }

    @Test
    void testRedisAPISet(){
        redisAPI.set("username","yxc");
    }

    @Test
    void testRedisAPIGet(){
        System.out.println(redisAPI.get("username"));
    }


}
