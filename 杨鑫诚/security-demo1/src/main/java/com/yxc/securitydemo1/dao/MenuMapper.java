package com.yxc.securitydemo1.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yxc.securitydemo1.entity.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MenuMapper extends BaseMapper<Menu> {
    List<Menu> getAllMenus();//菜单权限一起
}