package com.yxc.securitydemo1.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("menu")
public class Menu {
    @TableId(value="mid",type= IdType.AUTO)
    private Integer mid;
    private String menuname;
    private String url;
    @TableField(exist = false)
    private List<Role> roles; //拥有该权限的所有角色
}

