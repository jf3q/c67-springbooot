package com.yxc.securitydemo1.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Collection;

/**
 * (Userinfo)实体类
 *
 * @author makejava
 * @since 2024-01-22 16:21:28
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("user")
public class Userinfo{
    @TableId(value="uid",type= IdType.AUTO)
    private Integer uid;
    private String username;
    private String password;
    @TableField(exist = false)
    private Collection<Role> roles;//当前用户具有的角色
}

