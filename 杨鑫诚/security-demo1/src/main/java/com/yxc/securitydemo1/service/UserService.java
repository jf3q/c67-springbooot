package com.yxc.securitydemo1.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxc.securitydemo1.entity.Userinfo;

public interface UserService extends IService<Userinfo>  {
}
