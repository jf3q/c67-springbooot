package com.yxc.securitydemo1.config;

import com.yxc.securitydemo1.filter.ValidCodeFilter;
import com.yxc.securitydemo1.service.UserServiceImpl;
import com.yxc.securitydemo1.utils.MyAccessDecisionManager;
import com.yxc.securitydemo1.utils.MyFilterInvocationSecurityMetadataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true,jsr250Enabled = true)
public class SecurityConfig {

    @Autowired
    ValidCodeFilter validCodeFilter;

    @Autowired
    UserServiceImpl userService;

    @Autowired
    MyAccessDecisionManager myAccessDecisionManager;

    @Autowired
    MyFilterInvocationSecurityMetadataSource myFilterInvocationSecurityMetadataSource;
    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
        //如果不想加密就返回
//        return NoOpPasswordEncoder.getInstance();
    }

    //静态资源直接放行
    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        //忽略这些静态资源（不拦截）  新版本 Spring Security 6.0 已经弃用 antMatchers()
        return (web) -> web.ignoring().requestMatchers("/webjars/**","/js/**", "/css/**","/images/**");
    }

    @Bean
    SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.addFilterBefore(validCodeFilter, UsernamePasswordAuthenticationFilter.class);
        httpSecurity.authorizeRequests() //开启登录配置
                .requestMatchers("/","/validcode").permitAll() //允许直接访问的路径,包括验证码
//                .requestMatchers("/level1/**").hasRole("vip1")//用户需要有role1的角色才能访问/level1/**
//                .requestMatchers("/level2/**").hasRole("vip2")
//                .requestMatchers("/level3/**").hasRole("vip3")
                .withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
                    @Override
                    public <O extends FilterSecurityInterceptor> O postProcess(O object) {
                        //查找访问当前url需要什么角色（权限）
                        object.setSecurityMetadataSource(myFilterInvocationSecurityMetadataSource);
                        //判断（裁决）当前用户有没有这个角色（权限）
                        object.setAccessDecisionManager(myAccessDecisionManager);
                        return object;
                    }
                })
                .anyRequest().authenticated();//其他任何请求都必须经过身份验证
        httpSecurity.formLogin()//开启表单验证
                .loginPage("/toLogin")//跳转到自定义的登录页面
                .usernameParameter("username")//自定义表单的用户名的name,默认为username
                .passwordParameter("password")//自定义表单的密码的name,默认为password
                .loginProcessingUrl("/login")//表单请求的地址，使用Security定义好的/login，并且与自定义表单的action一致
                .failureUrl("/toLogin/error")//如果登录失败跳转到
                .permitAll();//允许访问登录有关的路径
        //开启注销
        httpSecurity.logout().logoutSuccessUrl("/");//注销后跳转到index页面
        httpSecurity.csrf().disable();//关闭csrf
        httpSecurity.rememberMe(); //记住我
        httpSecurity.exceptionHandling().accessDeniedPage("/errorRole");
        httpSecurity.userDetailsService(userService);
        return httpSecurity.build();
    }

//    @Bean
//    public UserDetailsService userDetailsService() {
//        // 1.使用内存数据进行认证
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        // 2.创建4个用户
//        ////设置内存用户名与密码,并赋与角色
//        UserDetails user1 = User.withUsername("user1").password(passwordEncoder().encode("123")).roles("vip1","vip2","vip3").build();
//        UserDetails user2 = User.withUsername("user2").password(passwordEncoder().encode("123")).roles("vip2","vip3").build();
//        UserDetails user3 = User.withUsername("user3").password(passwordEncoder().encode("123")).roles("vip1").build();
//
//        // 3.将这4个用户添加到内存中
//        manager.createUser(user1);
//        manager.createUser(user2);
//        manager.createUser(user3);
//        return manager;
//    }
}