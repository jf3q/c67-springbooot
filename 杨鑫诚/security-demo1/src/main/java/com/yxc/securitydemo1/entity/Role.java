package com.yxc.securitydemo1.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("role")
public class Role implements Serializable {
    @TableId(value="rid",type= IdType.AUTO)
    private Integer rid;
    private String name;
    private String nameChinese;
    @TableField(exist = false)
    private List<Menu> menus;//一个角色可能有多个权限（菜单）
}

