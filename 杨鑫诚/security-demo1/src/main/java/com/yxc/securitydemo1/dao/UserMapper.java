package com.yxc.securitydemo1.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yxc.securitydemo1.entity.Role;
import com.yxc.securitydemo1.entity.Userinfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Collection;

@Mapper
public interface UserMapper extends BaseMapper<Userinfo>{
    Collection<Role> findRolesByUserId(Integer id);
}
