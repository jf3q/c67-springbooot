package com.yxc.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * (Classinfo)实体类
 *
 * @author makejava
 * @since 2023-12-29 08:13:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Classinfo {
    private Integer classno;
    private String classname;
    private List<Student> students;
}

