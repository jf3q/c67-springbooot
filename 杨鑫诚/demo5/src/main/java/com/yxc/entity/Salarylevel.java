package com.yxc.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * (Salarylevel)实体类
 *
 * @author makejava
 * @since 2023-12-29 08:14:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Salarylevel {
    private String level;
    private Object minsal;
    private Object maxsal;
}

