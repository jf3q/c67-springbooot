package com.yxc.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * (Employee)实体类
 *
 * @author makejava
 * @since 2023-12-29 08:14:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    private Integer empno;
    private String ename;
    private String job;
    private Integer leaderid;
    private Integer salary;
    private Employee leader;
    private List<Employee> employees;
    private String level;
}

