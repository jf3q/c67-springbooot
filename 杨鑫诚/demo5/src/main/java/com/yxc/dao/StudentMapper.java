package com.yxc.dao;

import com.yxc.entity.Student;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StudentMapper {
    Student findStudentById(Integer id);

}
