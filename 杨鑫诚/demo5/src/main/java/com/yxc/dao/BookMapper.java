package com.yxc.dao;

import com.yxc.entity.Book;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookMapper {
    @Select("select * from book")
    public List<Book> findAllBooks();  //查询所有书

    @Select("select * from book where id=#{id}")
    public Book findBookById(Integer id);  //根据id号查找一本书

    @Insert("insert into book values(null,#{name},#{price},#{category},#{pnum},#{imgurl},#{description},#{author},#{sales})")
    public void addBook(Book book);   //添加一本书

    @Update("update book set name=#{name},price=#{price},category=#{category}, " +
            "pnum=${pnum},imgurl=#{imgurl},description=#{description}," +
            "author=#{author},sales=#{sales} where id=#{id}")
    public void updateBook(Book book);  //修改一本书

    @Delete("delete from book where id=#{id}")
    public void deleteBook(Integer id);  //删除一本书
    public void deleteBooks(Integer[] ids);  //删除一本书

}