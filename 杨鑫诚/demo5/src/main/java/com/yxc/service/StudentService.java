package com.yxc.service;

import com.yxc.dao.StudentMapper;
import com.yxc.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    @Autowired
    StudentMapper studentMapper;


    public Student findStudentById(Integer id) {
        return studentMapper.findStudentById(id);
    }
}
