package com.yxc.service;

import com.yxc.dao.ClassinfoMapper;
import com.yxc.entity.Classinfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ClassinfoService {

    @Autowired
    ClassinfoMapper classinfoMapper;


    public Classinfo findClassInfoById(Integer classno) {
        return classinfoMapper.findClassInfoById(classno);
    }
}
