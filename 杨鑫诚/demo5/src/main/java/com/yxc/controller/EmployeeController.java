package com.yxc.controller;

import com.yxc.entity.Employee;
import com.yxc.entity.Student;
import com.yxc.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @GetMapping("/{id}")
    public String findClassInfoById(@PathVariable Integer id, Model model){
        Employee employee=employeeService.findEmployeeById(id);
        model.addAttribute("employee",employee);
        return "employee";
    }

    @GetMapping("/leader/{id}")
    public String findLeaderAndEmpById(@PathVariable Integer id, Model model){
        Employee employee=employeeService.findLeaderAndEmpById(id);
        model.addAttribute("employee",employee);
        return "leader";
    }

    @GetMapping("/employees")
    public String findEmployeeSalaryLevel(Model model){
        List<Employee> employees=employeeService.findEmployeeSalaryLevel();
        model.addAttribute("employees",employees);
        return "employees";
    }

}
