package com.yxc.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yxc.entity.Book;
import com.yxc.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    BookService bookService;

    @GetMapping("/page")
    public String index(Model model,@RequestParam(defaultValue = "1")Integer pageNum){
        PageInfo<Book> pageInfo = bookService.findAllBooks(pageNum);
        model.addAttribute("page",pageInfo);
        return "book";
    }

    @GetMapping("/addBook")
    public String addBook(){
        return "addBook";
    }

    @GetMapping("/toUpdate")
    public String update(Model model,Integer id){
        Book book = bookService.findAllById(id);
        model.addAttribute("book",book);
        return "update";
    }

    @PostMapping("/del")
    public String del(Integer[] ids){
        bookService.dels(ids);
        return "redirect:/book/page";
    }

    @PostMapping("/add")
    public String add(Book book){
        bookService.add(book);
        return "redirect:/book/page";
    }

    @PostMapping("/update")
    public String update(Book book){
        bookService.update(book);
        return "redirect:/book/page";
    }

}
