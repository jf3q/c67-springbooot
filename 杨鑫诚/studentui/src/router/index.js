import { createRouter, createWebHistory } from 'vue-router'
import AppView from '../components/vueproject/App.vue'
import StudentView from '../views/StudentView.vue'
import TestView from '../views/TestView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: AppView
  },
  {
    path: '/student',
    name: 'student',
    component: AppView,
    children:[{path:'',component:StudentView}]
  },
  {
    path: '/test',
    name: 'test',
    component: AppView,
    children:[{path:'',component:TestView}]
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
