package com.yxc.spring_cache.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yxc.spring_cache.entity.Book;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BookDao extends BaseMapper<Book> {
}
