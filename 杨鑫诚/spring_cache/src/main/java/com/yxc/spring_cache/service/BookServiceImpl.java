package com.yxc.spring_cache.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yxc.spring_cache.dao.BookDao;
import com.yxc.spring_cache.entity.Book;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl extends ServiceImpl<BookDao, Book> implements BookService {
}
