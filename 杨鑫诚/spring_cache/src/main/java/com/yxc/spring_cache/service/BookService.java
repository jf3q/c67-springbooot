package com.yxc.spring_cache.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yxc.spring_cache.entity.Book;

public interface BookService extends IService<Book> {
}
