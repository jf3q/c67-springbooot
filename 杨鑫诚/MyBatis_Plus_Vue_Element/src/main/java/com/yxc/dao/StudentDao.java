package com.yxc.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yxc.entity.Student;

public interface StudentDao extends BaseMapper<Student> {
}
