package com.yxc.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yxc.dao.StudentDao;
import com.yxc.entity.Student;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class StudentServiceImpl extends ServiceImpl<StudentDao, Student> implements IStudentService {
    @Override
    public Page<Student> getpage(Integer pageNum,@RequestBody String search) {
        Page<Student> page = new Page<>(pageNum, 3);
        LambdaQueryWrapper<Student> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (search != null && !search.isEmpty()){
            lambdaQueryWrapper.like(Student::getStudentname,search);
        }
        lambdaQueryWrapper.orderByDesc(Student::getId);
        return this.page(page,lambdaQueryWrapper);
    }
}
