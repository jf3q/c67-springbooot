package com.yxc.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yxc.entity.Student;

public interface IStudentService extends IService<Student> {
    Page<Student> getpage(Integer pageNum, String search);
}
