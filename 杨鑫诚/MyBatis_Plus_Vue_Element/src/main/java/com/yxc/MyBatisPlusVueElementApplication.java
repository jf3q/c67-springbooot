package com.yxc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.yxc.dao")
public class MyBatisPlusVueElementApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyBatisPlusVueElementApplication.class, args);
    }

}
