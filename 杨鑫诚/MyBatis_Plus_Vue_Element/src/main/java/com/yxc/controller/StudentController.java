package com.yxc.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yxc.entity.Student;
import com.yxc.service.IStudentService;
import com.yxc.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    IStudentService StudentService;

    @GetMapping
    public Result<?> findPage(@RequestParam(defaultValue = "1") Integer pageNum, @RequestParam(defaultValue = "",required = false) String search) {
        System.out.println("search:"+search);
        Page<Student> page = StudentService.getpage(pageNum,search);
        return Result.success(page);
    }

    @PostMapping
    public Result<?> saveOrUpdate(@RequestBody Student student) {
        StudentService.saveOrUpdate(student);
        return Result.success();
    }

    @DeleteMapping("/{id}")
    public Result<?> delete(@PathVariable long id) {
        StudentService.removeById(id);
        return Result.success();
    }

}
