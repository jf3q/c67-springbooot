package com.yxc.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Result<T> {
    private String code;
    private String message;
    private T data;
    public static<T> Result<T> success(String message,T data){
        return new Result<>("0",message,data);
    }
    public static<T> Result<T> success(T data){
        return new Result<>("0",null,data);
    }
    public static<T> Result<T> success(){
        return new Result<>("0",null,null);
    }
    public static<T> Result<T> error(String message){
        return new Result<>("500",message,null);
    }
}
