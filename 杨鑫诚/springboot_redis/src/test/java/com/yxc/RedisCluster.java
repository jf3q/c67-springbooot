package com.yxc;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

@SpringBootTest
public class RedisCluster {
    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Test
    void test(){
        stringRedisTemplate.opsForValue().set("redis_cluster","hello");
        System.out.println(stringRedisTemplate.opsForValue().get("redis_cluster"));
    }
}