package com.yxc;

import com.yxc.dao.BookInfoDao;
import com.yxc.entity.BookInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Iterator;

@SpringBootTest
public class RedisRepository {
    @Autowired
    BookInfoDao bookInfoDao;
    @Test //添加图书
    void testSaveBook() {
        BookInfo book1=new BookInfo(1,"西游记","文学","吴承恩",88);
        bookInfoDao.save(book1);
        BookInfo book2=new BookInfo(2,"三国演义","文学","罗贯中",78);
        bookInfoDao.save(book2);
        BookInfo book3=new BookInfo(3,"水浒传","文学","施耐俺",68);
        bookInfoDao.save(book3);
        BookInfo book4=new BookInfo(4,"三国志","文学","佚名",78);
        bookInfoDao.save(book4);
        System.out.println("添加成功！");
    }

    @Test
//        查询所有图书
    void testFindAllBooks() {
        Iterable<BookInfo> iterable=bookInfoDao.findAll();
        Iterator<BookInfo> it=iterable.iterator();
        while(it.hasNext()){
            BookInfo book= it.next();
            System.out.println(book);
        }
    }


    @Test //根据Id号图书
    void testFindBookById() {
        BookInfo book=bookInfoDao.findById(1).get();
        System.out.println(book);
    }

    @Test //根据Name查询图书
    void testFindBooksByName() {
        Iterable<BookInfo> iterable=bookInfoDao.findByName("三国志");
        Iterator<BookInfo> it=iterable.iterator();
        while(it.hasNext()){
            BookInfo book= it.next();
            System.out.println(book);
        }
    }

    @Test //修改图书
    void testUpdateBook() {
        BookInfo book=bookInfoDao.findById(1).get();
        book.setPrice(98);
        bookInfoDao.save(book);
        System.out.println("修改成功！");
    }

    @Test //删除图书
    void testDeleteBook() {
        bookInfoDao.deleteById(2);
        System.out.println("删除成功！");
    }
}