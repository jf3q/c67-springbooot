package com.yxc.service;

import com.yxc.dao.BookDao;
import com.yxc.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    BookDao bookDao;

    public void setValue(String key,String value){
        bookDao.setValue(key,value);
    }

    public String getValue(String key){
        return bookDao.getValue(key);
    }

    public void delete(String key){
        bookDao.delete(key);
    }

    public void saveBooks(String key, List<Book> books){
        bookDao.saveBooks(key,books);
    }

    public List<Book> findBooks(String key){
        return bookDao.findBooks(key);
    }

    public void saveBook(Book book){
        bookDao.saveBook(book);
    }

    public Book getBookByKey(Integer key){
        return bookDao.getBookByKey(key);
    }

    public void delBook(Integer key){
        bookDao.delBook(key);
    }
}
