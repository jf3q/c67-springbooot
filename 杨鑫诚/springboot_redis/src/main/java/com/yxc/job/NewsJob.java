package com.yxc.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;

@Slf4j
@Component
public class NewsJob {
    @Autowired
    private RedisTemplate redisTemplate;

    @Scheduled(cron = "0 0/1 * * * ? ")
    public void saveNew(){
        Set<String> keys = redisTemplate.keys("news_*");
        for(String key:keys){
            Map map=redisTemplate.opsForHash().entries(key);
            String id=key.substring(5);//获取key中包含的id号，key是news_+id的格式
            //然后就可调用业务层将这些数据update到数据库，自行实现。
            log.info("编号："+id+",阅读次数："+map.get("read")+",点赞次数："+map.get("countZan"));
        }
        log.info("以上数据已保存到数据库");
    }
}