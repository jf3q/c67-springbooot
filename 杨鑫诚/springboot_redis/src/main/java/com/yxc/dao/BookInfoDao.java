package com.yxc.dao;

import com.yxc.entity.BookInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookInfoDao extends CrudRepository<BookInfo,Integer>{
    Iterable<BookInfo> findByName(String name);
}
