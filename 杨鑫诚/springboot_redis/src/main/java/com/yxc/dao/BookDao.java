package com.yxc.dao;

import com.yxc.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Repository
public class BookDao {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate redisTemplate;

    public void setValue(String key,String value){
        stringRedisTemplate.opsForValue().set(key,value,24, TimeUnit.HOURS);
    }

//    public void setValue(String key,String value){
//        stringRedisTemplate.opsForValue().set(key,value);
//    }

    public String getValue(String key){
        return stringRedisTemplate.opsForValue().get(key);
    }

    public void delete(String key){
        stringRedisTemplate.delete(key);
    }

    public void saveBooks(String key, List<Book> books){
        redisTemplate.opsForValue().set(key,books);
    }

    public List<Book> findBooks(String key){
        return (List<Book>) redisTemplate.opsForValue().get(key);
    }

    public void saveBook(Book book){
        redisTemplate.opsForValue().set(book.getId(),book);
    }

    public Book getBookByKey(Integer key){
        return (Book) redisTemplate.opsForValue().get(key);
    }

    public void delBook(Integer key){
        redisTemplate.delete(key);
    }


}
