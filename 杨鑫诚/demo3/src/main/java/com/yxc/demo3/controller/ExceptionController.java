package com.yxc.demo3.controller;

import com.yxc.demo3.entity.UserInfo;
import com.yxc.demo3.vo.ResultVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/exception")
@RestController
public class ExceptionController {

    @GetMapping("/test1")
    public ResultVo test1() {
        UserInfo userInfo = null;
        userInfo.setUsername("zhangsan");
        return ResultVo.success("",userInfo);
    }

    @GetMapping("/test2")
    public ResultVo test2() {
        int[] arr= {1,2,3};
        System.out.println(arr[3]);//发生越界针异常
        //以下是正常业务代码 省略


        return ResultVo.success("",arr);
    }

    @GetMapping("/test3")
    public ResultVo test3() {
        int i=10/0; //发生算术异常
        //以下是正常业务代码
        return ResultVo.success("",i);
    }


}