package com.yxc.demo3.controller;

import com.yxc.demo3.entity.UserInfo;
import com.yxc.demo3.vo.ResultVo;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/restful")
@CrossOrigin
public class RestfulController {

    @GetMapping("/getMsg")
    public String getMsg(){
        return "GET SUCCESS";
    }

    @DeleteMapping("/delMsg")
    public String delMsg(){
        return "DELETE SUCCESS";
    }

    @PostMapping
    public ResultVo add(@RequestBody UserInfo userInfo){
        return ResultVo.success("新增成功",userInfo);
    }

    @DeleteMapping("/{id}")
    public ResultVo del(@PathVariable Integer id){
        return ResultVo.success("删除成功",id);
    }

    @PutMapping
    public ResultVo update(@RequestBody UserInfo userInfo){
        return ResultVo.success("新增成功",userInfo);
    }

    @GetMapping("/{id}")
    public ResultVo getById(@PathVariable Integer id){
        return ResultVo.success("查询单个",id);
    }

    @GetMapping
    public ResultVo getAll(){
        return ResultVo.success("查询全部",null);
    }


}
