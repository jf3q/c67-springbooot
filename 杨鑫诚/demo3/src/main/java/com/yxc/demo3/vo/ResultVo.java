package com.yxc.demo3.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultVo{
    private Integer code;
    private String message;
    private Object data;

    public static ResultVo success(String message,Object data){
        return new ResultVo(200,message,data);
    }

    public static ResultVo error(String message){
        return new ResultVo(500,message,null);
    }
}
