package com.yxc.demo3.entity;

import lombok.Data;

@Data
public class UserInfo {
    private Integer id;
    private String username;
}
