package com.yxc.demo3.controller;

import com.yxc.demo3.entity.User;
import com.yxc.demo3.vo.ResultVo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class HelloController {

    @GetMapping("/login")
    @ResponseBody
    public ResultVo login(User user){
        return ResultVo.success(user.toString(),user);
    }

    @GetMapping("/test")
    public String test(Model model){
        List<String> list = new ArrayList<>();
        list.add("北京");
        list.add("上海");
        list.add("广州");
        list.add("深圳");
        model.addAttribute("list",list);
        model.addAttribute("date",new Date());
        model.addAttribute("salary",2000.168);
        model.addAttribute("word","hello world");
        return "test";
    }

    @GetMapping("/index")
    public String index(Model model) {
        model.addAttribute("username", "jf3q");
        model.addAttribute("text1", "yang");
        model.addAttribute("text2","<h1>hello world</h1>");
        model.addAttribute("gender","男");
        model.addAttribute("city","广州");
        User user1=new User(1,"李白","123");
        User user2=new User(2,"杜甫","123");
        List<User> users=new ArrayList<>();
        users.add(user1);
        users.add(user2);
        model.addAttribute("users",users);
        model.addAttribute("user",user1);
        model.addAttribute("name","telepone");
        model.addAttribute("score",85);
        model.addAttribute("password",123);
        return "hello";
    }
}
