package com.yxc;

import com.yxc.entity.UserInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Demo2ApplicationTests {

    @Test
    void contextLoads() {
    }

    @Value("${mobilephone}")
    String mobilephone;

    @Value("${user.username}")
    String username;

    @Value("${city[2]}")
    String city;

    @Value("${books[1].name}")
    String bookName;

    @Test
    void testUserName(){
        System.out.println(username);
    }

    @Autowired
    UserInfo userInfo;

    @Test
    void testUser(){
        System.out.println(userInfo);
    }
}
