package com.yxc.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @Value("${db.url}")
    private String url;
    @Value("${db.username}")
    private String username;
    @Value("${db.password}")
    private String password;
    @GetMapping("/test")
    public String test(){
        String str="<h1>服务端数据库信息如下</h1>";
        str+="<h2>url:"+url+"</h2>";
        str+="<h2>username:"+username+"</h2>";
        str+="<h2>password:"+password+"</h2>";
        return str;
    }
    @RequestMapping("/hello")
    public String hello(){
        System.out.println("Hello world");
        return "Hello world";
    }

}
