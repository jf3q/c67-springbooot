package com.yxc.admin.controller;

import com.yxc.admin.service.AdminService;
import com.yxc.common.dto.LoginUserDto;
import com.yxc.common.vo.LoginUserVo;
import com.yxc.common.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    AdminService adminService;

    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginUserDto loginUserDto){
        LoginUserVo vo = null;
        try {
            vo = adminService.login(loginUserDto);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
        return ResultVo.success("登录成功",vo);
    }

}
