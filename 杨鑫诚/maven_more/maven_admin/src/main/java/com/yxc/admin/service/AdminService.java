package com.yxc.admin.service;

import com.yxc.common.dao.BackendUserDao;
import com.yxc.common.dto.LoginUserDto;
import com.yxc.common.entity.BackendUser;
import com.yxc.common.utils.SessionUtils;
import com.yxc.common.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class AdminService {

    @Autowired
    BackendUserDao backendUserDao;

    public LoginUserVo login(LoginUserDto loginUserDto) {
        LoginUserVo vo = new LoginUserVo();
        BackendUser backendUser = new BackendUser();
        backendUser.setUsercode(loginUserDto.getAccount());
        backendUser.setUserpassword(loginUserDto.getPassword());
        List<BackendUser> list = backendUserDao.queryAllBy(backendUser);
        if (list.size()==0){
            throw new RuntimeException("账号或密码错误");
        }
        vo.setUserName(loginUserDto.getAccount());
        vo.setUserCode("admin");

        StringBuilder token = new StringBuilder();
        String uuid = UUID.randomUUID().toString().replace("-","");
        token.append(uuid).append("-").append(list.get(0).getUsercode())
                .append("-").append(list.get(0).getId())
                .append("-admin-").append(System.currentTimeMillis());
        // uuid-userCode-id-admin-time
        vo.setToken(token.toString());
        SessionUtils.put(token.toString(),list.get(0));
        return vo;
    }
}
