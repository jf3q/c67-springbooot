package com.yxc.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AppInfoReviewDto {
    private Long id;
    private Long status;
}
