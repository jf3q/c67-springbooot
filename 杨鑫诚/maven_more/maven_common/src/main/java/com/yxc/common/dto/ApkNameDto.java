package com.yxc.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ApkNameDto {
    private Long id;
    private String apkName;
}
