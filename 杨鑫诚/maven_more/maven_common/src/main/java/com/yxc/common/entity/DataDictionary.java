package com.yxc.common.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.io.Serializable;

/**
 * (DataDictionary)实体类
 *
 * @author makejava
 * @since 2023-12-11 09:07:13
 */
@Data
@Accessors(chain = true)
public class DataDictionary {
    /**
     * 主键ID
     */
    private Long id;
    /**
     * 类型编码
     */
    private String typecode;
    /**
     * 类型名称
     */
    private String typename;
    /**
     * 类型值ID
     */
    private Long valueid;
    /**
     * 类型值Name
     */
    private String valuename;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    private Long createdby;
    /**
     * 创建时间
     */
    private Date creationdate;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    private Long modifyby;
    /**
     * 最新更新时间
     */
    private Date modifydate;
}

