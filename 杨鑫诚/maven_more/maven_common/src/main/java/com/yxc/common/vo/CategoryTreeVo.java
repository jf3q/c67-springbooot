package com.yxc.common.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;
@Data
@Accessors(chain = true)
public class CategoryTreeVo {
    private Long id;
    private String categoryname;
    private Long parentid;
    private List<CategoryTreeVo> children;
}
