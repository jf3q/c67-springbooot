package com.yxc.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class ResultVo {
    private String code;
    private String message;
    private Object data;
    public static ResultVo success(String message,Object data){
        return new ResultVo("2000",message,data);
    }

    public static ResultVo success(String message){
        return new ResultVo("2000",message,null);
    }

    public static ResultVo success(Object data){
        return new ResultVo("2000",null,data);
    }

    public static ResultVo error(String message){
        return new ResultVo("5000",message,null);
    }
}
