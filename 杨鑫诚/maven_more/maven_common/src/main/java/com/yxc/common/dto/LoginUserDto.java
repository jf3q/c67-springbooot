package com.yxc.common.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LoginUserDto {
    private String account;
    private String password;
    private Integer loginType;
}
