package com.yxc.dev.controller;


import com.yxc.common.dto.LoginUserDto;
import com.yxc.common.utils.SessionUtils;
import com.yxc.common.vo.LoginUserVo;
import com.yxc.common.vo.ResultVo;
import com.yxc.dev.service.DevService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/devUser")
public class DevUserController {

    @Autowired
    DevService devService;

    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginUserDto loginUserDto){
        LoginUserVo vo = null;
        try {
            vo = devService.login(loginUserDto);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
        return ResultVo.success("登录成功",vo);
    }

    @GetMapping("/loginOut")
    public ResultVo loginOut(HttpServletRequest request){
        String token = request.getHeader("token");
        SessionUtils.remove(token);
        return ResultVo.success("退出成功");
    }
}
