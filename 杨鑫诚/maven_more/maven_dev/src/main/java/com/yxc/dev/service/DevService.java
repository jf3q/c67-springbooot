package com.yxc.dev.service;

import com.yxc.common.dao.DevUserDao;
import com.yxc.common.dto.LoginUserDto;
import com.yxc.common.entity.BackendUser;
import com.yxc.common.entity.DevUser;
import com.yxc.common.utils.SessionUtils;
import com.yxc.common.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DevService {

    @Autowired
    DevUserDao devUserDao;

    public LoginUserVo login(LoginUserDto loginUserDto) {
        LoginUserVo vo = new LoginUserVo();
        DevUser devUser = new DevUser();
        devUser.setDevcode(loginUserDto.getAccount());
        devUser.setDevpassword(loginUserDto.getPassword());
        List<DevUser> list = devUserDao.queryAllBy(devUser);
        if (list.size() == 0) {
            throw new RuntimeException("账号或密码错误");
        }
        vo.setUserName(loginUserDto.getAccount());
        vo.setUserCode("dev");

        StringBuilder token = new StringBuilder();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        token.append(uuid).append("-").append(list.get(0).getDevcode())
                .append("-").append(list.get(0).getId())
                .append("-dev-").append(System.currentTimeMillis());
        // uuid-userCode-id-dev-time
        vo.setToken(token.toString());
        SessionUtils.put(token.toString(), list.get(0));
        return vo;

    }
}
