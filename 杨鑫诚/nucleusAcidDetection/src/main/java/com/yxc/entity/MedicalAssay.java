package com.yxc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * (MedicalAssay)实体类
 *
 * @author makejava
 * @since 2023-12-27 15:23:54
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("medical_assay")
public class MedicalAssay {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String assayUser;
    private Integer hospitalId;
    private Integer assayResult;
    private String phone;
    private String cardNum;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date assayTime;
    @TableField(exist = false)
    private String name;
}

