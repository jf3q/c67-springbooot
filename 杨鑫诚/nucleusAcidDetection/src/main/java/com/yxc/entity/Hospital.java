package com.yxc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (Hospital)实体类
 *
 * @author makejava
 * @since 2023-12-27 15:23:53
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("hospital")
public class Hospital {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String name;
}

