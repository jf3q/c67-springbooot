package com.yxc.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yxc.entity.Hospital;
import com.yxc.entity.MedicalAssay;
import org.apache.ibatis.annotations.Mapper;

/**
 * (Hospital)表数据库访问层
 *
 * @author makejava
 * @since 2023-12-27 15:23:53
 */
@Mapper
public interface HospitalDao extends BaseMapper<Hospital> {
}

