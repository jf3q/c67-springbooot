package com.yxc.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yxc.entity.MedicalAssay;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.Mapping;

import java.util.List;

/**
 * (MedicalAssay)表数据库访问层
 *
 * @author makejava
 * @since 2023-12-27 15:23:54
 */
@Mapper
public interface MedicalAssayDao extends BaseMapper<MedicalAssay> {
}

