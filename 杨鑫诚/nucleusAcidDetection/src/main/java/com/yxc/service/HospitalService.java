package com.yxc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yxc.dao.HospitalDao;
import com.yxc.entity.Hospital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@CacheConfig(cacheNames = "hospital",keyGenerator = "nucleusAcidDetectionGenerator")
@Cacheable
public class HospitalService extends ServiceImpl<HospitalDao,Hospital> implements IService<Hospital> {
}
