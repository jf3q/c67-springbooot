package com.yxc.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yxc.dao.MedicalAssayDao;
import com.yxc.entity.MedicalAssay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@CacheConfig(cacheNames = "medicalAssay",keyGenerator = "nucleusAcidDetectionGenerator")
@Cacheable
public class MedicalAssayService extends ServiceImpl<MedicalAssayDao, MedicalAssay> implements IService<MedicalAssay> {

    @Autowired
    MedicalAssayDao medicalAssayDao;

    public Page<MedicalAssay> getPage(Integer hospitalId, Integer pageNum) {
        Page<MedicalAssay> page = new Page<>(pageNum, 3);
        LambdaQueryWrapper<MedicalAssay> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (hospitalId != null && hospitalId!=0){
            lambdaQueryWrapper.like(MedicalAssay::getHospitalId,hospitalId);
        }
        lambdaQueryWrapper.orderByDesc(MedicalAssay::getId);
        return this.page(page,lambdaQueryWrapper);
    }
}
 