package com.yxc.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yxc.entity.Hospital;
import com.yxc.entity.MedicalAssay;
import com.yxc.service.HospitalService;
import com.yxc.service.MedicalAssayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/index")
public class MedicalAssayController {

    @Autowired
    MedicalAssayService medicalAssayService;

    @Autowired
    HospitalService hospitalService;

    @GetMapping
    public String getAll(@RequestParam(defaultValue = "0",required = false) Integer hospitalId, @RequestParam(defaultValue = "1") Integer pageNum, Model model){
        Page<MedicalAssay> page = medicalAssayService.getPage(hospitalId, pageNum);
        List<Hospital> hospitalList = hospitalService.list();
        model.addAttribute("page",page);
        model.addAttribute("hospitalList",hospitalList);
        model.addAttribute("hospitalId",hospitalId);
        return "index";
    }

    @GetMapping("/yes")
    public String getYesNo(Integer id){
        medicalAssayService.saveOrUpdate(new MedicalAssay().setId(id));
        return "redirect:/index";
    }

    @GetMapping("/toAdd")
    public String add(Model model){
        List<Hospital> hospitalList = hospitalService.list();
        model.addAttribute("hospitalList",hospitalList);
        return "add";
    }

    @PostMapping("/add")
    public String add(MedicalAssay medicalAssay){
        medicalAssay.setAssayResult(0);
        medicalAssayService.saveOrUpdate(medicalAssay);
        return "redirect:/index";
    }

}
