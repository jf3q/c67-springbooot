package org.example.demo7.dao;

import jakarta.annotation.Resource;
import org.example.demo7.entity.Book;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BookDao {

    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    //存储字符串类型的key-value
    public void setValue(String key,String value){
        stringRedisTemplate.opsForValue().set(key,value);
    }

    //通过键获取字符串类型的值
    public String getValue(String key){
        return stringRedisTemplate.opsForValue().get(key);
    }

    //查找所有书
    public List<Book> findBooks(String id){
        return (List<Book>) redisTemplate.opsForValue().get(id);
    }

    //保存书
    public void saveBooks(Object key,List<Book> books){
        redisTemplate.opsForValue().set(key,books);
    }

    //通过id查找一本书
    public Book findBookById(int id){
        return (Book) redisTemplate.opsForValue().get(String.valueOf(id));
    }

    //保存书
    public void saveBook(Book book){
        redisTemplate.opsForValue().set(String.valueOf(book.getId()),book);
    }

    public void deleteBook(int id){
        redisTemplate.delete(id);
    }

}
