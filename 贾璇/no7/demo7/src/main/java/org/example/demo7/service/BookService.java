package org.example.demo7.service;

import jakarta.annotation.Resource;
import org.example.demo7.dao.BookDao;
import org.example.demo7.entity.Book;
import org.springframework.stereotype.Service;
import org.thymeleaf.inline.IInliner;

import java.util.List;

@Service
public class BookService {

    @Resource
    private BookDao bookDao;

    public String getValue(String key){
        return bookDao.getValue(key);
    }

    public void setValue(String key,String value){
        bookDao.setValue(key,value);
    }

    public List<Book> findBooks(String id){
        return bookDao.findBooks(id);
    }

    public void savaBooks(Object key,List<Book> books){
        bookDao.saveBooks(key, books);
    }

    public Book findBookById(int id){
        return (Book) bookDao.findBookById(id);
    }

    public void saveBook(Book book){
        bookDao.saveBook(book);
    }

    public void deleteBook(int id){
        bookDao.deleteBook(id);
    }

}
