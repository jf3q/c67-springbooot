package org.example.demo2.utils;

import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

public class CorsConfig implements WebMvcConfigurer {
    public void addCorsMapping(CorsRegistry registry){
        //指定可以跨域的路径
        registry.addMapping("/**")
                 //允许的origin域名
                .allowedOrigins("*")
                .allowedOriginPatterns("*")
                 //允许的请求方法
                .allowedMethods("GET","HEAD","POST","PUT","DELETE")
                //是否允许发送Cookie
                .allowCredentials(true)
                //从预检请求得到相应的最大时间,默认为30min
                .maxAge(3600)
                //允许请求头
                .allowedHeaders("*");

    }
}
