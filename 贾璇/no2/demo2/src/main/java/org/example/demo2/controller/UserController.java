package org.example.demo2.controller;

import org.example.demo2.entity.User;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    @PostMapping
    public String add(@RequestBody User user){
        System.out.println("新增用户信息:"+user);
        return "新增成功";
    }


    @DeleteMapping("/{id}")
    public String del(@PathVariable int id){
        System.out.println("删除用户id:"+id);
        return "删除成功";
    }

    @PutMapping
    public String update(@RequestBody User user){
        System.out.println("修改用户信息:"+user);
        return "修改成功";
    }

    @GetMapping("/{id}")
    public String getUserId(@PathVariable int id){
        System.out.println("查找用户id:"+id);
        return "查找用户id成功";
    }

    @GetMapping
    public String getUser(){
        System.out.println("查找所有用户");
        return "查找所有用户成功";
    }
}
