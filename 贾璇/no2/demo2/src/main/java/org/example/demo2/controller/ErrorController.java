package org.example.demo2.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class ErrorController {
    @GetMapping("/test1")
    public String test1(){
        int i =1/0;
        return "false";
    }
}
