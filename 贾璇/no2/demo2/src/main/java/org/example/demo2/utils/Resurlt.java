package org.example.demo2.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Resurlt<T> {
    private String code;
    private String message;
    private Boolean result;
    private T data ;
}
