package com.jx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwoDemo2Application {

	public static void main(String[] args) {
		SpringApplication.run(TwoDemo2Application.class, args);
	}

}
