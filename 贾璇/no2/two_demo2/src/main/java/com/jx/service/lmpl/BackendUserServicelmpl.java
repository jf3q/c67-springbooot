package com.jx.service.lmpl;

import com.jx.dao.BackendUserDao;
import com.jx.dto.LoginDto;
import com.jx.entity.BackendUser;
import com.jx.service.BackendUserService;
import com.jx.utils.SessionUtils;
import com.jx.vo.LoginVo;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;

import java.util.List;
import java.util.UUID;

@Service
public class BackendUserServicelmpl implements BackendUserService {

    @Resource
    BackendUserDao backendUserDao;

    @Override
    public LoginVo login(LoginDto loginDto) {
        if (loginDto.getAccount()==null){
            throw new RuntimeException("账号必填!");
        }
        if (loginDto.getPwd()==null) {
            throw new RuntimeException("密码必填!");
        }

        BackendUser bakUser = new BackendUser();
        bakUser.setUsercode(loginDto.getAccount());
        bakUser.setUserpassword(loginDto.getPwd());

        List<BackendUser> backList = backendUserDao.queryAll(bakUser);
        if (backList.size()==0){
            throw new RuntimeException("账号密码错误!!");
        }

        LoginVo vo = new LoginVo();
        vo.setAccount(backList.get(0).getUsercode());
        vo.setType(1);

        String replace = UUID.randomUUID().toString().replace("-","");
        String token = replace+"-"+System.currentTimeMillis()+"-"+"1"+"-"+backList.get(0).getId();
        vo.setToken(token);
        SessionUtils.put(token,backList.get(0));
        return vo;
    }

}
