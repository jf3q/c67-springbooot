package com.jx.vo;

import java.util.List;

public class CategoryVo {
    private Long id;
    private String categoryname;
    private Long parentid;
    private List<CategoryVo> children;

    @Override
    public String toString() {
        return "CategoryVo{" +
                "id=" + id +
                ", categoryname='" + categoryname + '\'' +
                ", parentid=" + parentid +
                ", children=" + children +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    public List<CategoryVo> getChildren() {
        return children;
    }

    public void setChildren(List<CategoryVo> children) {
        this.children = children;
    }
}
