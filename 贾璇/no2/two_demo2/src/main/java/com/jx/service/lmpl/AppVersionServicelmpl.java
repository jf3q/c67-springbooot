package com.jx.service.lmpl;

import com.jx.dao.AppVersionDao;
import com.jx.entity.AppVersion;
import com.jx.service.AppVersionService;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.List;

@Service
public class AppVersionServicelmpl implements AppVersionService {

    @Resource
    AppVersionDao appVersionDao;

    @Override
    public List<AppVersion> selVersion(Long id) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(id);
        List<AppVersion> appVersionList = appVersionDao.queryAll(appVersion);
        return appVersionList;
    }

    @Override
    public void insVersion(AppVersion appVersion) {
        appVersionDao.insert(appVersion);
    }
}
