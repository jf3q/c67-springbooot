package com.jx.controller;

import com.jx.dto.LoginDto;
import com.jx.service.BackendUserService;
import com.jx.service.DevUserService;
import com.jx.utils.SessionUtils;
import com.jx.vo.LoginVo;
import com.jx.vo.ResultVo;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;


@RestController
public class LoginController {

    @Resource
    DevUserService devUserService;

    @Resource
    BackendUserService backendUserService;

    @RequestMapping("login")
    public ResultVo login(@RequestBody LoginDto loginDto){
        if (loginDto.getType()==1){
            try {
                LoginVo vo = backendUserService.login(loginDto);
                return ResultVo.success("登录成功",vo);
            } catch (Exception e) {
                e.printStackTrace();
                return ResultVo.error("登录失败");
            }
        }
        else if (loginDto.getType()==2){
            try {
                LoginVo vo = devUserService.login(loginDto);
                return ResultVo.success("登录成功",vo);
            } catch (Exception e) {
                e.printStackTrace();
                return ResultVo.error("登录失败");
            }
        }
        return ResultVo.error("请求失败,没有此用户！！");
    }

    @RequestMapping("loginOut")
    public ResultVo loginOut(HttpServletRequest request){
        String token = request.getHeader("token");
        SessionUtils.remove(token);
        return ResultVo.success("操作成功",null);
    }

}
