package com.jx.service;

import com.jx.dto.LoginDto;
import com.jx.vo.LoginVo;

public interface DevUserService {
    LoginVo login(LoginDto loginDto);
}
