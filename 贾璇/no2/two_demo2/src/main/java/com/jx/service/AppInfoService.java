package com.jx.service;

import com.github.pagehelper.PageInfo;
import com.jx.dto.AppInfoDto;
import com.jx.entity.AppInfo;
import jakarta.servlet.http.HttpServletRequest;


public interface AppInfoService {
    PageInfo<AppInfo> page(AppInfoDto appInfoDto, Integer num);

    void getInsOrUpdate(AppInfo appInfo);

    AppInfo updateId(Long id);

    void getStatus(Long id);

    void del(Long id, HttpServletRequest request);

    Boolean selApkName(String apkName, Long id);
}
