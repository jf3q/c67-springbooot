package com.jx.service.lmpl;

import com.jx.dao.DevUserDao;
import com.jx.dto.LoginDto;
import com.jx.entity.DevUser;
import com.jx.service.DevUserService;
import com.jx.utils.SessionUtils;
import com.jx.vo.LoginVo;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.List;
import java.util.UUID;

@Service
public class DevUserServicelmpl implements DevUserService {

    @Resource
    DevUserDao devUserDao;

    @Override
    public LoginVo login(LoginDto loginDto) {
        if (loginDto.getAccount()==null){
            throw new RuntimeException("账号必填!");
        }
        if (loginDto.getPwd()==null) {
            throw new RuntimeException("密码必填!");
        }

        DevUser devUser = new DevUser();
        devUser.setDevcode(loginDto.getAccount());
        devUser.setDevpassword(loginDto.getPwd());

        List<DevUser> devUserList = devUserDao.queryAll(devUser);
        if (devUserList.size()==0){
            throw new RuntimeException("账号密码错误!!");
        }

        LoginVo vo = new LoginVo();
        vo.setAccount(devUserList.get(0).getDevcode());
        vo.setType(2);

        String replace = UUID.randomUUID().toString().replace("-","");
        String token = replace+"-"+System.currentTimeMillis()+"-"+"2"+"-"+devUserList.get(0).getId();
        vo.setToken(token);
        SessionUtils.put(token,devUserList.get(0));
        return vo;
    }
}
