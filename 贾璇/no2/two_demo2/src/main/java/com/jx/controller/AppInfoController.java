package com.jx.controller;

import com.github.pagehelper.PageInfo;
import com.jx.dto.AppInfoDto;
import com.jx.entity.AppInfo;
import com.jx.entity.AppVersion;
import com.jx.entity.DevUser;
import com.jx.service.AppInfoService;
import com.jx.service.AppVersionService;
import com.jx.service.CategoryService;
import com.jx.utils.SessionUtils;
import com.jx.vo.CategoryVo;
import com.jx.vo.ResultVo;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class AppInfoController {

    @Resource
    AppInfoService appInfoService;
    @Resource
    CategoryService categoryService;
    @Resource
    AppVersionService appVersionService;

    @RequestMapping("page")
    public ResultVo page(@RequestBody AppInfoDto appInfoDto,
                         @RequestParam(defaultValue = "1") Integer num,
                         HttpServletRequest request){
        String token = request.getHeader("token");
        String[] split = token.split("-");
        if (split[2].equals("1")){
            appInfoDto.setStatus(1L);
        }
        else if (split[2].equals("2")){
            appInfoDto.setDevid(Long.valueOf(split[3]));
        }
        PageInfo<AppInfo> pageInfo = appInfoService.page(appInfoDto,num);
        return ResultVo.success("",pageInfo);
    }

    @RequestMapping("sanJi")
    public ResultVo sanJi(){
        CategoryVo cateVo = categoryService.sanJi();
        return ResultVo.success("",cateVo);
    }

    @RequestMapping("insOrUpdate")
    public ResultVo insOrUpdate(AppInfo appInfo, MultipartFile file,HttpServletRequest request){
        String token = request.getHeader("token");
        if (file!=null&&(!file.isEmpty())){
            String realPath = request.getServletContext().getRealPath("/img");
            File file1 = new File(realPath);
            if (!file1.exists()){
                file1.mkdirs();
            }
            String originalFilename = file.getOriginalFilename();
            String substring = originalFilename.substring(originalFilename.lastIndexOf("."));
            if (file.getSize()>1024000) {
                return ResultVo.error("文件过大!");
            }else if (substring.equalsIgnoreCase(".jpg")||
                    substring.equalsIgnoreCase(".gif")||
                    substring.equalsIgnoreCase(".jpeg")||
                    substring.equalsIgnoreCase(".png")){
                String new_file = UUID.randomUUID()+substring;
                File file2 = new File(realPath + "/" + new_file);
                try {
                    file.transferTo(file2);
                    appInfo.setLogopicpath("img/"+new_file);
                } catch (IOException e) {
                    e.printStackTrace();
                    return ResultVo.error("文件上传错误！！!");
                }
            }
            else {
                return ResultVo.error("文件格式错误!");
            }
        }

        DevUser devUser = (DevUser) SessionUtils.get(token);
        if (appInfo.getId()==null){
            appInfo.setCreatedby(devUser.getId());
            appInfo.setCreationdate(new Date());
            appInfo.setStatus(1L);
            appInfo.setDevid(devUser.getId());
            appInfo.setDownloads(0L);
        }
        else {
            appInfo.setModifyby(devUser.getId());
            appInfo.setModifydate(new Date());
            appInfo.setStatus(1L);
        }
        appInfoService.getInsOrUpdate(appInfo);
        return ResultVo.success("操作成功",null);
    }

    @RequestMapping("updateId")
    public ResultVo updateId(Long id){
        AppInfo appInfo = appInfoService.updateId(id);
        return ResultVo.success("",appInfo);
    }

    @RequestMapping("getStatus")
    public ResultVo getStatus(Long id){
        appInfoService.getStatus(id);
        return ResultVo.success("",null);
    }

    @RequestMapping("del")
    public ResultVo del(Long id,HttpServletRequest request){
        appInfoService.del(id,request);
        return ResultVo.success("",null);
    }

    @RequestMapping("selVersion")
    public ResultVo selVersion(Long id){
        List<AppVersion> appVersionList =  appVersionService.selVersion(id);
        return  ResultVo.success("",appVersionList);
    }

    @RequestMapping("insVersion")
    public ResultVo insVersion(AppVersion appVersion, MultipartFile apkfile,HttpServletRequest request){
        String token = request.getHeader("token");
        if (apkfile!=null&&(!apkfile.isEmpty())){
            String realPath = request.getServletContext().getRealPath("/apk");
            File file1 = new File(realPath);
            if (!file1.exists()){
                file1.mkdirs();
            }
            String originalFilename = apkfile.getOriginalFilename();
            String substring = originalFilename.substring(originalFilename.lastIndexOf("."));
            if (apkfile.getSize()>1024000) {
                return ResultVo.error("文件过大!");
            }else if (substring.equalsIgnoreCase(".apk")){
                String new_file = UUID.randomUUID()+substring;
                File file2 = new File(realPath + "/" + new_file);
                try {
                    apkfile.transferTo(file2);
                    appVersion.setDownloadlink("apk/"+new_file);
                } catch (IOException e) {
                    e.printStackTrace();
                    return ResultVo.error("文件上传错误！！!");
                }
            }
            else {
                return ResultVo.error("文件格式错误!");
            }
        }

        DevUser devUser = (DevUser) SessionUtils.get(token);
        if (appVersion.getId()==null){
            appVersion.setCreatedby(devUser.getId());
            appVersion.setCreationdate(new Date());
            appVersion.setPublishstatus(3L);
        }
        appVersionService.insVersion(appVersion);
        AppInfo appInfo = new AppInfo();
        appInfo.setVersionid(appVersion.getId());
        appInfo.setId(appVersion.getAppid());
        appInfoService.getInsOrUpdate(appInfo);
        return ResultVo.success("操作成功",null);
    }

    @RequestMapping("sheng")
    public ResultVo sheng(@RequestBody AppInfo appInfo){
        appInfoService.getInsOrUpdate(appInfo);
        return ResultVo.success("操作成功",null);
    }

    @RequestMapping("getApkName")
    public ResultVo getApkName(String apkName,Long id){
        Boolean aBoolean = appInfoService.selApkName(apkName,id);
        if (aBoolean){
            return ResultVo.success("",null);
        }
        else {
            return ResultVo.error("此apk名称重复不可使用！");
        }
    }
}
