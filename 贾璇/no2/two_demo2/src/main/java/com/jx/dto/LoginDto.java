package com.jx.dto;

public class LoginDto {
    private String account;
    private String pwd;
    private Integer type;

    @Override
    public String toString() {
        return "LoginDto{" +
                "account='" + account + '\'' +
                ", pwd='" + pwd + '\'' +
                ", type=" + type +
                '}';
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
