package com.jx.service;

import com.jx.entity.AppVersion;

import java.util.List;

public interface AppVersionService {
    List<AppVersion> selVersion(Long id);

    void insVersion(AppVersion appVersion);

}
