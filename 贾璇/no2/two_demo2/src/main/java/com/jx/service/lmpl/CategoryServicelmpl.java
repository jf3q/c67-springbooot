package com.jx.service.lmpl;

import com.jx.dao.AppCategoryDao;
import com.jx.entity.AppCategory;
import com.jx.service.CategoryService;
import com.jx.vo.CategoryVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import jakarta.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryServicelmpl implements CategoryService {

    @Resource
    AppCategoryDao appCategoryDao;

    @Override
    public CategoryVo sanJi() {
        List<CategoryVo> voList = new ArrayList<>();
        List<AppCategory> appCateList = appCategoryDao.queryAll(new AppCategory());
        for (AppCategory appCategory : appCateList) {
            CategoryVo vo = new CategoryVo();
            BeanUtils.copyProperties(appCategory,vo);
            voList.add(vo);
        }
        CategoryVo vo = new CategoryVo();
        for (CategoryVo categoryVo : voList) {
            if (categoryVo.getParentid()==null) {
                vo=getChildren(categoryVo,voList);
            }
        }
        return vo;
    }

    private CategoryVo getChildren(CategoryVo categoryVo, List<CategoryVo> voList) {
        List<CategoryVo> childrenList = new ArrayList<>();
        for (CategoryVo vo : voList) {
            if (categoryVo.getId()==vo.getParentid()){
                childrenList.add(vo);
                getChildren(vo,voList);
            }
        }
        categoryVo.setChildren(childrenList);
        return categoryVo;
    }
}
