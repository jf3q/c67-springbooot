package com.jx.vo;

public class LoginVo {
    private String account;
    private Integer type;
    private String token;

    @Override
    public String toString() {
        return "LoginVo{" +
                "account='" + account + '\'' +
                ", type=" + type +
                ", token='" + token + '\'' +
                '}';
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
