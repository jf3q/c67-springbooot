package com.jx.controller;

import com.jx.entity.MedicalAssay;
import com.jx.service.MedicalAssayService;
import jakarta.annotation.Resource;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * (MedicalAssay)表控制层
 *
 * @author makejava
 * @since 2023-12-27 15:14:08
 */
@Controller
@RequestMapping("medicalAssay")
public class MedicalAssayController {
    /**
     * 服务对象
     */
    @Resource
    private MedicalAssayService medicalAssayService;

    /**
     * 分页查询
     *
     * @param medicalAssay 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
//    @GetMapping
//    public ResponseEntity<Page<MedicalAssay>> queryByPage(MedicalAssay medicalAssay, PageRequest pageRequest) {
//        return ResponseEntity.ok(this.medicalAssayService.queryByPage(medicalAssay, pageRequest));
//    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public ResponseEntity<MedicalAssay> queryById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(this.medicalAssayService.queryById(id));
    }

    /**
     * 新增数据
     *
     * @param medicalAssay 实体
     * @return 新增结果
     */
    @PostMapping
    public String add(MedicalAssay medicalAssay) {
        MedicalAssay insert = medicalAssayService.insert(medicalAssay);
        if (insert.getId()!=null){
            return "redirect:/medicalAssay/index";
        }
        return "toAdd";
    }

    /**
     * 编辑数据
     *
     * @return 编辑结果
     */
    @RequestMapping ("upd")
    public String edit(@RequestParam("id")Integer id) {
        MedicalAssay medicalAssay = medicalAssayService.queryById(id);
        if (medicalAssay.getAssayResult()==0){
            medicalAssay.setAssayResult(1);
        }
        medicalAssayService.update(medicalAssay);
        return "redirect:/medicalAssay/index";
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public ResponseEntity<Boolean> deleteById(Integer id) {
        return ResponseEntity.ok(this.medicalAssayService.deleteById(id));
    }
    @RequestMapping("/index")
    public String index(Integer hospitalId, Model model){
        List<MedicalAssay> list =  medicalAssayService.queryByList(hospitalId);
        model.addAttribute("list",list);
        model.addAttribute("hospitalId",hospitalId);
        return "index";
    }

    @RequestMapping("toAdd")
    public String toAdd(){
        return "add";
    }
}

