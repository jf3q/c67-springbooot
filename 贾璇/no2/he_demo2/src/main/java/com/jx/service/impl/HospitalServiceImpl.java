package com.jx.service.impl;

import com.jx.entity.Hospital;
import com.jx.dao.HospitalDao;
import com.jx.service.HospitalService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * (Hospital)表服务实现类
 *
 * @author makejava
 * @since 2023-12-27 15:14:07
 */
@Service("hospitalService")
public class HospitalServiceImpl implements HospitalService {
    @Resource
    private HospitalDao hospitalDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Hospital queryById(Integer id) {
        return this.hospitalDao.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param hospital 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
   /* @Override
    public Page<Hospital> queryByPage(Hospital hospital, PageRequest pageRequest) {
        long total = this.hospitalDao.count(hospital);
        return new PageImpl<>(this.hospitalDao.queryAllByLimit(hospital, pageRequest), pageRequest, total);
    }*/

    /**
     * 新增数据
     *
     * @param hospital 实例对象
     * @return 实例对象
     */
    @Override
    public Hospital insert(Hospital hospital) {
        this.hospitalDao.insert(hospital);
        return hospital;
    }

    /**
     * 修改数据
     *
     * @param hospital 实例对象
     * @return 实例对象
     */
    @Override
    public Hospital update(Hospital hospital) {
        this.hospitalDao.update(hospital);
        return this.queryById(hospital.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.hospitalDao.deleteById(id) > 0;
    }

    @Override
    public List<Hospital> queryAll() {
        return hospitalDao.queryAll();
    }
}
