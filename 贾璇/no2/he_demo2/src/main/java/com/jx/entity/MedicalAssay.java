package com.jx.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * (MedicalAssay)实体类
 *
 * @author makejava
 * @since 2023-12-27 14:46:52
 */
@Data
@AllArgsConstructor
@Accessors(chain = true)
@NoArgsConstructor
public class MedicalAssay implements Serializable {
    private static final long serialVersionUID = 504092339178648789L;

    private Integer id;

    private String assayUser;

    private Integer hospitalId;
    private String hospitalName;

    private Integer assayResult;

    private String phone;

    private String cardNum;

    private String assayTime;



}

