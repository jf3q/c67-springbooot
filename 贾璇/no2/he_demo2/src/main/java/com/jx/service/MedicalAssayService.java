package com.jx.service;

import com.jx.entity.MedicalAssay;

import java.util.List;

/**
 * (MedicalAssay)表服务接口
 *
 * @author makejava
 * @since 2023-12-27 15:14:08
 */
public interface MedicalAssayService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    MedicalAssay queryById(Integer id);

    /**
     * 分页查询
     *
     * @param medicalAssay 筛选条件
     * @return 查询结果
     */
//    Page<MedicalAssay> queryByPage(MedicalAssay medicalAssay, PageRequest pageRequest);

    /**
     * 新增数据
     *
     * @param medicalAssay 实例对象
     * @return 实例对象
     */
    MedicalAssay insert(MedicalAssay medicalAssay);

    /**
     * 修改数据
     *
     * @param medicalAssay 实例对象
     * @return 实例对象
     */
    MedicalAssay update(MedicalAssay medicalAssay);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    List<MedicalAssay> queryByList(Integer hospitalId);
}
