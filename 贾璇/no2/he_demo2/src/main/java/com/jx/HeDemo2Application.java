package com.jx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeDemo2Application {

    public static void main(String[] args) {
        SpringApplication.run(HeDemo2Application.class, args);
    }

}
