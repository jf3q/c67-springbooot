package com.jx.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Hospital)实体类
 *
 * @author makejava
 * @since 2023-12-27 14:46:47
 */
@Data
@AllArgsConstructor
@Accessors(chain = true)
@NoArgsConstructor
public class Hospital implements Serializable {
    private static final long serialVersionUID = 531031894327543440L;

    private Integer id;

    private String name;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

