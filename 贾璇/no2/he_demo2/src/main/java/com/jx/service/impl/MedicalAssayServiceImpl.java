package com.jx.service.impl;

import com.jx.entity.MedicalAssay;
import com.jx.dao.MedicalAssayDao;
import com.jx.service.MedicalAssayService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * (MedicalAssay)表服务实现类
 *
 * @author makejava
 * @since 2023-12-27 15:14:08
 */
@Service("medicalAssayService")
public class MedicalAssayServiceImpl implements MedicalAssayService {
    @Resource
    private MedicalAssayDao medicalAssayDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public MedicalAssay queryById(Integer id) {
        return this.medicalAssayDao.queryById(id);
    }

    /**
     * 分页查询
     *
     * @param medicalAssay 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
//    @Override
//    public Page<MedicalAssay> queryByPage(MedicalAssay medicalAssay, PageRequest pageRequest) {
//        long total = this.medicalAssayDao.count(medicalAssay);
//        return new PageImpl<>(this.medicalAssayDao.queryAllByLimit(medicalAssay, pageRequest), pageRequest, total);
//    }

    /**
     * 新增数据
     *
     * @param medicalAssay 实例对象
     * @return 实例对象
     */
    @Override
    public MedicalAssay insert(MedicalAssay medicalAssay) {
        this.medicalAssayDao.insert(medicalAssay);
        return medicalAssay;
    }

    /**
     * 修改数据
     *
     * @param medicalAssay 实例对象
     * @return 实例对象
     */
    @Override
    public MedicalAssay update(MedicalAssay medicalAssay) {
        this.medicalAssayDao.update(medicalAssay);
        return this.queryById(medicalAssay.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.medicalAssayDao.deleteById(id) > 0;
    }

    @Override
    public List<MedicalAssay> queryByList(Integer hospitalId) {
        return medicalAssayDao.queryAll(new MedicalAssay().setHospitalId(hospitalId));
    }
}
