package Test;

import com.jx.config.AppConfig;
import com.jx.entity.UserInfo;
import com.jx.service.UserInfoService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class test {
    public static void main(String[] args) {
        //加载配置
        ApplicationContext a = new AnnotationConfigApplicationContext(AppConfig.class);
        UserInfoService userInfoService = a.getBean(UserInfoService.class);
        userInfoService.add(new UserInfo().setLiuname("67676").setPwd("111"));
    }
}
