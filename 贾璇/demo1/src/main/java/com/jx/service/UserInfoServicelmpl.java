package com.jx.service;

import com.jx.dao.UserInfoDao;
import com.jx.entity.UserInfo;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserInfoServicelmpl implements UserInfoService{
    /*public void setUserInfoDao(UserInfoDao userInfoDao) {
        this.userInfoDao = userInfoDao;
    }*/

    @Autowired
    private UserInfoDao  userInfoDao;

    @Override
    public void add(UserInfo userInfo) {
        userInfoDao.add(userInfo);
    }
}
