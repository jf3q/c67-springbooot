package com.jx;

import com.jx.controller.UserInfoController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class QiDong {
    public static void main(String[] args) {
         SpringApplication.run(QiDong.class,args);
    }
}
