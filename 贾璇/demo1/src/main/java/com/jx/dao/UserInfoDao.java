package com.jx.dao;

import com.jx.entity.UserInfo;
import org.springframework.stereotype.Repository;

public interface UserInfoDao {
    public void add(UserInfo userInfo);
}
