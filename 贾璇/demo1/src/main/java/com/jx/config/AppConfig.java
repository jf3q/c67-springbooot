package com.jx.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration //applicationContext.xml
@ComponentScan(basePackages = {"com.jx.dao","com.jx.service"})
/*@EnableWebMvc//相当于<mvc:annotation-driven/>*/
public class AppConfig implements WebMvcConfigurer {

    /*@Bean //<ben UserInfoDaolmpl相当于class=  userInfoDao方法名=id
    public UserInfoDaolmpl userInfoDao() {
        return new UserInfoDaolmpl();
    }

    @Bean //先调用dao方法
    public UserInfoServicelmpl userInfoService(){
        UserInfoServicelmpl userInfoService = new UserInfoServicelmpl();
        userInfoService.setUserInfoDao(userInfoDao());
        return userInfoService;
    }*/

   /* @Bean
    public InternalResourceViewResolver internalResourceViewResolver(){
        InternalResourceViewResolver in = new InternalResourceViewResolver();
        in.setPrefix("/");
        in.setSuffix(".jsp");
        return in;
    }


    //静态映射
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
       configurer.enable();
    }*/
}
