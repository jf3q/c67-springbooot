package com.jx.demo5.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jx.demo5.entity.Book;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BookMapper extends BaseMapper<Book> {
    //父接口已包含增删改查方法
}
