package com.jx.demo5.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jx.demo5.entity.Book;
import com.jx.demo5.service.BookService;
import io.micrometer.common.util.StringUtils;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("book")
public class BookController {
    @Resource
    BookService bookService;

//    @RequestMapping("books")
//    public String bookList(Model model, Book book){
//        List<Book> books = bookService.BOOK_LIST(book);
//        model.addAttribute("books",books);
//        return "index";
//    }

    @RequestMapping("page")
    public String page(Model model,@RequestParam(defaultValue = "1") int num,Book book){
        LambdaQueryWrapper<Book> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(book.getName()),Book::getName,book.getName());
        queryWrapper.like(StringUtils.isNotEmpty(book.getAuthor()),Book::getAuthor,book.getAuthor());
        queryWrapper.like(StringUtils.isNotEmpty(book.getCategory()),Book::getCategory,book.getCategory());
        IPage<Book> iPage = new Page<>(num, 3);
        bookService.page(iPage, queryWrapper);
        model.addAttribute("page",iPage);
        return "index";
    }

    @RequestMapping("/del")
    public String del(Integer[] ids){
        if (ids.length>0){
            bookService.removeByIds(Arrays.stream(ids).toList());
        }
        return "redirect:/book/page";
    }

    @RequestMapping("add")
    public String add(){
        return "addIndex";
    }

    @Value("${web.upload-path}")
    private String path;
    @RequestMapping("/toAdd")
    public String upload(@RequestParam MultipartFile file,Book book){
        if (!file.isEmpty()){
            File file1 = new File(path);
            if (!file1.exists()){
                file1.mkdir();
            }
            String originalFilename = file.getOriginalFilename();
            String substring = originalFilename.substring(originalFilename.lastIndexOf("."));
            if (file.getSize()>1024000) {
                throw  new RuntimeException("文件过大!");
            }else if (substring.equalsIgnoreCase(".gif")||
                    substring.equalsIgnoreCase(".jpg")||
                    substring.equalsIgnoreCase(".png")||
                    substring.equalsIgnoreCase(".jpeg")) {
                String new_file = UUID.randomUUID() + substring;
                File file2 = new File(path  + new_file);
                try {
                    file.transferTo(file2);
                    book.setImgurl(new_file);
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new RuntimeException("文件上传错误!!");
                }
            } else {
                throw new RuntimeException("文件格式错误!!");
            }
        }
        else {
            book.setImgurl(book.getImgurl());
        }
        if (book.getId()==null){
            bookService.save(book);
        }
        else {
            bookService.updateById(book);
        }
        return "redirect:/book/page";
    }

    @RequestMapping("updId")
    public String updId(Integer id,Model model){
        Book book = bookService.getById(id);
        model.addAttribute("bookId",book);
        return "update";
    }
}
