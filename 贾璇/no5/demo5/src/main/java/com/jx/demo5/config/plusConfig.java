package com.jx.demo5.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class plusConfig {
    @Bean
    public MybatisPlusInterceptor plusInterceptor(){
        MybatisPlusInterceptor mybatis = new MybatisPlusInterceptor();
        mybatis.addInnerInterceptor(new PaginationInnerInterceptor());
        return mybatis;
    }
}
