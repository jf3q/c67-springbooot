package com.jx.demo3.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Salarylevel)实体类
 *
 * @author makejava
 * @since 2023-12-29 10:56:20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Salarylevel implements Serializable {
    private static final long serialVersionUID = -42489308368607799L;

    private String level;

    private Object minsal;

    private Object maxsal;



}

