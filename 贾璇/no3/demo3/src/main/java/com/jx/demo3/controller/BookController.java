package com.jx.demo3.controller;


import com.github.pagehelper.PageInfo;
import com.jx.demo3.entity.Book;
import com.jx.demo3.service.BookService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("book")
public class BookController {

    @Autowired
    BookService bookService;

    @RequestMapping("/all")
    public String getAll(Model model){
        List<Book> bookList =  bookService.getAll();
        model.addAttribute("books",bookList);
        return "bookIndex";
    }

    @RequestMapping("/page")
    public String page(@RequestParam(defaultValue = "1") Integer num,Model model){
        PageInfo<Book> pageInfo = bookService.getPage(num);
        model.addAttribute("page",pageInfo);
        return "bookIndex";
    }

    @RequestMapping("/del")
    public String del(Integer[] ids){
        if (ids.length>0){
            bookService.del(ids);
        }
        return "redirect:/book/page";
    }

    @RequestMapping("/add")
    public String add(){
        return "add";
    }

    @Value("${web.upload-path}")
    private String path;
    @RequestMapping("/toAdd")
    public String upload(@RequestParam MultipartFile file, HttpServletRequest request,Book book){
        if (!file.isEmpty()){
            File file1 = new File(path);
            if (!file1.exists()){
                file1.mkdir();
            }
            String originalFilename = file.getOriginalFilename();
            String substring = originalFilename.substring(originalFilename.lastIndexOf("."));
            if (file.getSize()>1024000) {
                throw  new RuntimeException("文件过大!");
            }else if (substring.equalsIgnoreCase(".gif")||
                    substring.equalsIgnoreCase(".jpg")||
                    substring.equalsIgnoreCase(".png")||
                    substring.equalsIgnoreCase(".jpeg")) {
                String new_file = UUID.randomUUID() + substring;
                File file2 = new File(path  + new_file);
                try {
                    file.transferTo(file2);
                    book.setImgurl(new_file);
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new RuntimeException("文件上传错误!!");
                }
            } else {
                throw new RuntimeException("文件格式错误!!");
            }
        }
        else {
            book.setImgurl(book.getImgurl());
        }
        if (book.getId()==null){
            bookService.add(book);
        }
        else {
            bookService.update(book);
        }
        return "redirect:/book/page";
    }

    @RequestMapping("updId")
    public String updId(Integer id,Model model){
        Book book = bookService.updateId(id);
        model.addAttribute("bookId",book);
        return "update";
    }
}
