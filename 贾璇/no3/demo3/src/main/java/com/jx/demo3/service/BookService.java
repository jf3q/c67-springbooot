package com.jx.demo3.service;

import com.github.pagehelper.PageInfo;
import com.jx.demo3.entity.Book;

import java.util.List;

public interface BookService {
    List<Book> getAll();

    PageInfo<Book> getPage(Integer num);

    void del(Integer[] ids);

    void add(Book book);

    void update(Book book);

    Book updateId(Integer id);
}
