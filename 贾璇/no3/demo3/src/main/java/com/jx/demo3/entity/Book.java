package com.jx.demo3.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Book)实体类
 *
 * @author makejava
 * @since 2023-12-29 10:56:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Book implements Serializable {
    private static final long serialVersionUID = -78440841375610941L;

    private Integer id;

    private String name;

    private Double price;

    private String category;

    private Integer pnum;

    private String imgurl;

    private String description;

    private String author;

    private Integer sales;



}

