package com.jx.demo3.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Classinfo)实体类
 *
 * @author makejava
 * @since 2023-12-29 10:56:18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Classinfo implements Serializable {
    private static final long serialVersionUID = 163340623354410463L;

    private Integer classno;

    private String classname;




}

