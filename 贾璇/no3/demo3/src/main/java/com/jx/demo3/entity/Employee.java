package com.jx.demo3.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Employee)实体类
 *
 * @author makejava
 * @since 2023-12-29 10:56:19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Employee implements Serializable {
    private static final long serialVersionUID = -87797603495523383L;

    private Integer empno;

    private String ename;

    private String job;

    private Integer leaderid;

    private Integer salary;




}

