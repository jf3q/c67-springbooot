package com.jx.demo3.dao;

import com.jx.demo3.entity.Book;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * (Book)表数据库访问层
 *
 * @author makejava
 * @since 2023-12-29 10:56:09
 */
@Mapper
public interface BookDao {

    List<Book> queryAll();

    void del(Integer[] ids);

    public void addBook(Book book);   //添加一本书

    public void updateBook(Book book);  //修改一本书

    public Book bookById(int id);  //根据id号查找一本书
}

