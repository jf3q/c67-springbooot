package com.jx.demo3.service.lmpl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jx.demo3.dao.BookDao;
import com.jx.demo3.entity.Book;
import com.jx.demo3.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServicelmpl implements BookService {
    @Autowired
    BookDao bookDao;
    @Override
    public List<Book> getAll() {
        return bookDao.queryAll();
    }

    @Override
    public PageInfo<Book> getPage(Integer num) {
        PageHelper.startPage(num,3,"id desc");
        List<Book> bookList = bookDao.queryAll();
        return new PageInfo<>(bookList);
    }

    @Override
    public void del(Integer[] ids) {
        bookDao.del(ids);
    }

    @Override
    public void add(Book book) {
        bookDao.addBook(book);
    }

    @Override
    public void update(Book book) {
        bookDao.updateBook(book);
    }

    @Override
    public Book updateId(Integer id) {
        Book book = bookDao.bookById(id);
        return book;
    }
}
