package com.jx.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jx.entity.Result;
import com.jx.entity.Studentdb2;
import com.jx.service.StudentService;
import io.micrometer.common.util.StringUtils;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("student")
public class StudentController {

    @Resource
    StudentService studentService;

    @RequestMapping("stuList")
    public Result stuList(@RequestParam(defaultValue = "1") int num,
                          @RequestParam(defaultValue = "",required = false) String name){
        LambdaQueryWrapper<Studentdb2> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.like(StringUtils.isNotEmpty(name),Studentdb2::getName,name);
        IPage<Studentdb2> iPage = new Page<>(num,5);
        studentService.page(iPage,queryWrapper);
        return Result.success("",iPage);
    }

//    @RequestMapping("ins")
//    public Result ins(@RequestBody Studentdb2 student){
//        studentService.save(student);
//        return Result.success("",null);
//    }
//
//    @RequestMapping("upd")
//    public Result upd(@RequestBody Studentdb2 student){
//        studentService.updateById(student);
//        return Result.success("",null);
//    }

    @RequestMapping("insOrUpd")
    public Result insOrUpd(@RequestBody Studentdb2 student){
        studentService.saveOrUpdate(student);
        return Result.success("操作成功",null );
    }

    @DeleteMapping("del/{id}")
    public Result del(@PathVariable int id){
        studentService.removeById(id);
        return Result.success("删除成功",null);
    }

}
