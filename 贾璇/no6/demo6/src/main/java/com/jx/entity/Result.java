package com.jx.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
public class Result {
    private String code;
    private String message;
    private Object data ;

    public static Result success(String message,Object data){
        return new Result("2000",message,data);
    }

    public static Result error(String message){
        return new Result("5000",message,null);
    }

    public Result() {
    }

    public Result(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
