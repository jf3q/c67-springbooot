package com.jx.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName("studentdb2")
public class Studentdb2 {
    @TableId(type = IdType.AUTO)
    private int id ;
    private String name ;
    private String gender;
    private int age;
    private String address ;

    //字段只为展示
//    @TableField(exist = false)

}
