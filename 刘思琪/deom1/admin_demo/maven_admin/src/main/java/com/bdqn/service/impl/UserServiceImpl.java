package com.bdqn.service.impl;

import com.bdqn.mapper.UserMapper;
import com.bdqn.pojo.User;
import com.bdqn.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper mapper;


    @Override
    public User login(User user) {
        return mapper.login(user);
    }

    @Override
    public User selectById(String userId) {
        return mapper.selectById(userId);
    }
}
