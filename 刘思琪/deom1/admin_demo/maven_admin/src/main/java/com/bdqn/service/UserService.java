package com.bdqn.service;


import com.bdqn.pojo.User;

public interface UserService {

    User login(User user);


    User selectById(String userId);
}
