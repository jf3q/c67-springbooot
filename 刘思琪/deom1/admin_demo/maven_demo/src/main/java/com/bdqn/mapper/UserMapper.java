package com.bdqn.mapper;

import com.bdqn.pojo.User;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface UserMapper {

    User login(User user);

    User selectById(String userId);
}
