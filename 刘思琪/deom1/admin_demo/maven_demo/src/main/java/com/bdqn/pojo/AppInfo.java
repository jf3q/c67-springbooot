package com.bdqn.pojo;


public class AppInfo  {

  private Integer id;
  private String  softName;
  private String  aPKName;
  private Integer status;
  private Integer platId;
  private Integer classify1Id;
  private Integer classify2Id;
  private Integer classify3Id;
  private String  logoPath;
  private Integer versionId;
  private String  ROMName;
  private String  longuage;
  private String  desc;
  private String  createDate;
  private Integer createId;
  private String  modifyDate;
  private Integer modifyId;
  private String  size;
  private String platName;
  private String statusName;
  private String classifyName;
  private String devUserName;
  private String versionNo;
  private String downloads;

  public String getDownloads() {
    return downloads;
  }

  public void setDownloads(String downloads) {
    this.downloads = downloads;
  }

  public String getSize() {
    return size;
  }

  public void setSize(String size) {
    this.size = size;
  }

  public String getDevUserName() {
    return devUserName;
  }

  public String getVersionNo() {
    return versionNo;
  }

  public void setVersionNo(String versionNo) {
    this.versionNo = versionNo;
  }

  public void setDevUserName(String devUserName) {
    this.devUserName = devUserName;
  }

  public String getPlatName() {
    return platName;
  }

  public void setPlatName(String platName) {
    this.platName = platName;
  }

  public String getStatusName() {
    return statusName;
  }

  public void setStatusName(String statusName) {
    this.statusName = statusName;
  }

  public String getClassifyName() {
    return classifyName;
  }

  public void setClassifyName(String classifyName) {
    this.classifyName = classifyName;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public String getSoftName() {
    return softName;
  }

  public void setSoftName(String softName) {
    this.softName = softName;
  }


  public String getaPKName() {
    return aPKName;
  }

  public void setaPKName(String aPKName) {
    this.aPKName = aPKName;
  }


  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }


  public Integer getPlatId() {
    return platId;
  }

  public void setPlatId(Integer platId) {
    this.platId = platId;
  }


  public Integer getClassify1Id() {
    return classify1Id;
  }

  public void setClassify1Id(Integer classify1Id) {
    this.classify1Id = classify1Id;
  }


  public Integer getClassify2Id() {
    return classify2Id;
  }

  public void setClassify2Id(Integer classify2Id) {
    this.classify2Id = classify2Id;
  }


  public Integer getClassify3Id() {
    return classify3Id;
  }

  public void setClassify3Id(Integer classify3Id) {
    this.classify3Id = classify3Id;
  }


  public String getLogoPath() {
    return logoPath;
  }

  public void setLogoPath(String logoPath) {
    this.logoPath = logoPath;
  }


  public Integer getVersionId() {
    return versionId;
  }

  public void setVersionId(Integer versionId) {
    this.versionId = versionId;
  }


  public String getROMName() {
    return ROMName;
  }

  public void setROMName(String ROMName) {
    this.ROMName = ROMName;
  }


  public String getLonguage() {
    return longuage;
  }

  public void setLonguage(String longuage) {
    this.longuage = longuage;
  }


  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }


  public String getCreateDate() {
    return createDate;
  }

  public void setCreateDate(String createDate) {
    this.createDate = createDate;
  }


  public Integer getCreateId() {
    return createId;
  }

  public void setCreateId(Integer createId) {
    this.createId = createId;
  }

  public AppInfo(Integer id) {
    this.id = id;
  }

  public AppInfo(String aPKName) {
    this.aPKName = aPKName;
  }

  public AppInfo() {
  }

  public String getModifyDate() {
    return modifyDate;
  }

  public void setModifyDate(String modifyDate) {
    this.modifyDate = modifyDate;
  }


  public Integer getModifyId() {
    return modifyId;
  }

  public void setModifyId(Integer modifyId) {
    this.modifyId = modifyId;
  }

}
