package com.bdqn.pojo;


public class Classify {

  private Integer id;
  private String classifyName;
  private Integer parentId;
  private String createDate;
  private Integer createId;
  private String modifyDate;
  private Integer modifyId;


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public String getClassifyName() {
    return classifyName;
  }

  public void setClassifyName(String classifyName) {
    this.classifyName = classifyName;
  }


  public Integer getParentId() {
    return parentId;
  }

  public void setParentId(Integer parentId) {
    this.parentId = parentId;
  }


  public String getCreateDate() {
    return createDate;
  }

  public void setCreateDate(String createDate) {
    this.createDate = createDate;
  }


  public Integer getCreateId() {
    return createId;
  }

  public void setCreateId(Integer createId) {
    this.createId = createId;
  }


  public String getModifyDate() {
    return modifyDate;
  }

  public void setModifyDate(String modifyDate) {
    this.modifyDate = modifyDate;
  }


  public Integer getModifyId() {
    return modifyId;
  }

  public void setModifyId(Integer modifyId) {
    this.modifyId = modifyId;
  }

}
