package com.bdqn.utils;

@SuppressWarnings({"all"})
public class Result {
    
    private Boolean success;
    private String mes;
    private Object data;
    private String errorMes;


    public Result(Boolean success, String mes, Object data, String errorMes) {
        this.success = success;
        this.mes = mes;
        this.data = data;
        this.errorMes = errorMes;
    }
    public static Result ok(String mes){
        return new Result(true,mes,null,null);
    }
    public static Result ok(String mes , Object data){
        return new Result(true,mes,data,null);
    }

    public static Result fail(String errorMes){
        return new Result(false,null,null,errorMes);
    }
    public static Result fail(String errorMes , Object data){
        return new Result(false,null,data,errorMes);
    }


    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getErrorMes() {
        return errorMes;
    }

    public void setErrorMes(String errorMes) {
        this.errorMes = errorMes;
    }
}
