package com.bdqn.utils;

import com.bdqn.pojo.User;

@SuppressWarnings({"all"})
public class UserHolder {

    private UserHolder() {
    }

    private static ThreadLocal<User> local = new ThreadLocal<>();

    public static User get() {
        return local.get();
    }

    public static void set(User user) {
        local.set(user);
    }

    public static void remove() {
        local.remove();
    }

}
