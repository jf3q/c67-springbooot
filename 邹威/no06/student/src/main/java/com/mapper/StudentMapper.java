package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sike.entity.Student;

public interface StudentMapper extends BaseMapper<Student> {
}
