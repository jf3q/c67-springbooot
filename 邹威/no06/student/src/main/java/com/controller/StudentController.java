package com.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sike.entity.Result;
import com.sike.entity.Student;
import com.sike.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    IStudentService StudentService;



    @GetMapping
    public Result<?> findPage(@RequestParam(defaultValue = "1") Integer pageNum, @RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "",required = false) String search) {
        System.out.println("search:"+search);
        Page<Student> page = StudentService.page(new Page(pageNum, pageSize), Wrappers.<Student>lambdaQuery().like(Student::getStudentname,search));
        return Result.success(page);
    }


    @PostMapping
    public Result<?> save(@RequestBody Student student) {
        StudentService.save(student);
        return Result.success();
    }


    @PutMapping
    public Result<?> update(@RequestBody Student student) {
        StudentService.updateById(student);
        return Result.success();
    }

    @DeleteMapping("/{id}")
    public Result<?> delete(@PathVariable long id) {
        StudentService.removeById(id);
        return Result.success();
    }
}
