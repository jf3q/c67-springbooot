package com.sike.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sike.entity.Student;
import com.sike.mapper.StudentMapper;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService{
}
