package com.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Result<T> {
    private String code;//这里约定数字0代表成功，-1代表失败
    private String msg;//code对应的中文描述，即成功或失败
    private T data;//返回给前端的数据

    public Result(T data) {
        this.data = data;
    }

    public static Result success() {
        Result result = new Result<>();
        result.setCode("0");
        result.setMsg("成功");
        return result;
    }

    public static <T> Result<T> success(T data) {
        Result<T> result = new Result<>(data);
        result.setCode("0");
        result.setMsg("成功");
        return result;
    }

    public static Result error(String msg) {
        Result result = new Result();
        result.setCode("-1");
        result.setMsg(msg);
        return result;
    }
}
