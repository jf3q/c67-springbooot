package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;
import java.util.Date;
import java.io.Serializable;

/**
 * (MedicalAssay)实体类
 *
 * @author makejava
 * @since 2023-12-29 13:59:13
 */
public class MedicalAssay implements Serializable {
    private static final long serialVersionUID = -55169216748915424L;

    private Integer id;

    private String assayUser;

    private Long hospitalId;

    private Integer assayResult;

    private String phone;

    private String cardNum;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate assayTime;


    private Hospital hospital;


    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAssayUser() {
        return assayUser;
    }

    public void setAssayUser(String assayUser) {
        this.assayUser = assayUser;
    }

    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }

    public Integer getAssayResult() {
        return assayResult;
    }

    public void setAssayResult(Integer assayResult) {
        this.assayResult = assayResult;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public LocalDate  getAssayTime() {
        return assayTime;
    }

    public void setAssayTime(LocalDate  assayTime) {
        this.assayTime = assayTime;
    }

}

