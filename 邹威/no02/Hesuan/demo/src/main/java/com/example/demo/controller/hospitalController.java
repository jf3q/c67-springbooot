package com.example.demo.controller;

import com.example.demo.entity.Hospital;
import com.example.demo.entity.MedicalAssay;
import com.example.demo.service.HospitalService;
import com.example.demo.service.MedicalAssayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class hospitalController {

    @Autowired
    HospitalService hospitalService;

    @Autowired
    MedicalAssayService medicalAssayService;

    @GetMapping("/list")
    public String findAll(Model model,@RequestParam(value = "nameId",defaultValue = "0")Long nameId){
        List<MedicalAssay> list = medicalAssayService.getlist(nameId);
        model.addAttribute("list",list);
        List<Hospital> gethosptialName = hospitalService.findAll();
        model.addAttribute("getHosptialName",gethosptialName);
        model.addAttribute("nameId",nameId);
        return "/index";
    }

    @GetMapping("/SelectAdd")
    public String selectfind(Model model){
        List<Hospital> gethosptialName = hospitalService.findAll();
        model.addAttribute("getHosptialName",gethosptialName);
        return "/add";
    }

    @RequestMapping("/add")
    public  String addFind(MedicalAssay medicalAssay){
        medicalAssayService.getAdd(medicalAssay);
        return "redirect:/list";
    }

    @RequestMapping("/home")
    public String update(MedicalAssay medicalAssay){
        medicalAssayService.updateId(medicalAssay);
        return "redirect:/list";
    }

}
