package com.example.demo.service;

import com.example.demo.dao.MedicalAssayDao;
import com.example.demo.entity.MedicalAssay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicalAssayService {

    @Autowired
    MedicalAssayDao medicalAssayDao;

    public List<MedicalAssay> getlist(Long nameId){
        return medicalAssayDao.queryAllBy(nameId);
    }

    public void getAdd(MedicalAssay medicalAssay){
         medicalAssayDao.insert(medicalAssay);
    }

    public void updateId(MedicalAssay medicalAssay) {
        medicalAssayDao.update(medicalAssay);
    }
}
