package com.jf3q.common.utils;

import java.util.HashMap;
import java.util.Map;

public class SessionUtils {
    static  Map map = new HashMap();

    //把token存起来
    public static void setToken(String token,Object user){
        map.put(token,user);
    }

    //验证我的token对不对
    public static Boolean validateToken(String token){
        Object o = map.get(token);
        if(o == null){
            return false;
        }

        //校验是否过期
        String[] strings = token.split("-");
        Long createTime = Long.valueOf(strings[4]);
        if (System.currentTimeMillis()-createTime>2*3600*1000) {
            //过期
            return false;
        }

        return true;
    }


    //移除token
    public static void removeToken(String token){
        map.remove(token);
    }
}
