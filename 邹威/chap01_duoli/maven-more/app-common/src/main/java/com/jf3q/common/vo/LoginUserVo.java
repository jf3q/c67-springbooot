package com.jf3q.common.vo;

import lombok.Data;

@Data
public class LoginUserVo {
    private String token;
    private Object user;
}
