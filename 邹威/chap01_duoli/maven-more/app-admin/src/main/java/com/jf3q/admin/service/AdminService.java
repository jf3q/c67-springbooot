package com.jf3q.admin.service;

import com.jf3q.common.dao.BackendUserDao;
import com.jf3q.common.dto.LoginUserDto;
import com.jf3q.common.entity.BackendUser;
import com.jf3q.common.utils.SessionUtils;
import com.jf3q.common.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class AdminService {

    @Autowired
    BackendUserDao backendUserDao;


    public LoginUserVo login(LoginUserDto userDto) {
        LoginUserVo userVo= new LoginUserVo();
        BackendUser backendUser = new BackendUser();
        backendUser.setUsercode(userDto.getAccount());
        backendUser.setUserpassword(userDto.getPassword());
        List<BackendUser> list = backendUserDao.queryAllBy(backendUser);
        if (list ==null ||list.size()==0) {
            throw new RuntimeException("账号密码错误");
        }
        userVo.setUser(list.get(0));
        //token  uuid-usercode-id-userType-创建时间
        StringBuffer stringBuffer= new StringBuffer();
        stringBuffer.append(UUID.randomUUID().toString().replace("-",""));
        stringBuffer.append("-"+list.get(0).getUsercode());
        stringBuffer.append("-"+list.get(0).getId());
        stringBuffer.append("-admin");
        stringBuffer.append("-"+System.currentTimeMillis());
        //将token存到缓存里
        SessionUtils.setToken(stringBuffer.toString(),list.get(0));

        userVo.setToken(stringBuffer.toString());
        return userVo;

    }
}
