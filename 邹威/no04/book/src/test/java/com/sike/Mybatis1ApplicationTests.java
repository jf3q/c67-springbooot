package com.sike;

import com.sike.entity.Employee;
import com.sike.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Mybatis1ApplicationTests {
    @Autowired
    private EmployeeService employeeService;
    @Test
    void contextLoads() {
        Employee employee=employeeService.findEmployeeById(1);
        System.out.println(employee);
    }

}
