package com.sike.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sike.entity.Book;
import com.sike.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;

@Controller
public class BookController {
    @Autowired
    private BookService bookService;

    @GetMapping("/books") //查找所有
    public ModelAndView findAllBooks(){
        List<Book> books=bookService.findAllBooks();
        ModelAndView mv=new ModelAndView();
        mv.addObject("books",books);
        mv.setViewName("booklist");
        return mv;
    }

    @GetMapping("/book/{id}") 
    public ModelAndView findStudentById(@PathVariable("id") int id){
        Book book=bookService.findBookById(id);
        ModelAndView mv=new ModelAndView();
        mv.addObject("book",book);
        mv.setViewName("bookdetail");
        return mv;
    }

    @PostMapping("/addBook")
    public ModelAndView addBook(Book book){
        bookService.addBook(book);
        ModelAndView mv=new ModelAndView();
        mv.setViewName("redirect:/books");
        return mv;
    }

    @GetMapping("/addBook")
    public String addBook(){ 
        return "addbook";
    }

    @GetMapping("/deleteBook/{id}") 
    public ModelAndView deleteBook(@PathVariable("id") int id){
        bookService.deleteBook(id);
        ModelAndView mv=new ModelAndView();
        mv.setViewName("redirect:/books");
        return mv;
    }


    @PostMapping("/updateBook")
    public ModelAndView UpdateBook(Book book){
        bookService.updateBook(book);
        ModelAndView mv=new ModelAndView();
        mv.setViewName("redirect:/books");
        return mv;
    }


    @GetMapping("/searchBooks")
    public ModelAndView searchBooks(Book book){ 
        List<Book> books=bookService.searchBooks(book);
        ModelAndView mv=new ModelAndView();
        mv.addObject("books",books);
        mv.setViewName("booklist");
        return mv;
    }

    @GetMapping("/booksPage")
    public ModelAndView booksPage(int pageNum
                                 int pageSize){ 
        PageHelper.startPage(pageNum,pageSize,"id asc"); 
        List<Book> books=bookService.findAllBooks();
        PageInfo<Book> page=new PageInfo<>(books);
        ModelAndView mv=new ModelAndView();
        mv.addObject("page",page);
        mv.setViewName("bookslistPage");
        return mv;
    }

    @GetMapping("/deleteBooks") 
    public ModelAndView deleteBook(int[] id){
        bookService.deleteBooks(id);
        ModelAndView mv=new ModelAndView();
        mv.setViewName("redirect:/books");
        return mv;
    }

}

