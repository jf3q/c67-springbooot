package com.sike.service;

import com.sike.entity.Book;

import java.util.List;

public interface BookService {
    public List<Book> findAllBooks();

    public Book findBookById(int id);

    public void addBook(Book book);

    public void updateBook(Book book);

    public void deleteBook(int id);


    public List<Book> searchBooks(Book book);

}
