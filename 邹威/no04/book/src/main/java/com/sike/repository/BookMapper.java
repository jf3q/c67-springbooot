package com.sike.repository;

import com.sike.entity.Book;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookMapper {

    public List<Book> findAllBooks();  

    public Book findBookById(int id);  

    public void addBook(Book book);   


    public void updateBook(Book book);  


    public void deleteBook(int id); 

    public List<Book> searchBooks(Book book);


}
