package com.chap02.datashow.controller;

import com.chap02.datashow.entity.AppInfo;
import com.chap02.datashow.entity.AppVersion;
import com.chap02.datashow.service.AppInfoService;
import com.chap02.datashow.service.AppVersionService;
import com.chap02.datashow.vo.ResultVo;
import com.chap02.datashow.entity.AppInfo;
import com.chap02.datashow.vo.ResultVo;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class AppVersionController {

    @Resource
    AppVersionService appVersionService;

    @Resource
    AppInfoService appInfoService;

    @PostMapping("/getList")
    public ResultVo getList(Long id){
       List<AppVersion>  list= appVersionService.VersionId(id);
       return ResultVo.success("",list);
    }

    @PostMapping("/addAppVersion")
    public ResultVo addAppVersion(AppVersion appVersion, MultipartFile apkFile, HttpServletRequest request){
        String token = request.getHeader("token");
        String[] split = token.split("-");
        String userId = split[3];


        if (apkFile !=null && !apkFile.isEmpty()) {
            String originalFilename = apkFile.getOriginalFilename();
            String exetsion = FilenameUtils.getExtension(originalFilename);
            if (apkFile.getSize() > 1024 * 1024 * 500) {
                return ResultVo.error("文件超过了50000MB");
            } else if (exetsion.equalsIgnoreCase("apk")
            ) {
                String realPath = request.getServletContext().getRealPath("/upload/apk");
                File file = new File(realPath);
                if (!file.exists()) {
                    file.mkdirs();
                }
                String fileName = UUID.randomUUID().toString().replace("-", "");
                File saveFile = new File(realPath + "/" + fileName + "." + exetsion);
                try {
                    apkFile.transferTo(saveFile);
                    appVersion.setDownloadlink("/upload/apk/" + fileName + "." + exetsion);
                } catch (IOException e) {
                    e.printStackTrace();
                    return ResultVo.error("文件上传失败");
                }
            } else {
                return ResultVo.error("文件格式不正确");
            }
        }
        if (appVersion.getId()==null){
            appVersion.setCreatedby(Long.valueOf(userId));
            appVersion.setPublishstatus(3L);
            appVersion.setCreationdate( new Date());
        }else{
            appVersion.setCreatedby(Long.valueOf(userId));
            appVersion.setCreationdate( new Date());
        }

        appVersionService.add(appVersion);

        AppInfo appInfo = new AppInfo();
        appInfo.setId(appVersion.getAppid());
        appInfo.setVersionid(appVersion.getId());
        appInfoService.update(appInfo);
        return ResultVo.success("",null);
    }

}
