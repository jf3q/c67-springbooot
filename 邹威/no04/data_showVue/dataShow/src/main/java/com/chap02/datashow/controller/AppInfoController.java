package com.chap02.datashow.controller;


import com.chap02.datashow.dto.AppInfoDto;
import com.chap02.datashow.dto.AppInfoReviewDto;
import com.chap02.datashow.entity.AppInfo;
import com.chap02.datashow.service.AppInfoService;
import com.chap02.datashow.vo.ResultVo;
import com.github.pagehelper.PageInfo;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@RestController
public class AppInfoController {

    @Resource
    AppInfoService appInfoService;


    @PostMapping("/Page")
    public ResultVo getPage(@RequestBody AppInfoDto appInfoDto, @RequestParam(defaultValue = "1") Integer pageNum, HttpServletRequest request) {

        //这一段表示管理员和开发者分界
        String token = request.getHeader("token");
        String[] split = token.split("-");
        String userType = split[4];
        String devId = split[3];
        if (userType.equals("Dev")){
            appInfoDto.setDevId(Long.valueOf(devId));
        }else{
            appInfoDto.setStatus(1L);
        }
        PageInfo<AppInfo> page = appInfoService.getPage(appInfoDto, pageNum);
        return ResultVo.success("", page);
    }


    //新增和修改
    @PostMapping("/saveOrUpdate")
    public ResultVo save(AppInfo appInfo, MultipartFile file,HttpServletRequest request){

        String token  = request.getHeader("token");
        String[] split = token.split("-");
        String userId = split[3];

        if (file !=null && !file.isEmpty()){
            //获取名称
            String originalFileName = file.getOriginalFilename();
            //获取后缀名
            String extension = FilenameUtils.getExtension(originalFileName);

            if (file.getSize()>1024*1024){
                return ResultVo.error("文件大小操作1m");
            }else if (extension.equalsIgnoreCase("jpg")||
                    extension.equalsIgnoreCase("png") ||
                    extension.equalsIgnoreCase("jpeg") ||
                    extension.equalsIgnoreCase("gif")
            ){
                //上传到哪里
                String realPath = request.getServletContext().getRealPath("/upload/logo");
                //创建目录
                File savePath = new File(realPath);
                if (!savePath.exists()) {
                    savePath.mkdirs();
                }

                //上传文件
                //重命名
                String fileName = UUID.randomUUID().toString().replace("-", "");
                File saveFile = new File(realPath + "/" + fileName + "." + extension);
                try{
                    file.transferTo(saveFile);

                    appInfo.setLogopicpath("/upload/logo"+"/"+fileName+"."+extension);
                } catch (IOException e) {
                    e.printStackTrace();
                    return ResultVo.error("文件上传失败");
                }
            }else{
                return ResultVo.error("文件格式不对，必须是图片格式");
            }

        }
        if (appInfo.getId()==null){
            appInfo.setDevid(Long.valueOf(userId));
            appInfo.setCreatedby(Long.valueOf(userId));
            appInfo.setCreationdate(new Date());
            appInfo.setStatus(1L);
            appInfoService.save(appInfo);
        }else{
            appInfo.setStatus(1L);
            appInfo.setModifyby(Long.valueOf(userId));
            appInfo.setModifydate(new Date());
            appInfoService.update(appInfo);
        }
        return ResultVo.success("",null);
    }



    /*判断是否唯一性*/
    @PostMapping("/getApkNameOne")
    public ResultVo getApkNameOne(@RequestBody AppInfo appInfo){
        Boolean flag = appInfoService.appInfoOne(appInfo);
        if (flag==true){
            return ResultVo.success("",null);
        }else{
            return ResultVo.error("");
        }
    }


    /*修改-通过id查找数据*/
    @PostMapping("/getId")
    public ResultVo getId(Long id){
        AppInfo appInfo = null;
        try {
            appInfo = appInfoService.getId(id);
            return ResultVo.success("",appInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
    }

    /*删除*/
    @PostMapping("/del")
    public ResultVo del(Long id, HttpServletRequest request){
        try {
              appInfoService.del(id,request);
            return ResultVo.success("",null);
        } catch (Exception e) {
            e.printStackTrace();
            return  ResultVo.error("删除失败");
        }
    }

    /*上下架*/
    @PutMapping("/sale")
    public ResultVo sale(Long id){
        try {
            appInfoService.sale(id);
            return ResultVo.success("",null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
    }

    /*通过id查找添加版本数据*/
    @PostMapping("/appInfoId")
    public ResultVo appInfoId(Long id){
        try {
          AppInfo appInfo =   appInfoService.appInfoId(id);
            return ResultVo.success("",appInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
    }


    //审核
    @PostMapping("/review")
    public ResultVo review(@RequestBody AppInfoReviewDto appInfoReviewDto) {
        AppInfo appInfo = new AppInfo();
        System.out.println(appInfoReviewDto);
        appInfo.setId(appInfoReviewDto.getId());
        appInfo.setStatus(appInfoReviewDto.getStatus());
        appInfoService.update(appInfo);

        return ResultVo.success("", null);
    }
}
