package com.chap02.datashow.controller;


import com.chap02.datashow.service.CategoryService;
import com.chap02.datashow.vo.ResultVo;
import com.chap02.datashow.vo.TreeVo;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CategoryDaoController {

    @Resource
    CategoryService categoryService;

    @GetMapping("/getTree")
    public ResultVo getTree(){
        TreeVo vo = categoryService.getTree();
        return  ResultVo.success("",vo);
    }
}
