package com.chap02.datashow.controller;


import com.chap02.datashow.dto.LoginDto;
import com.chap02.datashow.service.LoginUserService;
import com.chap02.datashow.vo.LoginUserVo;
import com.chap02.datashow.vo.ResultVo;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LoginController {

    @Resource
    LoginUserService loginUserService;


    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginDto loginDto){
        LoginUserVo vo = null;
        try {
            vo = loginUserService.login(loginDto);
            return ResultVo.success("登录成功",vo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
    }




}
