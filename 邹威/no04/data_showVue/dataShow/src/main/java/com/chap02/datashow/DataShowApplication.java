package com.chap02.datashow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataShowApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataShowApplication.class, args);
    }

}
