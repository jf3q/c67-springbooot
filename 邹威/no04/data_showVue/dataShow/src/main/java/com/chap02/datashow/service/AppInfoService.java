package com.chap02.datashow.service;

import com.chap02.datashow.dao.AppInfoDao;
import com.chap02.datashow.dao.AppVersionDao;
import com.chap02.datashow.dto.AppInfoDto;
import com.chap02.datashow.entity.AppInfo;
import com.chap02.datashow.entity.AppVersion;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.Date;
import java.util.List;

@Service
public class AppInfoService {

    @Resource
    AppInfoDao appInfoDao;

    @Resource
    AppVersionDao appVersionDao;

    public PageInfo<AppInfo> getPage(AppInfoDto appInfoDto, Integer pageNum) {
        AppInfo appInfo = new AppInfo();

        BeanUtils.copyProperties(appInfoDto, appInfo);

        PageHelper.startPage(pageNum,10,"id desc");
        List<AppInfo> list = appInfoDao.queryAllBy(appInfo);
        PageInfo<AppInfo> page = new PageInfo(list);
        return page;
    }


    public void save(AppInfo appInfo) {
        appInfoDao.insert(appInfo);
    }

    public void update(AppInfo appInfo) {
        appInfoDao.update(appInfo);
    }

    //判断账号唯一性
    public Boolean appInfoOne(AppInfo appInfo) {
        if (appInfo.getId() !=null){
            AppInfo app = appInfoDao.queryById(appInfo.getId());
            if (app.getApkname().equals(appInfo.getApkname())){
                return true;
            }
        }
        Integer  count = appInfoDao.appInfoOne(appInfo.getApkname());
        if (count>0){
            return  false;
        }
        return  true;
    }

    public AppInfo getId(Long id) {
        return appInfoDao.queryById(id);
    }


    /*删除业务层*/
    public void del(Long id, HttpServletRequest request) {
        //删除手游版本apk文件
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(id);
        List<AppVersion> list = appVersionDao.queryAllBy(appVersion);
        for (AppVersion version : list) {
            if (StringUtils.hasText(version.getDownloadlink())){  //查找是否下载链接是否为空
                String realPath = request.getServletContext().getRealPath(version.getDownloadlink());
                File file = new File(realPath);
                if (file.exists()){
                    file.delete();
                }
            }
        }
        //删除版本
        appVersionDao.deleteById(id);
        //删除logo
        AppInfo appInfo = appInfoDao.queryById(id);
        if (StringUtils.hasText(appInfo.getLogopicpath())){
            String realPath = request.getServletContext().getRealPath(appInfo.getLogopicpath());
            File file  = new File(realPath);
            if (file.exists()){
                file.delete();
            }
        }
        //删除基本信息
        appInfoDao.deleteById(id);
    }

    public void sale(Long id) {
        AppInfo appInfo = appInfoDao.queryById(id);
        if (appInfo.getStatus()==5L){
            appInfo.setStatus(4L);
            appInfo.setOnsaledate(new Date());
        }else if (appInfo.getStatus()==4L || appInfo.getStatus()==2L){
            appInfo.setStatus(5L);
            appInfo.setOffsaledate(new Date());
        }
        appInfoDao.update(appInfo);
    }

    public AppInfo appInfoId(Long id) {
        return appInfoDao.queryById(id);
    }
}
