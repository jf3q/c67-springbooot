package com.chap02.datashow.vo;

import lombok.Data;

@Data
public class LoginUserVo {
    //从登录到首页，我需要显示登录进来的信息
    //传到首页，我还需要token令牌，进行显示
    private Object user;

    private String token;
}
