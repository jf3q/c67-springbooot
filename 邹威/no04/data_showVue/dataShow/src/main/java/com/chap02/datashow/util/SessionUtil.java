package com.chap02.datashow.util;

import java.util.HashMap;
import java.util.Map;

public class SessionUtil {

    static Map map =new HashMap();


    //存储token值
    public static void setToken(String token, Object user){
        map.put(token,user);
    }

    //判断token
    public static Object get(String token){
        return map.get(token);
    }

    //删除token
    public static void removeToken(String token){
        map.remove(token);
    }
}
