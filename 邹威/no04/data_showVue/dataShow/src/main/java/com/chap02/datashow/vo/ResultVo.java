package com.chap02.datashow.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResultVo {
//全局变量
    private String code;//状态码2000成功 5000:失败

    private String message;//消息提醒

    private Object data;//携带数据

    public static ResultVo success(String message,Object data){
        return new ResultVo("2000",message,data);
    }

    public static ResultVo error(String message){
        return new ResultVo("5000",message,null);
    }
}
