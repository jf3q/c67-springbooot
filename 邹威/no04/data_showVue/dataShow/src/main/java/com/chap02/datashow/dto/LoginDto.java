package com.chap02.datashow.dto;

import lombok.Data;

@Data
public class LoginDto {

    //账号
    private String account;

    //密码
    private String password;

    //类型(1、管理员  2、开发者)
    private Integer userType;
}
