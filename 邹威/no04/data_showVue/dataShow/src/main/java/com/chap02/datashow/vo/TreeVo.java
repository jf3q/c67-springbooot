package com.chap02.datashow.vo;

import java.util.List;

public class TreeVo {

    private  Long id;

    /**
     * 父级节点id
     */
    private Long parentid;

    /**
     * 分类名称
     */
    private String categoryname;


    private List<TreeVo> children;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public List<TreeVo> getChildren() {
        return children;
    }

    public void setChildren(List<TreeVo> children) {
        this.children = children;
    }
}
