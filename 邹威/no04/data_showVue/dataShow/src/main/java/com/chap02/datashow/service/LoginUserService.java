package com.chap02.datashow.service;


import com.chap02.datashow.dao.BackendUserDao;
import com.chap02.datashow.dao.DevUserDao;
import com.chap02.datashow.dto.LoginDto;
import com.chap02.datashow.entity.BackendUser;
import com.chap02.datashow.entity.DevUser;
import com.chap02.datashow.util.SessionUtil;
import com.chap02.datashow.vo.LoginUserVo;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class LoginUserService {

    //开发者
    @Resource
    DevUserDao devUserDao;

    //管理员
    @Resource
    BackendUserDao backendUserDao;

    //登录启用方法   返回对象:vo:个人信息和token值      参数:前端传值三个对象
    public LoginUserVo login(LoginDto loginDto){
        LoginUserVo loginUserVo = new LoginUserVo();
        //判断用户类型是:是管理员还是开发者
        if (loginDto.getUserType() ==1){
            //如管理员
            /*管理员对象*/
            BackendUser backendUser =new BackendUser();
            /*把前端账号、密码给到后端管理员判断*/
            backendUser.setUsercode(loginDto.getAccount());
            backendUser.setUserpassword(loginDto.getPassword());
            List<BackendUser> list = backendUserDao.queryAllBy(backendUser);
            if (list.size()==0){
                throw new RuntimeException("账号密码错误");
            }
            //把list集合第一个对象返回到User对象中。
            loginUserVo.setUser(list.get(0));

            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(UUID.randomUUID().toString().replace("-",""));
            stringBuffer.append("-"+list.get(0).getUsercode());
            stringBuffer.append("-"+list.get(0).getUserpassword());
            stringBuffer.append("-"+list.get(0).getId());
            stringBuffer.append("-admin");
            stringBuffer.append("-"+System.currentTimeMillis());

            //赋值Util里面token值，进入首页有令牌
            SessionUtil.setToken(stringBuffer.toString(),list.get(0));

            //获取的stringBuffer赋值给vo返回对象:loginUserVo
            loginUserVo.setToken(stringBuffer.toString());
            return loginUserVo;
        }else if (loginDto.getUserType()==2){
            DevUser devUser = new DevUser();
            devUser.setDevcode(loginDto.getAccount());
            devUser.setDevpassword(loginDto.getPassword());
            List<DevUser> devUsers = devUserDao.queryAllBy(devUser);
            if (devUsers.size()==0){
                throw new RuntimeException("账号、密码、用户类型错误");
            }
            //注意:loginUserVo:从登录页面传数据到首页查找的
            loginUserVo.setUser(devUsers.get(0));
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(UUID.randomUUID().toString().replace("-",""));
            stringBuffer.append("-"+devUsers.get(0).getDevcode());
            stringBuffer.append("-"+devUsers.get(0).getDevpassword());
            stringBuffer.append("-"+devUsers.get(0).getId());
            stringBuffer.append("-Dev");
            stringBuffer.append("-"+System.currentTimeMillis());
            //赋值Util里面token值，进入首页有令牌
            SessionUtil.setToken(stringBuffer.toString(),devUsers.get(0));

            //获取的stringBuffer赋值给vo返回对象:loginUserVo
            loginUserVo.setToken(stringBuffer.toString());
            return loginUserVo;
        }else{
            throw new RuntimeException("不存在用户类型");
        }
    }


}
