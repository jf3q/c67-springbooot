package com.chap02.datashow.service;


import com.chap02.datashow.dao.AppVersionDao;
import com.chap02.datashow.entity.AppVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class AppVersionService {

    @Autowired
    AppVersionDao appVersionDao;


    public List<AppVersion> VersionId(Long id) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(id);
        return appVersionDao.queryAllBy(appVersion);
    }

    public void add(AppVersion appVersion) {
        if (appVersion.getId()==null){
            appVersionDao.insert(appVersion);
        }else {
            appVersionDao.update(appVersion);

        }
    }
}
