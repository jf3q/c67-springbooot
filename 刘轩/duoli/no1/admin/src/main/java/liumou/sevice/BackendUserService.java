package liumou.sevice;

import liumou.dao.BackendUserDao;
import liumou.dto.LoginDto;
import liumou.entity.BackendUser;
import liumou.vo.LoginUserVo;
import liumou.vo.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class BackendUserService {
    @Autowired
    BackendUserDao backendUserDao;
    public LoginUserVo slect(LoginDto loginDto){
        BackendUser devUser=new BackendUser().setUsercode(loginDto.getAccount()).setUserpassword(loginDto.getPassword());
        List<BackendUser> devUsers = backendUserDao.queryAllByLimit(devUser);
        if(devUsers.size()==0){
            throw new RuntimeException("账号或者密码错误");
        }
        LoginUserVo vo=new LoginUserVo();
        vo.setUserCode(devUsers.get(0).getUsercode());
        vo.setUserName(devUsers.get(0).getUserpassword());
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String token=  uuid+"-"+System.currentTimeMillis()+"-"+devUsers.get(0).getId()+"-"+ devUser.getUsercode();
        System.out.println("loginToken ----"+token);
        vo.setToken(token);
        SessionUtils.put(token,devUsers.get(0));
        return vo;
    }
}
