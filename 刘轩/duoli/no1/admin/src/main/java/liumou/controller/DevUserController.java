package liumou.controller;

import liumou.dto.LoginDto;
import liumou.sevice.BackendUserService;
import liumou.sevice.DevUserService;
import liumou.vo.LoginUserVo;
import liumou.vo.ResutVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class DevUserController {
    @Resource
    DevUserService devUserService;
    @Autowired
    BackendUserService backendUserService;
    @PostMapping("/login")
    public ResutVo login(@RequestBody LoginDto loginDto){
        if (loginDto.getUserType()==1){
            LoginUserVo slect = backendUserService.slect(loginDto);
            return ResutVo.sccess("",slect);
        }else if(loginDto.getUserType()==2){
            try{
                LoginUserVo slect = devUserService. slect(loginDto);
                return ResutVo.sccess("登录成功",slect);
            }catch (Exception e){
                e.printStackTrace();
                return ResutVo.error(e.getMessage());
            }
        }else {
            return ResutVo.error("没有正确输入");
        }
    }
}
