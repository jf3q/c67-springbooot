package liumou.sevice;

import liumou.dao.DevUserDao;
import liumou.dto.LoginDto;
import liumou.entity.DevUser;
import liumou.vo.LoginUserVo;
import liumou.vo.SessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DevUserService {
    @Autowired
    DevUserDao devUserDao;
    public LoginUserVo slect(LoginDto loginDto){
        DevUser devUser=new DevUser();
        devUser.setDevcode(loginDto.getAccount());
        devUser.setDevpassword(loginDto.getPassword());
        List<DevUser> devUsers = devUserDao.queryAllByLimit(devUser);
        if(devUsers.size()==0){
            throw new RuntimeException("账号密码错误");
        }
        LoginUserVo vo=new LoginUserVo();
        vo.setUserCode(devUsers.get(0).getDevcode());
        vo.setUserName(devUsers.get(0).getDevname());
        String uuid = UUID.randomUUID().toString().replace("-", "");
        String token = uuid + "-" + System.currentTimeMillis() + "-" + devUsers.get(0).getId() + "-" + devUser.getDevcode();
        System.out.println("loginToken ----"+token);
        vo.setToken(token);
        SessionUtils.put(token,devUsers.get(0));
        return vo;
    }
}
