package liumou.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AppVersionDto {
    private Long id;
    private String status;
}
