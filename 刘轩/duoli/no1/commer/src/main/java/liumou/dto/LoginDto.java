package liumou.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginDto {   //接收数据
    private  String account;
    private String password;
    private Integer userType;
}
