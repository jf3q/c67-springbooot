package liumou.vo;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Intersel implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        if(token==null){
            response.setStatus(401);
            return false;
        }else if(SessionUtils.get(token)==null){
            response.setStatus(403);
            return false;
        }
        String[] split = token.split("-");
        String s = split[1];
        if(System.currentTimeMillis()-Long.parseLong(s)>6000*2*20){
            response.setStatus(403);
            SessionUtils.get(token);
            return false;
        }
        return true;
    }
}
