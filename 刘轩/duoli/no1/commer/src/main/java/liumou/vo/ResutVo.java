package liumou.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class ResutVo {   //状态
    private  String code;
    private  String massages;
    private  Object data;
    public static  ResutVo sccess(String massages,Object data){
        return new  ResutVo("2000",massages,data);
    }
    public static  ResutVo error(String massages){
        return new  ResutVo("5000",massages,null);
    }
}
