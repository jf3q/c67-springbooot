package liumou.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * (DevUser)实体类
 *
 * @author makejava
 * @since 2023-12-08 16:43:48
 */
@AllArgsConstructor
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class DevUser implements Serializable {
    private static final long serialVersionUID = 716963424215112908L;
    /**
     * 主键id
     */
    private Long id;
    /**
     * 开发者帐号
     */
    private String devcode;
    /**
     * 开发者名称
     */
    private String devname;
    /**
     * 开发者密码
     */
    private String devpassword;
    /**
     * 开发者电子邮箱
     */
    private String devemail;
    /**
     * 开发者简介
     */
    private String devinfo;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    private Long createdby;
    /**
     * 创建时间
     */
    private Date creationdate;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    private Long modifyby;
    /**
     * 最新更新时间
     */
    private Date modifydate;
}

