package liumou.vo;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LoginUserVo {   //传出
    private  String userCode;
    private String userName;
    private String token;
}
