package com.meng.vue3demo01.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "student")
public class Student {

    @TableId(value = "id",type = IdType.AUTO)
    private int id;
    private String studentname;
    private String gender;
    private int age;
    private String address;
}


