package com.meng.vue3demo01.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResultVo<T> {
    private Integer code;
    private String mess;
    private T data;

    //success
    public static<T> ResultVo success(String mess,T data){
        return new ResultVo(200,mess,data);
    }
    //error
    public static<T> ResultVo error(String mess){
        return new ResultVo(500,mess,null);
    }
}