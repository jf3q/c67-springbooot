package com.meng.vue3demo01.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.meng.vue3demo01.entity.Student;
import com.meng.vue3demo01.service.StudentService;
import com.meng.vue3demo01.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    StudentService studentService;

    //分页带条件查询
    @GetMapping("/page")
    public ResultVo page(String studentname, @RequestParam(defaultValue = "1") Integer pageNum ){

        LambdaQueryWrapper<Student> wrapper =new LambdaQueryWrapper<>();
        if (studentname!=null && studentname!=""){
            wrapper.like(Student::getStudentname,studentname);
        }


        Page<Student> studentPage= studentService.page(new Page<>(pageNum,3),wrapper);
        return ResultVo.success("",studentPage);

    }

    //增 或者修改

    @PostMapping("/savaOrUpd")
    public ResultVo addOrUpdate(@RequestBody Student student){
        studentService.saveOrUpdate(student);
        return ResultVo.success("操作成功",null);
    }
    //删除
    @DeleteMapping("/del/{id}")
    public  ResultVo del(@PathVariable Integer id){
        studentService.removeById(id);
        return ResultVo.success("操作成功",null);
    }


}