package com.meng.vue3demo01.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meng.vue3demo01.entity.Student;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StudentMapper extends BaseMapper<Student> {
}
