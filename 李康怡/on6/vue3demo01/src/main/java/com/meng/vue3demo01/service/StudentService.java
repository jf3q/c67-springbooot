package com.meng.vue3demo01.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meng.vue3demo01.entity.Student;


public interface StudentService extends IService<Student> {
}
