package com.meng.vue3demo01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Vue3demo01Application {

    public static void main(String[] args) {
        SpringApplication.run(Vue3demo01Application.class, args);
    }

}
