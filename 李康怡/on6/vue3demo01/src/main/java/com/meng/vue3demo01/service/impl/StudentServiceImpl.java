package com.meng.vue3demo01.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.meng.vue3demo01.entity.Student;
import com.meng.vue3demo01.mapper.StudentMapper;
import com.meng.vue3demo01.service.StudentService;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements StudentService {
}
