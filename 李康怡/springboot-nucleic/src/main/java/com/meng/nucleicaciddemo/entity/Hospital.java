package com.meng.nucleicaciddemo.entity;

import java.io.Serializable;

/**
 * (Hospital)实体类
 *
 * @author makejava
 * @since 2023-12-27 19:34:10
 */
public class Hospital implements Serializable {
    private static final long serialVersionUID = 395126976431018598L;
    /**
     * 检测机构编号
     */
    private Long id;
    /**
     * 检测机构名称
     */
    private String name;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

