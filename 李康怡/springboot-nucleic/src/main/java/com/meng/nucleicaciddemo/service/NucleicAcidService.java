package com.meng.nucleicaciddemo.service;

import com.meng.nucleicaciddemo.dao.MedicalAssayDao;
import com.meng.nucleicaciddemo.entity.MedicalAssay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NucleicAcidService {

    @Autowired
    MedicalAssayDao medicalAssayDao;

    public List<MedicalAssay> getList(Long id) {
        return medicalAssayDao.getList(id);
    }

    public Integer add(MedicalAssay medicalAssay) {
        if (medicalAssayDao.insert(medicalAssay)>0) {
            return 1;
        }
        return 0;
    }

    public void update(Long id) {
        MedicalAssay medicalAssay = new MedicalAssay();
        medicalAssay.setId(id);
        medicalAssay.setAssayResult(1);
        medicalAssayDao.update(medicalAssay);
    }
}
