package com.meng.nucleicaciddemo.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * (MedicalAssay)实体类
 *
 * @author makejava
 * @since 2023-12-27 19:34:11
 */
public class MedicalAssay implements Serializable {
    private static final long serialVersionUID = 694510005319568822L;
    /**
     * 检测编号
     */
    private Long id;
    /**
     * 被检测人
     */
    private String assayUser;
    private String hospital_name;
    /**
     * 检测机构编号
     */
    private Long hospitalId;
    /**
     * 检测结果  0:检测中 1:确诊
     */
    private Integer assayResult;
    /**
     * 被检测人电话号
     */
    private String phone;
    /**
     * 被检测人身份证号
     */
    private String cardNum;
    /**
     * 检测日期  yyyy-MM-dd
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date assayTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAssayUser() {
        return assayUser;
    }

    public void setAssayUser(String assayUser) {
        this.assayUser = assayUser;
    }

    public Long getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Long hospitalId) {
        this.hospitalId = hospitalId;
    }

    public Integer getAssayResult() {
        return assayResult;
    }

    public void setAssayResult(Integer assayResult) {
        this.assayResult = assayResult;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public Date getAssayTime() {
        return assayTime;
    }

    public void setAssayTime(Date assayTime) {
        this.assayTime = assayTime;
    }

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }
}

