package com.meng.springbootbook.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.meng.springbootbook.dao.BookDao;
import com.meng.springbootbook.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    @Autowired
    BookDao bookDao;

    public PageInfo<Book> getPage(Integer pageNum) {
        PageHelper.startPage(pageNum,3,"id asc");
        List<Book> books = bookDao.queryAllBy(new Book());
        return new PageInfo<>(books);
    }

    public Integer addOrUpdateBook(Book book) {
        if(book.getId() != null){
            if(bookDao.update(book)>0){
                return 1;
            }else {
                return 0;
            }
        }else{
            if(bookDao.insert(book)>0){
                return 1;
            }else {
                return 0;
            }
        }
    }

    public Integer delBook(Integer id) {
        if (bookDao.deleteById(id)>0) {
            return 1;
        }
        return 0;
    }

    public Book getById(Integer id) {
        Book book = bookDao.queryById(id);
        return book;
    }

    public void delsBox(Integer[] ids) {
        bookDao.delsBox(ids);
    }
}
