package com.meng.springbootbook.controller;

import com.github.pagehelper.PageInfo;
import com.meng.springbootbook.entity.Book;
import com.meng.springbootbook.service.BookService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Controller
@RequestMapping("/book")
public class BookInfoController {

    @Autowired
    BookService bookService;


    @Value("${web.upload-path}")
    private String uploadPath;

    @GetMapping("/list")
    public String getPage(@RequestParam(defaultValue = "1") Integer pageNum, Model model) {
        PageInfo<Book> page = bookService.getPage(pageNum);
        model.addAttribute("page", page);
        return "index";
    }

    @GetMapping("/toAdd")
    public String toAdd(){
        return "add";
    }

    @GetMapping
    public String getById(Integer id,Model model){
        Book bookInfo = bookService.getById(id);
        model.addAttribute("bookInfo",bookInfo);
        return "update";
    }

    @PostMapping("/addOrUpdateBook")
    public String addOrUpdateBook(Book book, MultipartFile file, HttpServletRequest request){

        boolean flag = true;

        if (file!=null){
            if (!file.isEmpty()){
                String originalFilename = file.getOriginalFilename();
                String substring = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
                if (file.getSize()<500*1024) {
                    if(
                            substring.equalsIgnoreCase("png")||
                                    substring.equalsIgnoreCase("jpg")||
                                    substring.equalsIgnoreCase("jpeg")||
                                    substring.equalsIgnoreCase("gif")
                    ){

                        File file1 = new File(uploadPath);
                        if (!file1.exists()) {
                            file1.mkdirs();
                        }
                        String str = UUID.randomUUID().toString().replace("-","")+"."+substring;
                        try {
                            file.transferTo(new File(uploadPath+str));
                            book.setImgurl(str);
                        } catch (IOException e) {
                            flag = false;
                            request.setAttribute("error","文件上传失败！");
                        }

                    }else{
                        flag = false;
                        request.setAttribute("error","文件格式不正确！");
                    }
                }else{
                    flag = false;
                    request.setAttribute("error","文件大小超过500k");
                }
            }
        }

        if(!flag){
            return "add";
        }

        if(bookService.addOrUpdateBook(book)>0){
            return "redirect:/book/list";
        }

        return "add";
    }

    @DeleteMapping("/del/{id}")
    @ResponseBody
    public Map delBook(@PathVariable Integer id){
        Map map = new HashMap();
        if (bookService.delBook(id)>0) {
            map.put("flag",true);
        }else{
            map.put("flag",false);
        }
        return map;
    }

    @PostMapping("/delBox")
    public String delBox(Integer[] ids){
        bookService.delsBox(ids);
        return "redirect:/book/list";
    }


}
