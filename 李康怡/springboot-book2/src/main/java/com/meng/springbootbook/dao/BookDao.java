package com.meng.springbootbook.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.meng.springbootbook.entity.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Book)表数据库访问层
 *
 * @author makejava
 * @since 2023-12-29 14:15:09
 */
@Mapper
public interface BookDao extends BaseMapper<Book> {

}

