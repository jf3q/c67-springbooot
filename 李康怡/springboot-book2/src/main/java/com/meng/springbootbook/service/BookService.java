package com.meng.springbootbook.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.meng.springbootbook.entity.Book;

public interface BookService extends IService<Book> {
}
