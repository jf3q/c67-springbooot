package com.jzy.app.service.impl;

import com.jzy.app.dto.LoginDto;
import com.jzy.app.entity.BackendUser;
import com.jzy.app.mapper.BackendUserDao;
import com.jzy.app.service.BackendUserService;
import com.jzy.app.utils.StringBufferUtils;
import com.jzy.app.utils.sessionUtils;
import com.jzy.app.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class BackendUserServiceImpl implements BackendUserService {

    @Autowired
    BackendUserDao backendUserDao;
    @Override
    public LoginVo loginBackend(LoginDto loginDto) {
        BackendUser backendUser = new BackendUser();
        backendUser.setUsercode(loginDto.getAccount());
        backendUser.setUserpassword(loginDto.getPasswrod());
        List<BackendUser> backendUsers = backendUserDao.queryAllBy(backendUser);
        if (backendUsers.size()==0){
            throw  new RuntimeException("账号或密码错误!");
        }
        BackendUser backendUser1 = backendUsers.get(0);
        LoginVo loginVo = new LoginVo();
        loginVo.setUserType(loginDto.getUserType());
        loginVo.setUserName(backendUser1.getUsername());
        loginVo.setUseracount(backendUser1.getUsercode());
        loginVo.setToken(StringBufferUtils.joint(loginVo.getUseracount(), loginVo.getUserType()));
        sessionUtils.put(loginVo.getToken(),loginVo);
        return loginVo;
    }
}
