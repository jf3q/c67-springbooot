package com.jzy.app.controller;



import com.jzy.app.service.AppCategoryService;
import com.jzy.app.vo.AppCategoryVO;
import com.jzy.app.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class AppCategoryController {

    @Autowired
    AppCategoryService appCategoryService;

    @RequestMapping("/tree")
    public ResultVo tree(){
        AppCategoryVO appCategoryVO = appCategoryService.appCategoryTree();

        return ResultVo.success("",appCategoryVO);
    }



}
