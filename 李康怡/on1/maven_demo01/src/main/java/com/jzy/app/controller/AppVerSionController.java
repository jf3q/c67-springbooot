package com.jzy.app.controller;


import com.jzy.app.entity.AppInfo;
import com.jzy.app.entity.AppVersion;
import com.jzy.app.service.AppInfoService;
import com.jzy.app.service.AppVersionService;
import com.jzy.app.utils.sessionUtils;
import com.jzy.app.vo.LoginVo;
import com.jzy.app.vo.ResultVo;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/appVerSion")
public class AppVerSionController {

    @Autowired
    AppVersionService appVersionService;

    @Autowired
    AppInfoService appInfoService;

    @RequestMapping("/getAppVerSionList/{id}")
    public ResultVo getAppVerSionList(@PathVariable Long id){
        List<AppVersion> appVersionList = appVersionService.getAppVersionList(id);
        return ResultVo.success("",appVersionList);
    }



    @RequestMapping("/saveAppVerSion")
    public ResultVo saveAppVerSion(AppVersion appVersion, MultipartFile file, HttpServletRequest request){
        String realPath = request.getServletContext().getRealPath("/img/apk");
        File newFile = new File(realPath);
        if (!newFile.exists()){
            newFile.mkdirs();
        }
        if (file!=null){
            if (!file.isEmpty()){
                if (file.getSize()<=500*1024*1024){
                    String originalFilename = file.getOriginalFilename();
                    String extension = FilenameUtils.getExtension(originalFilename);
                    if (  extension.equalsIgnoreCase("apk")){
                        String fileName =  UUID.randomUUID()+"."+extension;
                        File saveFile = new File(realPath+File.separator+fileName);
                        try {
                            file.transferTo(saveFile);
                            appVersion.setDownloadlink("/img/apk/"+fileName);
                        } catch (IOException e) {
                            e.printStackTrace();
                            return ResultVo.error("apk上传失败");
                        }
                    }
                }else {
                    return ResultVo.error("apk文件大于了500MB");
                }

            }else {
                return ResultVo.error("apk上传失败");
            }
        }else {
            return ResultVo.error("请选择apk文件");
        }
        String header = request.getHeader("token");
        LoginVo loginVo  = (LoginVo) sessionUtils.get(header);
        appVersion.setCreatedby(loginVo.getId());
        appVersion.setCreationdate(new Date());
        appVersion.setPublishstatus(3L);
        int i = appVersionService.insertAppVersion(appVersion);
        if (i>0){
            appInfoService.saveUpdate(new AppInfo().setId(appVersion.getAppid()).setVersionid(appVersion.getId()));
            return ResultVo.success("添加成功!",null);
        }
        return ResultVo.error("添加失败!");
    }
}
