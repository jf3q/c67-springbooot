package com.jzy.app.vo;


import lombok.Data;

@Data
public class ResultVo {

    private String code;

    private String message;

    private Object data;


    public ResultVo() {
    }

    public ResultVo(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ResultVo(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public static ResultVo success(String message, Object data){
        return new ResultVo("2000",message,data);
    }

    public static ResultVo error(String message){
        return new ResultVo("5000",message);
    }
}
