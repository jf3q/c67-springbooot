package com.example.test01;
import org.springframework.boot.SpringApplication;
import com.example.test01.controller.HelloController;
import org.springframework.boot.ConfigurableBootstrapContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class Test01Application {
    public static void main(String[] args) {
//        ConfigurableApplicationContext context = SpringApplication.run(Test01Application.class,args);
//        String[] beans = context.getBeanDefinitionNames();
//        System.out.println(context.getBean(HelloController.class));
//        System.out.println(beans.length);
//        for (String bean : beans){
//            System.out.println(bean);
//        }
        SpringApplication.run(Test01Application.class, args);
    }
}