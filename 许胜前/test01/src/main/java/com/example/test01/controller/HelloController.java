package com.example.test01.controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Value;
@RestController
public class HelloController {
    @RequestMapping("/hello")
    public String hello(){
        return "<h1>Hello World!!!<h1>";
    }

    @Value("${db.url}")
    private String url;

    @Value("${db.user}")
    private String user;

    @Value("${db.password}")
    private String password;

    @RequestMapping("/test")
    public String test(){
        String str="<h1>当前环境连接的数据库URL: "+ url +"</h1>";
        str+="<h1>数据库用户: "+ user +"</h1>";
        str+="<h1>数据库密码: "+ password +"</h1>";
        return str;
    }
}