package com.example.test01;
import com.example.test01.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

@SpringBootTest
class Test01ApplicationTests {
    @Test
    void contextLoads() {}

//    @Value("${username}")
//    private String username;

//    @Test
//    void testUsername(){
//        System.out.println("username："+ username);
//    }

//    @Value("${user.username}")
//    private String username;

//    @Test
//    void testUsername(){
//        System.out.println("username："+ username);
//    }

//    @Value("${city[2]}")
//    private String city;

//    @Test
//    void testUsername(){
//        System.out.println("city："+ city);
//    }

//    @Value("${books[1].name}")
//    private String bookName;

//    @Test
//    void testUsername(){
//        System.out.println("bookName："+ bookName);
//    }

//    @Autowired
//    private Environment environment;

//    @Test
//    void testEnvironment(){
//        System.out.println("username: "+ environment.getProperty("username"));
//        System.out.println("password: "+ environment.getProperty("password"));
//    }

    @Autowired
    private User user;

    @Test
    void testUser(){
        System.out.println(user);
    }
}