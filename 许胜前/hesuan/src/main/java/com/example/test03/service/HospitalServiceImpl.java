package com.example.test03.service;
import java.util.List;
import com.example.test03.entity.Hospital;
import org.springframework.stereotype.Service;
import com.example.test03.mapper.HospitalMapper;
import org.springframework.beans.factory.annotation.Autowired;
@Service
public class HospitalServiceImpl implements  HospitalService{
    @Autowired
    private HospitalMapper hospitalMapper;
    @Override
    public List<Hospital> getAll() {
        return hospitalMapper.getAll();
    }
}