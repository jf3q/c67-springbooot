package com.example.test03.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Date;

public class MedicalAssay {
    private Integer id;

    private String assayUser;

    private Integer hospitalId;

    private Integer assayResult;

    private String phone;

    private String cardNum;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date assayTime;

    private Hospital hospital;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAssayUser() {
        return assayUser;
    }

    public void setAssayUser(String assayUser) {
        this.assayUser = assayUser;
    }

    public Integer getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Integer hospitalId) {
        this.hospitalId = hospitalId;
    }

    public Integer getAssayResult() {
        return assayResult;
    }

    public void setAssayResult(Integer assayResult) {
        this.assayResult = assayResult;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public Date getAssayTime() {
        return assayTime;
    }

    public void setAssayTime(Date assayTime) {
        this.assayTime = assayTime;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }
}