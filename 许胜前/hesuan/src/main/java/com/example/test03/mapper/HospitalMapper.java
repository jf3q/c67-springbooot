package com.example.test03.mapper;
import java.util.List;
import com.example.test03.entity.Hospital;
public interface HospitalMapper {
    List<Hospital> getAll();
}