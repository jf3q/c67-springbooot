package com.example.test03.mapper;
import java.util.List;
import jakarta.websocket.server.PathParam;
import com.example.test03.entity.MedicalAssay;
public interface MedicalAssayMapper {
    int addMedicalAssay(MedicalAssay medicalAssay);

    int updateStatus(@PathParam("assayResult") Integer assayResult,@PathParam("id") Integer id);

    List<MedicalAssay> selectAll(Integer hospitalId);
}