package com.example.test03.service;
import java.util.List;
import com.example.test03.entity.MedicalAssay;
public interface MedicalAssayService {
    int addMedicalAssay(MedicalAssay medicalAssay);

    int updateStatus(Integer assayResult,Integer id);

    List<MedicalAssay> selectAll(Integer hospitalId);
}