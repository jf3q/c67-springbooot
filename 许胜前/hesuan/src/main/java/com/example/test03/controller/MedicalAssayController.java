package com.example.test03.controller;
import java.util.List;
import java.io.IOException;
import com.example.test03.entity.*;
import com.example.test03.mapper.*;
import org.springframework.ui.Model;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
@RequestMapping("/hospital")
public class MedicalAssayController {
    @Autowired
    private HospitalMapper hospitalMapper;

    @Autowired
    private MedicalAssayMapper medicalAssayMapper;

    @RequestMapping("/index")
    public String index(Integer hospitalId, Model model) {
        List<MedicalAssay> medicalAssayList = null;
        if (hospitalId != null) {
            medicalAssayList = medicalAssayMapper.selectAll(hospitalId);
            model.addAttribute("hid", hospitalId);
        }else{
            medicalAssayList = medicalAssayMapper.selectAll(null);
        }
        List<Hospital> hospitalList = hospitalMapper.getAll();
        model.addAttribute("medicalAssay", medicalAssayList);
        model.addAttribute("list", hospitalList);
        return "index";
    }

    @RequestMapping("/toAdd")
    public String toAdd(Model model){
        List<Hospital> hospitalList = hospitalMapper.getAll();
        model.addAttribute("list", hospitalList);
        return "add";
    }

    @RequestMapping("/doAdd")
    public String doAdd(MedicalAssay medicalAssay){
        int i = medicalAssayMapper.addMedicalAssay(medicalAssay);
        if( i > 0){
            return "redirect:/hospital/index";
        }
        return "error";
    }

    @RequestMapping("/update")
    public void update(Integer assayResult, Integer id, HttpServletResponse response){
        try {
            int i = medicalAssayMapper.updateStatus(assayResult, id);
            response.getWriter().println(i);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}