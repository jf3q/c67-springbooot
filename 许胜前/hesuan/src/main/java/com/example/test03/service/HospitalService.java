package com.example.test03.service;
import java.util.List;
import com.example.test03.entity.Hospital;
public interface HospitalService {
    List<Hospital> getAll();
}