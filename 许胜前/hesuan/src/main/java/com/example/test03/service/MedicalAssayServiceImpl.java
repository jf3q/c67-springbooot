package com.example.test03.service;
import java.util.List;
import org.springframework.stereotype.Service;
import com.example.test03.entity.MedicalAssay;
import com.example.test03.mapper.MedicalAssayMapper;
import org.springframework.beans.factory.annotation.Autowired;
@Service
public class MedicalAssayServiceImpl implements MedicalAssayService{
    @Autowired
    private MedicalAssayMapper medicalAssayMapper;

    @Override
    public int addMedicalAssay(MedicalAssay medicalAssay) {
        return medicalAssayMapper.addMedicalAssay(medicalAssay);
    }

    @Override
    public int updateStatus(Integer assayResult, Integer id) {
        return medicalAssayMapper.updateStatus(assayResult, id);
    }

    @Override
    public List<MedicalAssay> selectAll(Integer hospitalId) {
        return medicalAssayMapper.selectAll(hospitalId);
    }
}