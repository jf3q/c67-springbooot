package com.example.test03;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan
@SpringBootApplication
public class Test03Application {
    public static void main(String[] args) {
        SpringApplication.run(Test03Application.class, args);
    }
}