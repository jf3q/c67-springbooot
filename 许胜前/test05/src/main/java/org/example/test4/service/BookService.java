package org.example.test4.service;
import java.util.List;
import org.example.test4.entity.Book;
public interface BookService {
    List<Book> getAllBook(String name, String category, String author);

    Book getBookById(Integer id);

    int insertBook(Book book);

    int updateBook(Book book);

    int deleteBook(Integer id);

    int deleteBooks(Integer[] ids);
}