package org.example.test4;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan(value = "org.example.test4.mapper")
@SpringBootApplication
public class Test4Application {
    public static void main(String[] args) {
        SpringApplication.run(Test4Application.class, args);
    }
}