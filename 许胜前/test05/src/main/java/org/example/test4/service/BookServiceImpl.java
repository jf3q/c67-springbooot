package org.example.test4.service;
import java.util.List;
import org.example.test4.entity.Book;
import org.example.test4.mapper.BookMapper;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class BookServiceImpl implements BookService{
    @Autowired
    private BookMapper bookMapper;

    @Override
    public List<Book> getAllBook(String name, String category, String author) {
        return bookMapper.getAllBook(name, category, author);
    }

    @Override
    public Book getBookById(Integer id) {
        return bookMapper.getBookById(id);
    }

    @Override
    public int insertBook(Book book) {
        return bookMapper.insertBook(book);
    }

    @Override
    public int updateBook(Book book) {
        return bookMapper.updateBook(book);
    }

    @Override
    public int deleteBook(Integer id) {
        return bookMapper.deleteBook(id);
    }

    @Override
    public int deleteBooks(Integer[] ids) {
        return bookMapper.deleteBooks(ids);
    }
}