package org.example.test4.mapper;
import java.util.List;
import org.example.test4.entity.Book;
import jakarta.websocket.server.PathParam;
public interface BookMapper {
    List<Book> getAllBook(@PathParam("name")String name, @PathParam("category")String category,
                          @PathParam("author")String author);

    Book getBookById(Integer id);

    int insertBook(Book book);

    int updateBook(Book book);

    int deleteBook(Integer id);

    int deleteBooks(Integer[] ids);
}