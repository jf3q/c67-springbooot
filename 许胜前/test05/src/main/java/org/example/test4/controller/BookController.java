package org.example.test4.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import jakarta.servlet.http.HttpServletRequest;
import org.example.test4.entity.Book;
import org.example.test4.mapper.BookMapper;
import org.example.test4.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {
    @Autowired
    private BookService bookService;

    public void scfile(MultipartFile mFile,HttpServletRequest request){
        if(mFile == null || mFile.isEmpty()){
            System.out.println("出错了!");
        }
        String filename = mFile.getOriginalFilename();
        String realPath = request.getServletContext().getRealPath("/static/img/");
        File file = new File(realPath);
        if(!file.exists()){
            file.mkdir();
        }
        File file1 = new File(file,filename);
        try {
            mFile.transferTo(file1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/getAll")
    public String getAll(@RequestParam(value = "start",defaultValue = "1")int start,
                         @RequestParam(value = "size",defaultValue = "3")int size,
            String name, String category, String author, Model model){
        PageHelper.startPage(start,size,"id desc");
        List<Book> bookList = bookService.getAllBook(name, category, author);
        PageInfo<Book> page = new PageInfo<>(bookList);
        model.addAttribute("page",page);
        model.addAttribute("book",bookList);
        model.addAttribute("name",name);
        model.addAttribute("category",category);
        model.addAttribute("author",author);
        return "index";
    }

    @RequestMapping()
    public String getBookById(Integer id, Model model){
        Book book = bookService.getBookById(id);
        model.addAttribute("book",book);
        return "update";
    }

    @RequestMapping("/toAdd")
    public String toAdd(){
        return "insert";
    }

    @RequestMapping("/doAdd")
    public String doAdd(@RequestParam("img") MultipartFile mFile, Book book, HttpServletRequest request){
        scfile(mFile, request);
        book.setImgurl(mFile.getOriginalFilename());
        int i = bookService.insertBook(book);
        if(i > 0){
            return "redirect:/book/getAll";
        }else{
            return "error";
        }
    }

    @RequestMapping("/toUpdate")
    public String toUpdate(Integer id,Model model){
        Book book = bookService.getBookById(id);
        model.addAttribute("book",book);
        return "update";
    }

    @RequestMapping("/doUpdate")
    public String doUpdate(@RequestParam("img") MultipartFile mFile, Book book, HttpServletRequest request){
        scfile(mFile,request);
        book.setImgurl(mFile.getOriginalFilename());
        int i = bookService.updateBook(book);
        if(i > 0){
            return "redirect:/book/getAll";
        }else{
            return "error";
        }
    }

    @RequestMapping("/delete")
    public String delete(Integer id){
        int i = bookService.deleteBook(id);
        if(i > 0){
            return "redirect:/book/getAll";
        }else{
            return "error";
        }
    }
}