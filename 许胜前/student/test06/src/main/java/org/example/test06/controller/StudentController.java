package org.example.test06.controller;
import java.util.List;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.example.test06.entity.Result;
import org.example.test06.entity.Student;
import org.example.test06.service.StudentService;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @RequestMapping("/select")
    public Result select(@RequestParam(value = "pageNum",defaultValue = "1")int pageNum,
                         @RequestParam(value = "size",defaultValue = "3")int size,
            String studentName, Integer id){
        PageHelper.startPage(pageNum,size,"id desc");
        List<Student> studentList = studentService.selectStudent(studentName, id);
        PageInfo<Student> page = new PageInfo<>(studentList);
        return Result.success(page);
    }

    @RequestMapping
    public Result addAndEdit(@RequestBody Student student){
        if(student.getId() != null){
            return Result.success(studentService.updateStudent(student));
        }else{
            return Result.success(studentService.insertStudent(student));
        }
    }

    @RequestMapping("/delete/{id}")
    public Result del(@PathVariable("id") Integer id){
        return Result.success(studentService.deleteStudent(id));
    }
}