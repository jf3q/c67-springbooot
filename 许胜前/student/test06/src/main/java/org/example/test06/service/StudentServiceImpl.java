package org.example.test06.service;
import java.util.List;
import org.example.test06.entity.Student;
import org.springframework.stereotype.Service;
import org.example.test06.mapper.StudentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
@Service
public class StudentServiceImpl implements StudentService{
    @Autowired
    private StudentMapper studentMapper;

    @Override
    @Transactional
    public int insertStudent(Student student) {
        return studentMapper.insertStudent(student);
    }

    @Override
    @Transactional
    public int updateStudent(Student student) {
        return studentMapper.updateStudent(student);
    }

    @Override
    @Transactional
    public int deleteStudent(Integer id) {
        return studentMapper.deleteStudent(id);
    }

    @Override
    public List<Student> selectStudent(String studentName, Integer id) {
        return studentMapper.selectStudent(studentName, id);
    }
}