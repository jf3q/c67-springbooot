package org.example.test06;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * org.mybatis.spring.annotation.MapperScan;
 * 作用：在启动类上配置，配置的是持久层接口的包的路径。编译后会把路径下所有的接口都生成动态代理类
 * 包下面的所有接口都会实现代理类，这就意味着在之前的写法中，除了impl类被注入了Spring容器中之外，
 * 还注入了一个Service的实现类，于是乎在Controller层调用Service时，并没有真正获取被注入的impl类，
 * 所以无法调用mapper接口。这种情况只有运行时才会报错。
 */
@MapperScan(value = "org.example.test06.mapper")
@SpringBootApplication
public class Test06Application {
    public static void main(String[] args) {
        SpringApplication.run(Test06Application.class, args);
    }
}