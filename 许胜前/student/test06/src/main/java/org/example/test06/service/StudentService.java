package org.example.test06.service;
import java.util.List;
import org.example.test06.entity.Student;
public interface StudentService {
    int insertStudent(Student student);

    int updateStudent(Student student);

    int deleteStudent(Integer id);

    List<Student> selectStudent(String studentName, Integer id);
}