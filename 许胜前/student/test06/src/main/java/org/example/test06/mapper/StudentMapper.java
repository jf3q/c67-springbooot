package org.example.test06.mapper;
import java.util.List;
import org.example.test06.entity.Student;
import jakarta.websocket.server.PathParam;
public interface StudentMapper {
    int insertStudent(Student student);

    int updateStudent(Student student);

    int deleteStudent(Integer id);

    List<Student> selectStudent(@PathParam("studentName") String studentName,@PathParam("id") Integer id);
}