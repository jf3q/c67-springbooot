package org.example.test02.exception;
import org.example.test02.entity.ResponseData;
import org.springframework.web.bind.annotation.*;
@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(NullPointerException.class)
    public ResponseData exception(NullPointerException ex){
        return new ResponseData("500","空指针异常",false,null);
    }

    @ExceptionHandler(IndexOutOfBoundsException.class)
    public ResponseData exception(IndexOutOfBoundsException ex){
        return new ResponseData("500","数组越界异常",false,null);
    }

    @ExceptionHandler(Exception.class)
    public ResponseData exception(Exception ex){
        return new ResponseData("500",ex.getMessage(),false,null);
    }
}