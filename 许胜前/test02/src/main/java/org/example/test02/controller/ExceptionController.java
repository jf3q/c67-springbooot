package org.example.test02.controller;
import java.util.Map;
import org.example.test02.entity.*;
import org.example.test02.dao.UserDao;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("/exception")
public class ExceptionController {
    @RequestMapping("/test1")
    public ResponseData test1(){
        Integer[] arr = {1,2,3};
        arr = null;
        System.out.println(arr);
        Map<Integer,User> map = UserDao.findAllUsers();
        return new ResponseData("200",null,true,map);
    }

    @RequestMapping("/test2")
    public ResponseData test2(){
        Integer[] arr = {1,2,3};
        System.out.println(arr[3]);
        Map<Integer,User> map = UserDao.findAllUsers();
        return new ResponseData("200",null,true,map);
    }

    @RequestMapping("/test3")
    public ResponseData test3(){
        Integer i = 10/0;
        Map<Integer,User> map = UserDao.findAllUsers();
        return new ResponseData("200",null,true,map);
    }
}