package org.example.test02.entity;
public class ResponseData<T> {
    /**
     * 响应码状态
     */
    private String code;
    /**
     * 用于封装异常信息,正常一般返回null
     */
    private String errorMsg;
    /**
     * 表示执行成功还是失败
     */
    private Boolean result;
    /**
     * 封装返回数据
     */
    private T data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ResponseData() {}

    public ResponseData(String code, String errorMsg, Boolean result, T data) {
        this.code = code;
        this.errorMsg = errorMsg;
        this.result = result;
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "code='" + code + '\'' +
                ", errorMsg='" + errorMsg + '\'' +
                ", result=" + result +
                ", data=" + data +
                '}';
    }
}