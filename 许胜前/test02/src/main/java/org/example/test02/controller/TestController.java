package org.example.test02.controller;
import org.springframework.web.bind.annotation.*;
@CrossOrigin
@RestController
public class TestController {
    @RequestMapping("/getMsg")
    public String getMsg(){
        return "SELECT SUCCESS";
    }

    @RequestMapping("/delMsg")
    public String delMsg(){
        return "DELETE SUCCESS";
    }

    @RequestMapping("/test")
    public String test(){
        Integer i = 1/0;
        return "FALSE";
    }
}