package org.example.test02.config;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.context.annotation.Configuration;
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry){
                //指定可以跨域的路径
        registry.addMapping("/**")
                //允许的origin域名
                .allowedOrigins("*")
                //允许的origin域名
                .allowedOriginPatterns("*")
                //允许的请求方法
                .allowedMethods("GET","HEAD","POST","PUT","DELETE","OPTIONS")
                //是否允许发送Cookie
                .allowCredentials(true)
                //从预检请求得到相应的最大时间,默认为30min
                .maxAge(3600)
                //允许的请求头
                .allowedHeaders("*");
    }
}