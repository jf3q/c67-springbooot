package org.example.test02.controller;
import java.util.Map;
import org.example.test02.entity.*;
import org.example.test02.dao.UserDao;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("/user")
public class UserController {
//    @PostMapping
//    public String addUser(@RequestBody User user){
//        System.out.println("新增用户信息: "+ user);
//        return "新增用户成功!";
//    }

//    @DeleteMapping("/{id}")
//    public String deleteUser(@PathVariable Integer id){
//        System.out.println("删除用户id: "+ id);
//        return "删除用户成功!";
//    }

//    @PutMapping
//    public String updateUser(@RequestBody User user){
//        System.out.println("修改后的用户信息: "+ user);
//        return "修改用户成功!";
//    }

//    @GetMapping("/{id}")
//    public String getUser(@PathVariable Integer id){
//        System.out.println("查找用户信息: "+ id);
//        return "查找用户id成功!";
//    }

//    @GetMapping
//    public String getUsers(){
//        System.out.println("查找所有的用户信息");
//        return "查找所有用户成功!";
//    }

    @GetMapping("/{id}")
    public ResponseData getUser(@PathVariable Integer id){
        User user = UserDao.findUserById(id);
        return new ResponseData("200",null,true,user);
    }

    @GetMapping
    public ResponseData getAllUsers(){
        Map<Integer,User> userMap = UserDao.findAllUsers();
        return new ResponseData("200",null,true,userMap);
    }

    @PostMapping
    public ResponseData addUser(@RequestBody User user){
        UserDao.addUser(user);
        return new ResponseData("200",null,true,null);
    }

    @PutMapping
    public ResponseData updateUser(@RequestBody User user){
        UserDao.updateUser(user);
        return new ResponseData("200",null,true,null);
    }

    @DeleteMapping("/{id}")
    public ResponseData deleteUser(@PathVariable Integer id){
        UserDao.deleteUser(id);
        return new ResponseData("200",null,true,null);
    }
}