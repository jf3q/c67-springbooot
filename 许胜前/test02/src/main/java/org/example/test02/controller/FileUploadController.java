package org.example.test02.controller;
import java.io.*;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
@RestController
public class FileUploadController {
    @RequestMapping("/upload")
    public String upload(@RequestParam("uploadFiles")MultipartFile[] files, HttpServletRequest request){
        if(files == null || files.length == 0){
            return "NULL FILE";
        }
        String realPath = request.getServletContext().getRealPath("/uploadFile");
        System.out.println("realPath: "+realPath);
        File dir = new File(realPath);
        if(!dir.exists()){
            dir.mkdir();
        }
        for (MultipartFile file : files){
            String filename = file.getOriginalFilename();
            File target = new File(dir,filename);
            try {
                file.transferTo(target);
                System.out.println(target.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "SUCCESS";
    }
}