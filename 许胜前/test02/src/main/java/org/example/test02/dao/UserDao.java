package org.example.test02.dao;
import java.util.*;
import org.example.test02.entity.User;
public class UserDao {
    private static Map<Integer,User> users = new HashMap<>();

    static {
        users.put(1,new User(1,"张三","123456","13512345678"));
        users.put(2,new User(1,"李四","098765","151887654321"));
    }

    public static Map<Integer,User> findAllUsers(){
        return users;
    }

    public static User findUserById(Integer id){
        return users.get(id);
    }

    public static void addUser(User user){
        users.put(user.getId(),user);
    }

    public static void updateUser(User user){
        users.put(user.getId(),user);
    }

    public static void deleteUser(Integer id){
        users.remove(id);
    }
}