package org.example.test02.config;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.context.annotation.Configuration;
@Configuration
public class MyWebConfig extends WebMvcConfigurationSupport {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:classpath:/META-INF/resources/","classpath:/resources/",
                        "classpath:/static/","classpath:/public/","file:C:/Users/pc/Pictures/图片/");
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/add").setViewName("add");
        registry.addViewController("/register").setViewName("register");
    }
}