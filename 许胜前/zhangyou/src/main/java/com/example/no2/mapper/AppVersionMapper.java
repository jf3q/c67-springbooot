package com.example.no2.mapper;
import java.util.List;
import com.example.no2.entity.AppVersion;
import org.apache.ibatis.annotations.Param;
/**
 * (AppVersion)表数据库访问层
 */
public interface AppVersionMapper {
    /**
     * 通过ID查询单条数据
     */
    AppVersion queryById(Long id);
    /**
     * 查询指定行数据
     */
    List<AppVersion> queryAll(AppVersion appVersion);
    /**
     * 统计总行数
     */
    long count(AppVersion appVersion);
    /**
     * 新增数据
     */
    int insert(AppVersion appVersion);
    /**
     * 批量新增数据（MyBatis原生foreach方法）
     */
    int insertBatch(@Param("entities") List<AppVersion> entities);
    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     */
    int insertOrUpdateBatch(@Param("entities") List<AppVersion> entities);
    /**
     * 修改数据
     */
    int update(AppVersion appVersion);
    /**
     * 通过主键删除数据
     */
    int deleteById(Long id);
}