package com.example.no2.mapper;
import java.util.List;
import com.example.no2.entity.DevUser;
import org.apache.ibatis.annotations.Param;
/**
 * (DevUser)表数据库访问层
 */
public interface DevUserMapper {
    /**
     * 通过ID查询单条数据
     */
    DevUser queryById(Long id);
    /**
     * 查询指定行数据
     */
    List<DevUser> queryAll(DevUser devUser);
    /**
     * 统计总行数
     */
    long count(DevUser devUser);
    /**
     * 新增数据
     */
    int insert(DevUser devUser);
    /**
     * 批量新增数据（MyBatis原生foreach方法）
     */
    int insertBatch(@Param("entities") List<DevUser> entities);
    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     */
    int insertOrUpdateBatch(@Param("entities") List<DevUser> entities);
    /**
     * 修改数据
     */
    int update(DevUser devUser);
    /**
     * 通过主键删除数据
     */
    int deleteById(Long id);
}