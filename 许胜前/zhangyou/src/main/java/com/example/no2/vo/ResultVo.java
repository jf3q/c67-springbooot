package com.example.no2.vo;
public class ResultVo {
    private String code;

    private String message;

    private Object data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static  ResultVo success(String message,Object data){
        return new ResultVo("2000",message,data);
    }

    public static  ResultVo error(String message){
        return new ResultVo("5000",message,null);
    }

    public ResultVo() {}

    public ResultVo(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}