package com.example.no2.service;
import java.util.List;
import com.example.no2.mapper.AppVersionMapper;
import com.example.no2.entity.AppVersion;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
@Service
public class AppVersionService {
    @Autowired
    AppVersionMapper appVersionDao;

    public List<AppVersion> selVersion(Long appid) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(appid);
        return appVersionDao.queryAll(appVersion);
    }

    public void addAndUp(AppVersion appVersion) {
        appVersionDao.insert(appVersion);
    }
}