package com.example.no2.service;
import java.util.*;
import com.example.no2.mapper.*;
import com.example.no2.util.*;
import com.example.no2.entity.*;
import com.example.no2.vo.LoginVo;
import com.example.no2.dto.LoginDto;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
@Service
public class LoginService {
    @Autowired
    DevUserMapper devUserDao;

    @Autowired
    BackendUserMapper backendUserDao;

    public LoginVo login(LoginDto loginDto) {
        LoginVo vo = new LoginVo();
       if (loginDto.getUserType().equals(SysConStatus.UserTypeInt.admin)){
           BackendUser backendUser = new BackendUser();
           backendUser.setUsername(loginDto.getAccount());
           backendUser.setUserpassword(loginDto.getPassword());
           List<BackendUser> list = backendUserDao.queryAll(backendUser);
           if (list == null || list.isEmpty()){
               throw new RuntimeException("密码或账号不正确");
           }
           vo.setUser(list.get(0));
           vo.setAccount(list.get(0).getUsercode());
           vo.setRealname(list.get(0).getUsername());
           vo.setUserType(SysConStatus.UserTypeStr.admin);
           StringBuffer str = new StringBuffer();
           String uuid = UUID.randomUUID().toString().replace("-", "");
           str.append(uuid).append("-");
           str.append(list.get(0).getUsercode()).append("-");
           str.append(list.get(0).getId()).append("-");
           str.append("admin-");
           str.append(System.currentTimeMillis());
           SessionUtil.getToKen(str.toString(),list.get(0));
           vo.setToKen(str.toString());
           return vo;
       }else if (loginDto.getUserType().equals(SysConStatus.UserTypeInt.dev)){
           DevUser devUser = new DevUser();
           devUser.setDevcode(loginDto.getAccount());
           devUser.setDevpassword(loginDto.getPassword());
           List<DevUser> list = devUserDao.queryAll(devUser);
           if (list == null || list.size() == 0){
               throw new RuntimeException("密码或账号不正确");
           }
           vo.setUser(list.get(0));
           vo.setAccount(list.get(0).getDevcode());
           vo.setRealname(list.get(0).getDevname());
           vo.setUserType(SysConStatus.UserTypeStr.dev);
           StringBuffer str = new StringBuffer();
           String uuid = UUID.randomUUID().toString().replace("-", "");
           str.append(uuid).append("-");
           str.append(list.get(0).getDevcode()).append("-");
           str.append(list.get(0).getId()).append("-");
           str.append("dev-");
           str.append(System.currentTimeMillis());
           SessionUtil.getToKen(str.toString(),list.get(0));
           vo.setToKen(str.toString());
           return vo;
       }else{
           throw new RuntimeException("不存在的用户类型");
       }
    }
}