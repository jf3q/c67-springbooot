package com.example.no2.controller;
import com.example.no2.vo.*;
import com.example.no2.dto.LoginDto;
import com.example.no2.service.LoginService;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
@RestController
public class LoginController {
    @Autowired
    LoginService loginService;

    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginDto loginDto){
        LoginVo vo;
        try {
            vo=loginService.login(loginDto);
            return ResultVo.success("登录成功",vo);
        }catch (Exception e){
            e.printStackTrace();
            return ResultVo.error("登录失败");
        }
    }
}