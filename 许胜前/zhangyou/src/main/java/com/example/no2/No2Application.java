package com.example.no2;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan

@SpringBootApplication
public class No2Application {
    public static void main(String[] args) {
        SpringApplication.run(No2Application.class, args);
    }
}