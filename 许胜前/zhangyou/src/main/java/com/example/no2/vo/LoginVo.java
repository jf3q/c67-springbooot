package com.example.no2.vo;
public class LoginVo {
    /**
     * uuid+userCode+id+admin|dev+创建时间
     */
    private String toKen;

    private Object user;

    private String account;

    private String realname;

    private String userType;

    public String getToKen() {
        return toKen;
    }

    public void setToKen(String toKen) {
        this.toKen = toKen;
    }

    public Object getUser() {
        return user;
    }

    public void setUser(Object user) {
        this.user = user;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public LoginVo() {}

    public LoginVo(String toKen, Object user, String account, String realname, String userType) {
        this.toKen = toKen;
        this.user = user;
        this.account = account;
        this.realname = realname;
        this.userType = userType;
    }
}