package com.example.no2.controller;
import java.io.*;
import java.util.*;
import com.example.no2.entity.*;
import com.example.no2.service.*;
import com.example.no2.vo.ResultVo;
import org.apache.commons.io.FilenameUtils;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Autowired;
@RestController
@RequestMapping("/version")
public class VersionController {
    @Autowired
    AppVersionService appVersionService;

    @Autowired
    AppInfoService appInfoService;

    @GetMapping("/{appid}")
    public ResultVo selVersion(@PathVariable Long appid){
        try {
            List<AppVersion> list = appVersionService.selVersion(appid);
            return ResultVo.success("",list);
        }catch (Exception e){
            e.printStackTrace();
            return ResultVo.error("");
        }
    }

    @PostMapping("/addAndUp/{appid}")
    public ResultVo addVersion( AppVersion appVersion, MultipartFile apkFile, @PathVariable Long appid, HttpServletRequest request){
        appVersion.setAppid(appid);
        AppInfo appInfo = new AppInfo();
        String realPath = request.getServletContext().getRealPath("/upload/apk/");
        String toKen = request.getHeader("toKen");
        String[] split = toKen.split("-");
        if (!apkFile.isEmpty()){
            String originalFilename = apkFile.getOriginalFilename();
            String extension = FilenameUtils.getExtension(originalFilename);
            if (apkFile.getSize()>500*1024*1024){
                return ResultVo.error("apk文件大于500MB");
            }else if (extension.equals("apk")){
                File file = new File(realPath);
                if (!file.exists()){
                    file.mkdirs();
                }
                String filename = UUID.randomUUID().toString().replace("-","")+"."+extension;
                File saveFile = new File(realPath + filename);
                try {
                    apkFile.transferTo(saveFile);
                    appVersion.setDownloadlink("/upload/apk/"+filename);
                }catch (IOException e){
                    e.printStackTrace();
                    return ResultVo.error("apk文件上传失败");
                }
            }else {
                return ResultVo.error("apk文件格式不正确");
            }
        }else {
            return ResultVo.error("apk文件不能为空");
        }
        try {
            String devId = split[2];
            appVersion.setCreatedby(Long.valueOf(devId));
            appVersion.setCreationdate(new Date());
            appVersion.setPublishstatus(3L);
            appVersionService.addAndUp(appVersion);
            appInfo.setId(appid);
            appInfo.setStatus(1L);
            appInfo.setVersionid(appVersion.getId());
            appInfoService.saveAndUp(appInfo);
            return ResultVo.success("",null);
        }catch (Exception e){
            e.printStackTrace();
            return ResultVo.error("");
        }
    }
}