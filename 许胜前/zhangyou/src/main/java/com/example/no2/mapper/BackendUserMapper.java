package com.example.no2.mapper;
import java.util.List;
import com.example.no2.entity.BackendUser;
import org.apache.ibatis.annotations.Param;
/**
 * (BackendUser)表数据库访问层
 */
public interface BackendUserMapper {
    /**
     * 通过ID查询单条数据
     */
    BackendUser queryById(Long id);
    /**
     * 查询指定行数据
     */
    List<BackendUser> queryAll(BackendUser backendUser);
    /**
     * 统计总行数
     */
    long count(BackendUser backendUser);
    /**
     * 新增数据
     */
    int insert(BackendUser backendUser);
    /**
     * 批量新增数据（MyBatis原生foreach方法）
     */
    int insertBatch(@Param("entities") List<BackendUser> entities);
    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     */
    int insertOrUpdateBatch(@Param("entities") List<BackendUser> entities);
    /**
     * 修改数据
     */
    int update(BackendUser backendUser);
    /**
     * 通过主键删除数据
     */
    int deleteById(Long id);
}