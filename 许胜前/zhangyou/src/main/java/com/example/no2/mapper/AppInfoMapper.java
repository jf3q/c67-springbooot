package com.example.no2.mapper;
import java.util.List;
import com.example.no2.entity.AppInfo;
import org.apache.ibatis.annotations.Param;
/**
 * (AppInfo)表数据库访问层
 */
public interface AppInfoMapper {
    /**
     * 通过ID查询单条数据
     */
    AppInfo queryById(Long id);
    /**
     * 查询指定行数据
     */
    List<AppInfo> queryAll(AppInfo appInfo);
    /**
     * 统计总行数
     */
    long count(AppInfo appInfo);
    /**
     * 新增数据
     */
    int insert(AppInfo appInfo);
    /**
     * 批量新增数据（MyBatis原生foreach方法）
     */
    int insertBatch(@Param("entities") List<AppInfo> entities);
    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     */
    int insertOrUpdateBatch(@Param("entities") List<AppInfo> entities);
    /**
     * 修改数据
     */
    int update(AppInfo appInfo);
    /**
     * 通过主键删除数据
     */
    int deleteById(Long id);

    int vailApkbyName(String apkname);

    int vailApk(@Param("appid") Long appid,@Param("apkname") String apkname);

    void upStatus(@Param("id") Long id,@Param("status") Long status);
}