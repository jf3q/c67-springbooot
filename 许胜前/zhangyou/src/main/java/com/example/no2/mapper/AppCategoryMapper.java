package com.example.no2.mapper;
import java.util.List;
import com.example.no2.entity.AppCategory;
import org.apache.ibatis.annotations.Param;
/**
 * 手游类别(AppCategory)表数据库访问层
 */
public interface AppCategoryMapper {
    /**
     * 通过ID查询单条数据
     */
    AppCategory queryById(Long id);
    /**
     * 查询指定行数据
     */
    List<AppCategory> queryAll(AppCategory appCategory);
    /**
     * 统计总行数
     */
    long count(AppCategory appCategory);
    /**
     * 新增数据
     */
    int insert(AppCategory appCategory);
    /**
     * 批量新增数据（MyBatis原生foreach方法）
     */
    int insertBatch(@Param("entities") List<AppCategory> entities);
    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     */
    int insertOrUpdateBatch(@Param("entities") List<AppCategory> entities);
    /**
     * 修改数据
     */
    int update(AppCategory appCategory);
    /**
     * 通过主键删除数据
     */
    int deleteById(Long id);
}