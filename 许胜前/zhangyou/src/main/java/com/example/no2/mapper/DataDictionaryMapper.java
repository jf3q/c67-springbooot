package com.example.no2.mapper;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.example.no2.entity.DataDictionary;
/**
 * (DataDictionary)表数据库访问层

 */
public interface DataDictionaryMapper {
    /**
     * 通过ID查询单条数据
     */
    DataDictionary queryById(Long id);
    /**
     * 查询指定行数据
     */
    List<DataDictionary> queryAll(DataDictionary dataDictionary);
    /**
     * 统计总行数
     */
    long count(DataDictionary dataDictionary);
    /**
     * 新增数据
     */
    int insert(DataDictionary dataDictionary);
    /**
     * 批量新增数据（MyBatis原生foreach方法）
     */
    int insertBatch(@Param("entities") List<DataDictionary> entities);
    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     */
    int insertOrUpdateBatch(@Param("entities") List<DataDictionary> entities);
    /**
     * 修改数据
     */
    int update(DataDictionary dataDictionary);
    /**
     * 通过主键删除数据
     */
    int deleteById(Long id);
}