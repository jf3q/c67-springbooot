package com.example.no2.service;
import java.io.File;
import java.util.List;
import com.example.no2.mapper.*;
import com.github.pagehelper.*;
import com.example.no2.entity.*;
import com.example.no2.dto.AppInfoDto;
import com.example.no2.util.SysConStatus;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
@Service
public class AppInfoService {
    @Autowired
    AppInfoMapper appInfoDao;

    @Autowired
    AppVersionMapper appVersionDao;

    public PageInfo<AppInfo> getPage(AppInfoDto appInfoDto, Integer pageNum) {
        PageHelper.startPage(pageNum, SysConStatus.pageSize,"id desc");
        AppInfo appInfo = new AppInfo();
        BeanUtils.copyProperties(appInfoDto,appInfo);
        List<AppInfo> list = appInfoDao.queryAll(appInfo);
        return  new PageInfo<>(list);
    }

    public boolean vailApk(Long appid, String apkname) {
        if (appid==null||appid==0){
            return appInfoDao.vailApkbyName(apkname) <= 0;
        }else {
            if (appInfoDao.vailApk(appid,apkname)==1) {
                return true;
            }else if(appInfoDao.vailApk(appid, apkname) == 0){
                return appInfoDao.vailApkbyName(apkname) <= 0;
            }
        }
        return false;
    }

    public void saveAndUp(AppInfo appinfo) {
        if (appinfo.getId()==0){
            appInfoDao.insert(appinfo);
        }else {
            appInfoDao.update(appinfo);
        }
    }

    public AppInfo selApp(Long appid) {
        return appInfoDao.queryById(appid);
    }

    public void delApp(Long appid,String realPath) {
        AppVersion appVersion = new AppVersion();//删除apk文件
        List<AppVersion> list = appVersionDao.queryAll(appVersion);
        for (AppVersion version : list) {
            if (version.getDownloadlink()!=null){
                new File(realPath,version.getDownloadlink()).delete();
            }
        }
        appVersionDao.deleteById(appid);//删除所有版本
        AppInfo appInfo = appInfoDao.queryById(appid); //删除logo文件
        if (appInfo.getLogopicpath()!=null){
            new File(realPath,appInfo.getLogopicpath()).delete();
        }
        appInfoDao.deleteById(appid);//删除app
    }

    public void upStatus(Long id, Long status) {
        appInfoDao.upStatus(id,status);
    }
}