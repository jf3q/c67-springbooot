package com.example.no2.vo;
import java.util.List;
public class CategoryTreeVo {
    /**
     * 主键ID
     */
    private Long id;
    /**
     * 分类名称
     */
    private String categoryname;
    /**
     * 父级节点id
     */
    private Long parentid;

    private List<CategoryTreeVo>  children;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    public List<CategoryTreeVo> getChildren() {
        return children;
    }

    public void setChildren(List<CategoryTreeVo> children) {
        this.children = children;
    }

    public CategoryTreeVo() {}

    public CategoryTreeVo(Long id, String categoryname, Long parentid, List<CategoryTreeVo> children) {
        this.id = id;
        this.categoryname = categoryname;
        this.parentid = parentid;
        this.children = children;
    }
}