package com.bdqn.aap.service;

import com.bdqn.aap.dto.LoginDto;
import com.bdqn.aap.vo.LoginVo;

public interface BackendUserService  {


    LoginVo loginBackend(LoginDto loginDto);

}
