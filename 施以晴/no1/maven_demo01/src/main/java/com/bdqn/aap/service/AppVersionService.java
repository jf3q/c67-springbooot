package com.bdqn.aap.service;

import com.bdqn.aap.entity.AppVersion;

import java.util.List;

public interface AppVersionService  {

    List<AppVersion> getAppVersionList(Long id);

    int insertAppVersion(AppVersion appVersion);
}
