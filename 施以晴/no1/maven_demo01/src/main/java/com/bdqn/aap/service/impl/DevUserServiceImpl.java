package com.bdqn.aap.service.impl;

import com.bdqn.aap.dto.LoginDto;
import com.bdqn.aap.entity.DevUser;
import com.bdqn.aap.utils.StringBufferUtils;
import com.bdqn.aap.utils.sessionUtils;
import com.bdqn.aap.vo.LoginVo;
import com.bdqn.aap.mapper.DevUserDao;
import com.bdqn.aap.service.DevUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DevUserServiceImpl implements DevUserService {


    @Autowired
    DevUserDao devUserDao;

    @Override
    public LoginVo loginUser(LoginDto loginDto) {

        DevUser devUser = new DevUser();
        devUser.setDevcode(loginDto.getAccount());
        devUser.setDevpassword(loginDto.getPasswrod());
        List<DevUser> devUsers = devUserDao.queryAllBy(devUser);
        if (devUsers.size()==0){
            throw new RuntimeException("账号或密码错误！");
        }
        DevUser devUser1 = devUsers.get(0);

        LoginVo loginVo = new LoginVo();
        loginVo.setId(devUser1.getId());
        loginVo.setUserType(loginDto.getUserType());
        loginVo.setUseracount(devUser1.getDevcode());
        loginVo.setUserName(devUser1.getDevname());
        loginVo.setToken(StringBufferUtils.joint(loginVo.getUseracount(),loginDto.getUserType()));
        sessionUtils.put(loginVo.getToken(),loginVo);


        return loginVo;
    }
}
