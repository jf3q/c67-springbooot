package com.bdqn.aap.service.impl;

import com.bdqn.aap.entity.AppVersion;
import com.bdqn.aap.mapper.AppVersionDao;
import com.bdqn.aap.service.AppVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AppVersionServiceImpl implements AppVersionService {


    @Autowired
    AppVersionDao appVersionDao;

    @Override
    public List<AppVersion> getAppVersionList(Long id) {
        return appVersionDao.queryAllBy(new AppVersion().setAppid(id));
    }

    @Override
    public int insertAppVersion(AppVersion appVersion) {
        return appVersionDao.insert(appVersion);
    }
}
