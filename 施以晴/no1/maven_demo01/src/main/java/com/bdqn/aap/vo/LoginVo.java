package com.bdqn.aap.vo;


import lombok.Data;

@Data
public class LoginVo {

    private Long id;

    private String useracount;

    private String userName;


    private String token;

    private Integer userType;

}
