package com.bdqn.aap.controller;


import com.bdqn.aap.dto.LoginDto;
import com.bdqn.aap.utils.sessionUtils;
import com.bdqn.aap.vo.LoginVo;
import com.bdqn.aap.vo.ResultVo;
import com.bdqn.aap.service.BackendUserService;
import com.bdqn.aap.service.DevUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/devUser")
public class LoginUserController {


    @Autowired
    DevUserService devUserService;

    @Autowired
    BackendUserService backendUserService;

    @RequestMapping("/loginUser")
    public ResultVo login(@RequestBody LoginDto loginDto){
        if (loginDto.getUserType()==1){
            try {
                LoginVo loginVo = backendUserService.loginBackend(loginDto);
                return ResultVo.success("登入成功!",loginVo);
            } catch (Exception e) {
                e.printStackTrace();
                return ResultVo.error(e.getMessage());
            }
        }else if (loginDto.getUserType()==2){
            try {
                LoginVo loginVo = devUserService.loginUser(loginDto);
                return ResultVo.success("登入成功!",loginVo);
            } catch (Exception e) {
                e.printStackTrace();
                return ResultVo.error(e.getMessage());
            }
        }else {
            return ResultVo.error("没有这个角色");
        }
    }

    @RequestMapping("/logout")
    public ResultVo logout(HttpServletRequest request){
        String header = request.getHeader("token");
        sessionUtils.remove(header);
        return ResultVo.success("退出成功!",null);
    }
}
