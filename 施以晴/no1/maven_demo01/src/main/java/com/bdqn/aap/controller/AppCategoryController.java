package com.bdqn.aap.controller;



import com.bdqn.aap.vo.AppCategoryVO;
import com.bdqn.aap.vo.ResultVo;
import com.bdqn.aap.service.AppCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class AppCategoryController {

    @Autowired
    AppCategoryService appCategoryService;

    @RequestMapping("/tree")
    public ResultVo tree(){
        AppCategoryVO appCategoryVO = appCategoryService.appCategoryTree();

        return ResultVo.success("",appCategoryVO);
    }



}
