package com.bdqn.aap.dto;

import lombok.Data;

@Data
public class AppInfoDto {
    private String softwarename;
    private String apkname;
    private Long status;
    private Long flatformid;
    private Long categorylevel1;
    private Long categorylevel2;
    private Long categorylevel3;

    private Long devid;
}
