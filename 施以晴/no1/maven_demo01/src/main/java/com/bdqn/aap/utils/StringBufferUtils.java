package com.bdqn.aap.utils;

import java.util.UUID;

public class StringBufferUtils {


    public static  String  joint(String account,Integer userType){
        StringBuffer sb = new StringBuffer();
        sb.append(UUID.randomUUID().toString().replace("-","")+"-");
        sb.append(account+"-");
        sb.append(System.currentTimeMillis()+"-");
        sb.append(userType);
        return sb.toString();
    }
}
