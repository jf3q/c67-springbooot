package com.bdqn.aap.entity;

import java.io.Serializable;

/**
 * (BackendUser)实体类
 *
 * @author makejava
 * @since 2023-12-07 09:01:33
 */
public class BackendUser implements Serializable {
    private static final long serialVersionUID = 691323585863170164L;
    /**
     * 主键id
     */
    private Long id;
    /**
     * 用户编码
     */
    private String usercode;
    /**
     * 用户名称
     */
    private String username;
    /**
     * 用户角色类型（来源于数据字典表，分为：超管、财务、市场、运营、销售）
     */
    private Long usertype;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    private Long createdby;
    /**
     * 创建时间
     */
    private Object creationdate;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    private Long modifyby;
    /**
     * 最新更新时间
     */
    private Object modifydate;
    /**
     * 用户密码
     */
    private String userpassword;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getUsertype() {
        return usertype;
    }

    public void setUsertype(Long usertype) {
        this.usertype = usertype;
    }

    public Long getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Long createdby) {
        this.createdby = createdby;
    }

    public Object getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(Object creationdate) {
        this.creationdate = creationdate;
    }

    public Long getModifyby() {
        return modifyby;
    }

    public void setModifyby(Long modifyby) {
        this.modifyby = modifyby;
    }

    public Object getModifydate() {
        return modifydate;
    }

    public void setModifydate(Object modifydate) {
        this.modifydate = modifydate;
    }

    public String getUserpassword() {
        return userpassword;
    }

    public void setUserpassword(String userpassword) {
        this.userpassword = userpassword;
    }

}

