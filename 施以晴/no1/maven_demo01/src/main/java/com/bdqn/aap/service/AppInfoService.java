package com.bdqn.aap.service;

import com.bdqn.aap.dto.AppInfoDto;
import com.bdqn.aap.entity.AppInfo;
import com.github.pagehelper.PageInfo;

import javax.servlet.http.HttpServletRequest;

public interface AppInfoService  {

    PageInfo<AppInfo> getPage(AppInfoDto appInfoDto, Integer pageNum);

    int saveUpdate(AppInfo appInfo);

    AppInfo getAppinfoId(Integer id);

    int delAppInfo(Integer id,HttpServletRequest request);

    int sale(Integer id);
}
