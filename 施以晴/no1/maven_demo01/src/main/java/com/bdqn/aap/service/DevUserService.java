package com.bdqn.aap.service;

import com.bdqn.aap.dto.LoginDto;
import com.bdqn.aap.vo.LoginVo;

public interface DevUserService  {


    LoginVo loginUser(LoginDto loginDto);
}
