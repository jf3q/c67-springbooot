package com.jiwen.service;

import com.jiwen.vo.CategoryVo;

/**
 * @author shrstart
 * @create 2023-12-16-10:57
 */
public interface AppCategoryService {

    /*查询三级分类*/
    CategoryVo selCategory();
}
