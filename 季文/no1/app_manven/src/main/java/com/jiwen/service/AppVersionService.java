package com.jiwen.service;

import com.jiwen.pojo.AppVersion;

import java.util.List;

/**
 * @author shrstart
 * @create 2023-12-18-11:43
 */
public interface AppVersionService {

    /*查询所有APP版本信息*/
    List<AppVersion> selAppVersions(AppVersion appVersion);

    /*添加版本信息*/
    Integer addAppVersion(AppVersion appVersion);

    /*删除APP所有版本信息*/
    void delVersion(Long id);
}
