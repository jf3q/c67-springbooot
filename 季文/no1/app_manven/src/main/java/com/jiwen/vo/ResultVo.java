package com.jiwen.vo;

/**
 * @author shrstart
 * @create 2023-12-16-9:23
 */
public class ResultVo {

    private String code;
    private String message;
    private Object data;

    public ResultVo(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static ResultVo success(String message, Object obj){
        return new ResultVo("2000",message,obj);
    }

    public static ResultVo error(String message){
        return new ResultVo("5000",message,null);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
