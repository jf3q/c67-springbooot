package com.jiwen.dto;

/**
 * @author shrstart
 * @create 2023-12-16-9:22
 */
public class LoginDto {

    private String account;
    private String password;
    private Long usertype;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getUsertype() {
        return usertype;
    }

    public void setUsertype(Long usertype) {
        this.usertype = usertype;
    }
}
