package com.jiwen.service;

import com.github.pagehelper.PageInfo;
import com.jiwen.pojo.AppInfo;

import javax.servlet.http.HttpServletRequest;

/**
 * @author shrstart
 * @create 2023-12-16-10:48
 */
public interface AppInfoService {

    /*分页条件查询*/
    PageInfo<AppInfo> selAppInfos(AppInfo appInfo, Integer pageNum);

    /*查询APK名称唯一性*/
    Long selApkSum(String apkName);

    /*查询APP信息*/
    AppInfo selAppInfo(Long appId);

    /*新增、修改*/
    Integer addOrUpdate(AppInfo appInfo, HttpServletRequest request);

    /*删除APP信息*/
    Integer delAppInfo(Long appId);
}
