package com.jiwen.vo;

import java.util.List;

/**
 * @author shrstart
 * @create 2023-12-16-10:54
 */
public class CategoryVo {

    private String categoryname;    //分类名称
    private Long id;                //分类Id
    private Long parentid;          //父级Id
    private List<CategoryVo> children;//子集分类

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    public List<CategoryVo> getChildren() {
        return children;
    }

    public void setChildren(List<CategoryVo> children) {
        this.children = children;
    }
}
