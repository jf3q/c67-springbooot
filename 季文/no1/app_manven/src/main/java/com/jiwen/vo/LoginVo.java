package com.jiwen.vo;

/**
 * @author shrstart
 * @create 2023-12-16-9:24
 */
public class LoginVo {

    private String userName;    //用户名称
    private Long usertype;      //用户类型
    private String token;

    public LoginVo(String userName, Long usertype, String token) {
        this.userName = userName;
        this.usertype = usertype;
        this.token = token;
    }

    public LoginVo() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getUsertype() {
        return usertype;
    }

    public void setUsertype(Long usertype) {
        this.usertype = usertype;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
