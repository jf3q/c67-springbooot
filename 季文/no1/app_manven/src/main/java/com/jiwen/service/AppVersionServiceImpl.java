package com.jiwen.service;

import com.jiwen.dao.AppVersionDao;
import com.jiwen.pojo.AppVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author shrstart
 * @create 2023-12-18-11:43
 */

@Service
public class AppVersionServiceImpl implements AppVersionService {

    @Autowired
    AppVersionDao appVersionDao;

    @Override
    public List<AppVersion> selAppVersions(AppVersion appVersion) {
        return appVersionDao.queryAllBy(appVersion);
    }

    @Override
    public Integer addAppVersion(AppVersion appVersion) {
        return appVersionDao.insert(appVersion);
    }

    @Override
    public void delVersion(Long id) {
        appVersionDao.deleteById(id);
    }
}
