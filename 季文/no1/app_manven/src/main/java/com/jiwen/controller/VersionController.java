package com.jiwen.controller;

import com.jiwen.pojo.AppInfo;
import com.jiwen.pojo.AppVersion;
import com.jiwen.service.AppInfoService;
import com.jiwen.service.AppVersionService;
import com.jiwen.vo.AppVersionVo;
import com.jiwen.vo.ResultVo;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author shrstart
 * @create 2023-12-18-15:52
 */

@RestController
public class VersionController {

    @Autowired
    AppInfoService appInfoService;

    @Autowired
    AppVersionService appVersionService;

    /*查询APP所有版本信息*/
    @GetMapping("/selVersions/{appId}")
    public ResultVo selVersions(@PathVariable Long appId) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(appId);
        List<AppVersion> appVersions = appVersionService.selAppVersions(appVersion);


        List<AppVersionVo> appVersionVos = new ArrayList<>();
        for (AppVersion version : appVersions) {
            AppVersionVo appVersionVo = new AppVersionVo();
            BeanUtils.copyProperties(version, appVersionVo);

            appVersionVos.add(appVersionVo);
        }

        if (appVersions.size() > 0) {
            return ResultVo.success("", appVersionVos);
        } else {
            return ResultVo.error("");
        }
    }

    /*添加APP新版本*/
    @PostMapping("/addVersion")
    public ResultVo addVersion(AppVersion appVersion, MultipartFile apkFile, HttpServletRequest request) {

        if (apkFile != null) {
            String filePath = request.getServletContext().getRealPath("/upload/apk");
            File file = new File(filePath);

            if (!file.exists()) {
                file.mkdirs();
            }

            if (apkFile.getSize() > 1024 * 1024 * 500) {
                return ResultVo.error("文件过大");
            } else {
                String fileName = apkFile.getOriginalFilename();
                String fileSuffix = FilenameUtils.getExtension(fileName);

                if (fileSuffix.equalsIgnoreCase("apk")) {
                    String newFileName = UUID.randomUUID().toString().replace("-", "") + "." + fileSuffix;
                    File newFile = new File(filePath + "/" + newFileName);

                    try {
                        apkFile.transferTo(newFile);
                        appVersion.setDownloadlink("/upload/apk/" + newFileName);
                        appVersion.setApklocpath(filePath + "/" + newFileName);
                        appVersion.setApkfilename(newFileName);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    return ResultVo.error("文件格式错误");
                }
            }
        }

        String token = request.getHeader("token");
        String[] split = token.split("-");

        appVersion.setCreatedby(Long.valueOf(split[2]));
        appVersion.setCreationdate(new Date());
        appVersion.setPublishstatus(3L);

        Integer num = appVersionService.addAppVersion(appVersion);
        if (num > 0) {
            AppInfo appInfo = new AppInfo();
            appInfo.setId(appVersion.getAppid());
            appInfo.setVersionid(appVersion.getId());
            appInfoService.addOrUpdate(appInfo, request);
            return ResultVo.success("添加成功", null);
        } else {
            return ResultVo.error("添加失败");
        }
    }
}
