package com.jiwen.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jiwen.dao.AppInfoDao;
import com.jiwen.pojo.AppInfo;
import com.jiwen.util.SysConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author shrstart
 * @create 2023-12-16-10:49
 */

@Service
public class AppInfoServiceImpl implements AppInfoService {

    @Autowired
    AppInfoDao appInfoDao;

    @Override
    public PageInfo<AppInfo> selAppInfos(AppInfo appInfo, Integer pageNum) {

        PageHelper.startPage(pageNum, SysConstant.page.pageSize);
        List<AppInfo> appInfos = appInfoDao.queryAllBy(appInfo);
        PageInfo<AppInfo> appInfoPageInfo = new PageInfo<>(appInfos);

        return appInfoPageInfo;
    }

    @Override
    public Long selApkSum(String apkName) {
        AppInfo appInfo = new AppInfo();
        appInfo.setApkname(apkName);
        return appInfoDao.count(appInfo);
    }

    @Override
    public AppInfo selAppInfo(Long appId) {
        AppInfo appInfo = appInfoDao.queryById(appId);
        return appInfo;
    }

    @Override
    public Integer addOrUpdate(AppInfo appInfo, HttpServletRequest request) {

        Integer num = 0;

        String token = request.getHeader("token");
        String[] split = token.split("-");

        if (appInfo.getId() == null) {
            /*新增*/
            appInfo.setDownloads(0L);
            appInfo.setStatus(1L);
            appInfo.setDevid(Long.valueOf(split[2]));
            appInfo.setCreatedby(Long.valueOf(split[2]));
            appInfo.setCreationdate(new Date());

            num = appInfoDao.insert(appInfo);
        } else {
            /*修改*/
            appInfo.setModifyby(Long.valueOf(split[2]));
            appInfo.setModifydate(new Date());

            if(appInfo.getStatus() == null) {
                appInfo.setStatus(1L);
            }

            num = appInfoDao.update(appInfo);
        }

        return num;
    }

    @Override
    public Integer delAppInfo(Long appId) {
        return appInfoDao.deleteById(appId);
    }

}
