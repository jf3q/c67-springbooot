package com.jiwen.service;

import com.jiwen.dto.LoginDto;
import com.jiwen.vo.LoginVo;

/**
 * @author shrstart
 * @create 2023-12-16-9:22
 */
public interface LoginService {

    /*用户登陆方法*/
    LoginVo userLogin(LoginDto loginDto);
}
