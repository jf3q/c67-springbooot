package com.jiwen.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shrstart
 * @create 2023-12-16-9:25
 */
public class SessionUtil {
    static Map<String,Object> map = new HashMap<>();

    public static void put(String token,Object obj){
        map.put(token,obj);
    }

    public static Object get(String token){
        return map.get(token);
    }

    public static void remove(String token){
        map.remove(token);
    }
}
