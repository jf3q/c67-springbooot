package com.jiwen.dto;

/**
 * @author shrstart
 * @create 2023-12-16-14:58
 */
public class ApkDto {

    private String apkName;
    private Long appId;

    public String getApkName() {
        return apkName;
    }

    public void setApkName(String apkName) {
        this.apkName = apkName;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }
}
