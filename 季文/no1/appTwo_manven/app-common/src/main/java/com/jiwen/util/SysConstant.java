package com.jiwen.util;

/**
 * @author shrstart
 * @create 2023-12-16-9:31
 */
public class SysConstant {

    public static class UserType{
        public static String admin="admin";
        public static String dev="dev";
    }


    public static class page{
        public static Integer pageSize = 5;
    }

}
