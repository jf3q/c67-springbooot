package com.jiwen.vo;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * @author shrstart
 * @create 2023-12-18-16:00
 */
public class AppVersionVo {
    private String versionno;
    private String versioninfo;
    private String downloadlink;

    @JSONField(format = "yyyy-MM-dd")
    private Date creationdate;

    public String getVersionno() {
        return versionno;
    }

    public void setVersionno(String versionno) {
        this.versionno = versionno;
    }

    public String getVersioninfo() {
        return versioninfo;
    }

    public void setVersioninfo(String versioninfo) {
        this.versioninfo = versioninfo;
    }

    public String getDownloadlink() {
        return downloadlink;
    }

    public void setDownloadlink(String downloadlink) {
        this.downloadlink = downloadlink;
    }

    public Date getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(Date creationdate) {
        this.creationdate = creationdate;
    }
}
