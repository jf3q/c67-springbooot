package com.jiwen.dao;

import com.jiwen.entity.Hospital;
import com.jiwen.entity.Medical_assay;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IndexDao {

    List<Medical_assay> selAll(Long hospitalId);

    List<Hospital> selHospital();

    void add(Medical_assay medicalAssay);

    Integer queZhen(Integer id);
}
