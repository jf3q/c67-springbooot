package com.jiwen.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Medical_assay {

    private Integer id;
    private String assay_user;
    private Long hospital_id;
    private Integer assay_result;
    private String phone;
    private String card_num;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date assay_time;

    private String hospitalName;

}
