package com.jiwen.controller;

import com.jiwen.entity.Hospital;
import com.jiwen.entity.Medical_assay;
import com.jiwen.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class IndexController {

    @Autowired
    IndexService indexService;

    //查询信息列表
    @RequestMapping("/page")
    public String index(@RequestParam(defaultValue = "0") Long hospitalId, Model model) {
        List<Medical_assay> medicalAssays = indexService.selAll(hospitalId);
        model.addAttribute("medicals", medicalAssays);

        List<Hospital> hospitals = indexService.selHospital();
        model.addAttribute("hospitals", hospitals);
        return "index";
    }

    /*查询条件列表*/
    @RequestMapping("/sel")
    public String selHospital(Model model) {
        List<Hospital> hospitals = indexService.selHospital();
        model.addAttribute("hospitals", hospitals);
        return "index";
    }

    /*添加信息*/
    @RequestMapping("/addInfo")
    public String add(Medical_assay medicalAssay) {
        medicalAssay.setAssay_result(0);
        indexService.add(medicalAssay);
        return "redirect:/page";
    }

    /*跳转添加页面*/
    @RequestMapping("/add")
    public String add(Model model) {
        List<Hospital> hospitals = indexService.selHospital();
        model.addAttribute("hospitals", hospitals);

        return "add";
    }

    /*确诊*/
    @RequestMapping("/quezhen/{id}")
    public void queZhen(@PathVariable Integer id) {
        Integer integer = indexService.queZhen(id);
    }
}
