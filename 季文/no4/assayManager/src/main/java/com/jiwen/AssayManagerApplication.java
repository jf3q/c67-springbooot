package com.jiwen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssayManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssayManagerApplication.class, args);
    }

}
