package com.jiwen.service;

import com.jiwen.entity.Hospital;
import com.jiwen.entity.Medical_assay;

import java.util.List;

public interface IndexService {

    /*查询核酸信息列表*/
    List<Medical_assay> selAll(Long hospitalId);

    /*查询条件列表*/
    List<Hospital> selHospital();

    /*添加信息*/
    void add(Medical_assay medicalAssay);

    /*确诊*/
    Integer queZhen(Integer id);
}
