package com.jiwen.service;

import com.jiwen.dao.IndexDao;
import com.jiwen.entity.Hospital;
import com.jiwen.entity.Medical_assay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IndexServiceImpl implements IndexService {

    @Autowired
    IndexDao indexDao;

    @Override
    public List<Medical_assay> selAll(Long hospitalId) {
        return indexDao.selAll(hospitalId);
    }

    @Override
    public List<Hospital> selHospital() {
        return indexDao.selHospital();
    }

    @Override
    public void add(Medical_assay medicalAssay) {
        indexDao.add(medicalAssay);
    }

    @Override
    public Integer queZhen(Integer id) {
        return indexDao.queZhen(id);
    }
}
