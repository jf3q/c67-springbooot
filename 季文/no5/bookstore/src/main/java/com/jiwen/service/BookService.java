package com.jiwen.service;

import com.github.pagehelper.PageInfo;
import com.jiwen.pojo.Book;

public interface BookService {
    /*分页条件查询*/
    PageInfo<Book> selBooks(String name, Integer pageNum);

    /*新增图书详情*/
    void addInfo(Book book);

    /*查询需要修改的图书信息*/
    Book selId(Integer id);

    /*修改图书信息*/
    void updateInfo(Book book);

    /*删除图书信息*/
    void del(String[] split);
}
