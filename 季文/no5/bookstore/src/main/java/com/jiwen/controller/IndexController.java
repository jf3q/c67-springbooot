package com.jiwen.controller;

import com.github.pagehelper.PageInfo;
import com.jiwen.pojo.Book;
import com.jiwen.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class IndexController {

    @Autowired
    BookService bookService;

    /*分页条件查询*/
    @RequestMapping("/page")
    public String page(Model model, String name, @RequestParam(defaultValue = "1") Integer pageNum) {
        PageInfo<Book> bookPageInfo = bookService.selBooks(name, pageNum);
        model.addAttribute("page", bookPageInfo);
        return "index";
    }

    /*新增页面跳转*/
    @RequestMapping("/add")
    public String add() {
        return "addOrUpdate";
    }

    /*新增、修改图书详细信息*/
    @PostMapping("/addInfo")
    public String addInfo(Book book){

        if(book.getId() == null) {
            bookService.addInfo(book);
        }else{
            bookService.updateInfo(book);
        }

        return "redirect:/page";
    }

    /*修改页面跳转*/
    @GetMapping("/update")
    public String update(Integer id,Model model){
        Book book = bookService.selId(id);
        model.addAttribute("bookInfo",book);
        return "addOrUpdate";
    }

    /*删除*/
    @RequestMapping("/del")
    public String del(String ids){
        String[] split = ids.split(",");
        bookService.del(split);
        return "redirect:/page";
    }
}
