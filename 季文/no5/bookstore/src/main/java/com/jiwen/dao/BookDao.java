package com.jiwen.dao;

import com.jiwen.pojo.Book;

import java.util.List;

public interface BookDao {
    /*条件查询书籍信息*/
    List<Book> selBooks(String name);

    /*新增图书详情*/
    void addInfo(Book book);

    /*查询单个图书信息*/
    Book selBook(Integer id);

    /*修改图书信息*/
    void updateInfo(Book book);

    /*根据id删除图书信息*/
    void del(String[] split);
}
