package com.jiwen.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    private String id;
    private String name;
    private String price;
    private String category;
    private String pnum;
    private String imgurl;
    private String description;
    private String author;
    private String sales;

}
