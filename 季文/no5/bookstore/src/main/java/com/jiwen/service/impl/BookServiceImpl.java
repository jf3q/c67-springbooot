package com.jiwen.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jiwen.dao.BookDao;
import com.jiwen.pojo.Book;
import com.jiwen.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    BookDao bookDao;

    @Override
    public PageInfo<Book> selBooks(String name, Integer pageNum) {
        PageHelper.startPage(pageNum, 3);
        List<Book> bookList = bookDao.selBooks(name);
        PageInfo<Book> bookPageInfo = new PageInfo<>(bookList);

        return bookPageInfo;
    }

    @Override
    public void addInfo(Book book) {
        bookDao.addInfo(book);
    }

    @Override
    public Book selId(Integer id) {
        return bookDao.selBook(id);
    }

    @Override
    public void updateInfo(Book book) {
        bookDao.updateInfo(book);
    }

    @Override
    public void del(String[] split) {
        bookDao.del(split);
    }
}
