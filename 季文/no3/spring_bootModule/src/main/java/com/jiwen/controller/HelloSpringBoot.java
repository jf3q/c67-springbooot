package com.jiwen.controller;

import com.jiwen.entity.DevUser;
import com.jiwen.service.DevUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloSpringBoot {

    @Autowired
    DevUserService devUserService;

    @RequestMapping("/hello")
    public String hello(){
        return "Hello SpringBoot";
    }

    @RequestMapping("/login")
    public void login(){
        DevUser devUser = devUserService.login();
        System.out.println(devUser);
    }
}
