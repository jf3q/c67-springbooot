package com.jiwen.service;

import com.jiwen.entity.DevUser;

public interface DevUserService {

    DevUser login();
}
