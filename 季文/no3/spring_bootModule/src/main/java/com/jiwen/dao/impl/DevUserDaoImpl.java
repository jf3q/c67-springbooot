package com.jiwen.dao.impl;

import com.jiwen.dao.DevUserDao;
import com.jiwen.entity.DevUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class DevUserDaoImpl  implements DevUserDao {

    @Value("${phone}")
    String phone;

    @Autowired
    DevUser devUser;

    @Override
    public DevUser login() {
        devUser.setPhone(phone);
        return devUser;
    }
}
