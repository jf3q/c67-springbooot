package com.jiwen.service.impl;

import com.jiwen.dao.DevUserDao;
import com.jiwen.entity.DevUser;
import com.jiwen.service.DevUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DevUserServiceImpl implements DevUserService {

    @Autowired
    DevUserDao devUserDao;

    @Override
    public DevUser login() {
        return devUserDao.login();
    }
}
