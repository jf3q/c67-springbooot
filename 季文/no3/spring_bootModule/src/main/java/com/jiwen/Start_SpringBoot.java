package com.jiwen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Start_SpringBoot {

    public static void main(String[] args) {
        SpringApplication.run(Start_SpringBoot.class,args);
    }
}
