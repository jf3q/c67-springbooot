package com.jiwen.dao;

import com.jiwen.entity.DevUser;

public interface DevUserDao {

    DevUser login();
}
