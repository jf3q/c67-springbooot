package com.example.service;

import com.example.dao.BookDao;
import com.example.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    BookDao bookDao;

    public void setValue(String key, String value) {
        bookDao.setValue(key, value);
    }

    public String getValue(String key){
        return bookDao.getValue(key);
    }

    public void del(String key) {
        bookDao.del(key);
    }

    public Book findBookById(Integer id){
        return bookDao.findBookById(id);
    }

    public void saveBooks(String key, List<Book> books) {
        bookDao.saveBooks(key, books);
    }

    public List<Book> findBooks(String key) {
        return (List<Book>) bookDao.findBooks(key);
    }

    public void saveBook(Book book){
        bookDao.saveBook(book);
    }

    public void delBook(Integer id) {
        bookDao.delBook(id);
    }
}
