package com.example.dao;

import com.example.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Repository
public class BookDao {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate redisTemplate;

    //存储字符串类型key-value
    public void setValue(String key,String value){
        stringRedisTemplate.opsForValue().set(key,value,24, TimeUnit.HOURS);
    }

    //获取字符串类型
    public String getValue(String key){
        return stringRedisTemplate.opsForValue().get(key);
    }

    //删除key-value
    public void del(String key){
        stringRedisTemplate.delete(key);
    }

    //保存多本书List
    public void saveBooks(String key, List<Book> books) {
        redisTemplate.opsForValue().set(key,books);
    }

    //通过id查找书(key)
    public Book findBookById(Integer id){
        return (Book) redisTemplate.opsForValue().get(id);
    }

    //查找所有书(key)
    public List<Book> findBooks(String key){
        return (List<Book>) redisTemplate.opsForValue().get(key);
    }

    //保存一本书
    public void saveBook(Book book){
        redisTemplate.opsForValue().set(book.getId(),book);
    }

    //根据Id删除一本书(key)
    public void delBook(Integer id){
        redisTemplate.delete(id);
    }


}
