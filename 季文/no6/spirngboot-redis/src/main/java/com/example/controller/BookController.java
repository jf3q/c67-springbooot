package com.example.controller;

import com.example.entity.Book;
import com.example.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class BookController {

    @Autowired
    BookService bookService;

    @RequestMapping("/setValue")
    @ResponseBody
    //存储字符串类型key-value
    public String selValue() {
        bookService.setValue("redis", "Spring Date Redis");
        return "保存键值对成功！";
    }

    @RequestMapping("/getValue")
    @ResponseBody
    //获取字符串类型的值
    public String getValue() {
        return bookService.getValue("redis");
    }

    @RequestMapping("/saveBook")
    @ResponseBody
    //保存一本书
    public String saveBook() {
        Book book = new Book(1, "C语言程序设计", 50.0, "计算机", 100, "101.jpg", "", "zhangsan", 50);
        bookService.saveBook(book);
        return "保存一本书成功！";
    }

    @RequestMapping("/saveBooks")
    @ResponseBody
    public String saveBooks() {
        Book book1 = new Book(1, "C语言程序设计", 50.0, "计算机", 100, "101.jpg", "", "zhangsan", 50);
        Book book2 = new Book(2, "java语言程序设计", 60.0, "计算机", 100, "102.jpg", "", "zhangsan", 50);
        Book book3 = new Book(3, "python语言程序设计", 70.0, "计算机", 100, "103.jpg", "", "zhangsan", 50);
        List<Book> books = new ArrayList<Book>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        bookService.saveBooks("101", books);
        return "保存多本书成功！";
    }

    //根据Id查询书籍
    @RequestMapping("/findBook/{id}")
    public ModelAndView findBookById(@PathVariable Integer id) {
        ModelAndView mv = new ModelAndView();
        Book book = bookService.findBookById(id);

        mv.addObject(book);
        mv.setViewName("book");

        return mv;
    }

    @RequestMapping("/findBooks/{id}")
    //在书籍列表根据Id查询书籍信息
    public ModelAndView findBooksById(@PathVariable Integer id) {
        ModelAndView mv = new ModelAndView();

        List<Book> books = bookService.findBooks("101");
        Book book = new Book();
        if (books != null && books.size() > 0) {
            book = books.get(id - 1);
        }
        mv.addObject("book", book);
        mv.setViewName("book");
        return mv;
    }

    //查询书籍信息列表
    @RequestMapping("/findBooks")
    public ModelAndView findBooks() {
        ModelAndView mv = new ModelAndView();

        List<Book> books = bookService.findBooks("101");
        mv.addObject("books", books);
        mv.setViewName("books");
        return mv;
    }

    //删除
    @RequestMapping("/delBook/{id}")
    public ModelAndView delBook(@PathVariable Integer id) {
        ModelAndView mv = new ModelAndView();

        List<Book> books = bookService.findBooks("101");
        books.remove(id - 1);
        bookService.saveBooks("101", books);

        mv.addObject("books", books);
        mv.setViewName("books");
        return mv;
    }

}
