package com.fc.app.service;

import com.fc.app.dto.AppInfoDto;
import com.fc.app.entity.AppInfo;
import com.github.pagehelper.PageInfo;
import jakarta.servlet.http.HttpServletRequest;

public interface AppInfoService {

    PageInfo<AppInfo> getSelApp(AppInfoDto appInfoDto, Integer pageNum, Integer pageSize);

    void saveOrUpdate(AppInfo appinfo);

    AppInfo selectById(Long id);

    void delAll(Long id, HttpServletRequest request);

    void getShelves(Long id);

    void getPass(Long id);

    void getRepulse(Long id);
}
