package com.fc.app.intercept;

import com.fc.app.Utils.SessionUtils;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.HandlerInterceptor;




public class LoginIntercept implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        if (token == null){
            response.setStatus(401);
        }else if (SessionUtils.get(token) == null){
            response.setStatus(403);
        }else{
            String[] split = token.split("-");

            String createTime = split[1];

            if (System.currentTimeMillis() - Long.parseLong(createTime) > 2*3600*1000){
                response.setStatus(403);
                SessionUtils.remove(token);
            }else {
                return true;
            }
        }
        return false;
    }
}
