package com.fc.app.controller;

import com.fc.app.service.impl.AppCategoryService;
import com.fc.app.vo.AppCategoryVo;
import com.fc.app.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppCategoryController {

    @Autowired
    AppCategoryService appCategoryService;

    @GetMapping("/tree")
    public ResultVo tree(){
        AppCategoryVo vo = appCategoryService.tree();

        return ResultVo.success(null,vo);
    }

}
