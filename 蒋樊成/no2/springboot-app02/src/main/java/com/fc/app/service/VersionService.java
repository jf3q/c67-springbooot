package com.fc.app.service;

import com.fc.app.entity.AppVersion;

import java.util.List;

public interface VersionService {

    List<AppVersion> getVersionList(Long id);

    void getSaveVersion(AppVersion appVersion);
}
