package com.fc.app.service.impl;

import com.fc.app.Utils.SessionUtils;
import com.fc.app.dao.BackendUserDao;
import com.fc.app.dto.LoginDto;
import com.fc.app.entity.BackendUser;
import com.fc.app.entity.DevUser;
import com.fc.app.service.BackendUserService;
import com.fc.app.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;

@Service
public class BackendUserServiceImpl implements BackendUserService {

    @Autowired
    BackendUserDao backendUserDao;

    @Override
    public LoginUserVo login(LoginDto loginDto) {

        if (StringUtils.isEmpty(loginDto.getAccount())){
            throw new RuntimeException("账号必填");
        }
        if (StringUtils.isEmpty(loginDto.getPassword())){
            throw new RuntimeException("密码必填");
        }

        BackendUser backendUser = new BackendUser().setUsercode(loginDto.getAccount()).setUserpassword(loginDto.getPassword()).setUsertype(1L);

        List<BackendUser> backend = backendUserDao.queryAllBy(backendUser);

        if (backend.size() == 0){
            throw new RuntimeException("账号密码错误");
        }

        LoginUserVo vo = new LoginUserVo();

        vo.setUserCode(backend.get(0).getUsercode());
        vo.setUserName(backend.get(0).getUsername());

        //生成token 格式 uuid + token的生成时间
        String uuid = UUID.randomUUID().toString().replace("-", "");

        String token = uuid+"-"+System.currentTimeMillis()+"-"+loginDto.getUserType();

        vo.setToken(token);

        //把 token 存起来
        SessionUtils.put(token,backend.get(0));

        return vo;
    }
}
