package com.fc.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class AppInfoDto {
  private String  softwarename;
  private String  apkname;
  private Long  categorylevel1;
  private Long  categorylevel2;
  private Long  categorylevel3;
  private Long  flatformid;
  private Long  status;

}
