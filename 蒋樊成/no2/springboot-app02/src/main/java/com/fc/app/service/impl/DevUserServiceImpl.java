package com.fc.app.service.impl;

import com.fc.app.Utils.SessionUtils;
import com.fc.app.dao.DevUserDao;
import com.fc.app.dto.LoginDto;
import com.fc.app.entity.DevUser;
import com.fc.app.service.DevUserService;
import com.fc.app.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;

@Service
public class DevUserServiceImpl implements DevUserService {

    @Autowired
    DevUserDao devUserDao;


    @Override
    public LoginUserVo login(LoginDto loginDto) {

        if (StringUtils.isEmpty(loginDto.getAccount())){
            throw new RuntimeException("账号必填");
        }
        if (StringUtils.isEmpty(loginDto.getPassword())){
            throw new RuntimeException("密码必填");
        }

        DevUser devUser = new DevUser().setDevname(loginDto.getAccount()).setDevpassword(loginDto.getPassword());

        List<DevUser> devUsers = devUserDao.queryAllBy(devUser);

        if (devUsers.size() == 0){
            throw new RuntimeException("账号密码错误");
        }

        LoginUserVo vo = new LoginUserVo();

        vo.setUserCode(devUsers.get(0).getDevcode());
        vo.setUserName(devUsers.get(0).getDevname());

        //生成token 格式 uuid + token 的生成时间
        String uuid = UUID.randomUUID().toString().replace("-", "");

        String token = uuid+"-"+System.currentTimeMillis()+"-"+loginDto.getUserType();

        vo.setToken(token);

        //把 token 存起来
        SessionUtils.put(token,devUsers.get(0));

        return vo;
    }
}
