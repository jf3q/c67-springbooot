package com.fc.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootApp02Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootApp02Application.class, args);
    }

}
