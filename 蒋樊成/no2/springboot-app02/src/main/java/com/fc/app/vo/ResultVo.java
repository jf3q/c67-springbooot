package com.fc.app.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
public class ResultVo {

    private String code;//状态码 2000 成功 5000 失败

    private String message; //提醒

    private Object data;  //携带的数据

    public static ResultVo success(String message,Object data){
        return new ResultVo("2000",message,data);
    }

    public static ResultVo error(String message){
        return new ResultVo("5000",message,null);
    }
}
