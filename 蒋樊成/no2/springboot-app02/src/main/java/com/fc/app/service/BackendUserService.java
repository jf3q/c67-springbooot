package com.fc.app.service;

import com.fc.app.dto.LoginDto;
import com.fc.app.vo.LoginUserVo;

public interface BackendUserService {


    LoginUserVo login(LoginDto loginDto);

}
