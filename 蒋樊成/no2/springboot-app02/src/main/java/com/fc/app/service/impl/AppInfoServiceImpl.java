package com.fc.app.service.impl;

import com.fc.app.dao.AppInfoDao;
import com.fc.app.dao.AppVersionDao;
import com.fc.app.dto.AppInfoDto;
import com.fc.app.entity.AppInfo;
import com.fc.app.entity.AppVersion;
import com.fc.app.service.AppInfoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


import java.io.File;
import java.util.List;

@Service
public class AppInfoServiceImpl implements AppInfoService {

    @Autowired
    AppInfoDao appInfoDao;

    @Autowired
    AppVersionDao versionDao;

    @Override
    public PageInfo<AppInfo> getSelApp(AppInfoDto appInfoDto, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);

        AppInfo appInfo = new AppInfo();
/*
        appInfo.setSoftwarename(appInfoDto.getSoftwarename()).setApkname(appInfoDto.getApkname()).setCategorylevel1(appInfoDto.getCategorylevel1())
                .setCategorylevel2(appInfoDto.getCategorylevel2()).setCategorylevel3(appInfoDto.getCategorylevel3()).setFlatformid(appInfoDto.getFlatformid())
                .setStatus(appInfoDto.getStatus());
*/
        BeanUtils.copyProperties(appInfoDto,appInfo);

        return new PageInfo<AppInfo>(appInfoDao.getSelApp(appInfo));
    }

    @Override
    public void saveOrUpdate(AppInfo appinfo) {
        if (appinfo.getId()==null) {
            appInfoDao.insert(appinfo);
        }else{
            appInfoDao.update(appinfo);
        }
    }

    @Override
    public AppInfo selectById(Long id) {
        return appInfoDao.queryById(id);
    }

    @Override
    public void delAll(Long id, HttpServletRequest request) {

        List<AppVersion> list = versionDao.queryAllBy(new AppVersion().setAppid(id));

        for (AppVersion version : list) {
            if (StringUtils.hasText(version.getDownloadlink())){
                String realPath = request.getServletContext().getRealPath(version.getDownloadlink());

                File file = new File(realPath);

                if (file.exists()){
                    file.delete();
                }

            }
        }

        versionDao.deleteByAllId(id);

        AppInfo appInfo = appInfoDao.queryById(id);
        if (StringUtils.hasText(appInfo.getLogopicpath())) {
            String realPath = request.getServletContext().getRealPath(appInfo.getLogopicpath());

            File file = new File(realPath);

            if (file.exists()){
                file.delete();
            }
        }

        appInfoDao.deleteById(id);
    }

    @Override
    public void getShelves(Long id) {

        AppInfo appInfo = appInfoDao.queryById(id);
        if (appInfo.getStatus() == 2L){
            appInfo.setStatus(5L);
        }else if (appInfo.getStatus() == 4L){
            appInfo.setStatus(5L);
        }else if (appInfo.getStatus() == 5L){
            appInfo.setStatus(4L);
        }

        appInfoDao.update(appInfo);
    }

    @Override
    public void getPass(Long id) {
        AppInfo appInfo = appInfoDao.queryById(id);

        if (appInfo.getStatus() == 1){
            appInfo.setStatus(2L);
        }

        appInfoDao.update(appInfo);
    }

    @Override
    public void getRepulse(Long id) {
        AppInfo appInfo = appInfoDao.queryById(id);

        if (appInfo.getStatus() == 1){
            appInfo.setStatus(3L);
        }

        appInfoDao.update(appInfo);
    }

}
