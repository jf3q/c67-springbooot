package com.fc.app.vo;

import lombok.Data;

@Data
public class LoginUserVo {

    private String userCode;

    private String userName;

    private String token;
}
