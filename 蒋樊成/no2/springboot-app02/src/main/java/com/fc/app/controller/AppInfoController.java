package com.fc.app.controller;

import com.fc.app.Utils.SessionUtils;
import com.fc.app.entity.AppInfo;
import com.fc.app.entity.DevUser;
import com.fc.app.service.AppInfoService;
import com.fc.app.vo.ResultVo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@RestController
public class AppInfoController {

    @Autowired
    AppInfoService appInfoService;


    @PostMapping("/saveOrUpdate")
    public ResultVo saveOrUpdate(AppInfo appinfo, MultipartFile img, HttpServletRequest request){

        String token = request.getHeader("token");

        if (img != null){
            if (!img.isEmpty()){

                String originalFilename = img.getOriginalFilename();

                String substring = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);

                if (img.getSize() > 500 * 1024) {
                    return ResultVo.error("文件大小超过500kb");
                }else if (substring.equalsIgnoreCase("png") || substring.equalsIgnoreCase("jpg")
                        || substring.equalsIgnoreCase("jpeg") || substring.equalsIgnoreCase("gif")){

                    String realPath = request.getServletContext().getRealPath("upload");

                    File file = new File(realPath);

                    if (!file.exists()){
                        file.mkdirs();
                    }

                    String newFileName = UUID.randomUUID().toString().replace("-","")+"."+substring;

                    File saveFile = new File(realPath,newFileName);

                    try {
                        img.transferTo(saveFile);

                        appinfo.setLogopicpath("/upload/"+newFileName);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    return ResultVo.error("文件格式不正确！");
                }

            }
        }

        DevUser devUser = (DevUser) SessionUtils.get(token);

        if (appinfo.getId() == null){
            //新增
            appinfo.setDownloads(0L);

            appinfo.setDevid(devUser.getId());

            appinfo.setCreationdate(new Date());

            appinfo.setStatus(1L);

            appinfo.setCreatedby(devUser.getId());
        }else{
            //修改
            appinfo.setModifyby(devUser.getId());

            appinfo.setModifydate(new Date());

            appinfo.setStatus(1L);
        }

        appInfoService.saveOrUpdate(appinfo);

        return ResultVo.success(null,null);
    }

    @PostMapping("/selectById")
    public ResultVo selectById(Long id){

        AppInfo appInfo = appInfoService.selectById(id);

        return ResultVo.success(null,appInfo);
    }

    @DeleteMapping("delAll/{id}")
    public ResultVo delAll(@PathVariable Long id,HttpServletRequest request){

        try {
            appInfoService.delAll(id,request);
        } catch (Exception e) {
            e.printStackTrace();
            ResultVo.error("删除失败");
        }

        return ResultVo.success(null,null);
    }


    @PutMapping("/getPass/{id}")
    public ResultVo getPass(@PathVariable Long id){
         appInfoService.getPass(id);
        return ResultVo.success(null,null);
    }

    @PutMapping("/getRepulse/{id}")
    public ResultVo getRepulse(@PathVariable Long id){
         appInfoService.getRepulse(id);
        return ResultVo.success(null,null);
    }



}
