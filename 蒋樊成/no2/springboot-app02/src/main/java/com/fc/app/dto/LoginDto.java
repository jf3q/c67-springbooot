package com.fc.app.dto;

import lombok.Data;

@Data
public class LoginDto {

    private String account;

    private String password;

    private Integer userType; // 1：管理员 2：开发者

}
