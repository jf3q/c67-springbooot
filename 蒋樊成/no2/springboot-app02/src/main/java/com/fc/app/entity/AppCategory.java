package com.fc.app.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.io.Serializable;

/**
 * 手游类别(AppCategory)实体类
 *
 * @author makejava
 * @since 2023-11-27 10:27:23
 */
@Data
@Accessors(chain = true)
public class AppCategory implements Serializable {
    private static final long serialVersionUID = -51735847574094852L;
    /**
     * 主键ID
     */
    private Long id;
    /**
     * 分类编码
     */
    private String categorycode;
    /**
     * 分类名称
     */
    private String categoryname;
    /**
     * 父级节点id
     */
    private Long parentid;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    private Long createdby;
    /**
     * 创建时间
     */
    private Date creationtime;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    private Long modifyby;
    /**
     * 最新更新时间
     */
    private Date modifydate;

}

