package com.fc.app.controller;

import com.fc.app.Utils.SessionUtils;
import com.fc.app.dto.AppInfoDto;
import com.fc.app.dto.LoginDto;
import com.fc.app.entity.AppInfo;
import com.fc.app.service.AppInfoService;
import com.fc.app.service.BackendUserService;
import com.fc.app.service.DevUserService;
import com.fc.app.vo.LoginUserVo;
import com.fc.app.vo.ResultVo;
import com.github.pagehelper.PageInfo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
public class LoginController {

    @Autowired
    DevUserService devUserService;

    @Autowired
    BackendUserService backendUserService;

    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginDto loginDto){

        if (loginDto.getUserType() == 1){
            //管理员
            LoginUserVo vo = null;
            try {
                vo = backendUserService.login(loginDto);
            } catch (Exception e) {
                e.printStackTrace();
                return ResultVo.error(e.getMessage());
            }
            return ResultVo.success("登录成功",vo);
        }else if (loginDto.getUserType() == 2){
            //开发者
            LoginUserVo vo = null;
            try {
                vo = devUserService.login(loginDto);
                return ResultVo.success("登录成功",vo);
            } catch (Exception e) {
                e.printStackTrace();
                return ResultVo.error(e.getMessage());
            }
        }else {
            return ResultVo.error("非法请求：没有此角色");
        }

    }

    @GetMapping("/loginOut")
    public ResultVo loginOut(HttpServletRequest request){
        String token = request.getHeader("token");

        SessionUtils.remove(token);
        return ResultVo.success("退出成功",null);
    }

    @Autowired
    AppInfoService appInfoService;

    @PostMapping("/getSelApp")
    public PageInfo<AppInfo> getSelApp(@RequestBody AppInfoDto appInfoDto, @RequestParam(defaultValue = "1") Integer pageNum,@RequestParam(defaultValue = "3") Integer pageSize){

        PageInfo<AppInfo> selApp = appInfoService.getSelApp(appInfoDto,pageNum,pageSize);

        return selApp;
    }

}
