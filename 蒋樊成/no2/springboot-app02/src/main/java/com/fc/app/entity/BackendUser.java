package com.fc.app.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.io.Serializable;

/**
 * (BackendUser)实体类
 *
 * @author makejava
 * @since 2023-11-27 10:27:23
 */
@Data
@Accessors(chain = true)
public class BackendUser implements Serializable {
    private static final long serialVersionUID = -19689815415285854L;
    /**
     * 主键id
     */
    private Long id;
    /**
     * 用户编码
     */
    private String usercode;
    /**
     * 用户名称
     */
    private String username;
    /**
     * 用户角色类型（来源于数据字典表，分为：超管、财务、市场、运营、销售）
     */
    private Long usertype;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    private Long createdby;
    /**
     * 创建时间
     */
    private Date creationdate;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    private Long modifyby;
    /**
     * 最新更新时间
     */
    private Date modifydate;
    /**
     * 用户密码
     */
    private String userpassword;



}

