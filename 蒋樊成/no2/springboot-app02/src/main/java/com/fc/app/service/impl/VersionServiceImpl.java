package com.fc.app.service.impl;

import com.fc.app.dao.AppVersionDao;
import com.fc.app.entity.AppCategory;
import com.fc.app.entity.AppVersion;
import com.fc.app.service.VersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VersionServiceImpl implements VersionService {

    @Autowired
    AppVersionDao appVersionDao;

    @Override
    public List<AppVersion> getVersionList(Long id) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(id);

        return appVersionDao.queryAllBy(appVersion);
    }

    @Override
    public void getSaveVersion(AppVersion appVersion) {
        if (appVersion.getId() == null) {
            appVersionDao.insert(appVersion);
        }else{
            appVersionDao.update(appVersion);
        }
    }
}
