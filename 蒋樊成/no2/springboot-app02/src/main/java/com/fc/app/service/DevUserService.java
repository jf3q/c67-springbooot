package com.fc.app.service;

import com.fc.app.dto.LoginDto;
import com.fc.app.vo.LoginUserVo;

public interface DevUserService {

    LoginUserVo login(LoginDto loginDto);
}
