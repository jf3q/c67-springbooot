package com.fc.app.controller;

import com.fc.app.Utils.SessionUtils;
import com.fc.app.entity.AppInfo;
import com.fc.app.entity.AppVersion;
import com.fc.app.entity.DevUser;
import com.fc.app.service.AppInfoService;
import com.fc.app.service.VersionService;
import com.fc.app.vo.ResultVo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class VersionController {

    @Autowired
    VersionService versionservice;

    @Autowired
    AppInfoService appInfoService;

    @PostMapping("/getVersionList")
    public ResultVo getVersionList(Long id){

        List<AppVersion> versionList = versionservice.getVersionList(id);

        return ResultVo.success(null,versionList);
    }

    @PostMapping("/getSaveVersion")
    public ResultVo saveOrUpdate(AppVersion appVersion, MultipartFile apkImg, HttpServletRequest request){

        String token = request.getHeader("token");

        if (apkImg != null){
            if (!apkImg.isEmpty()){

                String originalFilename = apkImg.getOriginalFilename();

                String substring = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);

                if (apkImg.getSize() > 500 * 1024 * 1024) {
                    return ResultVo.error("文件大小超过500MB");
                }else if (substring.equalsIgnoreCase("apk")){

                    String realPath = request.getServletContext().getRealPath("upload/apk");

                    File file = new File(realPath);

                    if (!file.exists()){
                        file.mkdirs();
                    }

                    String newFileName = UUID.randomUUID().toString().replace("-","")+"."+substring;

                    File saveFile = new File(realPath,newFileName);

                    try {
                        apkImg.transferTo(saveFile);

                        appVersion.setDownloadlink("/upload/apk/"+newFileName);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    return ResultVo.error("文件格式不正确！");
                }

            }
        }

        DevUser devUser = (DevUser) SessionUtils.get(token);

        if (appVersion.getId() == null){
            //新增
            appVersion.setCreatedby(devUser.getId());
            appVersion.setPublishstatus(3L);
            appVersion.setCreationdate(new Date());

        }else{
            //修改
            appVersion.setModifyby(devUser.getId());

            appVersion.setModifydate(new Date());
        }

        versionservice.getSaveVersion(appVersion);

        AppInfo appInfo = new AppInfo();
        appInfo.setId(appVersion.getAppid());
        appInfo.setVersionid(appVersion.getId());

        appInfoService.saveOrUpdate(appInfo);

        return ResultVo.success(null,null);
    }


    @PutMapping("/getShelves/{id}")
    public ResultVo getShelves(@PathVariable Long id){
        try {
            appInfoService.getShelves(id);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.success("操作失败",null);
        }
        return ResultVo.success(null,null);
    }



}
