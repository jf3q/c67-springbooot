package com.lyf.app.service.impl;

import com.lyf.app.dto.LoginDto;
import com.lyf.app.entity.DevUser;
import com.lyf.app.mapper.DevUserDao;
import com.lyf.app.service.DevUserService;
import com.lyf.app.utils.StringBufferUtils;
import com.lyf.app.utils.sessionUtils;
import com.lyf.app.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class DevUserServiceImpl implements DevUserService {


    @Autowired
    DevUserDao devUserDao;

    @Override
    public LoginVo loginUser(LoginDto loginDto) {

        DevUser devUser = new DevUser();
        devUser.setDevcode(loginDto.getAccount());
        devUser.setDevpassword(loginDto.getPasswrod());
        List<DevUser> devUsers = devUserDao.queryAllBy(devUser);
        if (devUsers.size()==0){
            throw new RuntimeException("账号或密码错误！");
        }
        DevUser devUser1 = devUsers.get(0);

        LoginVo loginVo = new LoginVo();
        loginVo.setId(devUser1.getId());
        loginVo.setUserType(loginDto.getUserType());
        loginVo.setUseracount(devUser1.getDevcode());
        loginVo.setUserName(devUser1.getDevname());
        loginVo.setToken(StringBufferUtils.joint(loginVo.getUseracount(),loginDto.getUserType()));
        sessionUtils.put(loginVo.getToken(),loginVo);


        return loginVo;
    }
}
