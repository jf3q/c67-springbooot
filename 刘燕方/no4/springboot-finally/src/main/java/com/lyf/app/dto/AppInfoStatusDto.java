package com.lyf.app.dto;


import lombok.Data;

@Data
public class AppInfoStatusDto {

    private Long id;

    private Long status;
}
