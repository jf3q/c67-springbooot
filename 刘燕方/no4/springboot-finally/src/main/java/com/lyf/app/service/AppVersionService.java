package com.lyf.app.service;

import com.lyf.app.entity.AppVersion;

import java.util.List;

public interface AppVersionService  {

    List<AppVersion> getAppVersionList(Long id);

    int insertAppVersion(AppVersion appVersion);
}
