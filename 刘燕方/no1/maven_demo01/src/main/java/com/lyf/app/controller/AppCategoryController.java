package com.lyf.app.controller;



import com.lyf.app.service.AppCategoryService;
import com.lyf.app.vo.AppCategoryVO;
import com.lyf.app.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class AppCategoryController {

    @Autowired
    AppCategoryService appCategoryService;

    @RequestMapping("/tree")
    public ResultVo tree(){
        AppCategoryVO appCategoryVO = appCategoryService.appCategoryTree();

        return ResultVo.success("",appCategoryVO);
    }



}
