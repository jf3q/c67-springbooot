package com.lyf.app.controller;

import com.github.pagehelper.PageInfo;
import com.lyf.app.dto.AppInfoDto;
import com.lyf.app.dto.AppInfoStatusDto;
import com.lyf.app.entity.AppInfo;
import com.lyf.app.service.AppInfoService;
import com.lyf.app.utils.sessionUtils;
import com.lyf.app.vo.LoginVo;
import com.lyf.app.vo.ResultVo;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/appInfo")
public class AppInfoController {

    @Autowired
    AppInfoService appinfoService;

    @RequestMapping("/page")
    public ResultVo getPage(@RequestBody AppInfoDto appInfoDto, @RequestParam(defaultValue = "1")Integer pageNum, HttpServletRequest request){
        String token = request.getHeader("token");
        if (token!=null){
            LoginVo loginVo = (LoginVo) sessionUtils.get(token);
            appInfoDto.setDevid(loginVo.getId());
            if (loginVo.getId()==null){
                appInfoDto.setStatus(1L);
            }
        }
        PageInfo<AppInfo> page = appinfoService.getPage(appInfoDto, pageNum);
        return ResultVo.success("",page);
    }



    @RequestMapping("/severAppInfo")
    public ResultVo severAppInfo(AppInfo appInfo, MultipartFile file,HttpServletRequest request){
        String realPath = request.getServletContext().getRealPath("/img/Logo");
        File newFile = new File(realPath);
        if (!newFile.exists()){
            newFile.mkdirs();
        }
        if (file!=null){
            if (!file.isEmpty()){
                if (file.getSize()<=500*1024){
                    String originalFilename = file.getOriginalFilename();
                    String extension = FilenameUtils.getExtension(originalFilename);
                    if (  extension.equalsIgnoreCase("jpg")||
                            extension.equalsIgnoreCase("jpeg")||
                            extension.equalsIgnoreCase("png")||
                            extension.equalsIgnoreCase("gif")){
                       String fileName =  UUID.randomUUID()+"."+extension;
                       File saveFile = new File(realPath+File.separator+fileName);
                        try {
                            file.transferTo(saveFile);
                            appInfo.setLogopicpath("/img/Logo/"+fileName);
                        } catch (IOException e) {
                            e.printStackTrace();
                            return ResultVo.error("logo图上传失败");
                        }
                    }
                }else {
                    return ResultVo.error("logo图大于了500MK");
                }

            }else {
                return ResultVo.error("请上传logo图");
            }
        }
        String header = request.getHeader("token");
        LoginVo loginVo = (LoginVo) sessionUtils.get(header);
        if (appInfo.getId()==null){
            appInfo.setDownloads(0L);
            appInfo.setDevid(loginVo.getId());
            appInfo.setStatus(1L);
            appInfo.setCreatedby(loginVo.getId());
            appInfo.setCreationdate(new Date());
        }else {
            appInfo.setModifyby(loginVo.getId());
            appInfo.setModifydate(new Date());
        }
        String ming = appInfo.getId()==null?"新增":"修改";
        int num =  appinfoService.saveUpdate(appInfo);
        if (num>=0){
            return ResultVo.success(ming+"成功!",null);
        }else{
            return  ResultVo.success(ming+"失败!",null);
        }
    }


    @RequestMapping("/getAppInfoId/{id}")
    public ResultVo getAppInfoId(@PathVariable Integer id){
       AppInfo appInfo =  appinfoService.getAppinfoId(id);
       return ResultVo.success("",appInfo);
    }


    @RequestMapping("/delAppInfo/{id}")
    public ResultVo delAppInfo(@PathVariable Integer id,HttpServletRequest request){
       int num =  appinfoService.delAppInfo(id,request);
       if (num>0){
           return ResultVo.success("删除成功!",null);
       }else {
           return ResultVo.error("删除失败!");
       }
    }



    @RequestMapping("/sale/{id}")
    public ResultVo Sale(@PathVariable Integer id){
        int num = appinfoService.sale(id);
        if (num>0){
            return  ResultVo.success("操作成功!",null);
        }else {
            return  ResultVo.error("操作失败!");
        }
    }




    @RequestMapping("/updateAppInfoStatus")
    public ResultVo updateAppInfoStatus(@RequestBody AppInfoStatusDto appInfoStatusDto){
        AppInfo appInfo  = new AppInfo();
        BeanUtils.copyProperties(appInfoStatusDto,appInfo);
        int i = appinfoService.saveUpdate(appInfo);
        if (i>0){
            return ResultVo.success("操作成功!",null);
        }
        return ResultVo.error("操作失败!");
    }
}
