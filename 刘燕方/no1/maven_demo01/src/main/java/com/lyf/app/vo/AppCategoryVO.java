package com.lyf.app.vo;


import lombok.Data;

import java.util.List;

@Data
public class AppCategoryVO {

    private Long id;


    private String categoryname;


    private Long parentid;

    private List<AppCategoryVO> children;

}
