package com.lyf.app.service;

import com.lyf.app.dto.LoginDto;
import com.lyf.app.vo.LoginVo;

public interface BackendUserService  {


    LoginVo loginBackend(LoginDto loginDto);

}
