package com.lyf.app.service;

import com.github.pagehelper.PageInfo;
import com.lyf.app.dto.AppInfoDto;
import com.lyf.app.entity.AppInfo;

import javax.servlet.http.HttpServletRequest;

public interface AppInfoService  {

    PageInfo<AppInfo> getPage(AppInfoDto appInfoDto, Integer pageNum);

    int saveUpdate(AppInfo appInfo);

    AppInfo getAppinfoId(Integer id);

    int delAppInfo(Integer id,HttpServletRequest request);

    int sale(Integer id);
}
