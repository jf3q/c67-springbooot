package com.lyf.springbootbook.entity;

import java.io.Serializable;

/**
 * (Salarylevel)实体类
 *
 * @author makejava
 * @since 2023-12-29 14:15:10
 */
public class Salarylevel implements Serializable {
    private static final long serialVersionUID = 325675242801012213L;

    private String level;

    private Object minsal;

    private Object maxsal;


    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Object getMinsal() {
        return minsal;
    }

    public void setMinsal(Object minsal) {
        this.minsal = minsal;
    }

    public Object getMaxsal() {
        return maxsal;
    }

    public void setMaxsal(Object maxsal) {
        this.maxsal = maxsal;
    }

}

