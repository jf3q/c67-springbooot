package com.lyf.springbootbook.dao;

import com.lyf.springbootbook.entity.Salarylevel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Salarylevel)表数据库访问层
 *
 * @author makejava
 * @since 2023-12-29 14:15:10
 */
@Mapper
public interface SalarylevelDao {

    /**
     * 通过ID查询单条数据
     *
     * @return 实例对象
     */
    Salarylevel queryById();

    /**
     * 查询指定行数据
     *
     * @param salarylevel 查询条件
     * @return 对象列表
     */
    List<Salarylevel> queryAllByLimit(Salarylevel salarylevel);

    /**
     * 统计总行数
     *
     * @param salarylevel 查询条件
     * @return 总行数
     */
    long count(Salarylevel salarylevel);

    /**
     * 新增数据
     *
     * @param salarylevel 实例对象
     * @return 影响行数
     */
    int insert(Salarylevel salarylevel);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Salarylevel> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Salarylevel> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Salarylevel> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<Salarylevel> entities);

    /**
     * 修改数据
     *
     * @param salarylevel 实例对象
     * @return 影响行数
     */
    int update(Salarylevel salarylevel);

    /**
     * 通过主键删除数据
     *
     * @return 影响行数
     */
    int deleteById();

}

