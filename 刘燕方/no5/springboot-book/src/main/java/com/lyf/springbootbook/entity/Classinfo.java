package com.lyf.springbootbook.entity;

import java.io.Serializable;

/**
 * (Classinfo)实体类
 *
 * @author makejava
 * @since 2023-12-29 14:15:09
 */
public class Classinfo implements Serializable {
    private static final long serialVersionUID = 137796311297921077L;

    private Integer classno;

    private String classname;


    public Integer getClassno() {
        return classno;
    }

    public void setClassno(Integer classno) {
        this.classno = classno;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

}

