package com.lyf.springbootbook.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lyf.springbootbook.entity.Book;

public interface BookService extends IService<Book> {

}
