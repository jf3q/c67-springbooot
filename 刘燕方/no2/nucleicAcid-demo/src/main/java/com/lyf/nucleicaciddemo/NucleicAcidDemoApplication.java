package com.lyf.nucleicaciddemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NucleicAcidDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(NucleicAcidDemoApplication.class, args);
    }

}
