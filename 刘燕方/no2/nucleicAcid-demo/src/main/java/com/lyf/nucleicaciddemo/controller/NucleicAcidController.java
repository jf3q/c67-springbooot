package com.lyf.nucleicaciddemo.controller;

import com.lyf.nucleicaciddemo.entity.MedicalAssay;
import com.lyf.nucleicaciddemo.service.NucleicAcidService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/hesuan")
public class NucleicAcidController {


    @Resource(name = "nucleicAcidService")
    NucleicAcidService nucleicAcidService;


    @GetMapping("/list")
    public String getAll(Long id, Model model){
        List<MedicalAssay> list = nucleicAcidService.getList(id);
        model.addAttribute("list",list);
        model.addAttribute("id",id);
        return "list";
    }

    @GetMapping("/toAdd")
    public String toAdd(){
        return "/add";
    }

    @PostMapping
    public String add(MedicalAssay medicalAssay){

        if(nucleicAcidService.add(medicalAssay)>0){
            return "redirect:/hesuan/list";
        }else{
            return "add";
        }

    }

    @PostMapping("/{id}")
    @ResponseBody
    public int edit(@PathVariable Long id){
        nucleicAcidService.update(id);
        return 1;
    }

}
