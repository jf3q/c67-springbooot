package com.lyf.service;

import com.github.pagehelper.PageHelper;
import com.lyf.dao.AppInfoDao;
import com.lyf.dao.AppVersionDao;
import com.lyf.dto.ApkNameDto;
import com.lyf.dto.AppInfoDto;
import com.lyf.entity.AppInfo;
import com.lyf.entity.AppVersion;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.List;

@Service
public class AppInfoService {

    @Autowired
    AppInfoDao appInfoDao;
    @Autowired
    AppVersionDao appVersionDao;

    public List<AppInfo> getPage(AppInfoDto appInfoDto, Integer pageNum) {

        PageHelper.startPage(pageNum,10,"id desc");
        AppInfo appInfo = new AppInfo();
        BeanUtils.copyProperties(appInfoDto,appInfo);

        return appInfoDao.queryAllBy(appInfo);
    }

    public boolean validate(ApkNameDto apkNameDto) {

        if(apkNameDto.getId() != null){
            AppInfo appInfo = appInfoDao.queryById(apkNameDto.getId());
            if(appInfo.getApkname().equals(apkNameDto.getApkName())){
                return true;
            }
        }

        if(appInfoDao.validate(apkNameDto.getApkName())>0){
            return false;
        }

        return true;
    }

    public AppInfo getAppById(Long id) {
        return appInfoDao.queryById(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public void addOrUpdate(AppInfo appInfo) {
        if(appInfo.getId() == null){
            appInfoDao.insert(appInfo);
        }else{
            appInfoDao.update(appInfo);
        }
    }

    public void del(Long id, HttpServletRequest request) {

        //删除对应手游版本所有apk文件
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(id);
        List<AppVersion> list = appVersionDao.queryAllBy(appVersion);
        for (AppVersion version : list) {
            if (StringUtils.hasText(version.getDownloadlink())) {

                String realPath = request.getServletContext().getRealPath(version.getDownloadlink());
                File file1 = new File(realPath);
                if(file1.exists()){
                    file1.delete();
                }
            }
        }

        //删除对应手游所有版本
        appVersionDao.deleteById(id);

        //删除对应手游logo图片
        AppInfo appInfo = appInfoDao.queryById(id);
        if (StringUtils.hasText(appInfo.getLogopicpath())) {

            String realPath = request.getServletContext().getRealPath(appInfo.getLogopicpath());
            File file1 = new File(realPath);
            if(file1.exists()){
                file1.delete();
            }
        }

        //删除对应手游信息
        appInfoDao.deleteById(id);

    }

    public void sale(Long id) {
        AppInfo appInfo = appInfoDao.queryById(id);
        if(appInfo.getStatus() == 4L){
            appInfo.setStatus(5L);
        } else if (appInfo.getStatus() == 5L || appInfo.getStatus() == 2L) {
            appInfo.setStatus(4L);
        }
        appInfoDao.update(appInfo);
    }
}
