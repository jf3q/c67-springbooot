package com.lyf.controller;

import com.lyf.service.CategoryService;
import com.lyf.vo.CategoryTreeVo;
import com.lyf.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @PostMapping("/tree")
    public ResultVo getTree(){
        CategoryTreeVo vo = categoryService.getTree();
        return ResultVo.success("", vo);
    }

}
