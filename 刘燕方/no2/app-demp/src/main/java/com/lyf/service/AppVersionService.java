package com.lyf.service;

import com.lyf.dao.AppVersionDao;
import com.lyf.entity.AppVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppVersionService {
    @Autowired
    AppVersionDao appVersionDao;


    public List<AppVersion> getList(Long id) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(id);
        List<AppVersion> list = appVersionDao.queryAllBy(appVersion);
        return list;
    }

    public int addVersion(AppVersion version) {
        if (appVersionDao.insert(version)>0) {
            return 1;
        }
        return 0;
    }
}
