package com.lyf.service;

import com.lyf.dao.AppCategoryDao;
import com.lyf.entity.AppCategory;
import com.lyf.vo.CategoryTreeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {
    @Autowired
    AppCategoryDao appCategoryDao;


    public CategoryTreeVo getTree() {

        List<CategoryTreeVo> voList = new ArrayList<>();
        List<AppCategory> list = appCategoryDao.queryAllBy(new AppCategory());
        for (AppCategory appCategory : list) {
            CategoryTreeVo categoryTreeVo = new CategoryTreeVo();
            BeanUtils.copyProperties(appCategory,categoryTreeVo);
            voList.add(categoryTreeVo);
        }

        CategoryTreeVo tree = new CategoryTreeVo();
        for (CategoryTreeVo vo : voList) {
            if(vo.getParentid() == null){
                tree = findChildren(vo,voList);
            }
        }

        return tree;
    }

    private CategoryTreeVo findChildren(CategoryTreeVo vo, List<CategoryTreeVo> voList) {
        vo.setChildren(new ArrayList<>());

        for (CategoryTreeVo categoryTreeVo : voList) {
            if(vo.getId() == categoryTreeVo.getParentid()){
                vo.getChildren().add(categoryTreeVo);
                findChildren(categoryTreeVo,voList);
            }
        }

        return vo;
    }
}
