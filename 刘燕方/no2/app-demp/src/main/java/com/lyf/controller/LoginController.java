package com.lyf.controller;

import com.lyf.dto.LoginDto;
import com.lyf.service.LoginService;
import com.lyf.vo.LoginUserVo;
import com.lyf.vo.ResultVo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.lyf.utils.SessionUtils;


@RestController
@RequestMapping("/user")
public class LoginController {

    @Autowired
    LoginService loginService;

    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginDto loginDto){
        LoginUserVo vo = null;
        try {
            vo = loginService.login(loginDto);
            return ResultVo.success("登录成功",vo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
    }

    @PostMapping("/loginOut")
    public ResultVo loginOut(HttpServletRequest request){
        String token = request.getHeader("token");
        SessionUtils.remove(token);
        return ResultVo.success("",null);
    }

}
