package com.lyf.sys;

public class SysConstant {
    public static class UserTypeStr{
        public static final String admin = "admin";
        public static final String dev = "dev";
    }
    public static class UserTypeInt{
        public static final Long admin = 1L;
        public static final Long dev = 2L;
    }

}
