package com.lyf.intecept;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.HandlerInterceptor;
import com.lyf.utils.SessionUtils;

public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String token = request.getHeader("token");
        String requestURI = request.getRequestURI();

        if(token == null){
            response.setStatus(401);
        } else if (SessionUtils.get(token) == null) {
            response.setStatus(403);
        }else {

            String[] split = token.split("-");
            String createTime = split[2];
            if(System.currentTimeMillis() - Long.valueOf(createTime) > 2*3600*1000){
                response.setStatus(403);
                SessionUtils.remove(token);
                return false;
            }

            if(requestURI.indexOf("/appInfo/review") != -1){
                if (split[3].equals("dev")) {
                    response.setStatus(403);
                    return false;
                }
            }

            return true;
        }
        return false;

    }
}
