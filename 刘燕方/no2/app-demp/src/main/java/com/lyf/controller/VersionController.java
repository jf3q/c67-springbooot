package com.lyf.controller;

import com.lyf.entity.AppInfo;
import com.lyf.entity.AppVersion;
import com.lyf.entity.DevUser;
import com.lyf.service.AppInfoService;
import com.lyf.service.AppVersionService;
import com.lyf.vo.ResultVo;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.lyf.utils.SessionUtils;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/version")
public class VersionController {
    @Autowired
    AppVersionService appVersionService;
    @Autowired
    AppInfoService appInfoService;

    @PostMapping("/getList/{id}")
    public ResultVo getList(@PathVariable Long id){
        List<AppVersion> list = appVersionService.getList(id);
        return ResultVo.success("",list);
    }

    @PostMapping("/addVersion")
    public ResultVo addVersion(AppVersion version, MultipartFile file, HttpServletRequest request){
        if (file != null) {
            if (!file.isEmpty()) {
                String originalFilename = file.getOriginalFilename();
                String extension = FilenameUtils.getExtension(originalFilename);
                if(file.getSize() < 1024*500){
                    if(
                            extension.equalsIgnoreCase("apk")
                    ){

                        String realPath = request.getServletContext().getRealPath("/upload/apk");
                        File file1 = new File(realPath);
                        if (!file1.exists()) {
                            file1.mkdirs();
                        }
                        String str = UUID.randomUUID().toString().replace("-", "") + "." + extension;
                        try {
                            file.transferTo(new File(realPath+"/"+str));
                            version.setDownloadlink("/upload/apk/"+str);
                        } catch (IOException e) {
                            e.printStackTrace();
                            return ResultVo.error("apk文件上传失败！");
                        }

                    }else{
                        return ResultVo.error("apk文件格式不正确！");
                    }
                }else{
                    return ResultVo.error("apk文件大小超过500k");
                }
            }
        }

        String token = request.getHeader("token");
        DevUser devUser = (DevUser) SessionUtils.get(token);

        version.setCreatedby(devUser.getId());
        version.setCreationdate(new Date());
        version.setPublishstatus(3L);


        try {
            if (appVersionService.addVersion(version)>0) {
                AppInfo appInfo = new AppInfo();
                appInfo.setStatus(1L);
                appInfo.setVersionid(version.getId());
                appInfo.setId(version.getAppid());
                appInfoService.addOrUpdate(appInfo);
            }
            return ResultVo.success("操作成功",null);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error("操作失败");
        }
    }

}
