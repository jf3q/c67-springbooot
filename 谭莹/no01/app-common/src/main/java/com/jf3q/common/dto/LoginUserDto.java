package com.jf3q.common.dto;

import lombok.Data;

@Data
public class LoginUserDto {
    private String account;
    private String password;
    private Integer userType;//1管理员  2开发者
}
