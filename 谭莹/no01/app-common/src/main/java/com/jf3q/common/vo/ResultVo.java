package com.jf3q.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultVo {
    private String code;//状态码  2000正常 5000异常
    private String message;//消息提醒
    private Object data;//携带的数据


    //成功的
    public static ResultVo success(String message,Object data){
        return new ResultVo("2000",message,data);
    }

    //失败的防范
    public static ResultVo error(String message){
        return new ResultVo("5000",message,null);
    }
}
