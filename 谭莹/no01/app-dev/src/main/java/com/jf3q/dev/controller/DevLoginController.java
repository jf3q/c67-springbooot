package com.jf3q.dev.controller;

import com.jf3q.common.dto.LoginUserDto;
import com.jf3q.common.vo.LoginUserVo;
import com.jf3q.common.vo.ResultVo;
import com.jf3q.dev.service.DevService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dev")
public class DevLoginController {

    @Autowired
    DevService devService;

    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginUserDto userDto){
        LoginUserVo vo= null;
        try {
            vo = devService.login(userDto);
            //用户的基本信息
            //token令牌
            return ResultVo.success("登录成功",vo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }

    }
}
