package com.jf3q.admin.controller;

import com.jf3q.admin.service.AdminService;
import com.jf3q.common.dto.LoginUserDto;
import com.jf3q.common.vo.LoginUserVo;
import com.jf3q.common.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class AdminLoginController {
    @Autowired
    AdminService adminService;

    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginUserDto userDto){
        LoginUserVo vo= null;
        try {
            vo = adminService.login(userDto);
            //用户的基本信息
            //token令牌
            return ResultVo.success("登录成功",vo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }

    }
}
