package cn.ssm.util;

import org.springframework.web.servlet.HandlerInterceptor;


public class LoginIntercept implements HandlerInterceptor {
    @Override
    public boolean preHandle(jakarta.servlet.http.HttpServletRequest request, jakarta.servlet.http.HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        String requestURI = request.getRequestURI();
        if(token==null){
            response.setStatus(401);
        }else if(SessionUtil.get(token)==null){
            response.setStatus(403);
        }else {
            String[] split=token.split("-");
            String time=split[2];
            if(System.currentTimeMillis()-Long.valueOf(time)>2*3600*1000){
                response.setStatus(403);
                SessionUtil.remove(token);
            }
            if(requestURI.equals("/audit")){
                if(split[3].equals("dev")){
                    response.setStatus(406);
                    return false;
                }
            }
            return true;
        }
        return false;
//        return HandlerInterceptor.super.preHandle(request, response, handler);
    }
}
