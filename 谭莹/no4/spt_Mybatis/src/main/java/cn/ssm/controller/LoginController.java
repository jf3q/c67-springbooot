package cn.ssm.controller;

import cn.ssm.dto.LoginDto;
import cn.ssm.service.BackendUserService;
import cn.ssm.service.DevUserService;
import cn.ssm.util.SessionUtil;
import cn.ssm.util.SysContant;
import cn.ssm.vo.LoginVo;
import cn.ssm.vo.ResultVo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class LoginController {
    @Autowired
    BackendUserService backendUserService;
    @Autowired
    DevUserService devUserService;

    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginDto loginDto){
        LoginVo vo=null;
        try {
            if(loginDto.getUserType()== SysContant.UserTypeInt.admin){
                vo=backendUserService.bacLogin(loginDto);
            }else if(loginDto.getUserType()==SysContant.UserTypeInt.dev){
                vo=devUserService.devLogin(loginDto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultVo.success("登录成功",vo);
    }

    @PostMapping("/logout")
    public ResultVo login(HttpServletRequest request){
        String token = request.getHeader("token");
        SessionUtil.remove(token);
        return ResultVo.success("注销成功",null);
    }

}
