package cn.ssm.service;

import cn.ssm.dao.AppInfoDao;
import cn.ssm.dao.AppVersionDao;
import cn.ssm.dto.ApkNameDto;
import cn.ssm.dto.AppInfoDto;
import cn.ssm.entity.AppInfo;
import cn.ssm.entity.AppVersion;
import cn.ssm.util.SysContant;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.io.File;
import java.util.Date;
import java.util.List;

@Service
public class AppInfoService {
    @Autowired
    AppInfoDao appInfoDao;
    @Autowired
    AppVersionDao appVersionDao;


    public PageInfo<AppInfo> getPage(AppInfoDto appInfoDto, Integer pageNum) {
        AppInfo appInfo=new AppInfo();
        BeanUtils.copyProperties(appInfoDto,appInfo);
        PageHelper.startPage(pageNum, SysContant.pageSize,"id desc");
        PageInfo<AppInfo> pageInfo=new PageInfo<>(appInfoDao.queryAllBy(appInfo));
        return pageInfo;
    }

    public Boolean getApk(ApkNameDto apkNameDto) {
        if (apkNameDto.getId()!=null) {
            AppInfo appInfo = appInfoDao.queryById(apkNameDto.getId());
            if (appInfo.getApkname().equals(apkNameDto.getApkname())) {
                return true;
            }
        }

        int apk = appInfoDao.getApk(apkNameDto.getApkname());
        if(apk>0){
            return false;
        }
        return true;
    }

    public void saveOrUpdate(AppInfo appInfo) {
        if(appInfo.getId()==null){
            appInfoDao.insert(appInfo);
        }else {
            appInfoDao.update(appInfo);
        }
    }

    public AppInfo selectById(Long id) {
        return appInfoDao.queryById(id);
    }

    public void upDown(Long id) {
        AppInfo appInfo = appInfoDao.queryById(id);
        if(appInfo.getStatus()==SysContant.status.yes){
            appInfo.setStatus(SysContant.status.on);
            appInfo.setOnsaledate(new Date());
        }else if(appInfo.getStatus()==SysContant.status.off){
            appInfo.setStatus(SysContant.status.on);
            appInfo.setOnsaledate(new Date());
        }else if(appInfo.getStatus()==SysContant.status.on){
            appInfo.setStatus(SysContant.status.off);
            appInfo.setOffsaledate(new Date());
        }
        appInfoDao.update(appInfo);
    }

    public void update(AppInfo appInfo) {
        appInfoDao.update(appInfo);
    }

    public void del(Long id, HttpServletRequest request) {
        AppVersion appVersion=new AppVersion();
        appVersion.setAppid(id);
        List<AppVersion> appVersions = appVersionDao.queryAllBy(appVersion);
        for (AppVersion version : appVersions) {
            if(version.getDownloadlink()!=null){
                String realPath = request.getServletContext().getRealPath(version.getDownloadlink());
                File file=new File(realPath);
                if(file.exists()){
                    file.delete();
                }
            }
        }

        appVersionDao.deleteById(id);

        AppInfo appInfo = appInfoDao.queryById(id);
        if(appInfo.getLogopicpath()!=null){
            String realPath = request.getServletContext().getRealPath(appInfo.getLogopicpath());
            File file=new File(realPath);
            if(file.exists()){
                file.delete();
            }
        }
        appInfoDao.deleteById(id);
    }
}
