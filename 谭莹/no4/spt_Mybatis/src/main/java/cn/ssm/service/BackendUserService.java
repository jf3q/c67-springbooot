package cn.ssm.service;

import cn.ssm.dao.BackendUserDao;
import cn.ssm.dto.LoginDto;
import cn.ssm.entity.BackendUser;
import cn.ssm.util.SessionUtil;
import cn.ssm.util.SysContant;
import cn.ssm.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class BackendUserService {
    @Autowired
    BackendUserDao backendUserDao;
    public LoginVo bacLogin(LoginDto loginDto) {
        LoginVo vo=new LoginVo();
        if(loginDto.getUserType()== SysContant.UserTypeInt.admin){
            BackendUser backendUser=new BackendUser();
            backendUser.setUsercode(loginDto.getAccount());
            backendUser.setUserpassword(loginDto.getPassword());

            List<BackendUser> users = backendUserDao.queryAllBy(backendUser);
            if(users.size()==0){
                throw new RuntimeException("账号或者密码错误");
            }

            vo.setAccount(loginDto.getAccount());
            vo.setUserType(SysContant.UserTypeStr.admin);
            StringBuffer buffer=new StringBuffer();
            buffer.append(UUID.randomUUID().toString().replace("-","")+"-");
            buffer.append(loginDto.getAccount()+"-");
            buffer.append(System.currentTimeMillis()+"-");
            buffer.append(SysContant.UserTypeStr.admin+"-");
            buffer.append(users.get(0).getId());
            vo.setToken(buffer.toString());
            SessionUtil.put(vo.getToken(),users.get(0));
            return vo;
        }else {
            throw new RuntimeException("账号类型错误");
        }
    }
}
