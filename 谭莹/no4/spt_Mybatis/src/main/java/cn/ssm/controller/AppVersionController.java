package cn.ssm.controller;

import cn.ssm.entity.AppInfo;
import cn.ssm.entity.AppVersion;
import cn.ssm.entity.DevUser;
import cn.ssm.service.AppInfoService;
import cn.ssm.service.AppVersionService;
import cn.ssm.util.SessionUtil;
import cn.ssm.util.SysContant;
import cn.ssm.vo.ResultVo;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class AppVersionController {
    @Autowired
    AppVersionService appVersionService;
    @Autowired
    AppInfoService appInfoService;

    //版本列表
    @PostMapping("/versionList")
    public ResultVo versionList(Long appid) {
        List<AppVersion> appVersions = appVersionService.versionList(appid);
        return ResultVo.success(null, appVersions);
    }

    //添加
    @PostMapping("/saveVersion")
    public ResultVo savelVersion(HttpServletRequest request, MultipartFile apkfile, AppVersion appVersion) {
        String token = request.getHeader("token");
        if (apkfile != null && !apkfile.isEmpty()) {
            String realPath = request.getServletContext().getRealPath("/file/apkFile");
            File f = new File(realPath);
            if (!f.exists()) {
                f.mkdirs();
            }
            String originalFilename = apkfile.getOriginalFilename();
            String extension = FilenameUtils.getExtension(originalFilename);
            if (apkfile.getSize() > 500 * 1024*1024) {
                return ResultVo.error("文件格式过大");
            } else if (extension.equals("apk")) {
                String newName = UUID.randomUUID().toString().replace("-", "");
                File newFile = new File(realPath + File.separator + newName + "." + extension);
                try {
                    apkfile.transferTo(newFile);
                    appVersion.setDownloadlink("/file/apkFile/" + newName + "." + extension);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                return ResultVo.error("文件格式不正确");
            }
        }

        DevUser devUser= (DevUser) SessionUtil.get(token);
        if(appVersion.getId()==null){
            appVersion.setCreatedby(devUser.getId());
            appVersion.setCreationdate(new Date());
            appVersion.setPublishstatus(SysContant.status.pre);
        }
        appVersionService.insert(appVersion);

        AppInfo appInfo = new AppInfo();
        appInfo.setStatus(1L);
        appInfo.setId(appVersion.getAppid());
        appInfo.setVersionid(appVersion.getId());

        appInfoService.update(appInfo);
        return ResultVo.success("添加成功", null);
    }
}
