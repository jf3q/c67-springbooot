package cn.ssm.dto;

public class ApkNameDto {
    private Long id;
    private String apkname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getApkname() {
        return apkname;
    }

    public void setApkname(String apkname) {
        this.apkname = apkname;
    }
}
