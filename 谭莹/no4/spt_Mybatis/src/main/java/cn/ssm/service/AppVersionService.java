package cn.ssm.service;

import cn.ssm.dao.AppVersionDao;
import cn.ssm.entity.AppVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppVersionService {
    @Autowired
    AppVersionDao appVersionDao;

    public List<AppVersion> versionList(Long appid) {
        AppVersion appVersion=new AppVersion();
        appVersion.setAppid(appid);
        return appVersionDao.queryAllBy(appVersion);
    }

    public void insert(AppVersion appVersion) {
        appVersionDao.insert(appVersion);
    }
}
