package cn.ssm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//启动类
@SpringBootApplication
@MapperScan("cn.ssm.dao")
public class SptMybatisApplication {
    public static void main(String[] args) {
        SpringApplication.run(SptMybatisApplication.class, args);
    }
}
