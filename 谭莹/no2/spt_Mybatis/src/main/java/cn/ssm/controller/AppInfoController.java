package cn.ssm.controller;

import cn.ssm.dto.ApkNameDto;
import cn.ssm.dto.AppInfoDto;
import cn.ssm.entity.AppInfo;
import cn.ssm.entity.DevUser;
import cn.ssm.service.AppCategoryService;
import cn.ssm.service.AppInfoService;
import cn.ssm.util.SessionUtil;
import cn.ssm.vo.CategoryVo;
import cn.ssm.vo.ResultVo;
import com.github.pagehelper.PageInfo;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@RestController
public class AppInfoController {
    @Autowired
    AppCategoryService appCategoryService;
    @Autowired
    AppInfoService appInfoService;

    //三级
    @PostMapping("/tree")
    public ResultVo tree() {
        CategoryVo tree = appCategoryService.getTree();
        return ResultVo.success(null, tree);
    }

    //分页
    @PostMapping("/page")
    public ResultVo page(@RequestBody AppInfoDto appInfoDto, @RequestParam(defaultValue = "1") Integer pageNum, HttpServletRequest request) {
        String token = request.getHeader("token");
        String[] split = token.split("-");
        String userType = split[3];
        String userId = split[4];
        if (userType.equals("dev")) {
            appInfoDto.setDevid(Long.valueOf(userId));
        } else {
            appInfoDto.setStatus(1L);
        }
        PageInfo<AppInfo> page = appInfoService.getPage(appInfoDto, pageNum);
        return ResultVo.success(null, page);
    }

    //判断apk
    @PostMapping("/isOnlyOne")
    public ResultVo isOnlyOne(@RequestBody ApkNameDto apkNameDto) {
        Boolean apk = appInfoService.getApk(apkNameDto);
        if (apk == true) {
            return ResultVo.success(null, null);
        } else {
            return ResultVo.error(null);
        }
    }

    //根据id查询数据
    @PostMapping("/selectById")
    public ResultVo selectById(Long id) {
        AppInfo appInfo = appInfoService.selectById(id);
        return ResultVo.success(null, appInfo);
    }

    //添加和修改
    @PostMapping("/saveOrUpdate")
    public ResultVo saveOrUpdate(HttpServletRequest request, MultipartFile file, AppInfo appInfo) {
        String token = request.getHeader("token");
        if (file != null && !file.isEmpty()) {
            String realPath = request.getServletContext().getRealPath("/file/appInfo");
            File f = new File(realPath);
            if (!f.exists()) {
                f.mkdirs();
            }
            String originalFilename = file.getOriginalFilename();
            String extension = FilenameUtils.getExtension(originalFilename);
            if (file.getSize() > 500 * 1024) {
                return ResultVo.error("文件格式过大");
            } else if (extension.equalsIgnoreCase("png") ||
                    extension.equalsIgnoreCase("jpg") ||
                    extension.equalsIgnoreCase("jpeg") ||
                    extension.equalsIgnoreCase("gif")) {
                String newName = UUID.randomUUID().toString().replace("-", "");
                File newFile = new File(realPath + File.separator + newName + "." + extension);
                try {
                    file.transferTo(newFile);
                    appInfo.setLogopicpath("/file/appInfo/" + newName + "." + extension);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                return ResultVo.error("文件格式不正确");
            }
        }

        DevUser devUser = (DevUser) SessionUtil.get(token);
        if (appInfo.getId() == null) {
            appInfo.setStatus(1L);
            appInfo.setCreatedby(devUser.getId());
            appInfo.setCreationdate(new Date());
            appInfo.setDevid(devUser.getId());
            appInfo.setDownloads(0L);
            appInfoService.saveOrUpdate(appInfo);
            return ResultVo.success("添加成功", null);
        } else {
            appInfo.setStatus(1L);
            appInfo.setModifydate(new Date());
            appInfo.setModifyby(devUser.getId());
            appInfoService.saveOrUpdate(appInfo);
            return ResultVo.success("修改成功", null);
        }
    }

    //上下架
    @PostMapping("/upDown")
    public ResultVo upDown(Long id) {
        appInfoService.upDown(id);
        return ResultVo.success("操作成功", null);
    }

    //删除
    @PostMapping("/del")
    public ResultVo del(Long id, HttpServletRequest request) {
        appInfoService.del(id, request);
        return ResultVo.success("删除成功", null);
    }

    //审核
    @PostMapping("/audit")
    public ResultVo audit(Long id, Long status) {
        AppInfo appInfo = new AppInfo();
        appInfo.setId(id);
        appInfo.setStatus(status);
        appInfoService.update(appInfo);
        return ResultVo.success("操作成功", null);
    }
}
