package cn.ssm;

import cn.ssm.dao.AppInfoDao;
import cn.ssm.entity.AppInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SptMybatisApplicationTests {

    @Autowired
    AppInfoDao appInfoDao;

    @Test
    void contextLoads() {
        AppInfo appInfo=new AppInfo();
        appInfo.setId(Long.valueOf(48));
        System.out.println(appInfoDao.queryById(appInfo.getId()));
    }

}
