package spt.plus.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import spt.plus.entity.Student;

public interface IStudentService extends IService<Student>{
    //分页
    Page<Student> getPage(Integer pageNum,String studentname);
}
