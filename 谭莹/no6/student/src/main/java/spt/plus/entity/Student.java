package spt.plus.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    @TableId(value = "id", type = IdType.AUTO)
	/*注解@TableId表示实体类中对应数据库表的主键的属性
	type= IdType.AUTO表示主键由数据库自增长*/
    private Integer id;
    private String studentname;
    private String gender;
    private int age;
    private String address;

    //@TableField(exist = false)代表以下属性只为展示使用，不保存数据库
    @TableField(exist = false)
    private String className;
}
