package spt.plus.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import spt.plus.entity.Student;
import spt.plus.service.IStudentService;
import spt.plus.util.ResultVo;

@RestController
public class StudentController {
    @Autowired
    IStudentService iStudentService;

    //分页
    @GetMapping("/page")
    public ResultVo findPage(@RequestParam(defaultValue = "1") Integer pageNum,String studentname) {
        Page<Student> page= iStudentService.getPage(pageNum,studentname);
        return ResultVo.success(null,page);
    }


    //添加或修改
    @PostMapping("/saveOrUpdate")
    public ResultVo saveOrUpdate(@RequestBody Student student) {
        iStudentService.saveOrUpdate(student);
        return ResultVo.success("操作成功",null);
    }


    //删除
    @DeleteMapping("/{id}")
    public ResultVo delete(@PathVariable Integer id) {
        iStudentService.removeById(id);
        return ResultVo.success("删除成功",null);
    }
}
