package spt.plus.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spt.plus.entity.Student;
import spt.plus.mapper.StudentMapper;
import spt.plus.service.IStudentService;

import java.awt.print.Book;


@Service
public class IStudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {
    /*
    方式一：使用mapper
    @Autowired
    StudentMapper studentMapper;
    方式二：直接使用service里面的page方法 this.page
    下面这种方式就是方式二
     */
    @Override
    public Page<Student> getPage(Integer pageNum,String studentname) {
        Page page=new Page(pageNum,3);
        LambdaQueryWrapper<Student> queryWrapper=new LambdaQueryWrapper<>();
        if(studentname !=null && studentname!=""){
            queryWrapper.like(Student::getStudentname,studentname);
        }
        //降序排序
        queryWrapper.orderByDesc(Student::getId);
        return this.page(page,queryWrapper);
    }
}
