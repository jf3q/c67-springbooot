package spt.plus.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultVo<T> {
    private Integer code;
    private String message;
    private T data;

    //成功
    public static<T> ResultVo success(String message,T data){
        return new ResultVo(200,message,data);
    }
    //失败
    public static<T> ResultVo error(String message){
        return new ResultVo(500,message,null);
    }
}