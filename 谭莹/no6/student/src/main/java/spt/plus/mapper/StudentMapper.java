package spt.plus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import spt.plus.entity.Student;

public interface StudentMapper extends BaseMapper<Student> {
}
