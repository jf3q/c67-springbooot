package spt.plus.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import spt.plus.entity.Book;

public interface IBookService extends IService<Book> {

    //分页
    IPage getPage(Book book, Integer pageNum);

    //回显
    Book getInfo(Integer id);

    //添加或者修改
    void addOrUpdate(Book book);

    //删除
    void delBook(String[] id);
}
