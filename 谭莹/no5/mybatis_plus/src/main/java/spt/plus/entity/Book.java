package spt.plus.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {
	@TableId(value = "id", type = IdType.AUTO)
	/*注解@TableId表示实体类中对应数据库表的主键的属性
	type= IdType.AUTO表示主键由数据库自增长*/
	private Integer id;

	//@TableField 跟数据库字段名对应即可
	@TableField("name")
	private String name;
	private double price;
	private String category;
	private int pnum;
	private String imgurl;
	private String description;
	private String author;
	private int sales;
}
