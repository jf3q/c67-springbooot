package spt.plus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import spt.plus.entity.Book;

//这里面可以不定义任何方法，因为父接口BaseMapper已经包含了基本的增删改查方法，在业务层中调用即可
public interface BookMapper extends BaseMapper<Book> {
}
