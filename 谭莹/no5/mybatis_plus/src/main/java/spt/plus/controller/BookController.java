package spt.plus.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import spt.plus.entity.Book;
import spt.plus.service.IBookService;

import java.util.HashMap;
import java.util.Map;

@Controller
public class BookController {
    @Autowired
    IBookService iBookService;

    //分页查询
    @RequestMapping("/page")
    public String page(@RequestParam(defaultValue = "1") Integer pageNum, Model model, Book book){
        IPage page = iBookService.getPage(book, pageNum);
        model.addAttribute("page",page);
        model.addAttribute("name",book.getName());
        model.addAttribute("author",book.getAuthor());
        return "index";
    }

    //回显
    @RequestMapping("/info")
    public String info(@RequestParam(defaultValue = "0") Integer id,Model model){
        Book info=new Book();
        if(id!=0 && id!=null){
            info=iBookService.getInfo(id);
        }
        model.addAttribute("show",info);
        return "info";
    }

    //新增or修改
    @RequestMapping("/addOrUpdate")
    public String addOrUpdate(Book book){
        iBookService.addOrUpdate(book);
        return "redirect:page";
    }

    //删除
    @RequestMapping("/del")
    public String del(String id) {
        //因为前端传的数据是有逗号分割的，所以后台对它进行拆分，让它变成一个数组的形式
        String[] split = id.split(",");
        iBookService.delBook(split);
        return "redirect:page";
    }
}
