package com.bdqn.admin.controller;


import com.bdqn.admin.service.LoginService;
import com.bdqn.common.dto.LoginDto;
import com.bdqn.common.vo.LoginUserVo;
import com.bdqn.common.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    @Autowired
    private LoginService loginService;       //开发者

    //登录接口
    @PostMapping("/loginUser")
    public ResultVo login(@RequestBody LoginDto loginDto) {
        LoginUserVo vo = null;
        try {
            vo = loginService.loginDevUser(loginDto);
            return ResultVo.success("登录成功", vo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
    }
}
