package com.bdqn.admin.service;

import com.bdqn.common.dto.LoginDto;
import com.bdqn.common.vo.LoginUserVo;

public interface LoginService {
    /**
     * 用户登录
     * @param loginDto
     * @return
     */
    LoginUserVo loginDevUser(LoginDto loginDto);
}
