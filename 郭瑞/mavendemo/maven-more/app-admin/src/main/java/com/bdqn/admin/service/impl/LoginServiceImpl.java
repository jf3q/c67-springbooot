package com.bdqn.admin.service.impl;
import com.bdqn.admin.service.LoginService;
import com.bdqn.common.dao.BackendUserDao;
import com.bdqn.common.dto.LoginDto;
import com.bdqn.common.entity.BackendUser;
import com.bdqn.common.sys.SessionUtils;
import com.bdqn.common.sys.SysConstant;
import com.bdqn.common.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private BackendUserDao backendUserDao;
    /**
     * 用户登录
     *
     * @param loginDto
     * @return
     */
    public LoginUserVo loginDevUser(LoginDto loginDto) {
        LoginUserVo loginUserVo = new LoginUserVo();
        BackendUser backendUser= new BackendUser().setUserCode(loginDto.getAccount()).setUserPassword(loginDto.getPassword());
        List<BackendUser> backendUsers = backendUserDao.queryAllBy(backendUser);
        if (backendUsers.size()==0) {
            throw new RuntimeException("账户密码错误");
        }
        loginUserVo.setAccount(loginDto.getAccount());
        loginUserVo.setUserType(SysConstant.UserTypeStr.admin);
        //生成token 格式  uuid-account-id-userType-createTime
        StringBuffer buffer = new StringBuffer();
        buffer.append(UUID.randomUUID().toString().replace("-","")+"-");
        buffer.append(loginDto.getAccount()+"-");
        buffer.append(backendUsers.get(0).getId()+"-");
        buffer.append(SysConstant.UserTypeStr.admin+"-");
        buffer.append(System.currentTimeMillis());
        loginUserVo.setToKen(buffer.toString());
        //模拟redis    当发生请求过来时效验
        SessionUtils.put(buffer.toString(),loginUserVo);
        return loginUserVo;
    }
}
