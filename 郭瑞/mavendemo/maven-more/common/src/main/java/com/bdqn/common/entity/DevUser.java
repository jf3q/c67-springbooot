package com.bdqn.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * (DevUser)实体类
 *
 * @author makejava
 * @since 2023-11-24 11:17:47
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DevUser implements Serializable {
    private static final long serialVersionUID = 118959972432676041L;
    /**
     * 主键id
     */
    private Integer id;
    /**
     * 开发者帐号
     */
    private String devCode;
    /**
     * 开发者名称
     */
    private String devName;
    /**
     * 开发者密码
     */
    private String devPassword;
    /**
     * 开发者电子邮箱
     */
    private String devEmail;
    /**
     * 开发者简介
     */
    private String devInfo;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    private Integer createdBy;
    /**
     * 创建时间
     */
    private Date creationDate;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    private Integer modifyBy;
    /**
     * 最新更新时间
     */
    private Date modifyDate;
}

