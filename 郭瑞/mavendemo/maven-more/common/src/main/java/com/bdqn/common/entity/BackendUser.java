package com.bdqn.common.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class BackendUser {
    private Integer id;
    private String userCode;
    private String userName;
    private Integer userType;
    private Integer createdBy;
    private Date creationDate;
    private long modifyBy;
    private Date modifyDate;
    private String userPassword;
}
