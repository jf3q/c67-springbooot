package com.bdqn.dev.service.impl;
import com.bdqn.common.dao.DevUserDao;
import com.bdqn.common.dto.LoginDto;
import com.bdqn.common.entity.DevUser;
import com.bdqn.common.sys.SessionUtils;
import com.bdqn.common.sys.SysConstant;
import com.bdqn.common.vo.LoginUserVo;
import com.bdqn.dev.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private DevUserDao devUserDao;
    /**
     * 用户登录
     *
     * @param loginDto
     * @return
     */
    public LoginUserVo loginDevUser(LoginDto loginDto) {
        LoginUserVo loginUserVo = new LoginUserVo();
            DevUser devUser = new DevUser().setDevCode(loginDto.getAccount()).setDevPassword(loginDto.getPassword());
            List<DevUser> devUsers = devUserDao.loginDevUser(devUser);
            if (devUsers.size() == 0) {
                throw new RuntimeException("账户密码错误");
            }
            loginUserVo.setAccount(loginDto.getAccount());
            loginUserVo.setUserType(SysConstant.UserTypeStr.dev);
            //生成token 格式  uuid-account-id-userType-createTime的生成
            StringBuffer buffer = new StringBuffer();
            String uuid = UUID.randomUUID().toString().replace("-", "");
            buffer.append(uuid + "-");
            buffer.append(devUsers.get(0).getDevCode()+"-");    //拿到开发者账户
            buffer.append(devUsers.get(0).getId()+"-");        //拿到开发者id
            buffer.append(SysConstant.UserTypeStr.dev+"-");     //拿到dev
            buffer.append(System.currentTimeMillis());    //时间
            loginUserVo.setToKen(buffer.toString());
            SessionUtils.put(buffer.toString(),loginUserVo);
            return loginUserVo;
    }
}
