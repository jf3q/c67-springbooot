package com.bdqn.app.service;

import com.bdqn.app.dto.LoginDto;
import com.bdqn.app.vo.LoginUserVo;

public interface LoginService {

    /**
     * 用户登录
     * @param loginDto
     * @return
     */
    LoginUserVo loginDevUser(LoginDto loginDto);
}
