package com.bdqn.app.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class DataDictionary {

  private Integer id;
  private String typeCode;
  private String typeName;
  private Integer valueId;
  private String valueName;
  private Integer createdBy;
  private Date creationDate;
  private long modifyBy;
  private Date modifyDate;


}
