package com.bdqn.app.controller;

import com.bdqn.app.dto.LoginDto;
import com.bdqn.app.service.LoginService;
import com.bdqn.app.sys.SessionUtils;
import com.bdqn.app.vo.LoginUserVo;
import com.bdqn.app.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;       //开发者

    //注销
    @RequestMapping("/logout")
    public ResultVo  logout(HttpServletRequest request){
        String token = request.getHeader("token");

        //效验token是否存在
        SessionUtils.remove(token);
        return ResultVo.success("退出成功",null);
    }


    //登录接口
    @PostMapping("/loginUser")
    public ResultVo login(@RequestBody LoginDto loginDto) {
        LoginUserVo vo = null;
        try {
            vo = loginService.loginDevUser(loginDto);
            return ResultVo.success("登录成功", vo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
    }
}
