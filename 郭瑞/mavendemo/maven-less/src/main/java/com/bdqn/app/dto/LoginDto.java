package com.bdqn.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
//前端页面传过来的属性
public class LoginDto {
    private String account;     //账户
    private String password;
    private Integer userType;  //人物  1，管理员  2.开发者
}
