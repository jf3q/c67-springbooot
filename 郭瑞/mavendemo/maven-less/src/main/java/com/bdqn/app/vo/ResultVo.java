package com.bdqn.app.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/*后端传前端
* 通过结果返回
* */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultVo {
    private String code;   //状态  2000 成功  5000失败
    private String message;    //提示
    private Object data;   //携带数据

    //返回成功
    public static ResultVo success(String message,Object data){
        return new ResultVo("2000",message,data);
    }

    //返回失败
    public static ResultVo error(String message){
        return new ResultVo("5000",message,null);
    }
}
