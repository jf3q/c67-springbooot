package com.bdqn.app.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class AppCategory {

  private Integer id;
  private String categoryCode;
  private String categoryName;
  private Integer parentId;
  private Integer createdBy;
  private Date creationTime;
  private Integer modifyBy;
  private Date modifyDate;

}
