package com.bdqn.app.sys;

import java.util.HashMap;
import java.util.Map;

public class SessionUtils {
    static Map<String, Object> map = new HashMap();

    //把token存起来   loginUserVo登录者的值
    public static void put(String token, Object loginUserVo) {
        map.put(token, loginUserVo);
    }

    //效验数据
    public static Object get(String token) {
        return map.get(token);
    }

    //删除token
    public static void remove(String token){
        map.remove(token);
    }
}
