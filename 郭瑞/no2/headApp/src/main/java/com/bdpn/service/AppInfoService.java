package com.bdpn.service;

import com.bdpn.dto.AppInfoDto;
import com.bdpn.entity.AppInfo;
import com.github.pagehelper.PageInfo;

import javax.servlet.http.HttpServletRequest;

public interface AppInfoService {
    /**
     * 分页查询
     * @param appInfo
     * @return
     */
    PageInfo<AppInfo> page(AppInfoDto appInfo, Integer pageNum);

    /**
     * 新增或者修改手游信息
     */
    int saveOrUpdate(AppInfo appInfo);

    /**
     * 通过手游id查看手游信息
     * @param id
     * @return
     */
    AppInfo selectId(Integer id);

    /**
     * 验证apk是否重复
     * @param apkName
     * @return
     */
    int OnSelect(String apkName);

    /**
     * 删除手游版本
     * @param appId
     * @return
     */
    void del(Integer appId, HttpServletRequest request);

    void saleApps(Integer id);
}

