package com.bdpn.vo;

import lombok.Data;

import java.util.List;

@Data
public class CategoryTreeVo {
    private Integer id;
    private String categoryName;
    private Integer parentId;

    List<CategoryTreeVo> categoryTreeVoList;
}
