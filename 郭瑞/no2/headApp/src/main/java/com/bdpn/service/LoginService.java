package com.bdpn.service;


import com.bdpn.dto.LoginDto;
import com.bdpn.vo.LoginUserVo;

public interface LoginService {

    /**
     * 用户登录
     * @param loginDto
     * @return
     */
    LoginUserVo loginDevUser(LoginDto loginDto);
}
