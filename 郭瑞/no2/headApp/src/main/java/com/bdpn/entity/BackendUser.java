package com.bdpn.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "管理员",description = "管理员的操作")
public class BackendUser {
    @ApiModelProperty(value = "管理员主键")
    private Integer id;
    @ApiModelProperty(value = "管理员编码")
    private String userCode;
    @ApiModelProperty(value = "管理员名称")
    private String userName;
    @ApiModelProperty(value = "管理员角色类型")
    private Integer userType;
    @ApiModelProperty(value = "创建者")
    private Integer createdBy;
    @ApiModelProperty(value = "创建时间")
    private Date creationDate;
    @ApiModelProperty(value = "更新者")
    private long modifyBy;
    @ApiModelProperty(value = "最新更新时间")
    private Date modifyDate;
    @ApiModelProperty(value = "管理员密码")
    private String userPassword;
}
