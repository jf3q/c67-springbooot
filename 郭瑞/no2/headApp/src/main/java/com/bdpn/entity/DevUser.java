package com.bdpn.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * (DevUser)实体类
 *
 * @author makejava
 * @since 2023-11-24 11:17:47
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "开发者",description = "开发者的操作")
public class DevUser implements Serializable {
    private static final long serialVersionUID = 118959972432676041L;
    /**
     * 主键id
     */
    @ApiModelProperty(value = "开发者主键")
    private Integer id;
    /**
     * 开发者帐号
     */
    @ApiModelProperty(value = "开发者帐号")
    private String devCode;
    /**
     * 开发者名称
     */
    @ApiModelProperty(value = "开发者名称")
    private String devName;
    /**
     * 开发者密码
     */
    @ApiModelProperty(value = "开发者密码")
    private String devPassword;
    /**
     * 开发者电子邮箱
     */
    @ApiModelProperty(value = "开发者电子邮箱")
    private String devEmail;
    /**
     * 开发者简介
     */
    @ApiModelProperty(value = "开发者简介")
    private String devInfo;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    @ApiModelProperty(value = "创建者")
    private Integer createdBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Date creationDate;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    @ApiModelProperty(value = "更新者")
    private Integer modifyBy;
    /**
     * 最新更新时间
     */
    @ApiModelProperty(value = "最新更新时间")
    private Date modifyDate;
}

