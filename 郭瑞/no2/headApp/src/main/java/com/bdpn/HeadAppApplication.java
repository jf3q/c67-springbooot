package com.bdpn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeadAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(HeadAppApplication.class, args);
    }

}
