package com.bdpn.service;


import com.bdpn.vo.CategoryTreeVo;

public interface AppCategoryService {
    CategoryTreeVo list();
}
