package com.bdpn.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "版本信息",description = "版本的操作")
public class AppVersion {
    @ApiModelProperty(value = "版本id")
    private Integer id;
    @ApiModelProperty(value = "appId")
    private Integer appId;
    @ApiModelProperty(value = "版本号")
    private String versionNo;
    @ApiModelProperty(value = "版本介绍")
    private String versionInfo;
    @ApiModelProperty(value = "发布状态（1 不发布 2 已发布 3 预发布）")
    private Integer publishStatus;
    @ApiModelProperty(value = "下载链接")
    private String downloadLink;
    @ApiModelProperty(value = "版本大小（单位：M）")
    private double versionSize;
    @ApiModelProperty(value = "创建者")
    private Integer createdBy;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date creationDate;
    @ApiModelProperty(value = "更新者")
    private long modifyBy;
    @ApiModelProperty(value = "最新更新时间")
    private Date modifyDate;
    @ApiModelProperty(value = "apk文件的服务器存储路径")
    private String apkLocPath;
    @ApiModelProperty(value = "上传的apk文件名称")
    private String apkFileName;

}
