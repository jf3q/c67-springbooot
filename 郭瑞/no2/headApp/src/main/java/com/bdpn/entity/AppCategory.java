package com.bdpn.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ApiModel(value = "三级联动",description = "三级联动的操作")
public class AppCategory {
  @ApiModelProperty(value = "主键")
  private Integer id;
  @ApiModelProperty(value = "分类编码")
  private String categoryCode;
  @ApiModelProperty(value = "分类名称")
  private String categoryName;
  @ApiModelProperty(value = "父级节点id")
  private Integer parentId;
  @ApiModelProperty(value = "创建者")
  private Integer createdBy;
  @ApiModelProperty(value = "创建时间")
  private Date creationTime;
  @ApiModelProperty(value = "更新者")
  private Integer modifyBy;
  @ApiModelProperty(value = "最新更新时间")
  private Date modifyDate;

}
