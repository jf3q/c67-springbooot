package com.bdqn.heshuan.service;

import com.bdqn.heshuan.entity.Hospital;

import java.util.List;

public interface HospitalService {
    List<Hospital> hospitalList();
}
