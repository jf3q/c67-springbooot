package com.bdqn.heshuan.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * (MedicalAssay)实体类
 *
 * @author makejava
 * @since 2024-01-09 14:56:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class MedicalAssay implements Serializable {
    private static final long serialVersionUID = 821993697894282781L;

    private Integer id;

    private String assayUser;

    private Long hospitalId;

    private Integer assayResult;

    private String phone;

    private String cardNum;

    private String hospitalName;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date assayTime;

}

