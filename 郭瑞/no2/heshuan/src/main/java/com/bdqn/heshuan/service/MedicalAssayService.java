package com.bdqn.heshuan.service;

import com.bdqn.heshuan.entity.MedicalAssay;

import java.util.List;

public interface MedicalAssayService {
    List<MedicalAssay> all(Long hospitalId);
    void add(MedicalAssay medicalAssay);
    void update(Integer id);
}
