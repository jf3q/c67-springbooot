package com.bdqn.heshuan.service.impl;

import com.bdqn.heshuan.dao.HospitalDao;
import com.bdqn.heshuan.entity.Hospital;
import com.bdqn.heshuan.service.HospitalService;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class HospitalServiceImpl implements HospitalService {
    @Resource
    HospitalDao hospitalDao;
    @Override
    public List<Hospital> hospitalList() {
        return hospitalDao.queryAllBy(new Hospital());
    }
}
