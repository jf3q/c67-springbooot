package com.bdqn.heshuan.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Hospital)实体类
 *
 * @author makejava
 * @since 2024-01-09 14:56:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Hospital implements Serializable {
    private static final long serialVersionUID = 282085479061673003L;

    private Long id;

    private String name;

}

