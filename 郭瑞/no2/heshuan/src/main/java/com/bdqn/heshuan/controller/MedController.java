package com.bdqn.heshuan.controller;

import com.bdqn.heshuan.entity.MedicalAssay;
import com.bdqn.heshuan.service.HospitalService;
import com.bdqn.heshuan.service.MedicalAssayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class MedController {
    @Autowired
    HospitalService hospitalService;
    @Autowired
    MedicalAssayService medicalAssayService;
    @GetMapping("/all")
    public String queryAll(@RequestParam(name = "hospitalId",defaultValue = "") Long hospitalId, Model model){
        model.addAttribute("hospitalAll",hospitalService.hospitalList());
        model.addAttribute("medAll",medicalAssayService.all(hospitalId));
        model.addAttribute("hospitalId",hospitalId);
        return "index";
    }
    //添加跳转页面
    @RequestMapping("/toAdd")
    public String toAdd(Model model){
        model.addAttribute("hostList",hospitalService.hospitalList());
        return "add";
    }
    @PostMapping("/addMedical")
    public String addMedical(MedicalAssay medicalAssay){
        medicalAssayService.add(medicalAssay);
        return "redirect:/all";
    }
    @GetMapping("/updateMedical")
    public String updMedical(Integer id){
        medicalAssayService.update(id);
        return "redirect:/all";
    }
}
