package com.bdqn.heshuan.service.impl;

import com.bdqn.heshuan.dao.MedicalAssayDao;
import com.bdqn.heshuan.entity.MedicalAssay;
import com.bdqn.heshuan.service.MedicalAssayService;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class MedicalAssayServiceImpl implements MedicalAssayService {
    @Resource
    MedicalAssayDao medicalAssayDao;
    @Override
    public List<MedicalAssay> all(Long hospitalId) {
        MedicalAssay medicalAssay = new MedicalAssay();
        medicalAssay.setHospitalId(hospitalId);
        return medicalAssayDao.queryAllBy(medicalAssay);
    }

    @Override
    public void add(MedicalAssay medicalAssay) {
        medicalAssay.setAssayResult(0);
        medicalAssayDao.insert(medicalAssay);
    }

    @Override
    public void update(Integer id) {
        MedicalAssay medicalAssay = new MedicalAssay();
        medicalAssay.setId(id).setAssayResult(1);
        medicalAssayDao.update(medicalAssay);
    }
}
