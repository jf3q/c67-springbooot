package com.bdqn.heshuan;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.bdqn.heshuan.dao")
public class HeshuanApplication {
    public static void main(String[] args) {
        SpringApplication.run(HeshuanApplication.class, args);
    }
}
