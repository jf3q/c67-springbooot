package com.bdpn.studentdemo.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bdpn.studentdemo.entity.Student;
import com.bdpn.studentdemo.service.StudentService;
import com.bdpn.studentdemo.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("student")
public class StudentController {
    @Autowired
    StudentService studentService;
    @GetMapping
    public ResultVo getPage(String stuName, @RequestParam(defaultValue = "1")Integer pageNum){
        Page page=studentService.getPage(stuName,pageNum);
        return ResultVo.success("",page);
    }

    @PostMapping
    public ResultVo saveOrUpdate(@RequestBody Student student){
        studentService.saveOrUpdate(student);
        return ResultVo.success("操作成功",null);
    }

    @DeleteMapping("/{id}")
    public ResultVo del(@PathVariable Integer id){
        studentService.removeById(id);
        return ResultVo.success("删除成功",null);
    }
}
