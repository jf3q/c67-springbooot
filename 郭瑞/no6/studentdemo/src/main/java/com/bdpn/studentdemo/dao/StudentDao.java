package com.bdpn.studentdemo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bdpn.studentdemo.entity.Student;

public interface StudentDao extends BaseMapper<Student> {
}
