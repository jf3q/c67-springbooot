package com.bdpn.dto;

import lombok.Data;

@Data
public class AppInfoDto {
    private Integer id;     //效验管理员时必传
    private String softwarename;
    private String apkname;
    private Integer status;
    private Integer flatformid;
    private Integer categorylevel1;
    private Integer categorylevel2;
    private Integer categorylevel3;

    private Integer devid;
}
