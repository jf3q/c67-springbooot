package com.bdpn.dao;


import com.bdpn.entity.AppInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * (AppInfo)表数据库访问层
 *
 * @author makejava
 * @since 2023-11-29 10:54:45
 */
@Repository
public interface AppInfoDao {

    /**
     * 验证apk是否重复
     * @param apkName
     * @return
     */
    int OnSelect(String apkName);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AppInfo queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param appInfo 查询条件
     * @return 对象列表
     */
    List<AppInfo> queryAllBy(AppInfo appInfo);

    /**
     * 统计总行数
     *
     * @param appInfo 查询条件
     * @return 总行数
     */
    long count(AppInfo appInfo);

    /**
     * 新增数据
     *
     * @param appInfo 实例对象
     * @return 影响行数
     */
    int insert(AppInfo appInfo);

    /**
     * 修改数据
     *
     * @param appInfo 实例对象
     * @return 影响行数
     */
    int update(AppInfo appInfo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}

