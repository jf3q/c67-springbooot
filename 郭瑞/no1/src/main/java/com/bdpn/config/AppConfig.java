package com.bdpn.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/*@Configuration
@EnableWebMvc//相当于<mvc:annotation-driven/>
@ComponentScan(basePackages = {"com.bdpn.dao","com.bdpn.service","com.bdpn.controller"})
public class AppConfig implements WebMvcConfigurer {
    //<bean class="org.example.dao.impl.UserDaoImpl" id="userDao"/>
//    @Bean //<bean UserDaoImpl相当于class="org.example.dao.impl.UserDaoImpl" userDao方法名相当于id="userDao"
//    public UserDaoImpl userDao(){
//        return new UserDaoImpl();
//    }
//    @Bean
//    public UserServiceImpl userService(){
//        UserServiceImpl userService = new UserServiceImpl();
//        userService.setUserDao(userDao());
//        return userService;
//    }

    @Bean
    public InternalResourceViewResolver internalResourceViewResolver(){
        InternalResourceViewResolver in = new InternalResourceViewResolver();
        in.setPrefix("/");
        in.setSuffix(".jsp");
        return in;
    }
//相当于<mvc:default-servlet-handler/>
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
}*/
