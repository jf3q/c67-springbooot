package com.bdpn;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.bdpn.dao")
public class No1Application {

    public static void main(String[] args) {
        SpringApplication.run(No1Application.class, args);
    }

}
