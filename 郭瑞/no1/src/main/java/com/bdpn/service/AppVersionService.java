package com.bdpn.service;

import com.bdpn.entity.AppVersion;

import java.util.List;

public interface AppVersionService {

    /**
     * 查看手游版本
     * @param appId
     * @return
     */
    List<AppVersion> listVersion(Integer appId);

    int saveOrUpdate(AppVersion appVersion);
}
