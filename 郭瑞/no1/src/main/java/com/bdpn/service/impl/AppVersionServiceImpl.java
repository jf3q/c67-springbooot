package com.bdpn.service.impl;


import com.bdpn.dao.AppVersionDao;
import com.bdpn.entity.AppVersion;
import com.bdpn.service.AppVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AppVersionServiceImpl implements AppVersionService {

    @Autowired
    private AppVersionDao appVersionDao;
    /**
     * 查看手游版本
     * @param appId
     * @return
     */
    @Override
    public List<AppVersion> listVersion(Integer appId) {
        return appVersionDao.queryAllBy(new AppVersion().setAppId(appId));
    }

    //新增 修改
    @Override
    public int saveOrUpdate(AppVersion appVersion) {
        if(appVersion.getId()==null){
          return appVersionDao.insert(appVersion);
        }else {
            return appVersionDao.update(appVersion);
        }
    }
}
