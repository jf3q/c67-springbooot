package com.bdpn.controller;

import com.bdpn.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @Autowired
    User user;

    @Value("${city[2]}")
    String city;
    @GetMapping("/hello")
    public String hello(){
        System.out.println(user);
        return "hello springboot"+city;
    }
}
