package com.bdpn.controller;


import com.bdpn.entity.AppInfo;
import com.bdpn.entity.AppVersion;
import com.bdpn.service.AppInfoService;
import com.bdpn.service.AppVersionService;
import com.bdpn.sys.SysConstant;
import com.bdpn.vo.ResultVo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController     //所有方法都返回json格式
@RequestMapping("/version")
public class AppVersionController {
    @Autowired
    private AppVersionService appVersionService;

    @Autowired
    private AppInfoService appInfoService;

    //查询某手游版本信息
    @GetMapping("/versionList")
    public ResultVo appVersionList(Integer appId) {
        List<AppVersion> appVersions = appVersionService.listVersion(appId);
        return ResultVo.success("查询成功", appVersions);
    }

    //新增 修改
    @PostMapping("/addVersion")
    public ResultVo addAndUpd(AppVersion appVersion, HttpServletRequest request, MultipartFile apkFile) {
        String token = request.getHeader("token");
        if (apkFile!=null && !apkFile.isEmpty()) {
            String apkFilename = apkFile.getOriginalFilename();
            String extension = apkFilename.substring(apkFilename.lastIndexOf("."));
            if (apkFile.getSize() > 500 * 1024 * 1024) {
                return ResultVo.error("图片大小超过500MB");
            } else if (extension.equalsIgnoreCase(".apk")
            ) {
                String realPath = request.getServletContext().getRealPath("/upload/apk");
                File file = new File(realPath);
                if (!file.exists()) {
                    file.mkdirs();
                }
                String apkFileName = UUID.randomUUID().toString().replace("-", "") + extension;
                try {
                    apkFile.transferTo(new File(realPath + "/" + apkFileName));
                    appVersion.setDownloadLink("/upload/apk/" + apkFileName);
                } catch (IOException e) {
                    e.printStackTrace();
                    ResultVo.error("apk文件上传失败");
                }
            } else {
                return ResultVo.error("文件格式错误!");
            }
        }
        String[] split = token.split("-");
        String userId = split[2];      //截取token里面存的开发者id
        if (appVersion.getId() == null) {
            appVersion.setPublishStatus(SysConstant.AppVersionInt.dev);
            appVersion.setCreatedBy(Integer.parseInt(userId));
            appVersion.setCreationDate(new Date());
        } else {
            appVersion.setModifyBy(Integer.parseInt(userId));    //更新者
            appVersion.setModifyDate(new Date());
        }
        appVersionService.saveOrUpdate(appVersion);
        appInfoService.saveOrUpdate(new AppInfo().setId(appVersion.getAppId()).setVersionid(appVersion.getId()));
        return ResultVo.success("操作成功", null);
    }
}
