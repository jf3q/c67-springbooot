package com.bdpn.controller;


import com.bdpn.service.AppCategoryService;
import com.bdpn.vo.CategoryTreeVo;
import com.bdpn.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class AppCategoryController {

    @Autowired
    private AppCategoryService appCategoryService;

    //三级联动       使用递归技术
    @GetMapping("/tree")
    public ResultVo tree(){
        CategoryTreeVo list = appCategoryService.list();
        return ResultVo.success("",list);
    }
}
