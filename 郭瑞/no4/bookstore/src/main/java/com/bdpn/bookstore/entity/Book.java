package com.bdpn.bookstore.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Book)实体类
 *
 * @author makejava
 * @since 2024-01-11 16:34:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Book implements Serializable {
    private static final long serialVersionUID = 544497358003194467L;

    private Integer id;

    private String name;

    private Double price;

    private String category;

    private Integer pnum;

    private String imgurl;

    private String description;

    private String author;

    private Integer sales;


}

