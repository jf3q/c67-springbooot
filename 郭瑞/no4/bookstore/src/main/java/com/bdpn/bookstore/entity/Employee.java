package com.bdpn.bookstore.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Employee)实体类
 *
 * @author makejava
 * @since 2024-01-11 16:34:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Employee implements Serializable {
    private static final long serialVersionUID = 250797657340767570L;

    private Integer empno;

    private String ename;

    private String job;

    private Integer leaderid;

    private Integer salary;

}

