package com.bdpn.bookstore.controller;

import com.bdpn.bookstore.entity.Book;
import com.bdpn.bookstore.service.BookService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/book")
public class BookController {
    @Autowired
    BookService bookService;

    @GetMapping("/page")
    public String getAll(@RequestParam(defaultValue = "1") Integer pageNum,String name, Model model){
        PageInfo<Book> bookList=bookService.getAll(pageNum,name);
        model.addAttribute("books",bookList);
        return "/bookList";
    }

    @PostMapping("/del")
    public String del(Integer[] ids){
        if (ids.length>0){
            bookService.del(ids);
        }
        return "redirect:/book/page";
    }
    @RequestMapping("/add")
    public String add() {
        return "addOrUpdate";
    }
    @PostMapping("/addInfo")
    public String addInfo(Book book){

        if(book.getId() == null) {
            bookService.addInfo(book);
        }else{
            bookService.updateInfo(book);
        }

        return "redirect:/book/page";
    }

    /*修改页面跳转*/
    @GetMapping("/update")
    public String update(Integer id,Model model){
        Book book = bookService.selId(id);
        model.addAttribute("bookInfo",book);
        return "addOrUpdate";
    }
}
