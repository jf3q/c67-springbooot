package com.bdpn.bookstore.service.impl;

import com.bdpn.bookstore.dao.BookDao;
import com.bdpn.bookstore.entity.Book;
import com.bdpn.bookstore.service.BookService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BookServiceImpl implements BookService {
    @Resource
    BookDao bookDao;
    @Override
    public PageInfo<Book> getAll(Integer pageNum,String name) {
        PageHelper.startPage(pageNum,3,"id desc");
        List<Book> bookList = bookDao.selectList(name);
        return new PageInfo<>(bookList);
    }

    @Override
    public void del(Integer[] ids) {
        bookDao.del(ids);
    }
    @Override
    public void addInfo(Book book) {
        bookDao.addInfo(book);
    }

    @Override
    public Book selId(Integer id) {
        return bookDao.selBook(id);
    }

    @Override
    public void updateInfo(Book book) {
        bookDao.updateInfo(book);
    }
}
