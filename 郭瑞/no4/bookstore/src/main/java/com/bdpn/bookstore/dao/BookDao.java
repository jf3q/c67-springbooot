package com.bdpn.bookstore.dao;

import com.bdpn.bookstore.entity.Book;

import java.util.List;

public interface BookDao {
    List<Book> selectList(String name);

    void del(Integer[] ids);
    void addInfo(Book book);

    /*查询单个图书信息*/
    Book selBook(Integer id);

    /*修改图书信息*/
    void updateInfo(Book book);
}
