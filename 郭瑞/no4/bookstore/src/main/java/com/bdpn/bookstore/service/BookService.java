package com.bdpn.bookstore.service;

import com.bdpn.bookstore.entity.Book;
import com.github.pagehelper.PageInfo;

public interface BookService {
    PageInfo<Book> getAll(Integer pageNum,String name);

    void del(Integer[] ids);
    void addInfo(Book book);

    /*查询需要修改的图书信息*/
    Book selId(Integer id);

    /*修改图书信息*/
    void updateInfo(Book book);
}
