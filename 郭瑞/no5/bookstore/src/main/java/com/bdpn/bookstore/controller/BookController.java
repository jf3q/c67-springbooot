package com.bdpn.bookstore.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bdpn.bookstore.entity.Book;
import com.bdpn.bookstore.service.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class BookController {
    @Autowired
    IBookService bookService;
    /*@GetMapping("/all")
    public String getAll(Model model){
        List<Book> bookList = bookService.list();
        model.addAttribute("bookList",bookList);
        return "index";
    }*/

    @GetMapping("/page")
    public String getpage(Model model,Book book,@RequestParam(defaultValue = "1") Integer pageNum){
        IPage bookList=bookService.selectBooks(book,pageNum);
        model.addAttribute("books",bookList);
        model.addAttribute("book",book);
        return "index";
    }
    @PostMapping("/del")
    public String del(Integer[] ids){
        if (ids.length>0){
            bookService.del(ids);
        }
        return "redirect:/page";
    }

    @RequestMapping("/add")
    public String add() {
        return "addOrUpdate";
    }
    @PostMapping("/addInfo")
    public String addInfo(Book book){
        if(book.getId() == null) {
            bookService.addInfo(book);
        }else{
            bookService.updateInfo(book);
        }

        return "redirect:/page";
    }

    /*修改页面跳转*/
    @GetMapping("/update")
    public String update(Integer id,Model model){
        Book book = bookService.selId(id);
        model.addAttribute("bookInfo",book);
        return "addOrUpdate";
    }
}
