package com.bdpn.bookstore.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bdpn.bookstore.dao.BookMapper;
import com.bdpn.bookstore.entity.Book;
import com.bdpn.bookstore.service.IBookService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements IBookService {
    @Resource
    BookMapper bookMapper;
    @Override
    public IPage selectBooks(Book book,Integer pageNum) {
        //方法一
        /*QueryWrapper queryWrapper=new QueryWrapper();
        if (book.getAuthor()!=null && book.getAuthor()!=""){
            queryWrapper.like("author",book.getAuthor());
        }
        if (book.getName()!=null && book.getName()!=""){
            queryWrapper.like("name",book.getName());
        }
        if (book.getCategory() != null && book.getCategory() != "") {
            queryWrapper.eq("category",book.getCategory());
        }
        return bookMapper.selectList(queryWrapper);*/
        //方法二
        LambdaQueryWrapper<Book> queryWrapper = new LambdaQueryWrapper();
        if (book.getName() != null && book.getName() != "") {
            queryWrapper.like(Book::getName,book.getName());
        }
        if (book.getCategory() != null && book.getCategory() != "") {
            queryWrapper.eq(Book::getCategory,book.getCategory());
        }
        if (book.getAuthor() != null && book.getAuthor() != "") {
            queryWrapper.eq(Book::getAuthor,book.getAuthor());
        }
        IPage page=new Page(pageNum,3);
        return bookMapper.selectPage(page,queryWrapper);
    }

    @Override
    public void del(Integer[] ids) {
        bookMapper.deleteBatchIds(Arrays.asList(ids));
    }

    @Override
    public void addInfo(Book book) {
        bookMapper.insert(book);
    }

    @Override
    public Book selId(Integer id) {
        return bookMapper.selectById(id);
    }

    @Override
    public void updateInfo(Book book) {
        bookMapper.updateById(book);
    }
}