package com.bdpn.bookstore.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Salarylevel)实体类
 *
 * @author makejava
 * @since 2024-01-11 16:34:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Salarylevel implements Serializable {
    private static final long serialVersionUID = 182916491886213776L;

    private String level;

    private Object minsal;

    private Object maxsal;


}

