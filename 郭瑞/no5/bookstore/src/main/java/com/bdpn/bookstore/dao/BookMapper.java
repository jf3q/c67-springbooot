package com.bdpn.bookstore.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bdpn.bookstore.entity.Book;

public interface BookMapper extends BaseMapper<Book> {
    /*void del(Integer[] ids);*/
}