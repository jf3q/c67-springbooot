package com.bdpn.bookstore.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Book)实体类
 *
 * @author makejava
 * @since 2024-01-11 16:34:54
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName("book")
public class Book implements Serializable {
    private static final long serialVersionUID = 544497358003194467L;
    @TableId(value="id",type= IdType.AUTO)
    private Integer id;
    @TableField("name")
    private String name;

    private Double price;

    private String category;

    private Integer pnum;

    private String imgurl;

    private String description;

    private String author;

    private Integer sales;


}

