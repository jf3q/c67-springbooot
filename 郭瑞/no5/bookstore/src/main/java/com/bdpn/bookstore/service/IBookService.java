package com.bdpn.bookstore.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bdpn.bookstore.entity.Book;

import java.util.List;

public interface IBookService extends IService<Book> {

    IPage selectBooks(Book book,Integer pageNum);
    void del(Integer[] ids);
    void addInfo(Book book);

    /*查询需要修改的图书信息*/
    Book selId(Integer id);

    /*修改图书信息*/
    void updateInfo(Book book);
}