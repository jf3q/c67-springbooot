package com.jf.admin.controller;

import com.jf.admin.service.AdminService;
import com.jf.commen.dto.LoginDto;
import com.jf.commen.vo.LoginVo;
import com.jf.commen.vo.Rsutilvo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/admin")
public class AdminLoginController {

    @Autowired
    AdminService adminService;

    //登录
    @PostMapping("/Login")
    public Rsutilvo Login(@RequestBody LoginDto loginDto){
        LoginVo loginVo=null;
        try {
            loginVo=adminService.Login(loginDto);
        } catch (Exception e) {
            e.printStackTrace();
            return Rsutilvo.error(e.getMessage());
        }
        return Rsutilvo.success("登录成功",loginVo);
    }

}
