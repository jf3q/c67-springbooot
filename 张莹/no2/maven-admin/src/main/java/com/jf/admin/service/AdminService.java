package com.jf.admin.service;

import com.jf.commen.dao.BackendUserDao;
import com.jf.commen.dto.LoginDto;
import com.jf.commen.entity.BackendUser;
import com.jf.commen.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class AdminService {
    @Autowired
    BackendUserDao backendUserDao;
    public LoginVo Login(LoginDto loginDto) {
        LoginVo loginVo = new LoginVo();
        BackendUser backendUser = new BackendUser();
        backendUser.setUserpassword(loginDto.getPassword());
        backendUser.setUsercode(loginDto.getUserName());
        List<BackendUser> backendUsers = backendUserDao.queryAllByLimit(backendUser);
        if (backendUsers.get(0) == null) {
            throw new RuntimeException("账号或密码错误");
        }
        loginVo.setPassword(loginVo.getPassword());
        loginVo.setToken(loginVo.getToken());
        loginVo.setUserName(loginDto.getUserName());
        loginVo.setUserType(loginDto.getUserType());
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(UUID.randomUUID().toString().replace("-", ""));
        stringBuffer.append("-" + backendUsers.get(0).getUsercode());
        stringBuffer.append("-" + backendUsers.get(0).getId());
        stringBuffer.append("-admin");
        stringBuffer.append("-" + System.currentTimeMillis());
        loginVo.setToken(stringBuffer.toString());
        return loginVo;
    }
}
