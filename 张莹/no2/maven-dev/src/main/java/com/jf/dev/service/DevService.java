package com.jf.dev.service;

import com.jf.commen.dao.DevUserDao;
import com.jf.commen.dto.LoginDto;
import com.jf.commen.entity.DevUser;
import com.jf.commen.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DevService {
    @Autowired
    DevUserDao devUserDao;
    public LoginVo Login(LoginDto loginDto) {
        LoginVo loginVo=new LoginVo();
        DevUser devUser=new DevUser();
        devUser.setDevpassword(loginDto.getPassword());
        devUser.setDevcode(loginDto.getUserName());
        List<DevUser> devUsers = devUserDao.queryAllByLimit(devUser);
        if (devUsers.get(0)==null){
            throw new RuntimeException("账号或密码错误");
        }
        loginVo.setPassword(loginVo.getPassword());
        loginVo.setToken(loginVo.getToken());
        loginVo.setUserName(loginDto.getUserName());
        loginVo.setUserType(loginDto.getUserType());
        StringBuffer stringBuffer=new StringBuffer();
        stringBuffer.append(UUID.randomUUID().toString().replace("-",""));
        stringBuffer.append("-"+devUsers.get(0).getDevcode());
        stringBuffer.append("-"+devUsers.get(0).getId());
        stringBuffer.append("-dev");
        stringBuffer.append("-"+System.currentTimeMillis());
        loginVo.setToken(stringBuffer.toString());
        return loginVo;
    }
}
