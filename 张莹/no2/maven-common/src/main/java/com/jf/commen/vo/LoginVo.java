package com.jf.commen.vo;

public class LoginVo {
    //后端给前端的值 因为管理员和开发者 都需要登录
    // 所以要把管理员和开发者从前端传过来的值在server
    // 判断到底是开发者还是管理员然后把值传给这个类最
    // 后用这个类把值都传递给前端
    private String token;//前端需要token
    private String userName;//前端需要用户名
    private String password;//密码
    private Integer userType;//类型

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }
}
