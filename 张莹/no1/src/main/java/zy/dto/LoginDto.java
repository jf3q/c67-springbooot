package zy.dto;

public class LoginDto {
    private String userName;
    private String password;
    private Integer userType;

    public LoginDto(String userName, String password, Integer userType) {
        this.userName = userName;
        this.password = password;
        this.userType = userType;
    }



    public LoginDto() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }
}
