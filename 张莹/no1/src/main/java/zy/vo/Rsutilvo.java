package zy.vo;

public class Rsutilvo {
    //给前端返回的信息
    private String code;//状态码 2000 5000
    private String message;//返回消息 正确与错误后要返回的信息
    private Object data;//返回的数据

    public static Rsutilvo success(String message,Object data){
        //new 一个新的本类调用放值进去
         return new Rsutilvo("2000",message,data);
    }

    public static Rsutilvo error(String message){
        return new Rsutilvo("5000",message,null);
    }

    public Rsutilvo(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Rsutilvo() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
