package zy.server;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zy.dao.AppInfoDao;
import zy.dao.AppVersionDao;
import zy.entity.AppInfo;
import zy.entity.AppVersion;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;

@Service
public class AppInfoServer {
    @Autowired
    AppInfoDao appInfoDao;

    @Autowired
    AppVersionDao appVersionDao;

    public AppInfo AppInfoId(Long appid) {
        return appInfoDao.queryById(appid);
    }


    public PageInfo<AppInfo> fenYe(AppInfo appInfo, Integer pageNum) {
        PageHelper.startPage(pageNum,8,"id desc");
        List<AppInfo> list = appInfoDao.queryAllByLimit(appInfo);
        return new PageInfo<>(list);
    }

    //apk验证
    //直接把appInfo整个拿过来 然后获取里面的appId 如果有id说明是修改操作
    //然后用id查询数据库里面的单条数据因为要获取它apk的名称与前端传递过来
    // 的apk名称进行比对如果一样就证明没改就直接reten true 就行了
    //如果修改了就进行正常的比对 查询这个appInfo名称是否存在（查询条数）
    // 有的话就已经存在没有就返回一个true 说明apk不存在



    //通过id查询数据库里面的apk名称才能进行匹配
//    AppInfo appInfo1 = appInfoDao.queryById(appInfo.getId());
//        if (appInfo.getId()!=null){
//        //先判断它有没有这个id有就说明它是修改
//        //然后判断修改的里面的apk的name与修改里面的name一样
//        // 一样就说明没改值直接return出去
//        if (appInfo.getApkname().equals(appInfo1.getApkname())){
//            return true;
//        }
//        Integer count=appInfoDao.countApkname(appInfo.getApkname());
//        if (count>0){
//            return false;
//        }
//    }
    public Boolean yanZheng(AppInfo appInfo) {
        if (appInfo.getId()!=null){
            AppInfo appInfo1 = appInfoDao.queryById(appInfo.getId());
            if (appInfo.getApkname().equals(appInfo1.getApkname())) {
                return true;
            }
            Integer count = appInfoDao.countApkname(appInfo.getApkname());
            if (count>0){
                return false;
            }
        }
        return true;
    }

    public void add(AppInfo appInfo) {
        appInfoDao.insert(appInfo);
    }

    public void update(AppInfo appInfo) {
        appInfoDao.update(appInfo);
    }
    //查询所有的版本信息但是我现在只有id但是sql里面需要版本类
    //判断是否有这个apk的相对路径
    //有的话就可以删除了

    public void delserver(Long id, HttpServletRequest request) {
        AppVersion appVersion=new AppVersion();
        appVersion.setId(id);
        List<AppVersion> appVers = appVersionDao.queryAllByLimit(appVersion);
        for (AppVersion appVer : appVers) {
            if (appVer.getDownloadlink()!=null){
                String realPath = request.getServletContext().getRealPath(appVer.getDownloadlink());
                File file=new File(realPath);
                if (!file.exists()){
                    file.delete();
                }
            }
            appVersionDao.deleteById(appVer.getId());
        }
        AppInfo appInfo=new AppInfo();
        appInfo.setId(id);
        List<AppInfo> list = appInfoDao.queryAllByLimit(appInfo);
        for (AppInfo info : list) {
            if (info.getLogopicpath()!=null){
                String realPath = request.getServletContext().getRealPath(info.getLogopicpath());
                File file2=new File(realPath);
                if (!file2.exists()){
                    file2.delete();
                }
            }
            appInfoDao.deleteById(info.getId());
        }
    }

    public AppInfo AppInfoAll(Long Id) {
        return appInfoDao.queryById(Id);
    }
}
