package zy.server;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zy.dao.AppCategoryDao;
import zy.entity.AppCategory;

import java.util.List;

@Service
public class CategoryServer {
    @Autowired
    AppCategoryDao appCategoryDao;


    public List<AppCategory> shanJi(Integer parentId) {
        return appCategoryDao.shanJiLian(parentId);
    }
}
