package zy.server;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zy.dao.AppVersionDao;
import zy.entity.AppVersion;

import java.util.List;

@Service
public class AppVersionServer {
    @Autowired
    AppVersionDao appVersionDao;

    public List<AppVersion> AllVersion(Long appid) {
        AppVersion appVersion=new AppVersion();
        appVersion.setAppid(appid);
        return  appVersionDao.queryAllByLimit(appVersion);
    }

    public void add(AppVersion appVersion) {
        appVersionDao.insert(appVersion);
    }


    public Integer maxId() {
        return appVersionDao.maxId();
    }
}
