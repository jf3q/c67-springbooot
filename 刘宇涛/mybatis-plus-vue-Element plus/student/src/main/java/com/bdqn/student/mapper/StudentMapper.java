package com.bdqn.student.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bdqn.student.entity.Student;
import org.springframework.stereotype.Repository;


@Repository
public interface StudentMapper extends BaseMapper<Student> {

}
