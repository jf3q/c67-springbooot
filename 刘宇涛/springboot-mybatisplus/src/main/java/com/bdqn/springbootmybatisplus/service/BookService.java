package com.bdqn.springbootmybatisplus.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bdqn.springbootmybatisplus.pojo.Book;

import java.util.List;

public interface BookService extends IService<Book> {

    List<Book> findAllBooks();

    List<Book> searchBooks(Book book);

    IPage<Book> getPage(Integer pageNum, Integer size, Book book);
}
