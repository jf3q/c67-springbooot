package com.bdqn.springbootmybatisplus.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bdqn.springbootmybatisplus.pojo.Book;
import org.apache.ibatis.annotations.Select;


public interface BookMapper extends BaseMapper<Book> {

}
