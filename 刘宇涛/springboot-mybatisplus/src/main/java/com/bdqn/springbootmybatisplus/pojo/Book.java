package com.bdqn.springbootmybatisplus.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName("book")
public class Book {
  @TableId(type = IdType.AUTO)   //value表示数据库中表中的主键， type表示自增
  private Integer id;
  private String name;
  private double price;
  private String category;
  private long pnum;
  private String imgurl;
  private String description;
  private String author;
  private Integer sales;
  @TableLogic
  private Integer deleted;
}
