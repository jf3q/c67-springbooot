package com.bdqn.springbootmybatisplus.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bdqn.springbootmybatisplus.mapper.BookMapper;
import com.bdqn.springbootmybatisplus.pojo.Book;
import com.bdqn.springbootmybatisplus.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements BookService {
    @Autowired
    private BookMapper bookMapper;


    @Override
    public List<Book> findAllBooks() {
        return bookMapper.selectList(null);
    }

    @Override
    public List<Book> searchBooks(Book book) {
        QueryWrapper<Book> wrapper = new QueryWrapper<>();
        wrapper.like(book.getName()!=""&&book.getName()!=null,"name",book.getName()).eq(book.getCategory()!=""&&book.getCategory()!=null,"category",book.getCategory());
        return bookMapper.selectList(wrapper);
    }

    //分页
    @Override
    public IPage<Book> getPage(Integer pageNum, Integer size, Book book) {
        LambdaQueryWrapper<Book> lambdaQueryWrapper = new LambdaQueryWrapper();
        lambdaQueryWrapper.like(book.getName()!=null&&book.getName()!="",Book::getName,book.getName());
        IPage<Book> bookIPage = new Page<>(pageNum,size);
        bookIPage = bookMapper.selectPage(bookIPage,lambdaQueryWrapper);
        return bookIPage;
    }
}
