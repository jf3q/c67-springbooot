package com.bdqn.springbootno2.sys;

/**
 *  常量类
 */
public class SysConstant {
    public final  static Integer pageSize=6;

    public static class UserTypeInt{
        public final static Integer admin=1;
        public final static Integer dev=2;
    }

    public static class UserTypeStr{
        public final static String admin="admin";
        public final static String dev="dev";
    }
    public static class AppVersionInt{
        public final static Integer no=1;
        public final static Integer yes=2;
        public final static Integer dev=3;
    }
    public static class statueApp{
        public final static Integer pre=1;
        public final static Integer no=2;
        public final static Integer yes=3;
        public final static Integer on=4;    //上架
        public final static Integer dev=5;   //下架
    }
}
