package com.bdqn.springbootno2.service;


import com.bdqn.springbootno2.dto.LoginDto;
import com.bdqn.springbootno2.vo.LoginUserVo;

public interface LoginService {

    /**
     * 用户登录
     * @param loginDto
     * @return
     */
    LoginUserVo loginDevUser(LoginDto loginDto);
}
