package com.bdqn.springbootno2.service;


import com.bdqn.springbootno2.vo.CategoryTreeVo;

public interface AppCategoryService {
    CategoryTreeVo list();
}
