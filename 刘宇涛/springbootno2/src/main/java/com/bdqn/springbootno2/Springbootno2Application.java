package com.bdqn.springbootno2;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.bdqn.springbootno2.dao")
public class Springbootno2Application {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(Springbootno2Application.class);
        springApplication.setAllowCircularReferences(Boolean.TRUE);
        springApplication.run(args);
    }

}
