package com.bdqn.springbootno2.service.impl;

import com.bdqn.springbootno2.dao.AppVersionDao;
import com.bdqn.springbootno2.entity.AppVersion;
import com.bdqn.springbootno2.service.AppVersionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AppVersionServiceImpl implements AppVersionService {

    @Autowired
    private AppVersionDao appVersionDao;
    /**
     * 查看手游版本
     * @param appId
     * @return
     */
    @Override
    public List<AppVersion> listVersion(Integer appId) {
        return appVersionDao.queryAllBy(new AppVersion().setAppId(appId));
    }

    //新增 修改
    @Override
    public int saveOrUpdate(AppVersion appVersion) {
        if(appVersion.getId()==null){
          return appVersionDao.insert(appVersion);
        }else {
            return appVersionDao.update(appVersion);
        }
    }
}
