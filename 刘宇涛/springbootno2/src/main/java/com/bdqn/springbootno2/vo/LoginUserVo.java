package com.bdqn.springbootno2.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginUserVo {
    private String account;      //用户账户
    private String userType;    //用户昵称
    private String toKen;         //判断
}
