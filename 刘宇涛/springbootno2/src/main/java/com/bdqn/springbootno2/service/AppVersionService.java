package com.bdqn.springbootno2.service;


import com.bdqn.springbootno2.entity.AppVersion;

import java.util.List;

public interface AppVersionService {

    /**
     * 查看手游版本
     * @param appId
     * @return
     */
    List<AppVersion> listVersion(Integer appId);

    int saveOrUpdate(AppVersion appVersion);
}
