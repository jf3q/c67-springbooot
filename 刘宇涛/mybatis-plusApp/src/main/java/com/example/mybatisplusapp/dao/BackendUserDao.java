package com.example.mybatisplusapp.dao;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mybatisplusapp.entity.BackendUser;

import java.util.List;

/**
 * (BackendUser)表数据库访问层
 *
 * @author makejava
 * @since 2023-11-27 15:49:14
 */
public interface BackendUserDao extends BaseMapper<BackendUser> {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    BackendUser queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param backendUser 查询条件
     * @return 对象列表
     */
    List<BackendUser> queryAllBy(BackendUser backendUser);

    /**
     * 统计总行数
     *
     * @param backendUser 查询条件
     * @return 总行数
     */
    long count(BackendUser backendUser);

    /**
     * 新增数据
     *
     * @param backendUser 实例对象
     * @return 影响行数
     */
    int insert(BackendUser backendUser);

    /**
     * 修改数据
     *
     * @param backendUser 实例对象
     * @return 影响行数
     */
    int update(BackendUser backendUser);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

}

