package com.example.mybatisplusapp.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class BackendUser {
    private Integer id;
    private String usercode;
    private String username;
    private Integer usertype;
    private Integer createdby;
    private Date creationdate;
    private long modifyby;
    private Date modifydate;
    private String userpassword;
}
