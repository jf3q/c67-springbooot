package com.example.mybatisplusapp.controller;


import com.example.mybatisplusapp.dto.LoginDto;
import com.example.mybatisplusapp.service.LoginService;
import com.example.mybatisplusapp.sys.SessionUtils;
import com.example.mybatisplusapp.vo.LoginUserVo;
import com.example.mybatisplusapp.vo.ResultVo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;       //开发者

    //注销
    @RequestMapping("/logout")
    public ResultVo logout(HttpServletRequest request){
        String token = request.getHeader("token");

        //效验token是否存在
        SessionUtils.remove(token);
        return ResultVo.success("退出成功",null);
    }


    //登录接口
    @PostMapping("/loginUser")
    public ResultVo login(@RequestBody LoginDto loginDto) {
        LoginUserVo vo = null;
        try {
            vo = loginService.loginDevUser(loginDto);
            return ResultVo.success("登录成功", vo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
    }
}
