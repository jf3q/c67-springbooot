package com.example.mybatisplusapp.service;


import com.example.mybatisplusapp.dto.LoginDto;
import com.example.mybatisplusapp.vo.LoginUserVo;

public interface LoginService{

    /**
     * 用户登录
     * @param loginDto
     * @return
     */
    LoginUserVo loginDevUser(LoginDto loginDto);
}
