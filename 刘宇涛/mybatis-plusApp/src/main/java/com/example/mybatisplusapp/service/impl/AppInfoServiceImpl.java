package com.example.mybatisplusapp.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.mybatisplusapp.dao.AppInfoDao;
import com.example.mybatisplusapp.dto.AppInfoDto;
import com.example.mybatisplusapp.entity.AppInfo;
import com.example.mybatisplusapp.service.AppInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppInfoServiceImpl extends ServiceImpl<AppInfoDao,AppInfo> implements AppInfoService {
    @Autowired
    private AppInfoDao appInfoDao;
    /**
     * 分页查询
     *
     * @return
     */
/*     @Override
    public IPage<AppInfoDto> page(Integer pageNum, AppInfoDto appInfoDto) {
        IPage<AppInfo> page = new Page<>(pageNum,6);
        LambdaQueryWrapper<AppInfoDto> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if(appInfoDto.getSoftwarename()!=null){
            lambdaQueryWrapper.like(AppInfoDto::getSoftwarename,appInfoDto.getSoftwarename());
        }
        if(appInfoDto.getApkname()!=null){
            lambdaQueryWrapper.like(AppInfoDto::getApkname,appInfoDto.getApkname());
        }
        if(appInfoDto.getFlatformid()!=null){
            lambdaQueryWrapper.eq(AppInfoDto::getFlatformid,appInfoDto.getFlatformid());
        }
        if(appInfoDto.getCategorylevel1()!=null){
            lambdaQueryWrapper.eq(AppInfoDto::getCategorylevel1,appInfoDto.getCategorylevel1());
        }
        if(appInfoDto.getCategorylevel2()!=null){
            lambdaQueryWrapper.eq(AppInfoDto::getCategorylevel3,appInfoDto.getCategorylevel3());
        }
        return appInfoDao.selectPage(page,lambdaQueryWrapper);
    } */
}
