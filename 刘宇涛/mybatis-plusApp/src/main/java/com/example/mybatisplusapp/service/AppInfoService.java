package com.example.mybatisplusapp.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.mybatisplusapp.entity.AppInfo;

public interface AppInfoService extends IService<AppInfo> {
    /**
     * 分页查询
     * @return
     */
   // IPage<AppInfo> page(Integer pageNum,AppInfo appInfo);
}

