package com.example.mybatisplusapp.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class AppVersion {

    private Integer id;
    private Integer appId;
    private String versionNo;
    private String versionInfo;
    private Integer publishStatus;
    private String downloadLink;
    private double versionSize;
    private Integer createdBy;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date creationDate;
    private long modifyBy;
    private Date modifyDate;
    private String apkLocPath;
    private String apkFileName;

}
