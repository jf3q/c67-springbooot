package com.example.mybatisplusapp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.mybatisplusapp.dao.BackendUserDao;
import com.example.mybatisplusapp.dao.DevUserDao;
import com.example.mybatisplusapp.dto.LoginDto;
import com.example.mybatisplusapp.entity.BackendUser;
import com.example.mybatisplusapp.entity.DevUser;
import com.example.mybatisplusapp.service.LoginService;
import com.example.mybatisplusapp.sys.SessionUtils;
import com.example.mybatisplusapp.sys.SysConstant;
import com.example.mybatisplusapp.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private DevUserDao devUserDao;

    @Autowired
    private BackendUserDao backendUserDao;
    /**
     * 用户登录
     *
     * @param loginDto
     * @return
     */
    @Override
    public LoginUserVo loginDevUser(LoginDto loginDto) {
        LoginUserVo loginUserVo = new LoginUserVo();
        if (loginDto.getUserType() == SysConstant.UserTypeInt.admin) {    //1
            LambdaQueryWrapper<BackendUser> lambdaQueryWrapper = new LambdaQueryWrapper();
            lambdaQueryWrapper.eq(BackendUser::getUsercode,loginDto.getAccount());
            lambdaQueryWrapper.eq(BackendUser::getUserpassword,loginDto.getPassword());
            List<BackendUser> backendUsers = backendUserDao.selectList(lambdaQueryWrapper);
            if (backendUsers.size()==0) {
                throw new RuntimeException("账户密码错误");
            }
            loginUserVo.setAccount(loginDto.getAccount());
            loginUserVo.setUserType(SysConstant.UserTypeStr.admin);
            //生成token 格式  uuid-account-id-userType-createTime
            StringBuffer buffer = new StringBuffer();
            buffer.append(UUID.randomUUID().toString().replace("-","")+"-");
            buffer.append(loginDto.getAccount()+"-");
            buffer.append(backendUsers.get(0).getId()+"-");
            buffer.append(SysConstant.UserTypeStr.admin+"-");
            buffer.append(System.currentTimeMillis());
            loginUserVo.setToKen(buffer.toString());
            //模拟redis    当发生请求过来时效验
            SessionUtils.put(buffer.toString(),loginUserVo);
            return loginUserVo;
        } else if (loginDto.getUserType() == SysConstant.UserTypeInt.dev) {  //2
            LambdaQueryWrapper<DevUser> lambdaQueryWrapper = new LambdaQueryWrapper();
            lambdaQueryWrapper.eq(DevUser::getDevcode,loginDto.getAccount());
            lambdaQueryWrapper.eq(DevUser::getDevpassword,loginDto.getPassword());
            List<DevUser> devUsers = devUserDao.selectList(lambdaQueryWrapper);
            if (devUsers.size() == 0) {
                throw new RuntimeException("账户密码错误");
            }
            loginUserVo.setAccount(loginDto.getAccount());
            loginUserVo.setUserType(SysConstant.UserTypeStr.dev);
            //生成token 格式  uuid-account-id-userType-createTime的生成
            StringBuffer buffer = new StringBuffer();
            String uuid = UUID.randomUUID().toString().replace("-", "");
            buffer.append(uuid + "-");
            buffer.append(devUsers.get(0).getDevcode()+"-");    //拿到开发者账户
            buffer.append(devUsers.get(0).getId()+"-");        //拿到开发者id
            buffer.append(SysConstant.UserTypeStr.dev+"-");     //拿到dev
            buffer.append(System.currentTimeMillis());    //时间
            loginUserVo.setToKen(buffer.toString());
            SessionUtils.put(buffer.toString(),loginUserVo);
            return loginUserVo;
        }else {
            throw new RuntimeException("账户不存在");
        }
    }
}
