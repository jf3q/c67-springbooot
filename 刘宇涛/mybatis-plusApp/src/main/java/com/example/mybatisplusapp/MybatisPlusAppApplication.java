package com.example.mybatisplusapp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.mybatisplusapp.dao")
public class MybatisPlusAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisPlusAppApplication.class, args);
    }

}
