package com.bdqn.springbootno1.dao;

import com.bdqn.springbootno1.entity.AppCategory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 手游类别(AppCategory)表数据库访问层
 *
 * @author makejava
 * @since 2023-11-30 09:27:55
 */
@Repository
public interface AppCategoryDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AppCategory queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param appCategory 查询条件
     * @return 对象列表
     */
    List<AppCategory> queryAllBy(AppCategory appCategory);

    /**
     * 统计总行数
     *
     * @param appCategory 查询条件
     * @return 总行数
     */
    long count(AppCategory appCategory);

    /**
     * 新增数据
     *
     * @param appCategory 实例对象
     * @return 影响行数
     */
    int insert(AppCategory appCategory);

    /**
     * 修改数据
     *
     * @param appCategory 实例对象
     * @return 影响行数
     */
    int update(AppCategory appCategory);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

}

