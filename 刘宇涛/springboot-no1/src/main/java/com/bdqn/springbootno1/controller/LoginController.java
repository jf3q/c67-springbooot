package com.bdqn.springbootno1.controller;

import com.bdqn.springbootno1.dto.LoginDto;
import com.bdqn.springbootno1.service.LoginService;
import com.bdqn.springbootno1.sys.SessionUtils;
import com.bdqn.springbootno1.vo.LoginUserVo;
import com.bdqn.springbootno1.vo.ResultVo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;       //开发者

    //注销
    @RequestMapping("/logout")
    public ResultVo logout(HttpServletRequest request){
        String token = request.getHeader("token");

        //效验token是否存在
        SessionUtils.remove(token);
        return ResultVo.success("退出成功",null);
    }


    //登录接口
    @PostMapping("/loginUser")
    public ResultVo login(@RequestBody LoginDto loginDto) {
        LoginUserVo vo = null;
        try {
            vo = loginService.loginDevUser(loginDto);
            return ResultVo.success("登录成功", vo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
    }
}
