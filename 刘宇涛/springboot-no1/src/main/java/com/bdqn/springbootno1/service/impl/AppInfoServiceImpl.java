package com.bdqn.springbootno1.service.impl;

import com.bdqn.springbootno1.dao.AppInfoDao;
import com.bdqn.springbootno1.dao.AppVersionDao;
import com.bdqn.springbootno1.dto.AppInfoDto;
import com.bdqn.springbootno1.entity.AppInfo;
import com.bdqn.springbootno1.entity.AppVersion;
import com.bdqn.springbootno1.service.AppInfoService;
import com.bdqn.springbootno1.sys.SysConstant;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.Date;
import java.util.List;

@Service
public class AppInfoServiceImpl implements AppInfoService {
    @Autowired
    private AppInfoDao appInfoDao;

    @Autowired
    private AppVersionDao appVersionDao;

    /**
     * 分页查询
     *
     * @param appInfo
     * @return
     */
    @Override
    public PageInfo<AppInfo> page(AppInfoDto appInfo, Integer pageNum) {
        PageHelper.startPage(pageNum, SysConstant.pageSize);
        AppInfo appInfo1 = new AppInfo();
        BeanUtils.copyProperties(appInfo, appInfo1);
        List<AppInfo> appInfos = appInfoDao.queryAllBy(appInfo1);
        PageInfo<AppInfo> page = new PageInfo<>(appInfos);
        return page;
    }

    /**
     * 新增或者修改手游信息
     *
     * @param appInfo
     * @return
     */
    @Override
    public int saveOrUpdate(AppInfo appInfo) {
        if (appInfo.getId() == null) {
            return appInfoDao.insert(appInfo);
        } else {
            return appInfoDao.update(appInfo);
        }
    }

    /**
     * 查看手游信息
     * @param id
     * @return
     */
    @Override
    public AppInfo selectId(Integer id) {
        return appInfoDao.queryById(id);
    }

    /**
     * 验证apk是否重复
     * @param apkName
     * @return
     */
    @Override
    public int OnSelect(String apkName) {
        return appInfoDao.OnSelect(apkName);
    }

    /**
     * 删除手游版本
     * @param appId
     * @param request
     */
    @Override
    public void del(Integer appId, HttpServletRequest request) {
      //删除对应手游版本的所有apk文件
        List<AppVersion> appVersions = appVersionDao.queryAllBy(new AppVersion().setAppId(appId));
        for (AppVersion appVersion : appVersions) {
            if(StringUtils.hasText(appVersion.getDownloadLink())){
                String realPath = request.getServletContext().getRealPath(appVersion.getDownloadLink());
                File file = new File(realPath);
                if(file.exists()){
                    file.delete();
                }
            }
        }
        //删除手游版本
        int i = appVersionDao.deleteByAppId(appId);
        //删除对应手游版本logo图片
        AppInfo appInfo = appInfoDao.queryById(appId);
        if (StringUtils.hasText(appInfo.getLogopicpath())) {
            String realPath = request.getServletContext().getRealPath(appInfo.getLogopicpath());
            File file = new File(realPath);
            if(file.exists()){
                file.delete();
            }
        }
        //删除手游信息
        appInfoDao.deleteById(appId);
    }

    //上下架
    @Override
    public void saleApps(Integer id) {
        AppInfo appInfo = appInfoDao.queryById(id);
        appInfo.setId(id);
        if(appInfo.getStatus()==SysConstant.statueApp.on){
            appInfo.setStatus(SysConstant.statueApp.dev);
           appInfo.setOffsaledate(new Date());
        }else if(appInfo.getStatus()==SysConstant.statueApp.dev || appInfo.getStatus()==SysConstant.statueApp.no){
            appInfo.setStatus(SysConstant.statueApp.on);
            appInfo.setOnsaledate(new Date());
        }
        appInfoDao.update(appInfo);
    }
}
