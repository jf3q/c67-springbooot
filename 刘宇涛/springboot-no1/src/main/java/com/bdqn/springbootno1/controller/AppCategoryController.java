package com.bdqn.springbootno1.controller;


import com.bdqn.springbootno1.service.AppCategoryService;
import com.bdqn.springbootno1.vo.CategoryTreeVo;
import com.bdqn.springbootno1.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
public class AppCategoryController {

    @Autowired
    private AppCategoryService appCategoryService;

    //三级联动       使用递归技术
    @GetMapping("/tree")
    public ResultVo tree(){
        CategoryTreeVo list = appCategoryService.list();
        return ResultVo.success("",list);
    }
}
