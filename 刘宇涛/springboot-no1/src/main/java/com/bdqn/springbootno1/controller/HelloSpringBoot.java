package com.bdqn.springbootno1.controller;

import com.bdqn.springbootno1.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloSpringBoot {

    //1.@value注解
    @Value("${user.username}")
    public String username;

    @Value("${address[0]}")
    public String address1;

    @Value("${books[0].name}")
    public String bookName;

    //2.Environment 对象
    @Autowired
    public Environment env;

    //3.@ConfigurationProperties 注解
    @Autowired
    public User users;

    //第一个springboot程序
    @RequestMapping("/hello")
    public String springboot1() {
        System.out.println("用户名:"+username+"爱好书本:"+bookName+"地址:"+address1);
        System.out.println("app名称:"+env.getProperty("books[0].name"));
        System.out.println("用户信息:"+users);
        return "hello si";
    }
}
