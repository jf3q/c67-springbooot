package com.bdqn.springbootno1.dao;


import com.bdqn.springbootno1.entity.AppVersion;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * (AppVersion)表数据库访问层
 *
 * @author makejava
 * @since 2023-12-02 21:50:14
 */
@Repository
public interface AppVersionDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    AppVersion queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param appVersion 查询条件
     * @return 对象列表
     */
    List<AppVersion> queryAllBy(AppVersion appVersion);

    /**
     * 统计总行数
     *
     * @param appVersion 查询条件
     * @return 总行数
     */
    long count(AppVersion appVersion);

    /**
     * 新增数据
     *
     * @param appVersion 实例对象
     * @return 影响行数
     */
    int insert(AppVersion appVersion);

    /**
     * 修改数据
     *
     * @param appVersion 实例对象
     * @return 影响行数
     */
    int update(AppVersion appVersion);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

    /**
     * 删除对应手游所有版本
     * @param
     * @return
     */
    int deleteByAppId(Integer appId);

}

