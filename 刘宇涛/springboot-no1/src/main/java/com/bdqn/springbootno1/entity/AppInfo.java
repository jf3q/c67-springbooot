package com.bdqn.springbootno1.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * (AppInfo)实体类
 *
 * @author makejava
 * @since 2023-11-29 10:54:53
 */
@Data
@Accessors(chain = true)
public class AppInfo implements Serializable {
    private static final long serialVersionUID = -30967817398890324L;
    /**
     * 主键id
     */
    private Integer id;
    /**
     * 软件名称
     */
    private String softwarename;
    /**
     * APK名称（唯一）
     */
    private String apkname;
    /**
     * 支持ROM
     */
    private String supportrom;
    /**
     * 界面语言
     */
    private String interfacelanguage;
    /**
     * 软件大小（单位：M）
     */
    private Double softwaresize;
    /**
     * 更新日期
     */
    private Date updatedate;
    /**
     * 开发者id（来源于：dev_user表的开发者id）
     */
    private Integer devid;
    /**
     * 应用简介
     */
    private String appinfo;
    /**
     * 状态（来源于：data_dictionary，1 待审核 2 审核通过 3 审核不通过 4 已上架 5 已下架）
     */
    private Integer status;
    /**
     * 上架时间
     */
    private Date onsaledate;
    /**
     * 下架时间
     */
    private Date offsaledate;
    /**
     * 所属平台（来源于：data_dictionary，1 手机 2 平板 3 通用）
     */
    private Integer flatformid;
    /**
     * 所属三级分类（来源于：data_dictionary）
     */
    private Integer categorylevel3;
    /**
     * 下载量（单位：次）
     */
    private Integer downloads;
    /**
     * 创建者（来源于dev_user开发者信息表的用户id）
     */
    private Integer createdby;
    /**
     * 创建时间
     */
    private Date creationdate;
    /**
     * 更新者（来源于dev_user开发者信息表的用户id）
     */
    private Integer modifyby;
    /**
     * 最新更新时间
     */
    private Date modifydate;
    /**
     * 所属一级分类（来源于：data_dictionary）
     */
    private Integer categorylevel1;
    private String categorylevel1Name;
    private String categorylevel2Name;
    private String categorylevel3Name;
    /**
     * 所属二级分类（来源于：data_dictionary）
     */
    private Integer categorylevel2;
    /**
     * LOGO图片url路径
     */
    private String logopicpath;
    /**
     * LOGO图片的服务器存储路径
     */
    private String logolocpath;
    /**
     * 最新的版本id
     */
    private Integer versionid;

    /**
     * 开发者名称
     */
    private String devName;

}

