package com.bdqn.springbootno1.dao;

import com.bdqn.springbootno1.entity.DevUser;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * (DevUser)表数据库访问层
 *
 * @author makejava
 * @since 2023-11-24 11:16:58
 */
@Repository
public interface DevUserDao {

    /**
     * 通过ID查询单条数据
     *
     * @param devUser 主键
     * @return 实例对象
     */
    List<DevUser> loginDevUser(DevUser devUser);
}

