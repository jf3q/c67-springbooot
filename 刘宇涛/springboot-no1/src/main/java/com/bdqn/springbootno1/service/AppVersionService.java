package com.bdqn.springbootno1.service;

import com.bdqn.springbootno1.entity.AppVersion;

import java.util.List;

public interface AppVersionService {

    /**
     * 查看手游版本
     * @param appId
     * @return
     */
    List<AppVersion> listVersion(Integer appId);

    int saveOrUpdate(AppVersion appVersion);
}
