package com.bdqn.springbootno1.service;


import com.bdqn.springbootno1.dto.LoginDto;
import com.bdqn.springbootno1.vo.LoginUserVo;

public interface LoginService {

    /**
     * 用户登录
     * @param loginDto
     * @return
     */
    LoginUserVo loginDevUser(LoginDto loginDto);
}
