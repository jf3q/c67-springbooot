package com.bdqn.springbootno1.service;


import com.bdqn.springbootno1.vo.CategoryTreeVo;

public interface AppCategoryService {
    CategoryTreeVo list();
}
