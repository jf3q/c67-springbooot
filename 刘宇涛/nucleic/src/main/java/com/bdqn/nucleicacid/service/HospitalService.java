package com.bdqn.nucleicacid.service;

import com.bdqn.nucleicacid.pojo.Hospital;

import java.util.List;

public interface HospitalService {

    /**
     * 核酸检测机构
     * @return
     */
    List<Hospital> listHospital();
}
