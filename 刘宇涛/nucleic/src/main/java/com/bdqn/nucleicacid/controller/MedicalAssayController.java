package com.bdqn.nucleicacid.controller;

import com.bdqn.nucleicacid.pojo.Hospital;
import com.bdqn.nucleicacid.pojo.MedicalAssay;
import com.bdqn.nucleicacid.service.HospitalService;
import com.bdqn.nucleicacid.service.MedicalAssayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class MedicalAssayController {
    @Autowired
    private MedicalAssayService medicalAssayService;
    @Autowired
    private HospitalService hospitalService;

    @GetMapping("/MedicalAssay")
    public String queryMedical(@RequestParam(required = false) Integer hospitalId, Model model){
        List<MedicalAssay> medicalAssays = medicalAssayService.listMedical(hospitalId);
        List<Hospital> hospitals = hospitalService.listHospital();
        model.addAttribute("listMedical",medicalAssays);
        model.addAttribute("hostList",hospitals);
        model.addAttribute("hospitalId",hospitalId);
        return "index";
    }

    //添加跳转页面
    @RequestMapping("/toAdd")
    public String toAdd(Model model){
        List<Hospital> hospitals = hospitalService.listHospital();
        model.addAttribute("hostList",hospitals);
        return "add";
    }
    //添加信息
    @PostMapping("/addMedical")
    public String addMedical(MedicalAssay medicalAssay){
        int i = medicalAssayService.addMedical(medicalAssay);
        return "redirect:/MedicalAssay";
    }

    //修改
    @GetMapping("/updateMedical")
    public String updMedical(MedicalAssay medicalAssay){
        int i = medicalAssayService.updateMedical(medicalAssay);
        return "redirect:/MedicalAssay";
    }
}
