package com.bdqn.nucleicacid.dao;

import com.bdqn.nucleicacid.pojo.MedicalAssay;

import java.util.List;

public interface MedicalAssayDao {
    /**
     * 核酸检测信息
     * @param hospitalId
     * @return
     */
    List<MedicalAssay> listMedical(Integer hospitalId);

    /**
     * 修改核酸信息
     */
    int updateMedical(MedicalAssay medicalAssay);

    /**
     * 添加核酸信息
     */
    int addMedical(MedicalAssay medicalAssay);
}
