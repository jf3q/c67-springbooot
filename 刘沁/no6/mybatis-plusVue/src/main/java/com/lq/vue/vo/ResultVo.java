package com.lq.vue.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultVo<T> {
    private String code;
    private String message;
    private T data ;

    public static<T> ResultVo<T> success(String message,T data){
        return new ResultVo<>("2000",message,data);
    }

    public static ResultVo error(String message){
        return new ResultVo("5000",message,"");
    }


}
