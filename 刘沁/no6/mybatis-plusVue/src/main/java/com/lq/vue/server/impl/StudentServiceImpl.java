package com.lq.vue.server.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lq.vue.dao.StudentDao;
import com.lq.vue.entity.Student;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl extends ServiceImpl<StudentDao, Student> implements StudentService {
}
