package com.lq.vue.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@TableName("student")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Student {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer  id;
    private String studentname;
    private String gender;
    private String address;
    private int age;

   /* *//*只查看，不会新增修改操作*//*
    @TableField(exist = false)
    private String user;*/

}
