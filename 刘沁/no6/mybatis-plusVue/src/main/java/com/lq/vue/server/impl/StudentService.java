package com.lq.vue.server.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lq.vue.entity.Student;

public interface StudentService extends IService<Student> {

}
