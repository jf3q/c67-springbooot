package com.lq.vue.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lq.vue.entity.Student;
import org.apache.ibatis.annotations.Mapper;

public interface StudentDao extends BaseMapper<Student> {

}
