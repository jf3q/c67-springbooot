package com.lq.vue.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lq.vue.entity.Student;
import com.lq.vue.server.impl.StudentService;
import com.lq.vue.vo.ResultVo;
import io.micrometer.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class StudentController {

    @Autowired
    StudentService studentService;

    @RequestMapping("/getPage")
    public ResultVo getPage(@RequestParam(defaultValue = "1") Integer num,Student student) {
        Page<Student> page = new Page<>(num, 3);
        LambdaQueryWrapper<Student> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.isNotEmpty(student.getStudentname()), Student::getStudentname, student.getStudentname());
        return ResultVo.success("成功", studentService.page(page, wrapper));
    }

    @PostMapping("/insOrUp")
    public ResultVo insOrUp(@RequestBody Student student) {
        return ResultVo.success("成功", studentService.saveOrUpdate(student));
    }

    @DeleteMapping("{id}")
    public ResultVo del(@PathVariable Integer id) {
        return ResultVo.success("成功", studentService.removeById(id));
    }
}
