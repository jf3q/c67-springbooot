package com.lq.vue;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.context.annotation.SessionScope;

@MapperScan("com.lq.vue.dao")
@SpringBootApplication
public class VueApplication {
    public static void main(String[] args) {
        SpringApplication.run(VueApplication.class,args);
    }
}
