package com.lq.mybatisplus.server.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lq.mybatisplus.dao.BookMapper;
import com.lq.mybatisplus.entity.Book;
import com.lq.mybatisplus.server.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    BookMapper bookMapper;

    @Override
    public List<Book> getBook(Book book) {
        LambdaQueryWrapper<Book> wrapper = new LambdaQueryWrapper();
        wrapper.like(book.getAuthor() != null && book.getAuthor() != "", Book::getAuthor, book.getAuthor());
        wrapper.like(book.getName() != null && book.getName() != "", Book::getName, book.getName());
        wrapper.like(book.getCategory() != null && book.getCategory() != "", Book::getCategory, book.getCategory());
        return bookMapper.selectList(wrapper);
    }

    @Override
    public IPage<Book> getPageBook(int num, Book book) {
        LambdaQueryWrapper<Book> wrapper = new LambdaQueryWrapper();
        wrapper.like(book.getAuthor() != null && book.getAuthor() != "", Book::getAuthor, book.getAuthor());
        wrapper.like(book.getName() != null && book.getName() != "", Book::getName, book.getName());
        wrapper.like(book.getCategory() != null && book.getCategory() != "", Book::getCategory, book.getCategory());
        IPage<Book> bookIPage = new Page<>(num, 3);
        bookIPage = bookMapper.selectPage(bookIPage, wrapper);

        return bookIPage;
    }

    @Override
    public int deleteBy(Integer[] id) {
        return bookMapper.deleteBatchIds(Arrays.stream(id).toList()) ;
    }

    @Override
    public void insAndUp(Book book) {
        if(book.getId()==null){

            bookMapper.insert(book);
        }else {
            bookMapper.updateById(book);
        }
    }

    @Override
    public Book getOne(Integer id) {
        return bookMapper.selectById(id);
    }
}
