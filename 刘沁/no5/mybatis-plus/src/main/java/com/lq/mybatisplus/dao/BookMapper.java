package com.lq.mybatisplus.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lq.mybatisplus.entity.Book;
import org.apache.ibatis.annotations.Param;

public interface BookMapper extends BaseMapper<Book> {
}
