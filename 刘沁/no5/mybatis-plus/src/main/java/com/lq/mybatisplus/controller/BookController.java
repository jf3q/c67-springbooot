package com.lq.mybatisplus.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lq.mybatisplus.entity.Book;
import com.lq.mybatisplus.server.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
public class BookController {

    @Autowired
    BookService bookService;

    @RequestMapping("/page")
    public String getPage(Model model, Book book, @RequestParam(defaultValue = "1") Integer num) {
        /*List<Book> book1 = bookService.getBook(book);*/
        IPage<Book> book1 = bookService.getPageBook(num, book);
        model.addAttribute("page", book1);
        model.addAttribute("bok",book);
        return "index";
    }

    @Value("${web.upload-path}")
    private String path;

    @RequestMapping("/del")
    public String del(Integer[] id) {

        for (int i = 0; i < id.length; i++) {
            Book one = bookService.getOne(id[i]);
            File file = new File(path + "/" + one.getImgurl());
            file.delete();
        }

        bookService.deleteBy(id);
        return "redirect:page";
    }

    @PostMapping("/toAdd")
    public String toAdd(@RequestParam MultipartFile file, Book book) {
        if (file != null && (!file.isEmpty())){
            File file1 = new File(path);
            if (!file1.exists()){
                file1.mkdirs();
            }
            String originalFilename = file.getOriginalFilename();
            String substring = originalFilename.substring(originalFilename.lastIndexOf("."));
            if (file.getSize()>1024000) {
                throw  new RuntimeException("文件过大!");
            }else if (substring.equalsIgnoreCase(".gif")||
                    substring.equalsIgnoreCase(".jpg")||
                    substring.equalsIgnoreCase(".png")||
                    substring.equalsIgnoreCase(".jpeg")) {
                substring = UUID.randomUUID() + substring;
                File file2 = new File(path  + substring);
                try {
                    file.transferTo(file2);
                    book.setImgurl(substring);
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new RuntimeException("文件上传错误!!");
                }
            } else {
                throw new RuntimeException("文件格式错误!!");
            }
        }
            bookService.insAndUp(book);
        return "redirect:page";
    }

    @GetMapping("/yeAdd")
    public String a(Model model) {
        model.addAttribute("bookId", new Book());
        return "add";
    }

    @GetMapping("/yeUpdata")
    public String yeUpdata(Integer id, Model model) {
        model.addAttribute("bookId", bookService.getOne(id));
        return "add";
    }

}
