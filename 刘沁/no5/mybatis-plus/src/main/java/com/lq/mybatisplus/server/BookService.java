package com.lq.mybatisplus.server;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.lq.mybatisplus.entity.Book;

import java.util.List;

public interface BookService {

    /*条件查询*/
    public List<Book> getBook(Book book);

    /*分页带条件*/
    public IPage<Book> getPageBook(int num,Book book);



    int deleteBy(Integer[] id);

    void insAndUp(Book book);

    Book getOne(Integer id);
}
