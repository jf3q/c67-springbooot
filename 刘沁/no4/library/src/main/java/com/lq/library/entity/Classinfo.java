package com.lq.library.entity;

import java.io.Serializable;

/**
 * (Classinfo)实体类
 *
 * @author makejava
 * @since 2023-12-29 10:47:29
 */
public class Classinfo implements Serializable {
    private static final long serialVersionUID = -26616724030546652L;

    private Integer classno;

    private String classname;


    public Integer getClassno() {
        return classno;
    }

    public void setClassno(Integer classno) {
        this.classno = classno;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

}

