package com.lq.library.entity;

import java.io.Serializable;

/**
 * (Salarylevel)实体类
 *
 * @author makejava
 * @since 2023-12-29 10:47:30
 */
public class Salarylevel implements Serializable {
    private static final long serialVersionUID = -24051664326451591L;

    private String level;

    private Object minsal;

    private Object maxsal;


    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Object getMinsal() {
        return minsal;
    }

    public void setMinsal(Object minsal) {
        this.minsal = minsal;
    }

    public Object getMaxsal() {
        return maxsal;
    }

    public void setMaxsal(Object maxsal) {
        this.maxsal = maxsal;
    }

}

