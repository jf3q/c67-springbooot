package com.lq.library.controller;

import com.github.pagehelper.PageInfo;
import com.lq.library.entity.Book;
import com.lq.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Controller
public class BookController {
    @Autowired
    BookService bookService;

    @Value("${web.upload-path}")
    private String path;

    @RequestMapping("/getPage")
    public String getPage(@RequestParam(defaultValue = "1") Integer num, Model model) {
        PageInfo<Book> page = bookService.getPage(num);
        model.addAttribute("page", page);
        return "index";
    }

    @RequestMapping("/del")
    public String del(Integer[] id) {

        for (int i = 0; i < id.length; i++) {
            Book one = bookService.getOne(id[i]);
            File file = new File(path + "/" + one.getImgurl());
            file.delete();
        }

        bookService.deleteBy(id);
        return "redirect:getPage";
    }

    @PostMapping("/toAdd")
    public String toAdd(@RequestParam MultipartFile file, Book book) {
        if (file != null && (!file.isEmpty())) {
            String filename = file.getOriginalFilename();
            String substring = filename.substring(filename.lastIndexOf("."));
            File file1 = new File(path);
            if (!file1.exists()) {
                file1.mkdirs();
            }

            if (file.getSize() > 500 * 10240) {
                throw new RuntimeException("文件过大");
            } else if (substring.equalsIgnoreCase(".jpg") ||
                    substring.equalsIgnoreCase(".png") ||
                    substring.equalsIgnoreCase(".gif") ||
                    substring.equalsIgnoreCase(".jpeg")
            ) {
                try {
                    substring = UUID.randomUUID().toString().replace("-", "") + substring;
                    File file2 = new File(path + substring);
                    file.transferTo(file2);
                    book.setImgurl(substring);
                } catch (IOException e) {
                    throw new RuntimeException("文件上传失败");
                }
            } else {
                throw new RuntimeException("文件格式不正确");
            }
        }
        bookService.insAndUp(book);
        return "redirect:getPage";
    }

    @GetMapping("/yeAdd")
    public String a(Model model) {
        model.addAttribute("bookId", new Book());
        return "add";
    }

    @GetMapping("/yeUpdata")
    public String updata(Integer id, Model model) {
        model.addAttribute("bookId", bookService.getOne(id));
        return "add";
    }


}
