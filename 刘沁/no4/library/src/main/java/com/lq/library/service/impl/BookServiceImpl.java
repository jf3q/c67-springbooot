package com.lq.library.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lq.library.dao.BookDao;
import com.lq.library.entity.Book;
import com.lq.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    @Autowired
    BookDao bookDao;


    @Override
    public PageInfo<Book> getPage(Integer num) {
        PageHelper.startPage(num,3);
        List<Book> books = bookDao.queryAll(new Book());
        PageInfo<Book> list = new PageInfo<>(books);
        return list;
    }

    @Override
    public int deleteBy(Integer[] id) {
        return bookDao.deleteBy(id);
    }

    @Override
    public void insAndUp(Book book) {
        if(book.getId()==null){
            bookDao.insert(book);
        }else {
            bookDao.update(book);
        }

    }

    @Override
    public Book getOne(Integer id) {
        return bookDao.queryById(id);
    }
}
