package com.lq.library.service;

import com.github.pagehelper.PageInfo;
import com.lq.library.entity.Book;

import java.util.List;

public interface BookService {

    PageInfo<Book> getPage(Integer num);
    int deleteBy(Integer[] id);

    void insAndUp(Book book);

    Book getOne(Integer id);
}
