package com.lq.utils;

import com.lq.dao.AppDao;
import com.lq.dao.impl.AppDaoImpl;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import com.lq.service.UserService;
import com.lq.service.UserServiceImpl;

@Configuration //相当于applicationContext.xml
@ComponentScan(basePackages = {"com.lq.service","com.lq.dao"})
public class AppConfig implements WebMvcConfigurer{
    //    @Override
//    public void configureViewResolvers(ViewResolverRegistry registry) {
//        registry.jsp("/templates/",".html");
//    }
    //    @Override
//    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
//        WebMvcConfigurer.super.configureDefaultServletHandling(configurer);
//    }
    /* @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/index").setViewName("index");
    }*/
    //    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry){
//        registry.addResourceHandler("/**").addResourceLocations("classpath:/**");
//        super.addResourceHandlers(registry);
//
//    }

//    @Override
//    protected void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/index").setViewName("/index.html");
//    }
    /*      @Bean
    public AppDaoImpl appDao(){
        return  new AppDaoImpl();
    }

    @Bean
    public UserServiceImpl userService(){
        UserServiceImpl userService = new UserServiceImpl();
        userService.setAppDao(appDao());
        return userService;
    }
*/
    /* @Bean
    public InternalResourceViewResolver resourceViewResolver(){
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setSuffix(".html");
        resolver.setPrefix("/");
        return resolver;
    }*/

}
