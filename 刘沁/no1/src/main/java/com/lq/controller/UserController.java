package com.lq.controller;

import com.lq.service.UserService;
import com.lq.entity.User;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {
    @Resource
    UserService userService;

    @RequestMapping("/look")
    public String lookUser(User user, Model model){
        System.out.println("1232");
        model.addAttribute("user", userService.getUser(user));
        return "user";
    }

    @RequestMapping("/")
    public String index() {
        // 返回视图名称或重定向到其他页面
        return "index";
    }
}
