package com.lq.dao;

import com.lq.entity.User;


public interface AppDao {
   public String getUser (User user);
}
