package com.lq.dao.impl;

import com.lq.dao.AppDao;
import com.lq.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public class AppDaoImpl implements AppDao {
    @Override
    public String getUser(User user) {
        return "姓名："+user.getName()+"  年龄："+user.getAge();
    }
}
