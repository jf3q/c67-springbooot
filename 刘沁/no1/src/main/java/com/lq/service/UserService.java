package com.lq.service;

import com.lq.entity.User;

public interface UserService {
    String getUser(User user);
}
