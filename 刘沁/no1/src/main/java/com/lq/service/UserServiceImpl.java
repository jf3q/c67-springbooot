package com.lq.service;

import com.lq.dao.AppDao;
import com.lq.entity.User;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private AppDao appDao;


    @Override
    public String getUser(User user) {
        return appDao.getUser(user);
    }
}
