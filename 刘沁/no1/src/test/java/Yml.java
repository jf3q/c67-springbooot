import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@PropertySource("classpath:application.yml")
@Component
public class Yml {

    @Value("${user.name}")
    private String name;

    @Test
    void Ces(){
        System.out.println(name);
    }

}
