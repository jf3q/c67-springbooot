package com.lq.hesuan.service;

import com.lq.hesuan.entity.MedicalAssay;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MedicalAssayService {
    List<MedicalAssay> queryAll(MedicalAssay medicalAssay);

    int insert(MedicalAssay medicalAssay);
    int update(MedicalAssay medicalAssay);
}
