package com.lq.hesuan.controller;

import com.lq.hesuan.entity.Hospital;
import com.lq.hesuan.entity.MedicalAssay;
import com.lq.hesuan.service.HospitalService;
import com.lq.hesuan.service.MedicalAssayService;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class MedicalAssayController {

    @Autowired
    MedicalAssayService medicalAssayService;

    @Autowired
    HospitalService hospitalService;

    @RequestMapping("/getPage")
    public String getPage(@RequestParam(defaultValue = "0") int hospitalId, Model model, HttpServletRequest request) {
        MedicalAssay assay = new MedicalAssay().setHospitalId(hospitalId);
        List<MedicalAssay> assays = medicalAssayService.queryAll(assay);
        List<Hospital> hospitals = hospitalService.queryAll(new Hospital());
        model.addAttribute("meList", assays);
        model.addAttribute("hospName", hospitals);
        request.getSession().setAttribute("hospName", hospitals);
        return "index";
    }

    @RequestMapping("getHname")
    public String getHname(Model model) {
        List<Hospital> hospitals = hospitalService.queryAll(new Hospital());
        model.addAttribute("hospName", hospitals);
        return "add";
    }

    @PostMapping("/addMe")
    public String addMe(MedicalAssay medicalAssay, Model model) {
        int i = medicalAssayService.insert(medicalAssay);
        if (i > 0) {
            return "redirect:getPage";
        }
        return "add";
    }

    @RequestMapping("/updata")
    public String updata(int id,int status) {
        if (status == 0) {
            MedicalAssay assay = new MedicalAssay().setId(id).setAssayResult(1);
            medicalAssayService.update(assay);
        }
        return "redirect:getPage";
    }

}
