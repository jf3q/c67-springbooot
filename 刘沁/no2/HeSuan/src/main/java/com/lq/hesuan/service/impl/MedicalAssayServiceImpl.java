package com.lq.hesuan.service.impl;

import com.lq.hesuan.dao.MedicalAssayDao;
import com.lq.hesuan.entity.MedicalAssay;
import com.lq.hesuan.service.MedicalAssayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicalAssayServiceImpl implements MedicalAssayService {

    @Autowired
    MedicalAssayDao medicalAssayDao;


    @Override
    public List<MedicalAssay> queryAll(MedicalAssay medicalAssay) {
        return medicalAssayDao.queryAll(medicalAssay);
    }

    @Override
    public int insert(MedicalAssay medicalAssay) {
        return medicalAssayDao.insert(medicalAssay);
    }

    @Override
    public int update(MedicalAssay medicalAssay) {
        return medicalAssayDao.update(medicalAssay);
    }
}
