package com.lq.hesuan.service;

import com.lq.hesuan.entity.Hospital;

import java.util.List;

public interface HospitalService {
    List<Hospital> queryAll(Hospital hospital);

}
