package com.lq.hesuan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeSuanApplication {

    public static void main(String[] args) {
        SpringApplication.run(HeSuanApplication.class, args);
    }

}
