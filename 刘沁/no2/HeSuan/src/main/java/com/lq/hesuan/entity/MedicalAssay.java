package com.lq.hesuan.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
import java.io.Serializable;

/**
 * (MedicalAssay)实体类
 *
 * @author makejava
 * @since 2023-12-27 19:09:09
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class MedicalAssay implements Serializable {
    private static final long serialVersionUID = 519253870828704313L;

    private Integer id;

    private String assayUser;

    private Integer hospitalId;

    private Integer assayResult;

    private String phone;

    private String cardNum;
    private String name;

    private String assayTime;


}

