package com.lq.hesuan.service.impl;

import com.lq.hesuan.dao.HospitalDao;
import com.lq.hesuan.entity.Hospital;
import com.lq.hesuan.service.HospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HospitalServiceImpl implements HospitalService {

    @Autowired
    HospitalDao hospitalDao;

    @Override
    public List<Hospital> queryAll(Hospital hospital) {
        return hospitalDao.queryAll(hospital);
    }
}
