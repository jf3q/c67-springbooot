package cn.lq.app.utils;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LanJieQi implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        String requestURI = request.getRequestURI();

        if(token==null){
            response.setStatus(401);
        }else if(SesstionUtils.get(token)==null){
            response.setStatus(403);
        }else {
            String[] split = token.split("-");
            if((System.currentTimeMillis()-Long.valueOf(split[1]))>2*3600*1000){
                response.setStatus(403);
            }
            if(request.equals("/appInfo/shenHe")){
                if(split[3].equals("2")){
                    SesstionUtils.remove(token);
                    response.setStatus(403);
                }
            }
        }
        return true;
    }
}
