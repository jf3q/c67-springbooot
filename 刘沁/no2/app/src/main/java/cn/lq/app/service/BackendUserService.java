package cn.lq.app.service;

import cn.lq.app.dto.LoginDto;
import cn.lq.app.vo.LoginVo;

public interface BackendUserService {
    LoginVo login(LoginDto loginDto);
}
