package cn.lq.app.dto;

public class ApkVo {
    private Long id;
    private String apkname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApkname() {
        return apkname;
    }

    public void setApkname(String apkname) {
        this.apkname = apkname;
    }

    @Override
    public String toString() {
        return "ApkVo{" +
                "id=" + id +
                ", apkname='" + apkname + '\'' +
                '}';
    }
}
