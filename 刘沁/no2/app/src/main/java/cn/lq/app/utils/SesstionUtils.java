package cn.lq.app.utils;

import java.util.HashMap;
import java.util.Map;

public class SesstionUtils {
    static Map<String,Object> map = new HashMap<>();

    public static Object get(String token){
        return map.get(token);
    }

    public static void put(String token,Object user){
        map.put(token,user);
    }

    public static void remove(String token){
        map.remove(token);
    }
}
