package cn.lq.app.vo;

public class LoginVo {
    private String userCode;
    private String userType;
    private String token;


    @Override
    public String toString() {
        return "LoginVo{" +
                "userCode='" + userCode + '\'' +
                ", userType='" + userType + '\'' +
                ", token='" + token + '\'' +
                '}';
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
