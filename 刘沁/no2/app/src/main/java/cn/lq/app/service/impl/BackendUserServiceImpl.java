package cn.lq.app.service.impl;

import cn.lq.app.dao.BackendUserDao;
import cn.lq.app.dto.LoginDto;
import cn.lq.app.entity.BackendUser;
import cn.lq.app.service.BackendUserService;
import cn.lq.app.utils.SesstionUtils;
import cn.lq.app.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class BackendUserServiceImpl implements BackendUserService {
    @Autowired
    BackendUserDao backendUserDao;

    @Override
    public LoginVo login(LoginDto loginDto) {
        if(loginDto.getAccount().equals(null)||loginDto.getAccount().equals("")){
            throw new RuntimeException("账号未填写");
        }
        if(loginDto.getPassword().equals(null)||loginDto.getPassword().equals("")){
            throw new RuntimeException("密码未填写");
        }

        BackendUser user = new BackendUser();
        user.setUsercode(loginDto.getAccount());
        user.setUserpassword(loginDto.getPassword());
        List<BackendUser> users = backendUserDao.queryAll(user);
        if(users.size()==0){
            throw new RuntimeException("账号或密码错误");
        }
        LoginVo loginVo = new LoginVo();
        loginVo.setUserCode(users.get(0).getUsercode());
        loginVo.setUserType("1");
        String replace = UUID.randomUUID().toString().replace("-", "");
        replace =replace+"-"+System.currentTimeMillis()+"-"+users.get(0).getId()+"-"+"1";
        loginVo.setToken(replace);

        SesstionUtils.put(replace,users.get(0));
        return loginVo;
    }
}
