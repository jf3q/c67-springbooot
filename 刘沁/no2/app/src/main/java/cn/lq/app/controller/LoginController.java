package cn.lq.app.controller;

import cn.lq.app.dto.LoginDto;
import cn.lq.app.service.BackendUserService;
import cn.lq.app.service.DevUserService;
import cn.lq.app.utils.SesstionUtils;
import cn.lq.app.vo.LoginVo;
import cn.lq.app.vo.ResultVo;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


@RestController
@Api(tags = "登录接口")
public class LoginController {

    @Resource
    DevUserService devUserService;

    @Resource
    BackendUserService backendUserService;


    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginDto loginDto){
        LoginVo loginVo =null;
        if(loginDto.getType()==1){
            try {
                loginVo = backendUserService.login(loginDto);

            } catch (Exception e) {
                e.printStackTrace();
                return  ResultVo.error(e.getMessage());
            }
        }else if(loginDto.getType()==2){
            try {
                loginVo = devUserService.login(loginDto);
            } catch (Exception e) {
                e.printStackTrace();
                return  ResultVo.error(e.getMessage());
            }
        }
        return  ResultVo.success("登录成功",loginVo);
    }


    @GetMapping("/loginOut")
    public ResultVo loginOut(HttpServletRequest request){
        String token = request.getHeader("token");
        if(SesstionUtils.get(token)==null){
            return ResultVo.error("退出失败");
        }
        SesstionUtils.remove(token);
        return ResultVo.success("退出成功","");
    }
}
