package cn.lq.app.service.impl;

import cn.lq.app.dao.DevUserDao;
import cn.lq.app.dto.LoginDto;
import cn.lq.app.entity.DevUser;
import cn.lq.app.service.DevUserService;
import cn.lq.app.utils.SesstionUtils;
import cn.lq.app.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DevUserServiceImpl implements DevUserService {
    @Autowired
    DevUserDao devUserDao;

    @Override
    public LoginVo login(LoginDto loginDto) {
        if(loginDto.getAccount().equals(null)||loginDto.getAccount().equals("")){
            throw new RuntimeException("账号未填写");
        }
        if(loginDto.getPassword().equals(null)||loginDto.getPassword().equals("")){
            throw new RuntimeException("密码未填写");
        }

        DevUser user = new DevUser();
        user.setDevcode(loginDto.getAccount());
        user.setDevpassword(loginDto.getPassword());
        List<DevUser> users = devUserDao.queryAll(user);
        if(users.size()==0){
            throw new RuntimeException("账号或密码错误");
        }
        LoginVo loginVo = new LoginVo();
        loginVo.setUserCode(users.get(0).getDevcode());
        loginVo.setUserType("2");
        String replace = UUID.randomUUID().toString().replace("-", "");
       String token =replace+"-"+System.currentTimeMillis()+"-"+users.get(0).getId()+"-"+"2";
        loginVo.setToken(token);
        SesstionUtils.put(token,users.get(0));
        return loginVo;
    }
}
