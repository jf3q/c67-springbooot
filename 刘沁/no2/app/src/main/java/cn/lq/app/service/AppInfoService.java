package cn.lq.app.service;

import cn.lq.app.entity.AppInfo;
import cn.lq.app.entity.AppVersion;
import cn.lq.app.vo.TreesVo;
import com.github.pagehelper.PageInfo;


import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface AppInfoService {
    PageInfo<AppInfo> getPage(AppInfo appInfo, Integer num);

   TreesVo getTrees(HttpServletRequest request);

    void addAndUp(AppInfo appInfo);

    boolean getApkName(AppInfo appInfo);

    AppInfo getOne(Integer id);

    void del(Long id,HttpServletRequest request);

    List<AppVersion> getVerList(Long id);

    void addVerOrUpVer(AppVersion appVersion);
}
