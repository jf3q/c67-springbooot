package cn.lq.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class No22Application {

    public static void main(String[] args) {
        SpringApplication.run(No22Application.class, args);
    }

}
