package cn.lq.app.service.impl;

import cn.lq.app.dao.AppCategoryDao;
import cn.lq.app.dao.AppInfoDao;
import cn.lq.app.dao.AppVersionDao;
import cn.lq.app.entity.AppCategory;
import cn.lq.app.entity.AppInfo;
import cn.lq.app.entity.AppVersion;
import cn.lq.app.service.AppInfoService;
import cn.lq.app.vo.TreesVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class AppInfoServiceImpl implements AppInfoService {
    @Resource
    AppInfoDao appInfoDao;

    @Resource
    AppCategoryDao appCategoryDao;

    @Resource
    AppVersionDao appVersionDao;

    @Override
    public PageInfo<AppInfo> getPage(AppInfo appInfo, Integer num) {
        PageHelper.startPage(num, 10, "id desc");
        PageInfo<AppInfo> pageInfo = new PageInfo<>(appInfoDao.queryAll(appInfo));
        return pageInfo;
    }

    @Override
    public TreesVo getTrees(HttpServletRequest request) {
        List<TreesVo> tList = new ArrayList<>();
        List<AppCategory> cList = appCategoryDao.queryAll(new AppCategory());

        for (AppCategory category : cList) {
            TreesVo vo = new TreesVo();
            BeanUtils.copyProperties(category, vo);
            tList.add(vo);
        }
        TreesVo vo = new TreesVo();
        for (TreesVo treesVo : tList) {
            if (treesVo.getParentid() == null) {
                vo = three(treesVo, tList);

            }
        }
        return vo;
    }

    @Override
    public void addAndUp(AppInfo appInfo) {
        if (appInfo.getId() == null) {
            appInfoDao.insert(appInfo);
        } else {
            appInfoDao.update(appInfo);
        }
    }

    @Override
    public boolean getApkName(AppInfo appInfo) {

        Integer count = appInfoDao.getapkCount(appInfo.getApkname());
        if (appInfo.getId() != null) {
            AppInfo info = appInfoDao.queryById(appInfo.getId());
            if (info.getApkname().equals(appInfo.getApkname())) {
                return true;
            }
        }
        if (count > 0) {
            return false;
        }
        return true;

    }

    @Override
    public AppInfo getOne(Integer id) {
        return appInfoDao.queryById(Long.valueOf(id));
    }

    @Override
    public void del(Long id, HttpServletRequest request) {
        String path = request.getServletContext().getRealPath("/apk");
        AppVersion appVersion = new AppVersion();
        appVersion.setId(id);
        List<AppVersion> appVersions = appVersionDao.queryAll(appVersion);
        for (AppVersion version : appVersions) {
            File file = new File(path + "/" + version.getDownloadlink());
            file.delete();
        }

        appVersionDao.deleteById(id);

        AppInfo appInfo = appInfoDao.queryById(id);
        File file = new File(path + "/" + appInfo.getLogopicpath());
        file.delete();

        appInfoDao.deleteById(id);
    }

    @Override
    public List<AppVersion> getVerList(Long id) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(id);
        return appVersionDao.queryAll(appVersion);
    }

    @Override
    public void addVerOrUpVer(AppVersion appVersion) {
        appVersionDao.insert(appVersion);
    }

    private TreesVo three(TreesVo treesVo, List<TreesVo> tList) {
        List<TreesVo> ziTree = new ArrayList<>();
        for (TreesVo vo : tList) {
            if (vo.getParentid() == treesVo.getId()) {
                ziTree.add(vo);
                three(vo, tList);
            }
        }
        treesVo.settList(ziTree);
        return treesVo;
    }
}
