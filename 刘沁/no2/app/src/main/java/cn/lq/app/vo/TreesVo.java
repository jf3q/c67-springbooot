package cn.lq.app.vo;

import java.util.List;

public class TreesVo {
    private Long id;
    private String categoryname;
    private Long parentid;
    List<TreesVo> tList;

    @Override
    public String toString() {
        return "TreesVo{" +
                "id=" + id +
                ", categoryname='" + categoryname + '\'' +
                ", parentid=" + parentid +
                ", tList=" + tList +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    public List<TreesVo> gettList() {
        return tList;
    }

    public void settList(List<TreesVo> tList) {
        this.tList = tList;
    }
}
