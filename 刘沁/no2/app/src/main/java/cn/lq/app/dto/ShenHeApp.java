package cn.lq.app.dto;

public class ShenHeApp {
    private Long id;
    private Long status;

    @Override
    public String toString() {
        return "ShenHeApp{" +
                "id=" + id +
                ", status=" + status +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
}
