package cn.lq.app.service;

import cn.lq.app.dto.LoginDto;
import cn.lq.app.vo.LoginVo;

public interface DevUserService {
    LoginVo login(LoginDto loginDto);
}
