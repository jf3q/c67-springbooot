package com.boot.mybatisplusdemo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boot.mybatisplusdemo.dao.StudentDao;
import com.boot.mybatisplusdemo.entity.Student;
import com.boot.mybatisplusdemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
@Service
public class StudentServiceImpl extends ServiceImpl<StudentDao, Student> implements StudentService {
    @Override
    public Page getPage(String stuName, Integer pageNum) {
        Page page=new Page(pageNum,3);
        LambdaQueryWrapper<Student> queryWrapper=new LambdaQueryWrapper<>();
//        if (StringUtils.hasText(stuName)) {
//            queryWrapper.like(Student::getStudentname,stuName);
//        }
        queryWrapper.like(StringUtils.hasText(stuName),Student::getStudentname,stuName);
        queryWrapper.orderByDesc(Student::getId);
        return this.page(page,queryWrapper);
    }
}
