package com.boot.mybatisplusdemo.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.boot.mybatisplusdemo.entity.Student;
import org.springframework.stereotype.Service;

@Service
public interface StudentService extends IService<Student> {

    Page getPage(String stuName, Integer pageNum);
}
