package com.example.demo.service;

import com.example.demo.dao.HospitalDao;
import com.example.demo.entity.Hospital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HospitalService {

    @Autowired
    HospitalDao hospitalDao;

    public List<Hospital> findAll(){
        return hospitalDao.queryAllBy();
    }

    public Hospital findAll(Long id){
        return hospitalDao.queryById(id);
    }

    public void addAll(Hospital hospital){
        hospitalDao.insert(hospital);
    }
}
