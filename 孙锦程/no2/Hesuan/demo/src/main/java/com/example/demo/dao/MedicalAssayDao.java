package com.example.demo.dao;

import com.example.demo.entity.MedicalAssay;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (MedicalAssay)表数据库访问层
 *
 * @author makejava
 * @since 2023-12-29 10:37:05
 */
@Mapper
public interface MedicalAssayDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    MedicalAssay queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @return 对象列表
     */
    List<MedicalAssay> queryAllBy(Long hospital_id);

    /**
     * 统计总行数
     *
     * @param medicalAssay 查询条件
     * @return 总行数
     */
    long count(MedicalAssay medicalAssay);

    /**
     * 新增数据
     *
     * @param medicalAssay 实例对象
     * @return 影响行数
     */
    int insert(MedicalAssay medicalAssay);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<MedicalAssay> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<MedicalAssay> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<MedicalAssay> 实例对象列表
     * @return 影响行数
     * @throws org.springframework.jdbc.BadSqlGrammarException 入参是空List的时候会抛SQL语句错误的异常，请自行校验入参
     */
    int insertOrUpdateBatch(@Param("entities") List<MedicalAssay> entities);

    /**
     * 修改数据
     *
     * @param medicalAssay 实例对象
     * @return 影响行数
     */
    int update(MedicalAssay medicalAssay);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);





}

