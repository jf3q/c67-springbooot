package com.boot.bookmybatis.controller;

import com.boot.bookmybatis.entity.Employee;
import com.boot.bookmybatis.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/emp")
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;
    @GetMapping("/{empno}")
    public String list(@PathVariable Integer empno, Model model){
        Employee employee=employeeService.getEmp(empno);
        model.addAttribute(employee);
        return "emp"                                                                                                            ;
    }
}
