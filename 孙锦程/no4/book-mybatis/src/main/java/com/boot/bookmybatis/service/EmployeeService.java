package com.boot.bookmybatis.service;

import com.boot.bookmybatis.dao.EmployeeMapper;
import com.boot.bookmybatis.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    @Autowired
    EmployeeMapper employeeMapper;

    public Employee getEmp(Integer empno) {
        return employeeMapper.getOne(empno);
    }
}
