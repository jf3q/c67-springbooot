package com.boot.bookmybatis.dao;

import com.boot.bookmybatis.entity.Book;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface BookMapper {
//    @Select("select * from book")
    List<Book> selectList();

    void del(Integer[] ids);
}
