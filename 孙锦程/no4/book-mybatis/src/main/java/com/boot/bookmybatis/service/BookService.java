package com.boot.bookmybatis.service;

import com.boot.bookmybatis.entity.Book;
import com.boot.bookmybatis.dao.BookMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    @Autowired
    BookMapper bookMapper;

    public List<Book> getAll() {
        return bookMapper.selectList();
    }

    public PageInfo<Book> getPage(Integer pageNum) {
        PageHelper.startPage(pageNum,3,"id desc");
        List<Book> list = bookMapper.selectList();
        return new PageInfo<>(list);
    }

    public void del(Integer[] ids) {
        bookMapper.del(ids);
    }
}
