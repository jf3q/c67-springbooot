package com.boot.bookmybatis.controller;

import com.boot.bookmybatis.entity.Book;
import com.boot.bookmybatis.service.BookService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {
    @Autowired
    BookService bookService;
    @GetMapping
    public String getAll(Model model){
        List<Book> list=bookService.getAll();
        model.addAttribute("books",list);
        return "booklist";
    }
    @GetMapping("/page")
    public String getPage(@RequestParam(defaultValue = "1") Integer pageNum,Model model){
        PageInfo<Book> pageInfo = bookService.getPage(pageNum);
        model.addAttribute("page",pageInfo);
        return "bookPage";
    }
    @PostMapping("/del")
    public String del(Integer[] ids){
        if (ids.length>0){
            bookService.del(ids);
        }
        return "redirect:/book/page";
    }
}
