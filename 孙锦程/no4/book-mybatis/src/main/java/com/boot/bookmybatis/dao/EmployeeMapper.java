package com.boot.bookmybatis.dao;

import com.boot.bookmybatis.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EmployeeMapper {
    Employee getOne(Integer empno);
}
