package com.jf3q.dev.service;

import com.jf3q.common.dao.DevUserDao;
import com.jf3q.common.dto.LoginUserDto;
import com.jf3q.common.entity.DevUser;
import com.jf3q.common.utils.SessionUtils;
import com.jf3q.common.vo.LoginUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DevService {

    @Autowired
    DevUserDao userDao;
    public LoginUserVo login(LoginUserDto userDto) {

        LoginUserVo userVo= new LoginUserVo();
        DevUser devUser = new DevUser();
        devUser.setDevcode(userDto.getAccount());
        devUser.setDevpassword(userDto.getPassword());
        List<DevUser> devUsers = userDao.queryAllBy(devUser);
        if (devUsers ==null ||devUsers.size()==0) {
            throw new RuntimeException("账号密码错误");
        }
        userVo.setUser(devUsers.get(0));

        //token  uuid-usercode-id-userType-创建时间
        StringBuffer stringBuffer= new StringBuffer();
        stringBuffer.append(UUID.randomUUID().toString().replace("-",""));
        stringBuffer.append("-"+devUsers.get(0).getDevcode());
        stringBuffer.append("-"+devUsers.get(0).getId());
        stringBuffer.append("-dev");
        stringBuffer.append("-"+System.currentTimeMillis());
        //将token存到缓存里
        SessionUtils.setToken(stringBuffer.toString(),devUsers.get(0));

        userVo.setToken(stringBuffer.toString());
        return userVo;

    }
}
