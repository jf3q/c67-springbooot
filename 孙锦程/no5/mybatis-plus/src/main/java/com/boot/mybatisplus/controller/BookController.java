package com.boot.mybatisplus.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.boot.mybatisplus.entity.Book;
import com.boot.mybatisplus.service.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/books")
public class BookController {
    @Autowired
    IBookService bookService;

    @GetMapping
    public String page(@RequestParam(defaultValue = "1") Integer pageNum, Model model) {
        IPage page = bookService.getPage(pageNum);
        model.addAttribute("page", page);
        return "bookPage";
    }

    @PostMapping
    public String searchBooks(Model model, Book book) {
        List<Book> list = bookService.searchBooks(book);
        model.addAttribute("list", list);
        return "books";
    }
}
