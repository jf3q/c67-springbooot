package com.boot.mybatisplus.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boot.mybatisplus.dao.BookMapper;
import com.boot.mybatisplus.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements IBookService {
    @Autowired
    BookMapper bookMapper;

    @Override
    public List<Book> searchBooks(Book book) {
        LambdaQueryWrapper<Book> queryWrapper = new LambdaQueryWrapper();
        if (book.getName() != null && book.getName() != "") {
            queryWrapper.like(Book::getName,book.getName());
        }
        if (book.getCategory() != null && book.getCategory() != "") {
            queryWrapper.eq(Book::getCategory,book.getCategory());
        }
        if (book.getAuthor() != null && book.getAuthor() != "") {
            queryWrapper.eq(Book::getAuthor,book.getAuthor());
        }
        return bookMapper.selectList(queryWrapper);
    }

    @Override
    public IPage getPage(Integer pageNum) {
        IPage page=new Page(pageNum,3);
        return bookMapper.selectPage(page,null);
    }
}