package com.boot.mybatisplus.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boot.mybatisplus.entity.Book;
import org.apache.ibatis.annotations.Mapper;

    @Mapper
    public interface BookMapper extends BaseMapper<Book> {

    }
