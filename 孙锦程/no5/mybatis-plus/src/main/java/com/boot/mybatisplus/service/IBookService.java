package com.boot.mybatisplus.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.boot.mybatisplus.entity.Book;

import java.util.List;

public interface IBookService extends IService<Book> {
    List<Book> searchBooks(Book book);

    IPage getPage(Integer pageNum);
}
