package com.jzy.app.service.impl;

import com.jzy.app.dto.LoginDto;
import com.jzy.app.entity.DevUser;
import com.jzy.app.mapper.DevUserDao;
import com.jzy.app.service.DevUserService;
import com.jzy.app.utils.StringBufferUtils;
import com.jzy.app.utils.sessionUtils;
import com.jzy.app.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class DevUserServiceImpl implements DevUserService {


    @Autowired
    DevUserDao devUserDao;

    @Override
    public LoginVo loginUser(LoginDto loginDto) {

        DevUser devUser = new DevUser();
        devUser.setDevcode(loginDto.getAccount());
        devUser.setDevpassword(loginDto.getPasswrod());
        List<DevUser> devUsers = devUserDao.queryAllBy(devUser);
        if (devUsers.size()==0){
            throw new RuntimeException("账号或密码错误！");
        }
        DevUser devUser1 = devUsers.get(0);

        LoginVo loginVo = new LoginVo();
        loginVo.setId(devUser1.getId());
        loginVo.setUserType(loginDto.getUserType());
        loginVo.setUseracount(devUser1.getDevcode());
        loginVo.setUserName(devUser1.getDevname());
        loginVo.setToken(StringBufferUtils.joint(loginVo.getUseracount(),loginDto.getUserType()));
        sessionUtils.put(loginVo.getToken(),loginVo);


        return loginVo;
    }
}
