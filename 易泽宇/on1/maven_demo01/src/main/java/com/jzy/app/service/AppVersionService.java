package com.jzy.app.service;

import com.jzy.app.entity.AppVersion;

import java.util.List;

public interface AppVersionService  {

    List<AppVersion> getAppVersionList(Long id);

    int insertAppVersion(AppVersion appVersion);
}
