package com.example.springboot_demo1.controller;

import com.example.springboot_demo1.enitiy.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HelloWordController {

    @Autowired
    UserInfo userInfo;

    @Value("${str[0]}")
    private String username;

    @Value("${str1[1]}")
    private String password;

    @RequestMapping("/helloWord")
    public String helloWord(){
        System.out.println(userInfo);
        System.out.println(username);
        System.out.println(password);
        return "hello spring-boot";
    }
}