package com.example.springboot_demo1.enitiy;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "user")
public class UserInfo {

    private String username;

    private String password;
}
