package com.example.springboot_demo01.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * (MedicalAssay)实体类
 *
 * @author makejava
 * @since 2023-12-28 11:05:24
 */
@Data
@Accessors(chain = true)
public class MedicalAssay implements Serializable {
    private static final long serialVersionUID = -30697703722848793L;
    /**
     * 检测编号
     */
    private Long id;
    /**
     * 被检测人
     */
    private String assayUser;
    /**
     * 检测机构编号
     */
    private Long hospitalId;
    /**
     * 检测结果  0:检测中 1:确诊
     */
    private Integer assayResult;
    /**
     * 被检测人电话号
     */
    private String phone;
    /**
     * 被检测人身份证号
     */
    private String cardNum;
    /**
     * 检测日期  yyyy-MM-dd
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date assayTime;


    private String hospitalName;


}

