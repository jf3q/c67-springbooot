package com.example.springboot_demo01.service.impl;

import com.example.springboot_demo01.dao.MedicalAssayDao;
import com.example.springboot_demo01.entity.MedicalAssay;
import com.example.springboot_demo01.service.MedicalassayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MedicalassayServiceImpl implements MedicalassayService {

    @Autowired
    MedicalAssayDao medicalAssayDao;
    @Override
    public List<MedicalAssay> getMedical(Long id) {
        return medicalAssayDao.queryAllBy(new MedicalAssay().setHospitalId(id));
    }

    @Override
    public int updataMed(Long id) {
        return medicalAssayDao.update(id);
    }

    @Override
    public void add(MedicalAssay medicalAssay) {
        medicalAssayDao.insert(medicalAssay);
    }
}
