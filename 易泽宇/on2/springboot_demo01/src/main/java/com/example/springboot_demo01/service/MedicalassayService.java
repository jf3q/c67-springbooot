package com.example.springboot_demo01.service;


import com.example.springboot_demo01.entity.MedicalAssay;

import java.util.List;

public interface MedicalassayService  {

    List<MedicalAssay> getMedical(Long id);

    int updataMed(Long id);

    void add(MedicalAssay medicalAssay);
}
