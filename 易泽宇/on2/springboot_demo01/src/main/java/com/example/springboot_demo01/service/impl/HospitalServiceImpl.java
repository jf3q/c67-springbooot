package com.example.springboot_demo01.service.impl;

import com.example.springboot_demo01.dao.HospitalDao;
import com.example.springboot_demo01.entity.Hospital;
import com.example.springboot_demo01.service.HospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class HospitalServiceImpl implements HospitalService {

    @Autowired
    HospitalDao hospitalDao;
    @Override
    public List<Hospital> getHosputalList() {
        return hospitalDao.queryAllBy(new Hospital());
    }
}
