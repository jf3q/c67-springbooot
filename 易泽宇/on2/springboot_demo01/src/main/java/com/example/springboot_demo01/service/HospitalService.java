package com.example.springboot_demo01.service;

import com.example.springboot_demo01.entity.Hospital;

import java.util.List;

public interface HospitalService  {

    List<Hospital> getHosputalList();
}
