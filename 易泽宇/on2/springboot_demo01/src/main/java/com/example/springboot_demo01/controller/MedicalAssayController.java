package com.example.springboot_demo01.controller;


import com.example.springboot_demo01.entity.Hospital;
import com.example.springboot_demo01.entity.MedicalAssay;
import com.example.springboot_demo01.service.HospitalService;
import com.example.springboot_demo01.service.MedicalassayService;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.resource.HttpResource;

import java.util.List;

@Controller
@RequestMapping("/medical")
public class MedicalAssayController {

    @Autowired
    MedicalassayService medicalassayService;

    @Autowired
    HospitalService hospitalService;


    @RequestMapping("/medicaList")
    public String getMedicaList(Long id, Model model){
        model.addAttribute("list",medicalassayService.getMedical(id));
        return "index";
    }

    @RequestMapping("/hospital")
    @ResponseBody
    public List<Hospital> getHospitalList(){
        return hospitalService.getHosputalList();
    }

    @RequestMapping("/updataMedical")
    public String updateMed(Long id){
        medicalassayService.updataMed(id);
        return "redirect:/medical/medicaList";
    }


    @RequestMapping("/add")
    public String addMed(MedicalAssay medicalAssay){
        medicalAssay.setAssayResult(0);
        medicalassayService.add(medicalAssay);
        return "redirect:/medical/medicaList";
    }
}
