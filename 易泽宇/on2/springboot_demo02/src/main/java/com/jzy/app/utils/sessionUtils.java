package com.jzy.app.utils;

import java.util.HashMap;
import java.util.Map;

public class sessionUtils {

    private static Map map = new HashMap();

    public  static Object get(String key){
        return map.get(key);
    }

    public static  void  put(String key,Object o){
        map.put(key,o);
    }

    public static void remove(String key){
        map.remove(key);
    }
}
