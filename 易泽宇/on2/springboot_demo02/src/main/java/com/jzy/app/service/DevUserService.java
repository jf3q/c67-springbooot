package com.jzy.app.service;

import com.jzy.app.dto.LoginDto;
import com.jzy.app.vo.LoginVo;

public interface DevUserService  {


    LoginVo loginUser(LoginDto loginDto);
}
