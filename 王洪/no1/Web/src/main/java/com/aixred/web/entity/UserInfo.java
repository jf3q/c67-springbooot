package com.aixred.web.entity;

import lombok.Data;

@Data
public class UserInfo {

    private String id;
    private String username;

    private String password;

}
