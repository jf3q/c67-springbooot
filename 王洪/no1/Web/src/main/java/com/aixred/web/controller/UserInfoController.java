package com.aixred.web.controller;

import com.aixred.web.entity.UserInfo;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserInfoController {

    @PostMapping()
    public String addUser(@RequestBody UserInfo userInfo){
        System.out.println("新增用户信息："+userInfo);
        return "新增用户成功";
    }

    @DeleteMapping("/{id}")
    public String delUser(@PathVariable Integer id){
        System.out.println("删除用户id"+id);
        return "删除用户成功";
    }

    @PutMapping("")
    public String putUser(@RequestBody UserInfo userInfo){
        System.out.println("修改后的用户信息："+userInfo);
        return "修改成功";
    }

    @GetMapping("/{id}")
    public String getUser(@PathVariable Integer id){
        System.out.println("查找用户信息id："+id);
        return "查找用户id成功";
    }

    @GetMapping("")
    public String getUsers(){
        System.out.println("查找所有用户");
        return "查找所有用户成功";
    }



}
