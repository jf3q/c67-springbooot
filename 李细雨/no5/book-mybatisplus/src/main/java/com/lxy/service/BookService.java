package com.lxy.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lxy.pojo.Book;

public interface BookService extends IService<Book> {
}
