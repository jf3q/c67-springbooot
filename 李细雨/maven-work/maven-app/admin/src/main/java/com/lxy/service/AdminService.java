package com.lxy.service;

import com.lxy.dto.LoginDto;
import com.lxy.entity.BackendUser;
import com.lxy.mapper.BackendUserDao;
import com.lxy.util.SessionUtils;
import com.lxy.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class AdminService {
    @Autowired
    BackendUserDao backendUserDao;

    public LoginVo login(LoginDto loginDto) {
        LoginVo loginVo  = new LoginVo();
        //管理员
        BackendUser devUser = new BackendUser();
        devUser.setUsercode(loginDto.getAccount());
        devUser.setUserpassword(loginDto.getPassword());
        List<BackendUser> devUsers = backendUserDao.queryAllBy(devUser);
        if(devUsers.size()==0){
            throw new RuntimeException("账号密码错误");
        }
        //生成token
        String token = UUID.randomUUID().toString().replace("-","");
        token+="-"+loginDto.getUserType();
        token+="-"+devUsers.get(0).getId();
        token+="-"+System.currentTimeMillis();

        loginVo.setToken(token);
        loginVo.setAccount(devUsers.get(0).getUsercode());
        loginVo.setLoginType(loginDto.getUserType());

        SessionUtils.add(token,devUsers.get(0));
        return loginVo;
    }
}
