package com.lxy.controller;

import com.lxy.dto.LoginDto;
import com.lxy.service.DevUserService;
import com.lxy.vo.LoginVo;
import com.lxy.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dev")
public class DevUserController {
    @Autowired
    DevUserService loginService;
    @RequestMapping("/login")
    public ResultVo login(@RequestBody LoginDto loginDto){
        try {
            LoginVo vo = loginService.login(loginDto);
            return ResultVo.success(vo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
    }
}
