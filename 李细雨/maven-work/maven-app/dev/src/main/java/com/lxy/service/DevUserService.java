package com.lxy.service;

import com.lxy.dto.LoginDto;
import com.lxy.entity.DevUser;
import com.lxy.mapper.DevUserDao;
import com.lxy.util.SessionUtils;
import com.lxy.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DevUserService {
    @Autowired
    DevUserDao devUserDao;

    public LoginVo login(LoginDto loginDto) {
        LoginVo loginVo  = new LoginVo();
        //开发者
        DevUser devUser = new DevUser();
        devUser.setDevcode(loginDto.getAccount());
        devUser.setDevpassword(loginDto.getPassword());
        List<DevUser> devUsers = devUserDao.queryAllBy(devUser);
        if(devUsers.size()==0){
            throw new RuntimeException("账号密码错误");
        }
        //生成token
        String token = UUID.randomUUID().toString().replace("-","");
        token+="-"+loginDto.getUserType();
        token+="-"+devUsers.get(0).getId();
        token+="-"+System.currentTimeMillis();

        loginVo.setToken(token);
        loginVo.setAccount(devUsers.get(0).getDevcode());
        loginVo.setLoginType(loginDto.getUserType());

        SessionUtils.add(token,devUsers.get(0));
        return loginVo;
    }
}
