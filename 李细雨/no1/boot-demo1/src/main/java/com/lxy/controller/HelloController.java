package com.lxy.controller;

import com.lxy.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @Value("${city[1]}")
    private String str;
    @Autowired
    private User user;
    @RequestMapping("/hello")
    public String hello() {
        System.out.println(user);
        return "helloSpringBoot:"+str;
    }
}