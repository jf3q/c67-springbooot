package org.lxy;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "lxy")
public class RedisProperties {
    private String host="localhost";
    private Integer port=6379;
    private Integer database=0;
    private Integer timeout=2000;
    private String password;
}
