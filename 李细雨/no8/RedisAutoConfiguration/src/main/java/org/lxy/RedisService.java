package org.lxy;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@Data
@AllArgsConstructor
@Component
@NoArgsConstructor
public class RedisService {
    private JedisPool jedisPool;
    private Jedis jedis;

    public void set(String key,String value,Integer endTime){
       try {
           jedis= jedisPool.getResource();
           jedis.setex(key,endTime,value);
       }catch (Exception e){
           e.printStackTrace();
       }finally {
           jedisPool.close();
       }
    }
    public void set(String key,String value){
        try {
            jedis= jedisPool.getResource();
            jedis.set(key,value);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            jedisPool.close();
        }
    }
    public void del(String key){
        try {
            jedis= jedisPool.getResource();
            jedis.del(key);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            jedisPool.close();
        }
    }
    public void get(String key){
        try {
            jedis= jedisPool.getResource();
            jedis.get(key);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            jedisPool.close();
        }
    }
}