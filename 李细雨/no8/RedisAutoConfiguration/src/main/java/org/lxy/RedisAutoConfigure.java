package org.lxy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Configuration
@EnableConfigurationProperties(RedisProperties.class)
public class RedisAutoConfigure {
    @Autowired
    RedisProperties redisProperties;
    @Bean
    public JedisPoolConfig jedisPoolConfig(){
        return new JedisPoolConfig();
    }
    @Bean
    public RedisService redisService(){
        RedisService redisService = new RedisService();
        redisService.setJedisPool(jedisPool());
        return redisService;
    }
    @Bean
    public JedisPool jedisPool(){
        return new JedisPool(jedisPoolConfig(),redisProperties.getHost(),redisProperties.getPort(),
                redisProperties.getTimeout(),redisProperties.getPassword(),redisProperties.getDatabase());
    }
}
