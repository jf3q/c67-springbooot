package com.lxy.controller;

import com.lxy.dto.LoginDto;
import com.lxy.service.LoginService;
import com.lxy.vo.LoginVo;
import com.lxy.vo.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "User操作接口")
public class LoginController {
    @Autowired
    private LoginService loginService;

    @RequestMapping("/login")
    @ApiOperation(value = "用户登录", notes = "根据信息查询用户")
    public ResultVo login(@RequestBody LoginDto loginDto) {
        try {
            LoginVo vo = loginService.login(loginDto);
            return ResultVo.success(vo);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultVo.error(e.getMessage());
        }
    }
}
