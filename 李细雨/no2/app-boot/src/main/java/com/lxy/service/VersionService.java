package com.lxy.service;

import com.lxy.entity.AppVersion;
import com.lxy.mapper.AppVersionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VersionService {
    @Autowired
    private AppVersionDao appVersionDao;
    public List<AppVersion> getVersion(Long appid) {
        AppVersion appVersion= new AppVersion();
        appVersion.setAppid(appid);
        return appVersionDao.queryAllBy(appVersion);
    }

    public void add(AppVersion appVersion) {
        appVersionDao.insert(appVersion);
    }
}
