package com.lxy.interceptor;

import com.lxy.util.SessionUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserInterceptor implements HandlerInterceptor {

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        if(token==null || SessionUtils.get(token)==null){
            response.setStatus(401);
        }else {
            String[] split = token.split("-");
            String time = split[3];
            if(System.currentTimeMillis()-Long.valueOf(time)>2*3600*1000){
                response.setStatus(401);
            }
            return true;
        }
        return false;
    }
}
