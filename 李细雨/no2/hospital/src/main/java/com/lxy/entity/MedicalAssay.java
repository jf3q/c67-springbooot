package com.lxy.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * (MedicalAssay)实体类
 *
 * @author makejava
 * @since 2023-12-28 09:51:25
 */
public class MedicalAssay implements Serializable {
    private static final long serialVersionUID = 533314661156199152L;

    private Integer id;
    private String name;

    private String assayUser;

    private Integer hospitalId;

    private Integer assayResult;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String phone;

    private String cardNum;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date assayTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAssayUser() {
        return assayUser;
    }

    public void setAssayUser(String assayUser) {
        this.assayUser = assayUser;
    }

    public Integer getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(Integer hospitalId) {
        this.hospitalId = hospitalId;
    }

    public Integer getAssayResult() {
        return assayResult;
    }

    public void setAssayResult(Integer assayResult) {
        this.assayResult = assayResult;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public Date getAssayTime() {
        return assayTime;
    }

    public void setAssayTime(Date assayTime) {
        this.assayTime = assayTime;
    }

}

