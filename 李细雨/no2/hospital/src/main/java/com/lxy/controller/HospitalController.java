package com.lxy.controller;

import com.lxy.entity.Hospital;
import com.lxy.entity.MedicalAssay;
import com.lxy.service.HospitalService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class HospitalController {
    @Autowired
    HospitalService hospitalService;
    @GetMapping("/get")
    public String getSel(Integer hospitalId,Model model){
        List<MedicalAssay> list =  hospitalService.getSel(hospitalId);
        List<Hospital> hospitals = hospitalService.getHosp();
        model.addAttribute("hosp",hospitals);
        model.addAttribute("med",list);
        return "index";
    }
    @GetMapping("/hosp")
    public String getHosp(Model model){
        List<Hospital> hospitals = hospitalService.getHosp();
        model.addAttribute("hosp",hospitals);
        return "add";
    }
    @PostMapping("/add")
    public String add(MedicalAssay medicalAssay){
        medicalAssay.setAssayResult(0);
        hospitalService.add(medicalAssay);
        return "redirect:/get";
    }
    @GetMapping("/up/{id}")
    public String up(@PathVariable Integer id){
        hospitalService.up(id);
        return "redirect:/get";
    }
}
