package com.lxy.service;

import com.lxy.dao.HospitalDao;
import com.lxy.dao.MedicalAssayDao;
import com.lxy.entity.Hospital;
import com.lxy.entity.MedicalAssay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HospitalService {
    @Autowired
    MedicalAssayDao medicalAssayDao;
    @Autowired
    HospitalDao hospitalDao;

    public List<MedicalAssay> getSel(Integer hospitalId) {
        MedicalAssay medicalAssay = new MedicalAssay();
        medicalAssay.setHospitalId(hospitalId);
       return   medicalAssayDao.queryAllBy(medicalAssay);
    }

    public List<Hospital> getHosp() {
      return   hospitalDao.queryAllBy(new Hospital());
    }

    public void add(MedicalAssay medicalAssay) {
        medicalAssayDao.insert(medicalAssay);
    }

    public void up(Integer id) {
        MedicalAssay medicalAssay = new MedicalAssay();
        medicalAssay.setId(id);
        medicalAssay.setAssayResult(1);
        medicalAssayDao.update(medicalAssay);
    }
}
