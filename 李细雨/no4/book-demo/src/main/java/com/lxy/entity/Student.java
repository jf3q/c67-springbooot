package com.yxc.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * (Student)实体类
 *
 * @author makejava
 * @since 2023-12-29 08:14:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private Integer id;
    private String studentname;
    private String gender;
    private Integer age;
    private Integer classno;
    private Classinfo classinfo;
}

