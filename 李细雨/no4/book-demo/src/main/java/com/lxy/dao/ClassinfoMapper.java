package com.yxc.dao;

import com.yxc.entity.Classinfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ClassinfoMapper {

    Classinfo findClassInfoById(Integer classno);

}
