package com.yxc.controller;

import com.yxc.entity.Classinfo;
import com.yxc.entity.Student;
import com.yxc.service.ClassinfoService;
import com.yxc.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequestMapping("/student")
public class StudentController {

    @Autowired
    StudentService studentService;

    @GetMapping("/{id}")
    public String findClassInfoById(@PathVariable Integer id, Model model){
        Student student=studentService.findStudentById(id);
        model.addAttribute("student",student);
        return "student";
    }

}
