package com.yxc.service;

import com.yxc.dao.EmployeeMapper;
import com.yxc.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    EmployeeMapper employeeMapper;


    public Employee findEmployeeById(Integer id) {
        return employeeMapper.findEmployeeById(id);
    }

    public Employee findLeaderAndEmpById(Integer id) {
        return employeeMapper.findLeaderAndEmpById(id);
    }

    public List<Employee> findEmployeeSalaryLevel() {
        return employeeMapper.findEmployeeSalaryLevel();
    }
}
