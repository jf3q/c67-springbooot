package com.yxc.controller;

import com.yxc.entity.Classinfo;
import com.yxc.entity.Student;
import com.yxc.service.ClassinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequestMapping("/classinfo")
public class ClassinfoController {

    @Autowired
    ClassinfoService classinfoService;

    @GetMapping("/{classno}")
    public String findClassInfoById(@PathVariable Integer classno, Model model){
        Classinfo classinfo=classinfoService.findClassInfoById(classno);
        model.addAttribute("classinfo",classinfo);
        return "classinfo";
    }

}
