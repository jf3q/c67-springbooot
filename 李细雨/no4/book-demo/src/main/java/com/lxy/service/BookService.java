package com.yxc.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yxc.dao.BookMapper;
import com.yxc.entity.Book;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    @Autowired
    BookMapper bookMapper;
    public PageInfo<Book> findAllBooks(Integer pageNum) {
        PageHelper.startPage(pageNum,3,"id desc");
        List<Book> books = bookMapper.findAllBooks();
        return new PageInfo<>(books);
    }

    public void dels(Integer[] ids) {
        bookMapper.deleteBooks(ids);
    }
}
