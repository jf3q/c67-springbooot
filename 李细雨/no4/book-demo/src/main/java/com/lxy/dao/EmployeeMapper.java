package com.yxc.dao;

import com.yxc.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface EmployeeMapper {

    Employee findEmployeeById(Integer id);
    Employee findLeaderAndEmpById(Integer empno);

    List<Employee> findEmployeeSalaryLevel();

}
