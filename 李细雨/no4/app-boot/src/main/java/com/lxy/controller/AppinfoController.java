package com.lxy.controller;

import com.github.pagehelper.PageInfo;
import com.lxy.dto.AppinfoDto;
import com.lxy.dto.AppinfoStatusDto;
import com.lxy.entity.AppInfo;
import com.lxy.entity.DevUser;
import com.lxy.service.AppinfoService;
import com.lxy.util.SessionUtils;
import com.lxy.vo.ResultVo;
import com.lxy.vo.TreeVo;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@RestController
public class AppinfoController {
    @Autowired
    private AppinfoService appinfoService;
    @RequestMapping("/page")
    public ResultVo page(@RequestParam(defaultValue = "1") Integer pageNum,@RequestBody AppinfoDto appinfoDto,HttpServletRequest request){
        String token = request.getHeader("token");
        String type = token.split("-")[1];
        if(type.equals("2")){
            appinfoDto.setDevid(Long.valueOf(token.split("-")[2]));
        }else {
            appinfoDto.setStatus(1L);
        }
        PageInfo<AppInfo> pageInfo =  appinfoService.page(appinfoDto,pageNum);
       return ResultVo.success(pageInfo);
    }

    @RequestMapping("/tree")
    public ResultVo tree(){
        TreeVo treeVo = appinfoService.tree();
        return ResultVo.success(treeVo);
    }
    @RequestMapping("/addOrUpdate")
    public ResultVo addOrUpdate(AppInfo appInfo, MultipartFile file, HttpServletRequest request){
        String token = request.getHeader("token");
        if(file!=null && !file.isEmpty()){
            if(file.getSize()>1024*1024) {
                return ResultVo.error("文件过大");
            }
            String originalFilename = file.getOriginalFilename();
            String extension = FilenameUtils.getExtension(originalFilename);
            if(extension.equalsIgnoreCase("jpg")||
                    extension.equalsIgnoreCase("jpeg")||
                    extension.equalsIgnoreCase("png")||
                    extension.equalsIgnoreCase("gif")){
                String upload = request.getServletContext().getRealPath("upload");
                File path = new File(upload);
                if(!path.exists())path.mkdirs();

               String lastName =  UUID.randomUUID().toString().replace("-","")+"."+extension;
                try {
                    file.transferTo(new File(upload+"/"+lastName));
                } catch (IOException e) {
                    e.printStackTrace();
                    return ResultVo.error("上传失败");
                }
                appInfo.setLogopicpath(lastName);
            }else {
                return ResultVo.error("文件不是图片");
            }
        }
        DevUser devUser = (DevUser) SessionUtils.get(token);
        if(appInfo.getId()==null){
            //新增
            appInfo.setStatus(1L);
            appInfo.setCreatedby(devUser.getId());
            appInfo.setDevid(devUser.getId());
            appInfo.setCreationdate(new Date());
            appInfo.setDownloads(0L);
            appinfoService.add(appInfo);
        }else {
            appInfo.setStatus(1L);
            appInfo.setModifydate(new Date());
            appInfo.setModifyby(devUser.getId());
            appinfoService.update(appInfo);
        }

        return ResultVo.success(null);
    }

    @DeleteMapping("/del")
    public ResultVo del(Long id,HttpServletRequest request){
        appinfoService.del(id,request);

        return ResultVo.success(null);
    }
    @PutMapping("/upStatus")
    public ResultVo upStatus(@RequestBody AppinfoStatusDto appinfoStatusDto){
        AppInfo appInfo = new AppInfo();
        BeanUtils.copyProperties(appinfoStatusDto,appInfo);
        appinfoService.update(appInfo);
        return ResultVo.success(null);
    }
}
