package com.lxy.redisdemo;

import org.junit.jupiter.api.Test;
import org.lxy.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class RedisStarterTest {
    @Autowired
    RedisService redisService;
    @Test
    void test(){
        redisService.set("lxy","lixiyu");
    }
    @Test
    void get(){
        redisService.get("lxy");
    }
}