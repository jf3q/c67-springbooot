package com.lxy.redisdemo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

//集群连接测试
@SpringBootTest
public class RedisClusterTest {
    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Test
    void test(){
        System.out.println(stringRedisTemplate.opsForValue().get("username"));
    }
}
