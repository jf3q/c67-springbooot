package com.lxy.redisdemo;

import com.lxy.redisdemo.mapper.BookInfoMapper;
import com.lxy.redisdemo.pojo.BookInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Iterator;

/**
 * 使用RedisRepository访问redis(类似于mybatis-plus的crud操作)
 */
@SpringBootTest
public class RedisBookInfoTest {
    @Autowired
    private BookInfoMapper bookInfoDao;
    @Test
    void saveBook(){
        BookInfo book1=new BookInfo(1,"西游记","文学","吴承恩",88);
        bookInfoDao.save(book1);
        BookInfo book2=new BookInfo(2,"三国演义","文学","罗贯中",78);
        bookInfoDao.save(book2);
        BookInfo book3=new BookInfo(3,"水浒传","文学","施耐俺",68);
        bookInfoDao.save(book3);
        BookInfo book4=new BookInfo(4,"三国志","文学","佚名",78);
        bookInfoDao.save(book4);
        System.out.println("添加成功！");
    }


    @Test //查询所有图书
    void testFindAllBooks() {
        Iterable<BookInfo> iterable = bookInfoDao.findAll();
        Iterator<BookInfo> it=iterable.iterator();
        while(it.hasNext()){
            BookInfo book= it.next();
            System.out.println(book);
        }
    }
    @Test //修改图书
    void testUpdateBook() {
        BookInfo book=bookInfoDao.findById(1).get();
        book.setPrice(98);
        bookInfoDao.save(book);
        System.out.println("修改成功！");
    }
}
