package com.lxy.redisdemo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootTest
class RedisDemoApplicationTests {
    @Autowired
    RedisTemplate redisTemplate;
    @Test
    void testList1(){
        redisTemplate.opsForList().leftPush("list1","a");
        redisTemplate.opsForList().leftPush("list1","b");
        redisTemplate.opsForList().leftPush("list1","v");
        System.out.println(redisTemplate.opsForList().size("list1"));
        System.out.println(redisTemplate.opsForList().range("list1", 0, -1));
    }
    @Test
    void testZset1() {
        redisTemplate.opsForZSet().add("zset","f1",80);
        redisTemplate.opsForZSet().add("zset","f2",70);
        redisTemplate.opsForZSet().add("zset","f3",90);
//        正序排列
        System.out.println(redisTemplate.opsForZSet().range("zset", 0, -1));
//        降序排列
        System.out.println(redisTemplate.opsForZSet().reverseRange("zset", 0, -1));
    }
}
