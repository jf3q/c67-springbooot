package com.lxy.redisdemo.controller;

import com.lxy.redisdemo.mapper.BookMapper;
import com.lxy.redisdemo.pojo.Book;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class BookController {
    @Autowired
    BookMapper bookMapper;
    @Value("${server.port}")
    String port;

//    集群开发Redis session数据共享
    @GetMapping("/session")
    public Map session(HttpSession session){
        Map map = new HashMap();
        map.put("port",port);
        map.put("sessionId",session.getId());
        return map;
    }
    @GetMapping("/save")
    public String save(){
        bookMapper.setValue("redis","spring boot");
        return "成功";
    }
    @GetMapping("/saveBook")
    public String saveBook(){
        Book book=new Book(1,"C语言程序设计",50.0,"计算机",100,"101.jpg","","zhangsan",50);
        bookMapper.saveBook(book);
        return "保存一本书成功！";
    }
}
