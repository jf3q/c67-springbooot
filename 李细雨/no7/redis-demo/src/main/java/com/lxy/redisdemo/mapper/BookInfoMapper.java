package com.lxy.redisdemo.mapper;

import com.lxy.redisdemo.pojo.BookInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookInfoMapper extends CrudRepository<BookInfo,Integer> {
}
