package com.lxy.redisdemo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@RedisHash(value = "bookinfo",timeToLive = 60)
@RedisHash(value = "bookinfo")
public class BookInfo {
    @Id
    private int id;
    @Indexed
    private String name;
    private String category;
    private String author;
    private double price;
}