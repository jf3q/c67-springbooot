package com.lxy.redisdemo.mapper;

import com.lxy.redisdemo.pojo.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class BookMapper{
    @Autowired
    StringRedisTemplate stringRedisTemplate;
    @Autowired
    RedisTemplate redisTemplate;
    public void setValue(String key,String value){
        stringRedisTemplate.opsForValue().set(key,value);
    }
    public void saveBook(Book book){
        redisTemplate.opsForValue().set("book",book);
    }
}