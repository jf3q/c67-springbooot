package com.lxy.redisdemo.job;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Set;

@Component
@Slf4j
public class NewsJob {
    @Autowired
    RedisTemplate redisTemplate;
//    定时任务 每隔1分钟执行一次
    @Scheduled(cron = "* 0/1 * * * ?")
    public void saveNew(){
        Set<String> keys = redisTemplate.keys("new_*");
        for (String key : keys) {
            Map map = redisTemplate.opsForHash().entries(key);
            String id = key.substring(4);
            log.info("编号：{}，阅读次数：{}，点赞数：{}",id,map.get("read"),map.get("countZan"));
        }
        //保存数据库
        log.info("保存数据库中");
    }
}
