package com.lxy.redisdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class NewController {
    @Autowired
    RedisTemplate redisTemplate;

    //更新浏览量
    @GetMapping("/detail/{id}")
    public String see(@PathVariable Integer id, Model model){
        redisTemplate.opsForHash().increment("new_"+id,"read",1);
        Map map = redisTemplate.opsForHash().entries("new_" + id);
        model.addAllAttributes(map);
        return "news"+id;
    }
    @GetMapping("/addZan/{id}/{opType}")
    @ResponseBody
    public Long addZan(@PathVariable Integer id,@PathVariable Integer opType){
        Long zan;
        if(opType==1){
            zan= redisTemplate.opsForHash().increment("new_"+id,"countZan",1);
        }else {
            zan= redisTemplate.opsForHash().increment("new_"+id,"countZan",-1);
        }
        return zan;
    }

}