package com.lxy.studentdemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxy.studentdemo.pojo.Student;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StudentMapper extends BaseMapper<Student> {
}
