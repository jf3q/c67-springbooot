package com.lxy.studentdemo.common;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResultVo<T> {
    private String code;
    private String message;
    private T data;

    public static <T> ResultVo<T> success(T data) {
        return new ResultVo<>("2000", null, data);
    }
    public static <T> ResultVo<T> error(String message) {
        return new ResultVo<>("2000", message, null);
    }
}
