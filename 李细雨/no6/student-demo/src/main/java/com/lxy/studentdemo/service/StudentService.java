package com.lxy.studentdemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lxy.studentdemo.pojo.Student;

public interface StudentService extends IService<Student> {
}
