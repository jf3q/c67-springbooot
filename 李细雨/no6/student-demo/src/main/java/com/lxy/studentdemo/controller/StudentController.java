package com.lxy.studentdemo.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxy.studentdemo.common.ResultVo;
import com.lxy.studentdemo.pojo.Student;
import com.lxy.studentdemo.service.StudentService;
import io.micrometer.common.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    private StudentService studentService;
    @GetMapping
    public ResultVo page(@RequestParam(defaultValue = "1")Integer pageNum,String studentname){
        Page page = new Page(pageNum,4);
        LambdaQueryWrapper<Student> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(studentname),Student::getStudentname,studentname);
        queryWrapper.orderByDesc(Student::getId);
        studentService.page(page,queryWrapper);
        return ResultVo.success(page);
    }
    @PostMapping
    public ResultVo saveOrUpdate(@RequestBody Student student){
        studentService.saveOrUpdate(student);
        return ResultVo.success(null);
    }
    @DeleteMapping("{id}")
    public ResultVo del(@PathVariable Integer id){
        studentService.removeById(id);
        return ResultVo.success(null);
    }

}