package com.lxy.studentdemo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lxy.studentdemo.mapper.StudentMapper;
import com.lxy.studentdemo.pojo.Student;
import com.lxy.studentdemo.service.StudentService;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements StudentService {
}
