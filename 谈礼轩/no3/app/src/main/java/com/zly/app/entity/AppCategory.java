package com.zly.app.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.io.Serializable;

/**
 * 手游类别(AppCategory)实体类
 *
 * @author makejava
 * @since 2023-12-08 14:11:12
 */

@Data
@ApiModel(value="手游类别实体类",description="手游分类名称描述")
public class AppCategory implements Serializable {
    private static final long serialVersionUID = -70116266331001550L;
    /**
     * 主键ID
     */
    @ApiModelProperty(value="主键ID")
    private Long id;
    /**
     * 分类编码
     */
    @ApiModelProperty(value="分类编码")
    private String categorycode;
    /**
     * 分类名称
     */
    @ApiModelProperty(value="分类名称")
    private String categoryname;
    /**
     * 父级节点id
     */
    @ApiModelProperty(value="父级节点id")
    private Long parentid;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    @ApiModelProperty(value="创建者")
    private Long createdby;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date creationtime;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    @ApiModelProperty(value="更新者")
    private Long modifyby;
    /**
     * 最新更新时间
     */
    @ApiModelProperty(value="最新更新时间")
    private Date modifydate;

}