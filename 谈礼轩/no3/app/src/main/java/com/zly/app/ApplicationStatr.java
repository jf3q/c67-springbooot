package com.zly.app;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.zly.app.dao")
public class ApplicationStatr {
    public static void main(String[] args) {
        SpringApplication.run(ApplicationStatr.class,args);
    }
}
