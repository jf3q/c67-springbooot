package com.zly.app.dto;

import lombok.Data;

@Data
public class AppInfoDto {
    /**
     * 软件名称
     */
    private String softwarename;
    /**
     * APK名称（唯一）
     */
    private String apkname;
    /**
     * 状态（来源于：data_dictionary，1 待审核 2 审核通过 3 审核不通过 4 已上架 5 已下架）
     */
    private Long status;
    /**
     * 所属平台（来源于：data_dictionary，1 手机 2 平板 3 通用）
     */
    private Long flatformid;
    /**
     * 所属三级分类（来源于：data_dictionary）
     */
    private Long categorylevel3;
    /**
     * 所属一级分类（来源于：data_dictionary）
     */
    private Long categorylevel1;
    /**
     * 所属二级分类（来源于：data_dictionary）
     */
    private Long categorylevel2;

    /**
     * 开发者id（来源于：dev_user表的开发者id）
     */

    private Long devid;

}
