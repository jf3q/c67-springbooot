package com.zly.hospital.service;

import com.zly.hospital.dao.HospitalMapper;
import com.zly.hospital.entity.Hospital;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HospitalService {
    @Autowired
    HospitalMapper hospitalMapper;

    public List<Hospital> getList() {
        List<Hospital> list = hospitalMapper.selList();
        return list;
    }
}
