package com.zly.hospital.service;

import com.zly.hospital.dao.MedicalAssayMapper;
import com.zly.hospital.entity.MedicalAssay;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicalAssayService {
    @Autowired
    MedicalAssayMapper medicalAssayMapper;
    public List<MedicalAssay> getList(Long hId) {
        return medicalAssayMapper.queryById(hId);
    }

    public int updateStatus(Integer assayResult, Long id) {
        return medicalAssayMapper.update(new MedicalAssay().setAssayResult(assayResult).setId(id));
    }

    public int addMedicalAssay(MedicalAssay medicalAssay) {
        return medicalAssayMapper.insert(medicalAssay);
    }
}
