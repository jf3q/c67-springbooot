package com.zly.hospital.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
import java.io.Serializable;

/**
 * (MedicalAssay)实体类
 *
 * @author makejava
 * @since 2023-12-28 08:30:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class MedicalAssay implements Serializable {
    private static final long serialVersionUID = 451232594519445376L;

    private Long id;

    private String assayUser;

    private Long hospitalId;

    private Integer assayResult;

    private String phone;

    private String cardNum;

    private Date assayTime;

    private String hospitalName;

}

