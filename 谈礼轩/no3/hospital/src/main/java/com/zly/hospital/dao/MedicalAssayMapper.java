package com.zly.hospital.dao;

import com.zly.hospital.entity.MedicalAssay;

import java.util.List;

import org.apache.ibatis.annotations.*;

/**
 * (MedicalAssay)表数据库访问层
 *
 * @author makejava
 * @since 2023-12-28 08:30:16
 */
@Mapper
public interface MedicalAssayMapper {

    /**
     * 通过医院ID查询指定数据
     * @param hId 主键
     * @return 实例对象
     */
    @Select("SELECT m.*,h.`name` as hospitalName \n" +
            "FROM medical_assay m,hospital h\n" +
            "WHERE m.hoospital_id = h.id")
    List<MedicalAssay> queryById(Long hId);
    
    /**
     * 新增数据
     *
     * @param medicalAssay 实例对象
     * @return 影响行数
     */
    @Insert(" insert into medical_assay(assay_user, hoospital_id, assay_result, phone, card_num, assay_time)\n" +
            " values (#{assayUser}, #{hoospitalId}, #{assayResult}, #{phone}, #{cardNum}, #{assayTime})")
    int insert(MedicalAssay medicalAssay);

    /**
     * 修改数据
     *
     * @param medicalAssay 实例对象
     * @return 影响行数
     */
    @Update(" update medical_assay\n" +
            "        <set>\n" +
            "            <if test=\"assayResult != null\">\n" +
            "                assay_result = #{assayResult},\n" +
            "            </if>\n" +
            "        </set>")
    int update(MedicalAssay medicalAssay);

}

