package com.zly.hospital.dao;

import com.zly.hospital.entity.Hospital;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface HospitalMapper {

    @Select("select * from hospital")
    public List<Hospital> selList();
}
