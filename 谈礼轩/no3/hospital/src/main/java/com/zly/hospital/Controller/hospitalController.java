package com.zly.hospital.Controller;

import com.zly.hospital.entity.Hospital;
import com.zly.hospital.entity.MedicalAssay;
import com.zly.hospital.service.HospitalService;
import com.zly.hospital.service.MedicalAssayService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Controller
public class hospitalController {

    @Autowired
    HospitalService hospitalService;
    @Autowired
    MedicalAssayService medicalAssayService;

    @GetMapping("/list")
    public String getList(HttpSession session) {
        List<Hospital> hospital = hospitalService.getList();
        session.setAttribute("hospital", hospital);
        return "hospitalList";
    }


    @GetMapping("/page")
    public String getPage(Long hId, Model model){
        List<MedicalAssay> list = medicalAssayService.getList(hId);
        model.addAttribute("page",list);
        model.addAttribute("hId",hId);
        return "hospitalList";
    }

    @PostMapping("/update")
    public void update(Integer assayResult, Long id, HttpServletResponse response){
        try {
            int i = medicalAssayService.updateStatus(assayResult, id);
            response.getWriter().println(i);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/toAdd")
    public String toAdd(Model model){
        List<Hospital> hospitalList = hospitalService.getList();
        model.addAttribute("list", hospitalList);
        return "add";
    }

    @RequestMapping("/add")
    public String doAdd(MedicalAssay medicalAssay) {
        int i = medicalAssayService.addMedicalAssay(medicalAssay);
        if (i > 0) {
            return "redirect:/page";
        }
        return null;
    }
}
