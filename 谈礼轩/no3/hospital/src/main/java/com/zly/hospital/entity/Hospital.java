package com.zly.hospital.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * (Hospital)实体类
 *
 * @author makejava
 * @since 2023-12-28 08:30:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Hospital implements Serializable {
    private static final long serialVersionUID = -70039639701110379L;

    private Long id;

    private String name;

}

