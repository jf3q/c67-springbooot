package com.zly.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zly.demo.dao.BookMapper;
import com.zly.demo.entity.Book;
import com.zly.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.ListUtils;

@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements BookService {

    @Autowired
    BookMapper bookMapper;

    @Override
    public IPage<Book> getPage(Book book, Integer pageNum) {
        QueryWrapper queryWrapper = new QueryWrapper();
        if (book.getAuthor()!=null && book.getAuthor()!=""){
            queryWrapper.like("author",book.getAuthor());
        }
        if (book.getName()!=null && book.getName()!=""){
            queryWrapper.like("name",book.getName());
        }
        if (book.getCategory()!=null && book.getCategory()!=""){
            queryWrapper.eq("category",book.getCategory());
        }
        IPage page = new Page(pageNum,3);
        return bookMapper.selectPage(page, queryWrapper);
    }

    @Override
    public void del(Integer id) {
        bookMapper.deleteById(id);
    }

    @Override
    public void dels(Integer[] ids) {
        bookMapper.deleteBatchIds(ListUtils.toList(ids));
    }

    @Override
    public Book sel(Integer id) {
        return bookMapper.selectById(id);
    }

    @Override
    public int saveAndUp(Book book) {
        int num;
        if (book.getId()!=null){
            num = bookMapper.updateById(book);

        }else {
            num = bookMapper.insert(book);
        }
        return num;
    }
}
