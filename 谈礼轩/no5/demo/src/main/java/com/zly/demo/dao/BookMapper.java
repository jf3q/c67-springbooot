package com.zly.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zly.demo.entity.Book;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Mapper
public interface BookMapper extends BaseMapper<Book> {

}
