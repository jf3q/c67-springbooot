package com.zly.demo.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.zly.demo.entity.Book;
import com.zly.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    BookService bookService;


    @GetMapping
    public String getAllList(Model model){
        List<Book> list = bookService.list();
        model.addAttribute("books",list);
        return "index";
    }

    @GetMapping("/page")
    public String getPage(Book book,@RequestParam(defaultValue = "1") Integer pageNum,Model model){
        if (book!=null){
            model.addAttribute("book",book);
        }
        IPage<Book> page = bookService.getPage(book,pageNum);
        model.addAttribute("page",page);
        return "index";
    }

    @GetMapping("/del")
    public String del(Integer id,Integer pageNum){
        bookService.del(id);
        return "redirect:/book/page?pageNum="+pageNum;
    }

    @PostMapping("/dels")
    public String dels(Integer[] ids){
         if (ids.length>0){
             bookService.dels(ids);
         }
         return "redirect:/book/page";
    }
    @GetMapping("/sel")
    public String sel(Integer id,Model model,Integer pageNum){
        Book book = bookService.sel(id);
        model.addAttribute("book",book);
        model.addAttribute("num",pageNum);
        return "add";
    }
    @GetMapping("/add")
    public String add(){
        return "add";
    }
}
