package com.zly.demo.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zly.demo.entity.Book;
import org.springframework.stereotype.Service;

@Service
public interface BookService extends IService<Book> {

    IPage<Book> getPage(Book book, Integer pageNum);

    void del(Integer id);

    void dels(Integer[] ids);

    Book sel(Integer id);

    int saveAndUp(Book book);
}
