package com.zly.app.controller;

import com.zly.app.entity.AppInfo;
import com.zly.app.entity.AppVersion;
import com.zly.app.service.AppInfoService;
import com.zly.app.service.AppVersionService;
import com.zly.app.vo.ResultVo;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/version")
public class VersionController {
    @Autowired
    AppVersionService appVersionService;
    @Autowired
    AppInfoService appInfoService;

    @GetMapping("/{appid}")
    public ResultVo selVersion(@PathVariable Long appid){
        try {
            List<AppVersion> list = appVersionService.selVersion(appid);
            return ResultVo.success("",list);
        }catch (Exception e){
            e.printStackTrace();
            return ResultVo.error("");
        }
    }

    @PostMapping("/addAndUp/{appid}")
    public ResultVo addVersion( AppVersion appVersion, MultipartFile apkFile, @PathVariable Long appid, HttpServletRequest request){
        appVersion.setAppid(appid);
        AppInfo appInfo = new AppInfo();
        String realPath = request.getServletContext().getRealPath("/upload/apk/");
        String toKen = request.getHeader("toKen");
        String[] split = toKen.split("-");
        if (!apkFile.isEmpty()){
            String originalFilename = apkFile.getOriginalFilename();
            String extension = FilenameUtils.getExtension(originalFilename);
            if (apkFile.getSize()>500*1024*1024){
                return ResultVo.error("apk文件大于500MB");
            }else if (extension.equals("apk")){
                File file = new File(realPath);
                if (!file.exists()){
                    file.mkdirs();
                }
                String filename = UUID.randomUUID().toString().replace("-","")+"."+extension;
                File saveFile = new File(realPath + filename);
                try {
                    apkFile.transferTo(saveFile);
                    appVersion.setDownloadlink("/upload/apk/"+filename);
                }catch (IOException e){
                    e.printStackTrace();
                    return ResultVo.error("apk文件上传失败");
                }
            }else {
                return ResultVo.error("apk文件格式不正确");
            }
        }else {
            return ResultVo.error("apk文件不能为空");
        }
        try {
            String devId = split[2];
            appVersion.setCreatedby(Long.valueOf(devId));
            appVersion.setCreationdate(new Date());
            appVersion.setPublishstatus(3L);
            appVersionService.addAndUp(appVersion);
            appInfo.setId(appid);
            appInfo.setStatus(1L);
            appInfo.setVersionid(appVersion.getId());
            appInfoService.saveAndUp(appInfo);
            return ResultVo.success("",null);
        }catch (Exception e){
            e.printStackTrace();
            return ResultVo.error("");
        }
    }
}
