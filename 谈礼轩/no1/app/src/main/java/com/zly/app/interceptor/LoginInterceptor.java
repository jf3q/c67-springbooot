package com.zly.app.interceptor;

import com.zly.app.util.SessionUtil;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        if (token == null || token.equals("")) {
            response.setStatus(401);
            return false;
        } else if (SessionUtil.get(token) == null) {
            response.setStatus(401);
            return false;
        } else {
            String[] split = token.split("-");
            long time = Long.parseLong(split[4]);
            if (System.currentTimeMillis() - time > 2 * 3600 * 1000) {
                response.setStatus(403);
                return false;
            }
            return true;
        }
    }
}