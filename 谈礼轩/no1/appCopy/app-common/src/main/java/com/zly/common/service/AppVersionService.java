package com.zly.common.service;

import com.zly.common.dao.AppVersionDao;
import com.zly.common.entity.AppVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AppVersionService {

    @Autowired
    AppVersionDao appVersionDao;

    public List<AppVersion> selVersion(Long appid) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(appid);
        return appVersionDao.queryAll(appVersion);
    }

    public void addAndUp(AppVersion appVersion) {
        appVersionDao.insert(appVersion);
    }
}
