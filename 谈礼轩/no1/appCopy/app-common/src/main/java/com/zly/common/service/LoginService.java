package com.zly.common.service;

import com.zly.common.dao.BackendUserDao;
import com.zly.common.dao.DevUserDao;
import com.zly.common.dto.LoginDto;
import com.zly.common.entity.BackendUser;
import com.zly.common.entity.DevUser;
import com.zly.common.util.SessionUtil;
import com.zly.common.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class LoginService {

    @Autowired
    DevUserDao devUserDao;
    @Autowired
    BackendUserDao backendUserDao;

    public LoginVo login(LoginDto loginDto) {
        LoginVo vo = new LoginVo();
       if (loginDto.getUserType().equals(0)){
           BackendUser backendUser = new BackendUser();
           backendUser.setUsercode(loginDto.getAccount());
           backendUser.setUserpassword(loginDto.getPassword());
           List<BackendUser> list = backendUserDao.queryAll(backendUser);
           if (list == null || list.size() == 0){
               throw new RuntimeException("密码或账号不正确");
           }
           vo.setUser(list.get(0));
           vo.setAccount(list.get(0).getUsercode());
           vo.setUserType("admin");
           StringBuffer str = new StringBuffer();
           String uuid = UUID.randomUUID().toString().replace("-", "");
           str.append(uuid).append("-");
           str.append(list.get(0).getUsercode()).append("-");
           str.append(list.get(0).getId()).append("-");
           str.append("admin-");
           str.append(System.currentTimeMillis());
           SessionUtil.getToKen(str.toString(),list.get(0));
           vo.setToKen(str.toString());
           return vo;
       }else  if (loginDto.getUserType().equals(1)){
           DevUser devUser = new DevUser();
           devUser.setDevcode(loginDto.getAccount());
           devUser.setDevpassword(loginDto.getPassword());
           List<DevUser> list = devUserDao.queryAll(devUser);
           if (list == null || list.size() == 0){
               throw new RuntimeException("密码或账号不正确");
           }
           vo.setUser(list.get(0));
           vo.setAccount(list.get(0).getDevcode());
           vo.setUserType("dev");
           StringBuffer str = new StringBuffer();
           String uuid = UUID.randomUUID().toString().replace("-", "");
           str.append(uuid).append("-");
           str.append(list.get(0).getDevcode()).append("-");
           str.append(list.get(0).getId()).append("-");
           str.append("dev-");
           str.append(System.currentTimeMillis());
           SessionUtil.getToKen(str.toString(),list.get(0));
           vo.setToKen(str.toString());
           return vo;
       }else{
           throw new RuntimeException("不存在的用户类型");
       }
    }
}
