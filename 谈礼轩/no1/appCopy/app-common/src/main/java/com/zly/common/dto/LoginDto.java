package com.zly.common.dto;

import lombok.Data;

@Data
public class LoginDto {
    private String account;
    private String password;
    private Integer userType;
    
}
