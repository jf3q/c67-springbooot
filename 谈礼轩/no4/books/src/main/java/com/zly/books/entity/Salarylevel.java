package com.zly.books.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Salarylevel)实体类
 *
 * @author makejava
 * @since 2024-01-03 10:34:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Salarylevel implements Serializable {
    private static final long serialVersionUID = -57766678522049863L;

    private String level;

    private Object minsal;

    private Object maxsal;
}