package com.zly.books.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zly.books.dao.BookMapper;
import com.zly.books.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    BookMapper bookMapper;

    public int saveAndUp(Book book) {
        int num;
        if (book.getId()!=null){
            num = bookMapper.updateBook(book);
        }else {
            num = bookMapper.insert(book);
        }
        return num;
    }

    public List<Book> getAll() {
        return bookMapper.selectList();
    }

    public PageInfo<Book> getPage(Integer pageNum) {
        PageHelper.startPage(pageNum,3,"id desc");
        List<Book> books = bookMapper.selectList();
        return new PageInfo<>(books);
    }
    public void dels(Integer[] ids) {
        bookMapper.dels(ids);
    }

    public void del(Integer id) {
        bookMapper.del(id);
    }

    public Book sel(Integer id) {
        return bookMapper.sel(id);
    }
}
