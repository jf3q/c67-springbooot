package com.zly.books.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Student)实体类
 *
 * @author makejava
 * @since 2024-01-03 10:34:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Student implements Serializable {
    private static final long serialVersionUID = -68912629290741249L;

    private Integer id;

    private String studentname;

    private String gender;

    private Integer age;

    private Integer classno;
}