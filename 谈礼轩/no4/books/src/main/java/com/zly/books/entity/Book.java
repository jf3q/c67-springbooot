package com.zly.books.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Book)实体类
 *
 * @author makejava
 * @since 2024-01-03 10:34:55
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Book implements Serializable {
    private static final long serialVersionUID = 762298333281013363L;

    private Integer id;

    private String name;

    private Double price;

    private String category;

    private Integer pnum;

    private String imgurl;

    private String description;

    private String author;

    private Integer sales;

}