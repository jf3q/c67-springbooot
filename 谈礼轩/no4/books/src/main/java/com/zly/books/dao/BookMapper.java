package com.zly.books.dao;

import com.zly.books.entity.Book;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

public interface BookMapper  {
    List<Book> selectList();

    int updateBook(Book book);


    void dels(Integer[] ids);

    void del(Integer id);

    Book sel(Integer id);

    int insert(Book book);
}
