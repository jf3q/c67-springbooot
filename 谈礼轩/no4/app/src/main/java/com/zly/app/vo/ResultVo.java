package com.zly.app.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResultVo {
    private String code;
    private String message;
    private Object data;

    public static  ResultVo success(String message,Object data){
        return new ResultVo("2000",message,data);
    }
    public static  ResultVo error(String message){
        return new ResultVo("5000",message,null);
    }

}
