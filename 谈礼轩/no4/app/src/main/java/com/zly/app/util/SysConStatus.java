package com.zly.app.util;

public class SysConStatus {
    public final static Integer pageSize = 6;
    public static class UserTypeStr{
        public static String admin = "admin";
        public static String dev = "dev";
    }

    public static class UserTypeInt{
        public static Integer admin = 0;
        public static Integer  dev = 1;
    }
}
