package com.zly.app.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.io.Serializable;

/**
 * (BackendUser)实体类
 *
 * @author makejava
 * @since 2023-12-08 14:11:13
 */
@Data
@ApiModel(value="管理员实体类",description="描述管理员信息")
public class BackendUser implements Serializable {
    private static final long serialVersionUID = 616066236582931031L;
    /**
     * 主键id
     */
    @ApiModelProperty(value="主键id")
    private Long id;
    /**
     * 用户编码
     */
    @ApiModelProperty(value="用户编码")
    private String usercode;
    /**
     * 用户名称
     */
    @ApiModelProperty(value="用户名称")
    private String username;
    /**
     * 用户角色类型（来源于数据字典表，分为：超管、财务、市场、运营、销售）
     */
    @ApiModelProperty(value="用户角色类型")
    private Long usertype;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    @ApiModelProperty(value="创建者")
    private Long createdby;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private Date creationdate;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    @ApiModelProperty(value="更新者")
    private Long modifyby;
    /**
     * 最新更新时间
     */
    @ApiModelProperty(value="最新更新时间")
    private Date modifydate;
    /**
     * 用户密码
     */
    @ApiModelProperty(value="用户密码")
    private String userpassword;

}

