package com.zly.app.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.io.Serializable;

/**
 * (DevUser)实体类
 *
 * @author makejava
 * @since 2023-12-08 14:11:14
 */
@Data
@ApiModel(value="开发者实体类",description="描述开发者信息")
public class DevUser implements Serializable {
    private static final long serialVersionUID = -26428928335563162L;
    /**
     * 主键id
     */
    @ApiModelProperty(value="主键id")
    private Long id;
    /**
     * 开发者帐号
     */
    @ApiModelProperty(value="开发者帐号")
    private String devcode;
    /**
     * 开发者名称
     */
    @ApiModelProperty(value="开发者名称")
    private String devname;
    /**
     * 开发者密码
     */
    @ApiModelProperty(value="开发者密码")
    private String devpassword;
    /**
     * 开发者电子邮箱
     */
    @ApiModelProperty(value="开发者电子邮箱")
    private String devemail;
    /**
     * 开发者简介
     */
    @ApiModelProperty(value="开发者简介")
    private String devinfo;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    @ApiModelProperty(value="创建者")
    private Long createdby;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    private Date creationdate;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    @ApiModelProperty(value="更新者")
    private Long modifyby;
    /**
     * 最新更新时间
     */
    @ApiModelProperty(value="最新更新时间")
    private Date modifydate;
}

