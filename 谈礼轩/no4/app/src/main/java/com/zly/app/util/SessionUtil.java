package com.zly.app.util;

import java.util.HashMap;
import java.util.Map;

public class SessionUtil {
    static Map map = new HashMap<>();

    //存储token
    public static void getToKen(String token, Object user) {
        map.put(token, user);
    }

    //删除token
    public static void remove(String token) {
        map.remove(token);
    }

    //通过toKen取User
    public static Object get(String toKen){
        return map.get(toKen);
    }
}
