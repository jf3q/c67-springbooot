package com.zly.app.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.io.Serializable;

/**
 * (AppInfo)实体类
 *
 * @author makejava
 * @since 2023-12-08 14:11:13
 */
@Data
@ApiModel(value="手游实体类",description="描述手游详细信息")
public class AppInfo implements Serializable {
    private static final long serialVersionUID = -17781928011683597L;
    /**
     * 主键id
     */
    @ApiModelProperty(value="主键id")
    private Long id;
    /**
     * 软件名称
     */
    @ApiModelProperty(value="软件名称")
    private String softwarename;
    /**
     * APK名称（唯一）
     */
    @ApiModelProperty(value="APK名称")
    private String apkname;
    /**
     * 支持ROM
     */
    @ApiModelProperty(value="支持ROM")
    private String supportrom;
    /**
     * 界面语言
     */
    @ApiModelProperty(value="界面语言")
    private String interfacelanguage;
    /**
     * 软件大小（单位：M）
     */
    @ApiModelProperty(value="软件大小")
    private Double softwaresize;
    /**
     * 更新日期
     */
    @ApiModelProperty(value="更新日期")
    private Date updatedate;
    /**
     * 开发者id（来源于：dev_user表的开发者id）
     */
    @ApiModelProperty(value="开发者id")
    private Long devid;
    /**
     * 应用简介
     */
    @ApiModelProperty(value="应用简介")
    private String appinfo;
    /**
     * 状态（来源于：data_dictionary，1 待审核 2 审核通过 3 审核不通过 4 已上架 5 已下架）
     */
    @ApiModelProperty(value="状态")
    private Long status;
    /**
     * 上架时间
     */
    @ApiModelProperty(value="上架时间")
    private Date onsaledate;
    /**
     * 下架时间
     */
    @ApiModelProperty(value="下架时间")
    private Date offsaledate;
    /**
     * 所属平台（来源于：data_dictionary，1 手机 2 平板 3 通用）
     */
    @ApiModelProperty(value="所属平台")
    private Long flatformid;
    /**
     * 所属三级分类（来源于：data_dictionary）
     */
    @ApiModelProperty(value="所属三级分类")
    private Long categorylevel3;
    /**
     * 下载量（单位：次）
     */
    @ApiModelProperty(value="下载量")
    private Long downloads;
    /**
     * 创建者（来源于dev_user开发者信息表的用户id）
     */
    @ApiModelProperty(value="创建者")
    private Long createdby;
    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date creationdate;
    /**
     * 更新者（来源于dev_user开发者信息表的用户id）
     */
    @ApiModelProperty(value="更新者")
    private Long modifyby;
    /**
     * 最新更新时间
     */
    @ApiModelProperty(value="最新更新时间")
    private Date modifydate;
    /**
     * 所属一级分类（来源于：data_dictionary）
     */
    @ApiModelProperty(value="所属一级分类")
    private Long categorylevel1;
    /**
     * 所属二级分类（来源于：data_dictionary）
     */
    @ApiModelProperty(value="所属二级分类")
    private Long categorylevel2;
    /**
     * LOGO图片url路径
     */
    @ApiModelProperty(value="LOGO图片url路径")
    private String logopicpath;
    /**
     * LOGO图片的服务器存储路径
     */
    @ApiModelProperty(value="LOGO图片的服务器存储路径")
    private String logolocpath;
    /**
     * 最新的版本id
     */
    @ApiModelProperty(value="最新的版本id")
    private Long versionid;

    @ApiModelProperty(value="一级分类名称")
    private String category1Name;
    @ApiModelProperty(value="二级分类名称")
    private String category2Name;
    @ApiModelProperty(value="三级分类名称")
    private String category3Name;
    @ApiModelProperty(value="开发者名称")
    private String devName;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSoftwarename() {
        return softwarename;
    }

    public void setSoftwarename(String softwarename) {
        this.softwarename = softwarename;
    }

    public String getApkname() {
        return apkname;
    }

    public void setApkname(String apkname) {
        this.apkname = apkname;
    }

    public String getSupportrom() {
        return supportrom;
    }

    public void setSupportrom(String supportrom) {
        this.supportrom = supportrom;
    }

    public String getInterfacelanguage() {
        return interfacelanguage;
    }

    public void setInterfacelanguage(String interfacelanguage) {
        this.interfacelanguage = interfacelanguage;
    }

    public Double getSoftwaresize() {
        return softwaresize;
    }

    public void setSoftwaresize(Double softwaresize) {
        this.softwaresize = softwaresize;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public Long getDevid() {
        return devid;
    }

    public void setDevid(Long devid) {
        this.devid = devid;
    }

    public String getAppinfo() {
        return appinfo;
    }

    public void setAppinfo(String appinfo) {
        this.appinfo = appinfo;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getOnsaledate() {
        return onsaledate;
    }

    public void setOnsaledate(Date onsaledate) {
        this.onsaledate = onsaledate;
    }

    public Date getOffsaledate() {
        return offsaledate;
    }

    public void setOffsaledate(Date offsaledate) {
        this.offsaledate = offsaledate;
    }

    public Long getFlatformid() {
        return flatformid;
    }

    public void setFlatformid(Long flatformid) {
        this.flatformid = flatformid;
    }

    public Long getCategorylevel3() {
        return categorylevel3;
    }

    public void setCategorylevel3(Long categorylevel3) {
        this.categorylevel3 = categorylevel3;
    }

    public Long getDownloads() {
        return downloads;
    }

    public void setDownloads(Long downloads) {
        this.downloads = downloads;
    }

    public Long getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Long createdby) {
        this.createdby = createdby;
    }

    public Date getCreationdate() {
        return creationdate;
    }

    public void setCreationdate(Date creationdate) {
        this.creationdate = creationdate;
    }

    public Long getModifyby() {
        return modifyby;
    }

    public void setModifyby(Long modifyby) {
        this.modifyby = modifyby;
    }

    public Date getModifydate() {
        return modifydate;
    }

    public void setModifydate(Date modifydate) {
        this.modifydate = modifydate;
    }

    public Long getCategorylevel1() {
        return categorylevel1;
    }

    public void setCategorylevel1(Long categorylevel1) {
        this.categorylevel1 = categorylevel1;
    }

    public Long getCategorylevel2() {
        return categorylevel2;
    }

    public void setCategorylevel2(Long categorylevel2) {
        this.categorylevel2 = categorylevel2;
    }

    public String getLogopicpath() {
        return logopicpath;
    }

    public void setLogopicpath(String logopicpath) {
        this.logopicpath = logopicpath;
    }

    public String getLogolocpath() {
        return logolocpath;
    }

    public void setLogolocpath(String logolocpath) {
        this.logolocpath = logolocpath;
    }

    public Long getVersionid() {
        return versionid;
    }

    public void setVersionid(Long versionid) {
        this.versionid = versionid;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCategory1Name() {
        return category1Name;
    }

    public void setCategory1Name(String category1Name) {
        this.category1Name = category1Name;
    }

    public String getCategory2Name() {
        return category2Name;
    }

    public void setCategory2Name(String category2Name) {
        this.category2Name = category2Name;
    }

    public String getCategory3Name() {
        return category3Name;
    }

    public void setCategory3Name(String category3Name) {
        this.category3Name = category3Name;
    }

    public String getDevName() {
        return devName;
    }

    public void setDevName(String devName) {
        this.devName = devName;
    }
}

