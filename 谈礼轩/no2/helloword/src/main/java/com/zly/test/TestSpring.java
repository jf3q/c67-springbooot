package com.zly.test;

import com.zly.entity.UserInfo;
import com.zly.service.UserInfoService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {
    public static void main(String[] args) {
        ApplicationContext app = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        UserInfoService bean = app.getBean(UserInfoService.class);
        bean.addUser(new UserInfo().setUser("zs").setPwd("123"));
    }
}
