package com.zly.service.impl;

import com.zly.dao.UserInfoDao;
import com.zly.entity.UserInfo;
import com.zly.service.UserInfoService;

public class UserInfoServiceImpl implements UserInfoService {
    private UserInfoDao userInfoDao;

    public void setUserInfoDao(UserInfoDao userInfoDao){
        this.userInfoDao = userInfoDao;
    }
    @Override
    public void addUser(UserInfo userInfo) {
        userInfoDao.add(userInfo);
    }
}
