package com.zly.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Accessors(chain = true)
@Component
@ConfigurationProperties(prefix = "user1")
public class UserInfo {
    private String user;
    private String pwd;
    private String phone;
}
