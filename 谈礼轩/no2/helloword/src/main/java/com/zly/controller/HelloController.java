package com.zly.controller;

import com.zly.entity.UserInfo;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @Autowired
    UserInfo userInfo;
    @Value("${user1.phone}")
    private String phone;

    @GetMapping("/hello")
    public String hello(){
        System.out.println(userInfo);
        return "hello phone"+phone;
    }

}
