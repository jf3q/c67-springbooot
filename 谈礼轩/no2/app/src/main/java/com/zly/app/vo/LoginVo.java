package com.zly.app.vo;

import lombok.Data;

@Data
public class LoginVo {

    private String toKen; //uuid+userCode+id+admin|dev+创建时间
    private Object user;
    private String account;
    private String userType;

}
