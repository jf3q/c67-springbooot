package com.zly.app.controller;

import com.zly.app.dto.LoginDto;
import com.zly.app.service.LoginService;
import com.zly.app.vo.LoginVo;
import com.zly.app.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class LoginController {
    @Autowired
    LoginService loginService;

    @PostMapping("/login")
    public ResultVo login(@RequestBody LoginDto loginDto){
        LoginVo vo;
        try {
            vo=loginService.login(loginDto);
            return ResultVo.success("登录成功",vo);
        }catch (Exception e){
            e.printStackTrace();
            return ResultVo.error("登录失败");
        }
    }
}

