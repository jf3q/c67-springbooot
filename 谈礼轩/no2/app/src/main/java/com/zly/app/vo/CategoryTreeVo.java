package com.zly.app.vo;


import lombok.Data;

import java.util.List;
@Data
public class CategoryTreeVo {

    /**
     * 主键ID
     */
    private Long id;
    /**
     * 分类名称
     */
    private String categoryname;
    /**
     * 父级节点id
     */
    private Long parentid;

    private List<CategoryTreeVo>  children;

}