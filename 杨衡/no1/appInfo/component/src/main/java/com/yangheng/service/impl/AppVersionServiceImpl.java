package com.yangheng.service.impl;

import com.yangheng.dao.AppVersionDao;
import com.yangheng.dto.R;
import com.yangheng.entity.AppInfo;
import com.yangheng.entity.AppVersion;
import com.yangheng.service.AppInfoService;
import com.yangheng.service.AppVersionService;
import com.yangheng.sys.SessionManager;
import com.yangheng.vo.LoginUser;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
public class AppVersionServiceImpl implements AppVersionService {
    @Autowired
    AppVersionDao appVersionDao;
    @Autowired
    AppInfoService appInfoService;
    @Override
    public List<AppVersion> getList(int appid) {
        AppVersion appVersion=new AppVersion();
        appVersion.setAppid((long) appid);
        return appVersionDao.queryAllByLimit(appVersion);
    }

    @Override
    public R addVersion(AppVersion appVersion, MultipartFile apkFIle, HttpServletRequest request) {
        String token = request.getHeader("Token");
        LoginUser loginUser =(LoginUser) SessionManager.get(token);
        //这个是判断是否有上传文件
        if (apkFIle!=null){
            //这个是判断 是否文件 内容 为空
            if (!apkFIle.isEmpty()){
                String apkName = apkFIle.getOriginalFilename();
                String extension = FilenameUtils.getExtension(apkName);
                if (apkFIle.getSize()>1024*1024){
                    return R.error("大小大于1mb");
                }else if (extension.equalsIgnoreCase("apk")){
                    String realPath = request.getSession().getServletContext().getRealPath("/apk");
                    File filePath=new File(realPath);
                    if (!filePath.exists()){
                        filePath.mkdirs();
                    }
                    //上传apk的时候 不要忘 把apk的名称 和 地址 赋值上
                    appVersion.setApklocpath(realPath+"/"+ apkName);
                    appVersion.setDownloadlink("/apk/"+ apkName);
                    appVersion.setApkfilename(apkName);
                    File fileUrl=new File(realPath+"/"+ apkName);
                    try {
                        apkFIle.transferTo(fileUrl);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }else {
                    return R.error("文件格式不对");
                }
            }
        }
        //这是给他上传状态值 默认是已发布
        appVersion.setPublishstatus(2L);
        //创建时间
        appVersion.setCreationdate(new Date());
        //谁创建的
        appVersion.setCreatedby(loginUser.getId());
        //添加进去
        int insert = appVersionDao.insert(appVersion);
        //返回的主键id在 appVersion 这个对象里面
        AppInfo appInfo=new AppInfo();
        //修改那个应用的版本
        appInfo.setId(appVersion.getAppid());
        //修改的最新版本好
        appInfo.setVersionid(appVersion.getId());
        //修改
        appInfoService.update(appInfo);
        return R.success("添加成功",insert);
    }
}
