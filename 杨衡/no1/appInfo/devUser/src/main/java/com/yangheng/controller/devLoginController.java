package com.yangheng.controller;

import com.yangheng.dto.LoginUserDto;
import com.yangheng.dto.R;
import com.yangheng.service.DevUserService;
import com.yangheng.vo.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class devLoginController {
    @Autowired
    DevUserService devUserService;
    @PostMapping("/devLogin")
    public R devLogin(@RequestBody LoginUserDto loginUserDto){
        LoginUser loginUser = devUserService.SelectUser(loginUserDto);
        return R.success("",loginUser);
    }

}
