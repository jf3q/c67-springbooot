package com.yangheng.sys;

import java.util.HashMap;
import java.util.Map;

public class SessionManager {
    private static Map map =new HashMap();
    public static void put(String token,Object object){
        map.put(token,object);
    }
    public static void remover(String key){
        map.remove(key);
    }
    public static Object get(String key){
        return map.get(key);
    }
}
