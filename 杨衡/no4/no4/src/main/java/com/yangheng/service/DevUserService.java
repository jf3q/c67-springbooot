package com.yangheng.service;

import com.yangheng.dto.LoginUserDto;
import com.yangheng.vo.LoginUser;

public interface DevUserService {
    LoginUser SelectUser(LoginUserDto userDto);
}
