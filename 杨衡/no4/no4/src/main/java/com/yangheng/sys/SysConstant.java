package com.yangheng.sys;

public class SysConstant {
    public static Integer pageSize=5;
    public static  class UsertypeStr{
        public final static  String admin="admin";
        public final static  String dev="dev";
    }
    public static  class UserTypeInt{
        public final static  Integer admin=2;
        public final static  Integer dev=1;
    }

}
