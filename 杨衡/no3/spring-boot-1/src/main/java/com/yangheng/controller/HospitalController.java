package com.yangheng.controller;

import com.yangheng.entity.Hospital;
import com.yangheng.service.HospitalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Hospital)表控制层
 *
 * @author makejava
 * @since 2023-12-27 16:45:00
 */
@RestController
@RequestMapping("hospital")
public class HospitalController {
    /**
     * 服务对象
     */
    @Resource
    private HospitalService hospitalService;

    /**
     * 分页查询
     *
     * @return 查询结果
     */
    @GetMapping
    public List<Hospital> queryByPage() {
        return hospitalService.getList();
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public ResponseEntity<Hospital> queryById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(this.hospitalService.queryById(id));
    }

    /**
     * 新增数据
     *
     * @param hospital 实体
     * @return 新增结果
     */
    @PostMapping
    public ResponseEntity<Hospital> add(Hospital hospital) {
        return ResponseEntity.ok(this.hospitalService.insert(hospital));
    }

    /**
     * 编辑数据
     *
     * @param hospital 实体
     * @return 编辑结果
     */
    @PutMapping
    public ResponseEntity<Hospital> edit(Hospital hospital) {
        return ResponseEntity.ok(this.hospitalService.update(hospital));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public ResponseEntity<Boolean> deleteById(Integer id) {
        return ResponseEntity.ok(this.hospitalService.deleteById(id));
    }

}

