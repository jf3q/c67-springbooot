package com.yangheng.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController()
@RequestMapping("/book")
public class BooksController {
    @PostMapping("")
    public String addUser(@RequestBody Map map){
        System.out.println(map.toString());
        return "添加成功";
    }
    @DeleteMapping("/{id}")
    public String delUser(@PathVariable Integer id){
        System.out.println("删除的ID为:"+id);

        return "删除成功";
    }
    @RequestMapping("/hello")
//    @CrossOrigin(value = "http://localhost:63342",maxAge = 3600)
    public String hello(){
        return "你好";
    }
}
