package com.yangheng.entity;

import java.io.Serializable;

/**
 * (Hospital)实体类
 *
 * @author makejava
 * @since 2023-12-27 16:45:00
 */
public class Hospital implements Serializable {
    private static final long serialVersionUID = 471695997667967658L;

    private Integer id;

    private String hosptialname;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHosptialname() {
        return hosptialname;
    }

    public void setHosptialname(String hosptialname) {
        this.hosptialname = hosptialname;
    }

}

