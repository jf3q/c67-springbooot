package com.yangheng.controller;

import com.yangheng.entity.AppInfo;
import com.yangheng.service.AppInfoService;
import com.yangheng.service.HospitalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AppInfo)表控制层
 *
 * @author makejava
 * @since 2023-12-26 11:19:32
 */
@Controller
@RequestMapping("appInfo")
@Api(tags = "应用操作接口")
public class AppInfoController {
    /**
     * 服务对象
     */
    @Autowired
    private AppInfoService appInfoService;
    @Autowired
    private HospitalService hospitalService;

    @GetMapping("getList")
    @ApiOperation(value = "查询所有的应用信息", notes = "无需要参数")
    public String getList(AppInfo appInfo, Model model){
      List<AppInfo> appInfos= appInfoService.getList(appInfo);
      model.addAttribute("getList",appInfos);
      model.addAttribute("hospitals",hospitalService.getList());
      model.addAttribute("hosptialId",appInfo.getHosptialId());
      return "index";
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public ResponseEntity<AppInfo> queryById(@ApiParam(value = "用户id",required = true) @PathVariable("id")  Long id) {
        return ResponseEntity.ok(this.appInfoService.queryById(id));
    }

    /**
     * 新增数据
     *
     * @param appInfo 实体
     * @return 新增结果
     */
    @PostMapping
    public ResponseEntity<AppInfo> add(AppInfo appInfo) {
        return ResponseEntity.ok(this.appInfoService.insert(appInfo));
    }

    /**
     * 编辑数据
     *
     * @param appInfo 实体
     * @return 编辑结果
     */
    @PutMapping
    public ResponseEntity<AppInfo> edit(AppInfo appInfo) {
        return ResponseEntity.ok(this.appInfoService.update(appInfo));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public ResponseEntity<Boolean> deleteById(Long id) {
        return ResponseEntity.ok(this.appInfoService.deleteById(id));
    }

}

