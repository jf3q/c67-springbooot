package com.yangheng.service.impl;

import com.yangheng.entity.AppInfo;
import com.yangheng.dao.AppInfoDao;
import com.yangheng.service.AppInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (AppInfo)表服务实现类
 *
 * @author makejava
 * @since 2023-12-26 11:46:52
 */
@Service("appInfoService")
public class AppInfoServiceImpl implements AppInfoService {
    @Resource
    private AppInfoDao appInfoDao;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public AppInfo queryById(Long id) {
        return this.appInfoDao.queryById(id);
    }


    /**
     * 新增数据
     *
     * @param appInfo 实例对象
     * @return 实例对象
     */
    @Override
    public AppInfo insert(AppInfo appInfo) {
        this.appInfoDao.insert(appInfo);
        return appInfo;
    }

    /**
     * 修改数据
     *
     * @param appInfo 实例对象
     * @return 实例对象
     */
    @Override
    public AppInfo update(AppInfo appInfo) {
        this.appInfoDao.update(appInfo);
        return this.queryById(appInfo.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.appInfoDao.deleteById(id) > 0;
    }

    @Override
    public List<AppInfo> getList(AppInfo appInfo) {
        return appInfoDao.queryAllByLimit(appInfo);
    }
}
