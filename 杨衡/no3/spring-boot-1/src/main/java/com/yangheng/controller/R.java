package com.yangheng.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class R {
    private Integer code;
    private String msg;
    private Object data;

    public static R success  (String msg,Object data){
        R r =new R();
        r.setCode(2000);
        r.setData(data);
        r.setMsg(msg);
        return r;
    }
    public static R error(String msg){
        R r=new R();
        r.setCode(5000);
        r.setMsg(msg);
        return r;
    }
}
