package com.yangheng.service.impl;

import com.yangheng.entity.Hospital;
import com.yangheng.dao.HospitalDao;
import com.yangheng.service.HospitalService;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.List;

/**
 * (Hospital)表服务实现类
 *
 * @author makejava
 * @since 2023-12-27 16:45:00
 */
@Service("hospitalService")
public class HospitalServiceImpl implements HospitalService {
    @Resource
    private HospitalDao hospitalDao;

    @Override
    public List<Hospital> getList() {
        return hospitalDao.queryAllByLimit();
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Hospital queryById(Integer id) {
        return this.hospitalDao.queryById(id);
    }


    /**
     * 新增数据
     *
     * @param hospital 实例对象
     * @return 实例对象
     */
    @Override
    public Hospital insert(Hospital hospital) {
        this.hospitalDao.insert(hospital);
        return hospital;
    }

    /**
     * 修改数据
     *
     * @param hospital 实例对象
     * @return 实例对象
     */
    @Override
    public Hospital update(Hospital hospital) {
        this.hospitalDao.update(hospital);
        return this.queryById(hospital.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.hospitalDao.deleteById(id) > 0;
    }
}
