package com.yangheng.service;

import com.yangheng.entity.Hospital;

import java.util.List;

/**
 * (Hospital)表服务接口
 *
 * @author makejava
 * @since 2023-12-27 16:45:00
 */
public interface HospitalService {
    List<Hospital> getList();

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Hospital queryById(Integer id);


    /**
     * 新增数据
     *
     * @param hospital 实例对象
     * @return 实例对象
     */
    Hospital insert(Hospital hospital);

    /**
     * 修改数据
     *
     * @param hospital 实例对象
     * @return 实例对象
     */
    Hospital update(Hospital hospital);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

}
