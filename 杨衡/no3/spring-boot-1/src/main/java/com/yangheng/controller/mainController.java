package com.yangheng.controller;

import com.yangheng.dao.AppInfoDao;
import com.yangheng.entity.AppInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class mainController {
    @Autowired
    AppInfoDao appInfoDao;

    @GetMapping("/hello")
    public String hello(Model model){
        model.addAttribute("title","");
        model.addAttribute("userName","张三");
        AppInfo appInfo=new AppInfo();
        appInfo.setApkname("阿瑟东破口");
        appInfo.setLogolocpath("213123");
        model.addAttribute("appinfo",appInfo);
        model.addAttribute("getList",appInfoDao.queryAllByLimit(new AppInfo()));
        return "index";
    }
}
