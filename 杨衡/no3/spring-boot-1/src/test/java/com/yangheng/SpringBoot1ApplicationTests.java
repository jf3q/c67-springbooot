package com.yangheng;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

@SpringBootTest
class SpringBoot1ApplicationTests {
    @Value("${books[1].name}")
    String userName;
    @Autowired
    Environment environment;

    @Test
    void contextLoads() {
        System.out.println(userName);
        System.out.println(environment.getProperty("userName"));
    }

}
