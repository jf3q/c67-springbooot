package com.yangheng.service;

import com.yangheng.controller.R;
import com.yangheng.entity.AppVersion;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface AppVersionService {
    List<AppVersion> getList(int appid);
    R addVersion(AppVersion appVersion, MultipartFile apkFIle, HttpServletRequest request);
}
