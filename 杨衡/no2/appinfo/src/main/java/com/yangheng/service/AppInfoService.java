package com.yangheng.service;

import com.github.pagehelper.PageInfo;
import com.yangheng.controller.R;
import com.yangheng.dto.AppInfoDto;
import com.yangheng.entity.AppInfo;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


public interface AppInfoService {
     PageInfo<AppInfo> getPage(AppInfoDto appInfoDto, Integer pageNum,Integer pageSize,String token) ;
     public R updateAddApp(AppInfo appInfo, MultipartFile log, HttpServletRequest request);

     AppInfo selectApknameInt(String apkName);

     int deleteApp(int id,HttpServletRequest request);
     AppInfo selectById(int id);

     int update(AppInfo appInfo);
}
