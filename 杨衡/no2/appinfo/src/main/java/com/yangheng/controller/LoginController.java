package com.yangheng.controller;

import com.yangheng.dto.LoginUserDto;
import com.yangheng.entity.AppVersion;
import com.yangheng.service.AppVersionService;
import com.yangheng.service.DevUserService;
import com.yangheng.sys.SessionManager;
import com.yangheng.sys.SysConstant;
import com.yangheng.vo.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
public class LoginController {
    @Autowired
    DevUserService devUserService;


    /**
     *  登录接口
     * @param userDto
     * @return
     */
    @RequestMapping("/login")
    public R login(@RequestBody LoginUserDto userDto){
        //判断类型管理员还是程序员
        LoginUser loginUser = null;
        try {
            loginUser = devUserService.SelectUser(userDto);
//                里面抛出异常外面接收
        } catch (Exception e) {
            return R.error(e.getMessage());
        }
        return R.success("登录成功！",loginUser);
    }
    @RequestMapping("/loginOut")
    public R loginOut(HttpServletRequest request){
        String token = request.getHeader("Token");
        SessionManager.remover(token);
        return R.success("移除成功",null);
    }

}
