package com.yangheng.service.impl;

import com.github.pagehelper.util.StringUtil;
import com.yangheng.dao.BackendUserDao;
import com.yangheng.dao.DevUserDao;
import com.yangheng.dto.LoginUserDto;
import com.yangheng.entity.BackendUser;
import com.yangheng.entity.DevUser;
import com.yangheng.service.DevUserService;
import com.yangheng.sys.SessionManager;
import com.yangheng.sys.SysConstant;
import com.yangheng.vo.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.UUID;

@Service
public class DevUserServiceImpl implements DevUserService {
    @Autowired
    DevUserDao devUserMapper;
    @Autowired
    BackendUserDao backendUserDao;

    /**
     *   登录页面 根据账号密码判断
     * @param userDto
     * @return
     */
    @Override
    public LoginUser SelectUser(LoginUserDto userDto) {
        DevUser devUser;
        LoginUser loginUser=new LoginUser();
        BackendUser backendUser;
        if (StringUtil.isEmpty(userDto.getAccount())){
            throw new RuntimeException("账号不能为空！");
        }
        if (StringUtil.isEmpty(userDto.getPassword())){
            throw new RuntimeException("密码不能为空！");
        }
        if (userDto.getType().equals(SysConstant.UserTypeInt.dev)){
            devUser=new DevUser();
            devUser.setDevcode(userDto.getAccount());
            devUser.setDevpassword(userDto.getPassword());
            List<DevUser> devUsers = devUserMapper.queryAllBy(devUser);
            if (devUsers.size()==0) {
                throw new RuntimeException("账号密码错误！");
            }
            loginUser.setUserCode(devUsers.get(0).getDevcode());
            loginUser.setUserName(devUsers.get(0).getDevname());
            loginUser.setId(devUsers.get(0).getId());
        }else if (userDto.getType().equals(SysConstant.UserTypeInt.admin)){
            backendUser=new BackendUser();
            backendUser.setUserpassword(userDto.getPassword());
            backendUser.setUsercode(userDto.getAccount());
            List<BackendUser> backendUsers = backendUserDao.queryAllByLimit(backendUser);
            if (backendUsers.size()==0) {
                throw new RuntimeException("账号密码错误！");
            }
            loginUser.setUserCode(backendUsers.get(0).getUsercode());
            loginUser.setUserName(backendUsers.get(0).getUsername());
            loginUser.setId(backendUsers.get(0).getId());
        }else {
            throw new RuntimeException("没有此类型");
        }
        StringBuffer stringBuffer=new StringBuffer();
        // token 秘钥 格式 UUID-类型-时间
        stringBuffer.append(UUID.randomUUID().toString().replaceAll("-",""));
        stringBuffer.append("-"+System.currentTimeMillis());
        stringBuffer.append("-"+userDto.getType());
        //存token值
        loginUser.setType(userDto.getType());
        loginUser.setToken(stringBuffer.toString());
        SessionManager.put(stringBuffer.toString(),loginUser);
        return loginUser;
    }
}
