package com.qq.ssm.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * 手游类别(AppCategory)实体类
 *
 * @author makejava
 * @since 2023-11-27 16:08:13
 */
@Data
@Accessors(chain = true)
public class AppCategory implements Serializable {
    private static final long serialVersionUID = -40187911710007094L;
    /**
     * 主键ID
     */
    private Long id;
    /**
     * 分类编码
     */
    private String categorycode;
    /**
     * 分类名称
     */
    private String categoryname;
    /**
     * 父级节点id
     */
    private Long parentid;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    private Long createdby;
    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date creationtime;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    private Long modifyby;
    /**
     * 最新更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date modifydate;



}

