package com.qq.entity;

import java.io.Serializable;

/**
 * (Classinfo)实体类
 *
 * @author makejava
 * @since 2024-01-02 10:24:48
 */
public class Classinfo implements Serializable {
    private static final long serialVersionUID = 312234737722423013L;

    private Integer classno;

    private String classname;


    public Integer getClassno() {
        return classno;
    }

    public void setClassno(Integer classno) {
        this.classno = classno;
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

}

