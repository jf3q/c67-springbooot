package com.qq.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qq.dao.BookDao;
import com.qq.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    @Autowired
    BookDao bookDao;

    public PageInfo<Book> getList(Integer pageNum) {
        PageHelper.startPage(pageNum,3,"id desc");
        List<Book> books = bookDao.queryAllBy(new Book());
        return new PageInfo<>(books);
    }

    public void del(Integer[] id) {
        bookDao.deleteById(id);
    }

    public int add(Book book) {
        int insert = bookDao.insert(book);
        return insert;
    }

    public Book queryById(Integer id) {
        Book book = bookDao.queryById(id);
        return book;
    }

    public void upd(Book book) {
        bookDao.update(book);
    }
}
