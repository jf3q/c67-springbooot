package com.qq.entity;

import java.io.Serializable;

/**
 * (Salarylevel)实体类
 *
 * @author makejava
 * @since 2024-01-02 10:24:49
 */
public class Salarylevel implements Serializable {
    private static final long serialVersionUID = -68276830694933338L;

    private String level;

    private Object minsal;

    private Object maxsal;


    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Object getMinsal() {
        return minsal;
    }

    public void setMinsal(Object minsal) {
        this.minsal = minsal;
    }

    public Object getMaxsal() {
        return maxsal;
    }

    public void setMaxsal(Object maxsal) {
        this.maxsal = maxsal;
    }

}

