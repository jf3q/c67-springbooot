package com.qq.controller;

import com.github.pagehelper.PageInfo;
import com.qq.entity.Book;
import com.qq.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
public class BookController {

    @Autowired
    BookService bookService;

    @RequestMapping("/queryAll")
    public String queryAll(@RequestParam(defaultValue = "1") Integer pageNum, Model model){
        PageInfo<Book> page = bookService.getList(pageNum);
        model.addAttribute("page",page);
        return "list";
    }

    @PostMapping("/del")
    public String del(Integer[] id){
        bookService.del(id);
        return "redirect:/queryAll";
    }

    @PostMapping("/add")
    public String add(Book book,Model model){
        if (book.getId() != null && book.getId()!= 0){
            bookService.upd(book);
            return "redirect:/queryAll";
        }else{
            book.setSales(0);
            int num = bookService.add(book);
            if (num > 0){
                return "redirect:/queryAll";
            }else{
                return "add";
            }
        }

    }

    @RequestMapping("/sert")
    public String sert(){
        return "add";
    }

    @RequestMapping("/upd")
    public String upd(Integer id,Model model){
        Book book = bookService.queryById(id);
        model.addAttribute("book",book);
        return "add";
    }
}
