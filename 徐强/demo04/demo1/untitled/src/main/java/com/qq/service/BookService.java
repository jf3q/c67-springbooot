package com.qq.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qq.dao.BookDao;
import com.qq.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


public interface BookService extends IService<Book> {
    IPage<Book> getList(Integer pageNum);
    void del(Integer[] id);
    int add(Book book);
    Book queryById(Integer id);
    void upd(Book book);

}
