package com.qq;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.qq.dao")
public class UntitledApplication {
    public static void main( String[] args ) {
        SpringApplication.run(UntitledApplication.class);

    }
}
