package com.qq.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qq.entity.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Book)表数据库访问层
 *
 * @author makejava
 * @since 2024-01-02 10:24:48
 */
public interface BookDao extends BaseMapper<Book> {


}

