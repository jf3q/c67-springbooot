package com.qq.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (Book)实体类
 *
 * @author makejava
 * @since 2024-01-02 10:24:48
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("book")
public class Book implements Serializable {
    private static final long serialVersionUID = 316207541637437986L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;

    private String name;

    private Double price;

    private String category;

    private Integer pnum;

    private String imgurl;

    private String description;

    private String author;

    private Integer sales;

}

