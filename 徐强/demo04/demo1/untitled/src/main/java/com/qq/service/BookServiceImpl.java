package com.qq.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qq.dao.BookDao;
import com.qq.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class BookServiceImpl extends ServiceImpl<BookDao,Book> implements BookService {

    @Autowired
    BookDao bookDao;

    public IPage<Book> getList(Integer pageNum) {
        IPage<Book> bookIPage = new Page<>(pageNum,3);
        bookIPage = bookDao.selectPage(bookIPage,null);
        return bookIPage;
    }

    public void del(Integer[] id) {


        bookDao.deleteBatchIds(Arrays.asList(id));
    }

    public int add(Book book) {
        int insert = bookDao.insert(book);
        return insert;
    }

    public Book queryById(Integer id) {
        Book book = bookDao.selectById(id);
        return book;
    }

    public void upd(Book book) {
        bookDao.updateById(book);
    }
}
