package com.lyf.nucleicaciddemo.controller;

import com.lyf.nucleicaciddemo.entity.Hospital;
import com.lyf.nucleicaciddemo.service.HospitalService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/hospital")
public class HospitalController {
    @Resource(name = "hospitalService")
    HospitalService hospitalService;

    @GetMapping
    @ResponseBody
    public List<Hospital> list(){
        return hospitalService.getList();
    }
}
