package com.lyf.nucleicaciddemo.service;

import com.alibaba.fastjson.JSON;
import com.lyf.nucleicaciddemo.dao.HospitalDao;
import com.lyf.nucleicaciddemo.entity.Hospital;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HospitalService {

    @Autowired
    HospitalDao HospitalDao;
    @Resource
    StringRedisTemplate stringRedisTemplate;
    public List<Hospital> getList() {
        //节省资源
        //先判断redis里是否存有医院机构的数据
//        if(stringRedisTemplate.hasKey("hospitals")){
//            String hospitals = stringRedisTemplate.opsForValue().get("hospitals");
//            return JSON.parseArray(hospitals, Hospital.class);
//        }else{
//            List<Hospital> hospitals = HospitalDao.getList();
//            stringRedisTemplate.opsForValue().set("hospitals",JSON.toJSONString(hospitals));
//            return hospitals;
//        }
        return HospitalDao.getList();

    }
}
