package com.qq.ssm.service;

import com.qq.ssm.dto.LoginDto;
import com.qq.ssm.vo.LoginVo;

public interface BackendUserService {
    LoginVo login(LoginDto loginDto);
}
