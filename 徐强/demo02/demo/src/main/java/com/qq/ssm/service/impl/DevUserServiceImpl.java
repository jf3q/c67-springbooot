package com.qq.ssm.service.impl;

import com.qq.ssm.dao.DevUserDao;
import com.qq.ssm.dto.LoginDto;
import com.qq.ssm.entity.DevUser;
import com.qq.ssm.service.DevUserService;
import com.qq.ssm.utils.Sessionutils;
import com.qq.ssm.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class DevUserServiceImpl implements DevUserService {
    @Autowired
    DevUserDao devUserDao;

    @Override
    public LoginVo login(LoginDto loginDto) {

        DevUser devUser = new DevUser();
        devUser.setDevcode(loginDto.getAccount());
        devUser.setDevpassword(loginDto.getPassword());
        List<DevUser> devUsers = devUserDao.queryAllBy(devUser);
        DevUser devUser1 = devUsers.get(0);
        if (devUsers.size() == 0) {
            throw new RuntimeException("账号或密码错误");
        }
        LoginVo loginVo = new LoginVo();
        loginVo.setUserCode(devUser1.getDevcode());
        loginVo.setUserName(devUser1.getDevname());

        String replace = UUID.randomUUID().toString().replace("-", "");
        String token = replace+"-"+System.currentTimeMillis()+"-"+devUser1.getId()+"-"+loginDto.getLoginType();
        loginVo.setToKen(token);

        Sessionutils.put(token,devUsers.get(0));
        return loginVo;
    }
}
