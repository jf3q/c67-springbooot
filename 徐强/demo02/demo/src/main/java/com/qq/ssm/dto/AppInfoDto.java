package com.qq.ssm.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppInfoDto {
    private String softwarename;
    private Long status;
    private String apkname;
    private Long flatformid;
    private Long categorylevel1;
    private Long categorylevel2;
    private Long categorylevel3;
    private Long devid;

}
