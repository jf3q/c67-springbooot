package com.qq.ssm.service;

import com.qq.ssm.dao.AppInfoDao;
import com.qq.ssm.dao.AppVersionDao;
import com.qq.ssm.entity.AppInfo;
import com.qq.ssm.entity.AppVersion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppversionService {

    @Autowired
    AppVersionDao appVersionDao;
    @Autowired
    AppInfoDao appInfoDao;

    public List<AppVersion> selAppversion(Integer id) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(Long.valueOf(id));
        List<AppVersion> appVersions = appVersionDao.queryAllBy(appVersion);
        return appVersions;
    }

    public void add(AppVersion appVersion) {
        appVersionDao.insert(appVersion);
        AppInfo appInfo = new AppInfo();
        appInfo.setVersionid(appVersion.getId());
        appInfo.setId(appVersion.getAppid());
        appInfoDao.update(appInfo);
    }

    public int upd(Long id,Long status) {
        AppInfo appInfo = new AppInfo();
        appInfo.setId(id);
        appInfo.setStatus(status);
        return appInfoDao.update(appInfo);
    }
}
