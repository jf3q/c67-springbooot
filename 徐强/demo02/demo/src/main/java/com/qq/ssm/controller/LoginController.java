package com.qq.ssm.controller;

import com.qq.ssm.dto.LoginDto;
import com.qq.ssm.service.BackendUserService;
import com.qq.ssm.service.DevUserService;
import com.qq.ssm.utils.Sessionutils;
import com.qq.ssm.vo.LoginVo;
import com.qq.ssm.vo.ResultVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;


@RestController
@Api("login类")
public class LoginController {

    @Autowired
    BackendUserService backendUserService;
    @Autowired
    DevUserService devUserService;

    @RequestMapping("/login")

    public ResultVo login(@ApiParam(value = "账号密码必填", example = "1", required = true)@RequestBody LoginDto loginDto){
        if (loginDto.getLoginType() == 1){
            //开发者
            LoginVo loginVo = null;
            try {
                loginVo = devUserService.login(loginDto);
                return ResultVo.success("登录成功",loginVo);
            }catch (Exception e){
                e.printStackTrace();
                return ResultVo.error("账号或密码错误",null);
            }
        }else if (loginDto.getLoginType() == 2){
            //管理员
            LoginVo loginVo = null;
            try {
                loginVo = backendUserService.login(loginDto);
                return ResultVo.success("登录成功",loginVo);
            }catch (Exception e){
                return ResultVo.error("账号密码错误",null);
            }
        }else{
            return ResultVo.error("非法请求：没有此角色",null);
        }
    }

    @GetMapping("/logOut")
    public ResultVo logOut(HttpServletRequest request){
        String token = request.getHeader("token");
        Sessionutils.remove(token);

        return ResultVo.success("退出成功",null);
    }
}
