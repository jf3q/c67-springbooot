package com.qq.ssm.entity;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * (BackendUser)实体类
 *
 * @author makejava
 * @since 2023-11-27 16:08:14
 */
@Data
public class BackendUser implements Serializable {
    private static final long serialVersionUID = -52270688019000538L;
    /**
     * 主键id
     */
    private Long id;
    /**
     * 用户编码
     */
    private String usercode;
    /**
     * 用户名称
     */
    private String username;
    /**
     * 用户角色类型（来源于数据字典表，分为：超管、财务、市场、运营、销售）
     */
    private Long usertype;
    /**
     * 创建者（来源于backend_user用户表的用户id）
     */
    private Long createdby;
    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date creationdate;
    /**
     * 更新者（来源于backend_user用户表的用户id）
     */
    private Long modifyby;
    /**
     * 最新更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date modifydate;
    /**
     * 用户密码
     */
    private String userpassword;



}

