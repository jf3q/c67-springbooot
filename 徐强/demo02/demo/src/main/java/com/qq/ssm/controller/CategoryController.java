package com.qq.ssm.controller;

import com.qq.ssm.service.CategroyService;
import com.qq.ssm.vo.CategoryTreeVo;
import com.qq.ssm.vo.ResultVo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "Category类")
public class CategoryController {

    @Autowired
    CategroyService categroyService;

    @GetMapping("/tree")
    public ResultVo tree(){
        CategoryTreeVo vo = categroyService.tree();
        return ResultVo.success(null,vo);
    }
}
