package com.qq.demo2.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qq.demo2.entity.Student;

public interface StudentService extends IService<Student> {
}
