package com.qq.demo2.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import com.qq.demo2.entity.*;

@Mapper
public interface StudentDao extends BaseMapper<Student> {
}
