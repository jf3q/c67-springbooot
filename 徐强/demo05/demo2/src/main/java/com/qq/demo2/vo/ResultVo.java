package com.qq.demo2.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultVo<T> {
    private String code;
    private String message;
    private T data;

    public static<T> ResultVo<T> success(String message,T data){
        return new ResultVo<T>("2000",message,data);
    }

    public static<T> ResultVo<T> error(String message,T data){
        return new ResultVo<T>("5000",message,data);
    }
}
