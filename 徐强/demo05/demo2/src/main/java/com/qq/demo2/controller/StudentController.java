package com.qq.demo2.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qq.demo2.entity.Student;
import com.qq.demo2.service.StudentService;
import com.qq.demo2.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class StudentController {

    @Autowired
    StudentService studentService;
    @GetMapping("/queryPage")
    public ResultVo queryPage(String studentname,Integer pageNum){
        Page page1 = new Page(pageNum,3);
        LambdaQueryWrapper<Student> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.orderByDesc(Student::getId);
        lambdaQueryWrapper.like(studentname!=""&&studentname!=null,Student::getStudentname,studentname);
        IPage<Student> page = studentService.page(page1,lambdaQueryWrapper);
        return ResultVo.success("",page);
    }

    @PostMapping("/saveOrUpdate")
    public ResultVo saveOrUpdate(@RequestBody Student student){
        studentService.saveOrUpdate(student);
        return ResultVo.success("操作成功",null);
    }

    @GetMapping("/{id}")
    public ResultVo del(@PathVariable Integer id){
        studentService.removeById(id);
        return ResultVo.success("删除成功",null);
    }
}
