package com.qq.demo2.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * (Student)实体类
 *
 * @author makejava
 * @since 2024-01-04 19:11:39
 */
@Data
@TableName("student")
public class Student implements Serializable {
    private static final long serialVersionUID = 841093234739497786L;

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    
    private String studentname;
    
    private String gender;
    
    private Integer age;
    
    private String address;


}

