package com.qq.demo.controller;

import com.qq.demo.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class HelloWordController {
    @Autowired
    UserInfo userInfo;
    @Value("${user.username}")
    private String username;
    @Value("${user.password}")
    private String password;

    @RequestMapping("/hello")
    public String hello(){
        System.out.println(userInfo);
        System.out.println(username);
        System.out.println(password);
        return "spring-boot";
    }
}
