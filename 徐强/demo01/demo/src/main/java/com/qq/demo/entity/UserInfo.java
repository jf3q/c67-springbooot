package com.qq.demo.entity;

import lombok.Data;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "user")
public class UserInfo {
    @Value("${str[0]}")
    private String username;
    @Value("${str[1]}")
    private String password;
}
