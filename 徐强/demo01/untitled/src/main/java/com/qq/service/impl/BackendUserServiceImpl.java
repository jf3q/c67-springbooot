package com.qq.service.impl;

import com.github.pagehelper.util.StringUtil;
import com.qq.dao.BackendUserDao;
import com.qq.dao.DevUserDao;
import com.qq.dto.LoginDto;
import com.qq.entity.BackendUser;
import com.qq.entity.DevUser;
import com.qq.service.BackendUserService;
import com.qq.utils.Sessionutils;
import com.qq.vo.LoginVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class BackendUserServiceImpl implements BackendUserService {
    @Autowired
    BackendUserDao backendUserDao;
    @Autowired
    DevUserDao devUserDao;

    @Override
    public LoginVo login(LoginDto loginDto) {

        BackendUser backendUser = new BackendUser();
        backendUser.setUsercode(loginDto.getAccount());
        backendUser.setUserpassword(loginDto.getPassword());
        List<BackendUser> backendUsers = backendUserDao.queryAllBy(backendUser);
        BackendUser backendUser1 = backendUsers.get(0);
        if (backendUsers.size() == 0) {
            throw new RuntimeException("账号或密码错误");
        }
        LoginVo loginVo = new LoginVo();
        loginVo.setUserCode(backendUser1.getUsercode());
        loginVo.setUserName(backendUser1.getUsername());

        String replace = UUID.randomUUID().toString().replace("-", "");
        String token = replace+"-"+System.currentTimeMillis()+"-"+backendUser1.getId()+"-"+loginDto.getLoginType();
        loginVo.setToKen(token);

        Sessionutils.put(token,backendUsers.get(0));
        return loginVo;
    }
}
