package com.qq.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultVo {
    private Object data;
    private String code;
    private String message;

    public static ResultVo success(String message, Object data){
        return new ResultVo(data,"2000",message);
    }

    public static ResultVo error(String message, Object data){
        return new ResultVo(null,"5000",message);
    }
}
