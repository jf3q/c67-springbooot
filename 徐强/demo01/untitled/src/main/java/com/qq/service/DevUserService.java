package com.qq.service;

import com.qq.dto.LoginDto;
import com.qq.entity.DevUser;
import com.qq.vo.LoginVo;

import java.util.List;

public interface DevUserService {
    LoginVo login(LoginDto loginDto);
}
