package com.qq.controller;

import com.github.pagehelper.PageInfo;
import com.qq.dto.AppInfoDto;
import com.qq.entity.AppInfo;
import com.qq.entity.DevUser;
import com.qq.service.AppInfoService;
import com.qq.utils.Sessionutils;
import com.qq.vo.ResultVo;
import org.apache.commons.io.FilenameUtils;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@RestController
public class AppInfoController {

    @Autowired
    AppInfoService appInfoService;

    @PostMapping("/page")
    public ResultVo getPage(@RequestBody AppInfoDto appInfoDto,HttpServletRequest request,@RequestParam(defaultValue = "1") Integer pageNum){
        String token = request.getHeader("token");
        String[] split = token.split("-");
        PageInfo<AppInfo> page = new PageInfo<>();
        if (split[3].equals("1")){
            appInfoDto.setDevid(Long.valueOf(split[2]));
            page = appInfoService.getPage(appInfoDto,pageNum);
        }else if(split[3].equals("2")){
            appInfoDto.setStatus(1L);
            page = appInfoService.getPage(appInfoDto,pageNum);
        }

        return ResultVo.success(null,page);
    }

    @PostMapping("/saveOrUpdate")
    public ResultVo saveOrUpdate(AppInfo appInfo, MultipartFile file, HttpServletRequest request){
        String[] token = request.getHeader("token").split("-");
        if (file != null) {
            String originalFilename = file.getOriginalFilename();
            String fileFormat = FilenameUtils.getExtension(originalFilename);
            if (file.getSize()>1024*1024) {
                return ResultVo.error("文件超过了1m",null);
            }else if (
                    fileFormat.equalsIgnoreCase("jpg") ||
                    fileFormat.equalsIgnoreCase("png") ||
                    fileFormat.equalsIgnoreCase("jpeg") ||
                    fileFormat.equalsIgnoreCase("gif")
            ){
                //正常上传文件
                String realPath = request.getServletContext().getRealPath("/upload");
                File savePath = new File(realPath);
                if (!savePath.exists()){
                    savePath.mkdirs();
                }
                String fileName = UUID.randomUUID().toString().replace("-", "");
                File saveFile = new File(realPath+"/"+fileName+"."+fileFormat);
                try {
                    file.transferTo(saveFile);
                    appInfo.setLogopicpath("/upload/"+fileName+"."+fileFormat);
                } catch (IOException e) {
                    e.printStackTrace();
                    return ResultVo.error("文件上传失败",null);
                }
            }else{
                return ResultVo.error("文件格式错误",null);
            }

        }else{
            if (appInfo.getId() == null){
                return ResultVo.error("请选择上传文件",null);
            }
        }
        DevUser devUser = new DevUser();
         devUser.setId(Long.valueOf(token[2]));
        if (appInfo.getId()==null) {
            appInfo.setDownloads(0L);
            appInfo.setDevid(devUser.getId());
            appInfo.setCreationdate(new Date());
            appInfo.setStatus(1L);
            appInfo.setCreatedby(devUser.getId());
        }else{
            appInfo.setStatus(1L);
            appInfo.setModifyby(devUser.getId());
            appInfo.setModifydate(new Date());
        }

        appInfoService.saveOrUpdate(appInfo);
        return ResultVo.success("添加成功",null);
    }

    @RequestMapping("/selById")
    public ResultVo selById(Integer id){
        AppInfo appInfo = appInfoService.selectById(id);
        return ResultVo.success("",appInfo);
    }

    @RequestMapping("/AppinfoOff/{id}")
    public ResultVo AppinfoOff(@PathVariable Integer id){
        AppInfo appInfo = appInfoService.selectById(id);
        if (appInfo.getStatus() == 4L){
            appInfo.setStatus(5L);
        }else if (appInfo.getStatus() == 5L || appInfo.getStatus() == 2L){
            appInfo.setStatus(4L);
        }
        appInfoService.updOnOff(appInfo);
        return ResultVo.success("操作成功",null);
    }

    @DeleteMapping("/delAppInfo/{id}")
    public ResultVo delAppInfo(@PathVariable Integer id,HttpServletRequest request){
        appInfoService.delAppInfo(id,request);
        return ResultVo.success("",null);
    }
}
