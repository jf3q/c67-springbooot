package com.qq.service;

import com.github.pagehelper.PageInfo;
import com.qq.dto.AppInfoDto;
import com.qq.entity.AppInfo;

import javax.servlet.http.HttpServletRequest;

public interface AppInfoService {
    public PageInfo<AppInfo> getPage(AppInfoDto appInfoDto, Integer pageNum);

    void saveOrUpdate(AppInfo appInfo);

    AppInfo selectById(Integer id);

    void updOnOff(AppInfo appInfo);

    void delAppInfo(Integer id, HttpServletRequest request);
}
