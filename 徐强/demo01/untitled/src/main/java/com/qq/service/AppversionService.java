package com.qq.service;

import com.qq.dao.AppInfoDao;
import com.qq.dao.AppVersionDao;
import com.qq.entity.AppInfo;
import com.qq.entity.AppVersion;
import com.qq.vo.ResultVo;
import jdk.net.SocketFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppversionService {

    @Autowired
    AppVersionDao appVersionDao;
    @Autowired
    AppInfoDao appInfoDao;

    public List<AppVersion> selAppversion(Integer id) {
        AppVersion appVersion = new AppVersion();
        appVersion.setAppid(Long.valueOf(id));
        List<AppVersion> appVersions = appVersionDao.queryAllBy(appVersion);
        return appVersions;
    }

    public void add(AppVersion appVersion) {
        appVersionDao.insert(appVersion);
        AppInfo appInfo = new AppInfo();
        appInfo.setVersionid(appVersion.getId());
        appInfo.setId(appVersion.getAppid());
        appInfoDao.update(appInfo);
    }

    public int upd(Long id,Long status) {
        AppInfo appInfo = new AppInfo();
        appInfo.setId(id);
        appInfo.setStatus(status);
        return appInfoDao.update(appInfo);
    }
}
