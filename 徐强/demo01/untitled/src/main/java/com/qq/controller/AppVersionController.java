package com.qq.controller;

import com.qq.entity.AppVersion;
import com.qq.entity.DevUser;
import com.qq.service.AppversionService;
import com.qq.vo.ResultVo;
import com.sun.org.glassfish.external.statistics.Stats;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class AppVersionController {

    @Autowired
    AppversionService appversionService;

    @RequestMapping("/selAppversion")
    public ResultVo selAppversion(Integer id){
        List<AppVersion> appVersions = appversionService.selAppversion(id);
        return ResultVo.success("",appVersions);
    }

    @PostMapping("/addAppversion")
    public ResultVo addAppversion(MultipartFile img,AppVersion appVersion, HttpServletRequest request){
        if (img != null){
            String realPath = request.getServletContext().getRealPath("/apk");
            File fileReqlPath = new File(realPath);
            if (!fileReqlPath.exists()){
                fileReqlPath.mkdirs();
            }
            String originalFilename = img.getOriginalFilename();
            String fileFormat = FilenameUtils.getExtension(originalFilename);
            if (img.getSize() > 1024*1024*500){
                return ResultVo.error("文件必须是500M",null);
            }else if (fileFormat.equalsIgnoreCase("apk")){
                String fileName = UUID.randomUUID().toString().replace("-","");
                File saveFile = new File(realPath+"/"+fileName+"."+fileFormat);
                try {
                    img.transferTo(saveFile);
                    appVersion.setApklocpath(realPath+"/"+fileName+"."+fileFormat);
                    appVersion.setDownloadlink("/apk/"+fileName+"."+fileFormat);
                    appVersion.setApkfilename(fileName+"."+fileFormat);
                } catch (IOException e) {
                    return ResultVo.error("上传失败",null);
                }
            }else{
                return ResultVo.error("文件格式必须是apk",null);
            }
        }else{
            return ResultVo.error("请上传文件",null);
        }
        DevUser devUser = new DevUser();
        String token = request.getHeader("token");
        String[] split = token.split("-");
        devUser.setId(Long.valueOf(split[2]));
        appVersion.setCreationdate(new Date());
        appVersion.setCreatedby(devUser.getId());
        appversionService.add(appVersion);
        return ResultVo.success("添加成功",null);
    }

    @PutMapping("/shenhe/{id}/{status}")
    public ResultVo shenhe(@PathVariable Long id,@PathVariable Long status){
        int upd = appversionService.upd(id, status);
        if (upd > 0){
            return ResultVo.success("操作成功",null);
        }else{
            return ResultVo.error("操作失败",null);
        }
    }
}
