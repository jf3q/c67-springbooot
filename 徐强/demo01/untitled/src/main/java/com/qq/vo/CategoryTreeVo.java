package com.qq.vo;

import lombok.Data;

import java.util.List;

@Data
public class CategoryTreeVo {
    private Long id;
    private String categoryname;
    private Long parentid;
    private List<CategoryTreeVo> children;
}
