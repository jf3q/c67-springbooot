package com.qq.intercept;

import com.qq.utils.Sessionutils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginIntercept implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        if (token == null){
            response.setStatus(401);
        }else if (Sessionutils.get(token) == null){
            response.setStatus(403);
        }else{
            String[] split = token.split("-");
            String createTime = split[1];
            if (System.currentTimeMillis()-Long.valueOf(createTime) > 2*3600*1000){
                response.setStatus(403);
                Sessionutils.remove(token);
            }else{
                return true;
            }

        }
        return false;
    }
}
