package com.qq.controller;

import com.qq.dto.LoginDto;
import com.qq.service.BackendUserService;
import com.qq.service.DevUserService;
import com.qq.utils.Sessionutils;
import com.qq.vo.LoginVo;
import com.qq.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class LoginController {

    @Autowired
    BackendUserService backendUserService;
    @Autowired
    DevUserService devUserService;

    @RequestMapping("/login")
    public ResultVo login(@RequestBody LoginDto loginDto){
        if (loginDto.getLoginType() == 1){
            //开发者
            LoginVo loginVo = null;
            try {
                loginVo = devUserService.login(loginDto);
                return ResultVo.success("登录成功",loginVo);
            }catch (Exception e){
                return ResultVo.error("账号或密码错误",null);
            }
        }else if (loginDto.getLoginType() == 2){
            //管理员
            LoginVo loginVo = null;
            try {
                loginVo = backendUserService.login(loginDto);
                return ResultVo.success("登录成功",loginVo);
            }catch (Exception e){
                return ResultVo.error("账号密码错误",null);
            }
        }else{
            return ResultVo.error("非法请求：没有此角色",null);
        }
    }

    @GetMapping("/logOut")
    public ResultVo logOut(HttpServletRequest request){
        String token = request.getHeader("token");
        Sessionutils.remove(token);

        return ResultVo.success("退出成功",null);
    }
}
