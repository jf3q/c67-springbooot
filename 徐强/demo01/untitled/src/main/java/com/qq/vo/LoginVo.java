package com.qq.vo;

import lombok.Data;

@Data
public class LoginVo {
    private String userCode;
    private String userName;
    private String toKen;
}
