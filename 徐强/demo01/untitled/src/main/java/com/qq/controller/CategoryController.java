package com.qq.controller;

import com.qq.service.CategroyService;
import com.qq.vo.CategoryTreeVo;
import com.qq.vo.ResultVo;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryController {

    @Autowired
    CategroyService categroyService;

    @GetMapping("/tree")
    public ResultVo tree(){
        CategoryTreeVo vo = categroyService.tree();
        return ResultVo.success(null,vo);
    }
}
