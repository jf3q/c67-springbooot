package com.qq.service;

import com.qq.dto.LoginDto;
import com.qq.entity.BackendUser;
import com.qq.vo.LoginVo;

import java.util.List;

public interface BackendUserService {
    LoginVo login(LoginDto loginDto);
}
